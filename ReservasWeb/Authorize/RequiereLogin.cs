﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ReservasWeb.Authorize
{
    public class RequiereLogin : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //base.OnActionExecuting(filterContext);
            try
            {
                if (HttpContext.Current.Session["SesionUsuario"] == null)
                {
                    mandameAlLogin(filterContext);
                }
            }
            catch
            {
                mandameAlLogin(filterContext);
            }
        }

        public void mandameAlLogin(ActionExecutingContext filterContext)
        {
            //filterContext.Result = new RedirectResult("/Login");
            filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary{{ "controller", "Login" },
                                          { "action", "Index" },
                                          {"area", ""}
                                         });
        }
    }
}