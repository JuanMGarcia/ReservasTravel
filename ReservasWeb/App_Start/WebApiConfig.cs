﻿using System.Web.Http;

namespace ReservasWeb
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // TODO: Add any additional configuration code.

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "CrearAlojamientoApi",
                routeTemplate: "api/AdministradorGral/CrearAlojamiento/{idCliente}/{nombreCliente}/{emailCliente}/{idProducto}/{nombre}/{ciudadNombre}/{standBy}/{cantidadHabitaciones}",
                defaults: new { controller = "AdministradorGral", action = "CrearAlojamiento" }
            );

            //config.Routes.MapHttpRoute(
            //    name: "CrearAlojamientoApiPost",
            //    routeTemplate: "api/AdministradorGral/CrearAlojamiento",
            //    defaults: new { controller = "AdministradorGral", action = "CrearAlojamiento" }
            //);usuarioclavealojamiento

            config.Routes.MapHttpRoute(
                name: "UsuarioClaveAlojamientoApi",
                routeTemplate: "api/AdministradorGral/UsuarioClaveAlojamiento/{idCliente}/{idProducto}/{nombre}/{apellido}/{nombreUsuario}/{clave}/{emailCliente}",
                defaults: new { controller = "AdministradorGral", action = "UsuarioClaveAlojamiento" }
            );

            config.Routes.MapHttpRoute(
                name: "AlojamientoStandByApi",
                routeTemplate: "api/AdministradorGral/AlojamientoStandBy/{idProducto}/{standBy}",
                defaults: new { controller = "AdministradorGral", action = "AlojamientoStandBy" }
            );

            config.Routes.MapHttpRoute(
                name: "AlojamientoStandByPorCantidadHabitacionesApi",
                routeTemplate: "api/AdministradorGral/AlojamientoStandByPorCantidadHabitaciones/{idProducto}/{standBy}/{cantidadHabitaciones}",
                defaults: new { controller = "AdministradorGral", action = "AlojamientoStandByPorCantidadHabitaciones" }
            );

            config.Routes.MapHttpRoute(
                name: "ClienteStandByApi",
                routeTemplate: "api/AdministradorGral/ClienteStandBy/{idCliente}/{standBy}",
                defaults: new { controller = "AdministradorGral", action = "ClienteStandBy" }
            );

            config.Routes.MapHttpRoute(
                name: "AlojamientoCantidadHabitacionesApi",
                routeTemplate: "api/AdministradorGral/AlojamientoCantidadHabitaciones/{idProducto}/{cantidad}",
                defaults: new { controller = "AdministradorGral", action = "AlojamientoCantidadHabitaciones" }
            );

            // WebAPI when dealing with JSON & JavaScript!
            // Setup json serialization to serialize classes to camel (std. Json format)
            var formatter = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            formatter.SerializerSettings.ContractResolver =
                new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
        }
    }
}