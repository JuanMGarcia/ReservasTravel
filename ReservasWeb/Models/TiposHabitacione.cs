//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReservasWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TiposHabitacione
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TiposHabitacione()
        {
            this.Habitaciones = new HashSet<Habitacione>();
            this.TarifasBases = new HashSet<TarifasBase>();
            this.TarifasPorDias = new HashSet<TarifasPorDia>();
            this.Comodidades = new HashSet<Comodidade>();
            this.CondReservas = new HashSet<CondReserva>();
        }
    
        public int idTipoHabitacion { get; set; }
        public Nullable<int> idGaleriaImagen { get; set; }
        public int idAlojamiento { get; set; }
        public string nombreEs { get; set; }
        public Nullable<int> plazas { get; set; }
        public Nullable<int> camaMatrimonial { get; set; }
        public Nullable<int> camasSimples { get; set; }
        public string horaCheckOutTardio { get; set; }
        public Nullable<int> camasAdicionales { get; set; }
        public string descripcionEs { get; set; }
        public System.DateTime fechaCreacion { get; set; }
        public Nullable<System.DateTime> fechaModificacion { get; set; }
        public Nullable<bool> permiteAdicionalCunasBebes { get; set; }
        public Nullable<bool> permiteAdicionalMenores { get; set; }
        public Nullable<bool> permiteAdicionalAdultos { get; set; }
        public Nullable<bool> compartida { get; set; }
        public string nombreEn { get; set; }
        public string nombrePt { get; set; }
        public string descripcionEn { get; set; }
        public string descripcionPt { get; set; }
        public Nullable<int> idLink { get; set; }
        public Nullable<bool> activoLink { get; set; }
    
        public virtual Alojamiento Alojamiento { get; set; }
        public virtual GaleriaImagene GaleriaImagene { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Habitacione> Habitaciones { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TarifasBase> TarifasBases { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TarifasPorDia> TarifasPorDias { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comodidade> Comodidades { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CondReserva> CondReservas { get; set; }
    }
}
