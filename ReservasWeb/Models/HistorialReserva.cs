//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReservasWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class HistorialReserva
    {
        public int idHistorialReserva { get; set; }
        public int idReserva { get; set; }
        public System.DateTime fechaModificacion { get; set; }
        public Nullable<System.DateTime> checkIn { get; set; }
        public Nullable<System.DateTime> checkOut { get; set; }
        public string precioPorDia { get; set; }
        public Nullable<System.DateTime> fechaEntrada { get; set; }
        public Nullable<System.DateTime> fechaSalida { get; set; }
        public Nullable<int> cantidadHuespedes { get; set; }
        public Nullable<int> idEstado { get; set; }
        public Nullable<int> idUsuario { get; set; }
        public string accion { get; set; }
        public Nullable<int> idHuesped { get; set; }
        public Nullable<int> idAlojamiento { get; set; }
        public string observaciones { get; set; }
        public string tiposHabitaciones { get; set; }
        public string habitaciones { get; set; }
    
        public virtual Alojamiento Alojamiento { get; set; }
        public virtual Huespede Huespede { get; set; }
        public virtual Reserva Reserva { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
