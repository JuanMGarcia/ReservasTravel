/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

var $l = jQuery.noConflict();

$l(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $l('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: '/Galerias/UploadImageTipoHabitacion/'
    });

    // Enable iframe cross-domain access via redirect option:
    $l('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    //alert(window.location.hostname);
    if (window.location.hostname === 'blueimp.github.io') {
        // Demo settings:
        $l('#fileupload').fileupload('option', {
            //url: '//jquery-file-upload.appspot.com/',
            url: '/Galerias/UploadImageTipoHabitacion/',
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            maxFileSize: 999000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        });
        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                //url: '//jquery-file-upload.appspot.com/',
                url: '/Galerias/UploadImageTipoHabitacion/',
                type: 'HEAD'
            }).fail(function () {
                $l('<div class="alert alert-danger"/>')
                    .text('Upload server currently unavailable - ' +
                            new Date())
                    .appendTo('#fileupload');
            });
        }
    } else {

        //alert($l('#fileupload').fileupload('option', 'url'));
        // Load existing files:
        $l('#fileupload').addClass('fileupload-processing');
        $l.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $l('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $l('#fileupload')[0]
        }).always(function () {
            $l(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $l(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
        });
    }

});
