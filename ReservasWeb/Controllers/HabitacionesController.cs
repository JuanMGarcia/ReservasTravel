﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReservasWeb.ViewModels;
using ReservasWeb.Services;
using ReservasWeb.Authorize;
using ReservasWeb.Models;

namespace ReservasWeb.Controllers
{
    //// will be applied to all actions in MyController, unless those actions override with their own decoration
    //[OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)] 
    [RequiereLogin]
    public class HabitacionesController : BaseController
    {
        // GET: Habitaciones
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index(string resultado)
        {
            chequearPermisos(1, 0);
            ListadoHabitacionesViewModel vm = new ListadoHabitacionesViewModel();
            AlojamientosService alojamientosService = new AlojamientosService(this.db);
            HabitacionesService sv = new HabitacionesService(this.db);
            Alojamiento alojamiento = alojamientosService.getAlojamiento();
            vm.cantidadDisponiblesWebMostrador = sv.getCantidadDisponiblesWebMostrador();
            if (alojamiento != null && alojamiento.cantidadHabitaciones.HasValue)
                vm.cantidadMaximaWebMostrador = alojamiento.cantidadHabitaciones.Value;
            else
                vm.cantidadMaximaWebMostrador = 0;
            //if (alojamiento != null && alojamiento.cantidadHabitaciones.HasValue)
            //    vm.cantidadDisponiblesWebMostrador = alojamiento.cantidadHabitaciones.Value;
            //else
            //    vm.cantidadDisponiblesWebMostrador = 0;
            vm.listado = sv.getHabitacionesListadoAbm();
            vm.ProcesarResultado(resultado);

            //alojamientosService.dispose();
            //sv.dispose();

            return View(vm);
        }

        public ActionResult Crear()
        {
            chequearPermisos(1, 1);
            HabitacionesService svHab = new HabitacionesService(this.db);
            TiposHabitacionService svTip = new TiposHabitacionService(this.db);
            CreacionHabitacionesViewModel vm = new CreacionHabitacionesViewModel();
            vm.listaEstados = svHab.getComboEstados();
            vm.listaTiposHabitacion = svTip.getComboTiposHabitacion();


            vm.disponibleAbonoHabitacion = true;


            return View(vm);
        }

        [HttpPost]
        public ActionResult Crear(CreacionHabitacionesViewModel vm)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {

                    AlojamientosService alojamientosService = new AlojamientosService(this.db);
                    HabitacionesService habitacionesService = new HabitacionesService(this.db);
                    Alojamiento alojamiento = alojamientosService.getAlojamiento();
                    //List<Habitacione> listadoHabitaciones = habitacionesService.getHabitacionesListadoAbm();
                    //if (listadoHabitaciones != null && alojamiento.cantidadHabitaciones.Value > listadoHabitaciones.Count())
                    //{                        

                    //if (habitacionesService.getHabitacion(vm.nombre, vm.idTipoHabitacion) == null)
                    //{
                        int cantidad = habitacionesService.getCantidadDisponiblesWebMostrador();
                        if (alojamiento.cantidadHabitaciones.HasValue &&
                            cantidad >= alojamiento.cantidadHabitaciones.Value &&
                            vm.disponibleAbonoHabitacion)
                        {
                            vm.disponibleAbonoHabitacion = false;
                        }

                        habitacionesService.Crear(vm);
                    //}
                    //else {
                    //    TiposHabitacionService svTip = new TiposHabitacionService(this.db);
                    //    resultado = "error_habitacion_duplicada";
                    //    vm.ProcesarResultado(resultado);
                    //    vm.listaEstados = habitacionesService.getComboEstados();
                    //    vm.listaTiposHabitacion = svTip.getComboTiposHabitacion();
                    //    return View(vm);
                    //}


                    //}else
                    //    resultado = "error_abono_habitaciones";
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "Habitaciones", new { resultado = resultado });
        }

        public ActionResult Editar(int id)
        {
            chequearPermisos(1, 3);
            HabitacionesService sv = new HabitacionesService(this.db);
            EdicionHabitacionesViewModel vm = sv.getHabitacionEdicion(id);
            return View(vm);
        }

        [HttpPost]
        public ActionResult Editar(EdicionHabitacionesViewModel vm)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    HabitacionesService habitacionesService = new HabitacionesService(this.db);
                    //if (habitacionesService.getHabitacion(vm.nombre, vm.idTipoHabitacion) == null)
                    //{
                        HabitacionesService sv = new HabitacionesService(this.db);
                        Habitacione habitacion = sv.getHabitacion(vm.idHabitacion);

                        //obtengo la cantidad de habitaciones disponibles para mostrador web , 
                        //teniendo en cuenta todas menos la habitacion pasada por parametro
                        int cantidad = sv.getCantidadDisponiblesWebMostradorExceptoHabitacion(vm.idHabitacion);

                        if (habitacion.TiposHabitacione.Alojamiento.cantidadHabitaciones.HasValue &&
                            cantidad >= habitacion.TiposHabitacione.Alojamiento.cantidadHabitaciones.Value &&
                            vm.disponibleAbonoHabitacion)
                        {
                            vm.disponibleAbonoHabitacion = false;
                        }
                        sv.Editar(vm);
                    //}
                    //else {
                    //    TiposHabitacionService svTip = new TiposHabitacionService(this.db);
                    //    resultado = "error_habitacion_duplicada";
                    //    vm.ProcesarResultado(resultado);
                    //    vm.listaEstados = habitacionesService.getComboEstados();
                    //    vm.listaTiposHabitacion = svTip.getComboTiposHabitacion();
                    //    return View(vm);
                    //}
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "Habitaciones", new { resultado = resultado });
        }

        public ActionResult Eliminar(int id)
        {
            chequearPermisos(1, 2);
            string resultado = "ok";
            try
            {
                ReservasService reservasService = new ReservasService(this.db);
                int cantidadHabitaciones = reservasService.getCantidadReservasPorHabitacion(id);
                if (cantidadHabitaciones > 0)
                    resultado = "error_reservas";
                else
                {
                    HabitacionesService sv = new HabitacionesService(this.db);
                    sv.Eliminar(id);
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "Habitaciones", new { resultado = resultado });
        }

        [HttpPost]
        public JsonResult DisponibleWebMostrador(int id, bool disponible)
        {
            string resultado = "error";
            try
            {
                HabitacionesService habitacionesService = new HabitacionesService(this.db);
                Habitacione habitacion = habitacionesService.getHabitacion(id);
                int cantidad = habitacionesService.getCantidadDisponiblesWebMostrador();
                if (
                    (habitacion.TiposHabitacione.Alojamiento.cantidadHabitaciones.HasValue &&
                    cantidad < habitacion.TiposHabitacione.Alojamiento.cantidadHabitaciones.Value &&
                    disponible) ||
                    !disponible)
                {
                    habitacionesService.EditarDisponibleAbonoHabitacion(id, disponible);
                    resultado = "ok";
                }
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
            return Json(new { Resultado = resultado });
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult VerificarNombreHabitacion(string nombre, int idHabitacion = 0)
        {
            try
            {
               HabitacionesService habitacionesService = new HabitacionesService(this.db);
               return Json(habitacionesService.getHabitacion(nombre, idHabitacion) == null);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }


    }
}