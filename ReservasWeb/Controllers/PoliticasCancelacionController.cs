﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReservasWeb.Models;
using ReservasWeb.ViewModels;
using System.Threading;
using ReservasWeb.Services;
using System.Globalization;
using ReservasWeb.Utils;
using ReservasWeb.Authorize;

namespace ReservasWeb.Controllers
{
    [RequiereLogin]
    public class PoliticasCancelacionController : BaseController
    {
        // GET: PoliticasCancelacion
        public ActionResult Index(string resultado)
        {
            PoliticasCancelacionService PoliticasCancelacionService = new PoliticasCancelacionService(this.db);//servicio donde llama a la lista y instancio la base de datos

            PoliticasCancelacionViewModel PoliticasCancelacionViewModel;//instancio el view model en vacio
            List<PoliticasCancelacionViewModel> listado = new List<PoliticasCancelacionViewModel>();//instancio la lista
            foreach ( PoliticasCancelacion PoliticasCancelacion in PoliticasCancelacionService.getPoliticaCancelacionListadoAbm())//cada elemento de la lista se guarda en una variable instanciada en poli vacia
            {
                PoliticasCancelacionViewModel = new PoliticasCancelacionViewModel();//en una variable vacia se instancia el view model
                PoliticasCancelacionViewModel.nombreEn = PoliticasCancelacion.nombreEn;
                PoliticasCancelacionViewModel.nombreEs = PoliticasCancelacion.nombreEs;
                PoliticasCancelacionViewModel.nombrePt = PoliticasCancelacion.nombrePt;
                PoliticasCancelacionViewModel.descripcionEn = PoliticasCancelacion.descripcionEn;
                PoliticasCancelacionViewModel.descripcionEs = PoliticasCancelacion.descripcionEs;
                PoliticasCancelacionViewModel.descripcionPt = PoliticasCancelacion.descripcionPt;
                PoliticasCancelacionViewModel.FechaCreacion = PoliticasCancelacion.fechaCreacion.Value;
                PoliticasCancelacionViewModel.idPoliticaCancelacion = PoliticasCancelacion.idPoliticaCancelacion;
                //vm.FechaModificacion = poli.fechaModificacion;
                listado.Add(PoliticasCancelacionViewModel);//agrega el listado al viewmodel
            }
            ListadoPoliticasCancelacionViewModel ListadoPoliticasCancelacionViewModel = new ListadoPoliticasCancelacionViewModel();
            ListadoPoliticasCancelacionViewModel.listado = listado;//se le asigna el listado creado al listado del vm
            ListadoPoliticasCancelacionViewModel.ProcesarResultado(resultado);
            return View(ListadoPoliticasCancelacionViewModel);
        }
        public ActionResult Crear()
        {
            //chequearPermisos(1, 12);
            PoliticasCancelacionService PoliticasCancelacionService = new PoliticasCancelacionService(this.db);
            PoliticasCancelacionViewModel PoliticasCancelacionViewModel = new PoliticasCancelacionViewModel();
            
             return View(PoliticasCancelacionViewModel);
        }

        [HttpPost]
        public ActionResult Crear(PoliticasCancelacionViewModel vm)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    PoliticasCancelacionService politicaspancelacionservice = new PoliticasCancelacionService(this.db);
                    PoliticasCancelacion politicascancelacion = new PoliticasCancelacion();
                    politicascancelacion.nombreEn = vm.nombreEn;//controlador
                    politicascancelacion.nombreEs = vm.nombreEs;
                    politicascancelacion.nombrePt = vm.nombrePt;
                    politicascancelacion.descripcionEn = vm.descripcionEn;
                    politicascancelacion.descripcionEs = vm.descripcionEs;
                    politicascancelacion.descripcionPt = vm.descripcionPt;
                    politicascancelacion.fechaCreacion = DateTime.Now;
                    politicascancelacion.fechaModificacion = politicascancelacion.fechaCreacion;
                    Sesion sesion = System.Web.HttpContext.Current.Session["SesionUsuario"] as Sesion;//trae la sesion actual
                    politicascancelacion.idAlojamiento = sesion.idAlojamientoActual;
                    politicaspancelacionservice.Crear(politicascancelacion);
                    //return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }
            return RedirectToAction("index", new { resultado = resultado });
        }
        public ActionResult Editar(int id)
        {
            //chequearPermisos(2, 15);
            PoliticasCancelacionService PoliticasCancelacionService = new PoliticasCancelacionService(this.db);
            PoliticasCancelacion PoliticasCancelacion = PoliticasCancelacionService.getPoliticasCancelacion(id);

            PoliticasCancelacionViewModel EdicionPoliticasCancelacionViewModel = new PoliticasCancelacionViewModel();
            EdicionPoliticasCancelacionViewModel.nombreEn = PoliticasCancelacion.nombreEn;
            EdicionPoliticasCancelacionViewModel.nombreEs = PoliticasCancelacion.nombreEs;
            EdicionPoliticasCancelacionViewModel.nombrePt = PoliticasCancelacion.nombrePt;
            EdicionPoliticasCancelacionViewModel.descripcionEn = PoliticasCancelacion.descripcionEn;
            EdicionPoliticasCancelacionViewModel.descripcionEs = PoliticasCancelacion.descripcionEs;
            EdicionPoliticasCancelacionViewModel.descripcionPt = PoliticasCancelacion.descripcionPt;
            EdicionPoliticasCancelacionViewModel.IdAlojamiento = PoliticasCancelacion.idAlojamiento;
            EdicionPoliticasCancelacionViewModel.idPoliticaCancelacion = PoliticasCancelacion.idPoliticaCancelacion;
            return View(EdicionPoliticasCancelacionViewModel);
        }
        [HttpPost]
        public ActionResult Editar(PoliticasCancelacionViewModel vm)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    PoliticasCancelacionService PoliticasCancelacionService = new PoliticasCancelacionService(this.db);
                    PoliticasCancelacion PoliticasCancelacion = db.PoliticasCancelacions.Find(vm.idPoliticaCancelacion);
                    PoliticasCancelacion.nombreEn = vm.nombreEn;
                    PoliticasCancelacion.nombreEs = vm.nombreEs;
                    PoliticasCancelacion.nombrePt = vm.nombrePt;
                    PoliticasCancelacion.descripcionEn = vm.descripcionEn;
                    PoliticasCancelacion.descripcionEs = vm.descripcionEs;
                    PoliticasCancelacion.descripcionPt = vm.descripcionPt;
                    PoliticasCancelacion.idAlojamiento = vm.IdAlojamiento;
                    PoliticasCancelacion.fechaModificacion = DateTime.Now;
                    PoliticasCancelacionService.Editar(PoliticasCancelacion);
                    //return RedirectToAction("index");
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }
            return RedirectToAction("index", new { resultado = resultado });
        }
        public ActionResult Eliminar(int id)
        {
            string resultado = "ok";
            try
            {
                PoliticasCancelacionService PoliticasCancelacionService = new PoliticasCancelacionService(this.db);
                PoliticasCancelacionService.Eliminar(id);
            }
            catch (Exception)
            {
                resultado = "error";
            }
            return RedirectToAction("index", new { resultado = resultado });
        }
    }
}