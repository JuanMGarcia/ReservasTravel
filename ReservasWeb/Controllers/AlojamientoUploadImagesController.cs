﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.Controllers
{
    public class AlojamientoUploadImagesController : BaseController
    {
        private int contador_imagen = 0;
        private int id_imagen = 0;

        public ActionResult UploadImage()
        {
            if (Request.Files.Count == 0)
            {
                return Json(new { statusCode = 500, status = "No image provided." }, "text/html");
            }

            var file = Request.Files[0];
            string filename = "";

            try
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    file = Request.Files[i];
                    filename = UploadFile(file);
                    filename = "1" + filename.Substring(1);
                }
            }
            catch (Exception)
            {

                throw;
            }

            return Json(new { statusCode = 200, status = "Ok." }, "text/html");
        }

        private string UploadFile(HttpPostedFileBase file)
        {
            // Initialize variables we'll need for resizing and saving
            string newFileName = "";
            contador_imagen++;
            if (contador_imagen > 3)
                contador_imagen = 1;

            if (id_imagen == 0 || contador_imagen == 1)
            {
                Random rnd = new Random();
                id_imagen = rnd.Next(1, 9999999);
            }

            newFileName = contador_imagen.ToString() + "_" + id_imagen.ToString() + "_" + file.FileName;

            // Build absolute path
            var absPath = Server.MapPath("~/Files/");
            var absFileAndPath = Server.MapPath("~/Files/" + newFileName);

            // Create directory as necessary and save image on server
            if (!Directory.Exists(absPath))
                Directory.CreateDirectory(absPath);
            file.SaveAs(absFileAndPath);

            return newFileName;
        }
    }
}