﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReservasWeb.Models;
using ReservasWeb.ViewModels;
using ReservasWeb.Services;
using ReservasWeb.Authorize;
using TodoPagoConnector;
using TodoPagoConnector.Utils;
using TodoPagoConnector.Exceptions;
using System.Xml;
using System.IO;
using ReservasWeb.Utils;

namespace ReservasWeb.Controllers
{
    [RequiereLogin]
    public class MediosdePagoController : BaseController
    {
        private ReservasWebEntities db = new ReservasWebEntities();

        // GET: MercadoPago
        public ActionResult Index(string resultado)
        {
            chequearPermisos(6, 0);
            //ListadoMercadoPagoViewModel mp = new ListadoMercadoPagoViewModel();
            ///MediosdePagoService mpPG = new MediosdePagoService();
            //mp.listado = mpPG.getMercadoPagoAbm();
            //mp.ProcesarResultado(resultado);

            MediosdePagoViewModel mp = new MediosdePagoViewModel();
            MediosdePagoService mpPG = new MediosdePagoService(this.db);

            var sesion = System.Web.HttpContext.Current.Session["SesionUsuario"] as Sesion;
            if (null != sesion)
            {
                TiposPagoConfiguracion unPagaHotel = mpPG.getMedioPago(sesion.idAlojamientoActual, 1);
                TiposPagoConfiguracion unMercadoPago = mpPG.getMedioPago(sesion.idAlojamientoActual, 2);
                TiposPagoConfiguracion unTodoPago = mpPG.getMedioPago(sesion.idAlojamientoActual, 3);
                TiposPagoConfiguracion unPayPal = mpPG.getMedioPago(sesion.idAlojamientoActual, 4);
                TiposPagoConfiguracion unTransfer = mpPG.getMedioPago(sesion.idAlojamientoActual, 5);

                mp.idAlojamiento = sesion.idAlojamientoActual;
                //MP
                if (unMercadoPago != null)
                { 
                    mp.MPclientId = unMercadoPago.clientId;
                    mp.MPclientSecret = unMercadoPago.clientSecret;
                    mp.MPclientStatus = unMercadoPago.clientStatus;
                    mp.MPidMercadoPago = unMercadoPago.idTipoPagoConfiguracion;
                    mp.MPstandBy = unMercadoPago.standBy;
                    mp.MPtipo = unMercadoPago.tipo;
                }
                else
                {
                    mp.MPclientId = "1";
                    mp.MPclientSecret = "1";
                    mp.MPclientStatus = "1";
                    mp.MPidMercadoPago = -1;
                    mp.MPstandBy = false;
                    mp.MPtipo = 2;
                }

                //TP
                if (unTodoPago != null)
                {
                    mp.TPclientId = unTodoPago.clientId;
                    mp.TPclientSecret = unTodoPago.clientSecret;
                    mp.TPclientStatus = unTodoPago.clientStatus;
                    mp.TPidMercadoPago = unTodoPago.idTipoPagoConfiguracion;
                    mp.TPstandBy = unTodoPago.standBy;
                    mp.TPtipo = unTodoPago.tipo;
                }
                else
                {
                    mp.TPclientId = "2";
                    mp.TPclientSecret = "2";
                    mp.TPclientStatus = "2";
                    mp.TPidMercadoPago = -1;
                    mp.TPstandBy = false;
                    mp.TPtipo = 3;
                }


                //PP
                if (unPayPal != null)
                {
                    mp.PPclientId = unPayPal.clientId;
                    mp.PPclientSecret = unPayPal.clientSecret;
                    mp.PPclientStatus = unPayPal.clientStatus;
                    mp.PPidMercadoPago = unPayPal.idTipoPagoConfiguracion;
                    mp.PPstandBy = unPayPal.standBy;
                    mp.PPtipo = unPayPal.tipo;
                }
                else
                {
                    mp.PPclientId = "3";
                    mp.PPclientSecret = "3";
                    mp.PPclientStatus = "3";
                    mp.PPidMercadoPago = -1;
                    mp.PPstandBy = false;
                    mp.PPtipo = 4;
                }
                

                //PH
                if (unPagaHotel != null)
                {
                    mp.PHidMercadoPago = unPagaHotel.idTipoPagoConfiguracion;
                    mp.PHstandBy = unPagaHotel.standBy;
                }
                else
                {
                    mp.PHidMercadoPago = -1;
                    mp.PHstandBy = false;
                }

                //TB
                if (unTransfer !=null)
                {
                    mp.TBidMercadoPago = unTransfer.idTipoPagoConfiguracion;
                    mp.TBstandBy = unTransfer.standBy;
                }
                else
                {
                    mp.TBidMercadoPago = -1;
                    mp.TBstandBy = false;
                }


            }
            else
            {
                //error
            }  

            mp.ProcesarResultado(resultado);
            return View(mp);
        }
                        
        public ActionResult Crear()
        {
            chequearPermisos(6, 1);
            MediosdePagoService svMercadoPago = new MediosdePagoService(this.db);
            CreacionMercadoPagoViewModel vm = new CreacionMercadoPagoViewModel();
            return View(vm);
        }

        [HttpPost]
        public ActionResult Index(MediosdePagoViewModel vm)
        {
            var sesion = System.Web.HttpContext.Current.Session["SesionUsuario"] as Sesion;
            TiposPagoConfiguracion PConf = db.TiposPagoConfiguracions.Where(s => s.idAlojamiento == sesion.idAlojamientoActual && s.tipo==1).FirstOrDefault();
            if (PConf != null)
            {
                PConf.standBy = vm.PHstandBy;
                db.Entry(PConf).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                PConf = new TiposPagoConfiguracion();
                PConf.idAlojamiento = sesion.idAlojamientoActual;
                PConf.standBy = vm.PHstandBy;
                PConf.clientId = "1";
                PConf.clientSecret = "1";
                PConf.fechaCreacion = DateTime.Now;
                PConf.tipo = 1;
                db.TiposPagoConfiguracions.Add(PConf);
                db.SaveChanges();
            }



            PConf = db.TiposPagoConfiguracions.Where(s => s.idAlojamiento == sesion.idAlojamientoActual && s.tipo == 2).FirstOrDefault();
            if (PConf != null)
            {
                PConf.standBy = vm.MPstandBy;
                db.Entry(PConf).State = EntityState.Modified;
                db.SaveChanges();

            }
            else
            {
                PConf = new TiposPagoConfiguracion();
                PConf.idAlojamiento = sesion.idAlojamientoActual;
                PConf.standBy = vm.MPstandBy;
                PConf.tipo = 2;
                PConf.clientId = "1";
                PConf.clientSecret = "1";
                PConf.fechaCreacion = DateTime.Now;
                db.TiposPagoConfiguracions.Add(PConf);
                db.SaveChanges();
            }

            PConf = db.TiposPagoConfiguracions.Where(s => s.idAlojamiento == sesion.idAlojamientoActual && s.tipo == 5).FirstOrDefault();
            if (PConf != null)
            {
                PConf.standBy = vm.TBstandBy;
                db.Entry(PConf).State = EntityState.Modified;
                db.SaveChanges();

            }
            else
            {
                PConf = new TiposPagoConfiguracion();
                PConf.idAlojamiento = sesion.idAlojamientoActual;
                PConf.standBy = vm.TBstandBy;
                PConf.tipo = 5;
                PConf.clientId = "1";
                PConf.clientSecret = "1";
                PConf.fechaCreacion = DateTime.Now;
                db.TiposPagoConfiguracions.Add(PConf);
                db.SaveChanges();
            }


            //     PConf = db.TiposPagoConfiguracions.Where(s => s.idAlojamiento == sesion.idAlojamientoActual && s.tipo == 3).FirstOrDefault();
            //     if (PConf != null)
            //     {
            //         PConf.standBy = vm.TPstandBy;
            //         db.Entry(PConf).State = EntityState.Modified;
            //         db.SaveChanges();
            //     }
            //     else
            //   {
            //   PConf = new TiposPagoConfiguracion();
            //     PConf.idAlojamiento = sesion.idAlojamientoActual;
            //             PConf.standBy = vm.TPstandBy;
            //           PConf.tipo = 3;
            //         db.TiposPagoConfiguracions.Add(PConf);
            //       db.SaveChanges();
            // }




            //       PConf = db.TiposPagoConfiguracions.Where(s => s.idAlojamiento == sesion.idAlojamientoActual && s.tipo == 4).FirstOrDefault();
            //        if (PConf != null)
            //       {
            //          PConf.standBy = vm.PPstandBy;
            //          db.Entry(PConf).State = EntityState.Modified;
            //          db.SaveChanges();
            //       }
            //      else
            //     {
            //        PConf = new TiposPagoConfiguracion();
            //       PConf.idAlojamiento = sesion.idAlojamientoActual;
            //      PConf.standBy = vm.PPstandBy;
            //      PConf.tipo = 4;
            ///       db.TiposPagoConfiguracions.Add(PConf);
            //     db.SaveChanges();
            //  }



            return View(vm);
        }

        [HttpPost]
        public ActionResult Crear(CreacionMercadoPagoViewModel vm)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    MediosdePagoService svMercadoPago = new MediosdePagoService(this.db);
                    svMercadoPago.Crear(vm);
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }
            
            return RedirectToAction("index", "MediosdePago", new { resultado = resultado });
        }

        public ActionResult Editar(int id,int tipo)
        {
            EdicionMercadoPagoViewModel vm;
            MediosdePagoService sv = new MediosdePagoService(this.db);

            // Se edita el MediodePago
            chequearPermisos(6, 3);
            if (id != -1)
            {
                vm = sv.getMercadoPagoEdicion(id);
            }
            else
            {
                var sesion = System.Web.HttpContext.Current.Session["SesionUsuario"] as Sesion;
                TiposPagoConfiguracion TPConf = new TiposPagoConfiguracion();
                
                TPConf.clientId = "1";
                TPConf.clientSecret = "1";
                TPConf.tipo = tipo;
                TPConf.fechaCreacion = DateTime.Now;
                TPConf.idAlojamiento = sesion.idAlojamientoActual;
                TPConf.standBy = false;
                db.TiposPagoConfiguracions.Add(TPConf);
                db.SaveChanges();
                id = TPConf.idTipoPagoConfiguracion;
                vm = sv.getMercadoPagoEdicion(id);
            }
            return View(vm);

        }

        [HttpPost]
        public ActionResult Editar(EdicionMercadoPagoViewModel vm)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    MediosdePagoService sv = new MediosdePagoService(this.db);
                    sv.Editar(vm);
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "MediosdePago", new { resultado = resultado });
        }

        public ActionResult CuentaBancaria()
        {
            CuentaBancariaViewModel vm = new CuentaBancariaViewModel();
            MediosdePagoService sv = new MediosdePagoService(this.db);
            var sesion = System.Web.HttpContext.Current.Session["SesionUsuario"] as Sesion;
            // Se edita el MediodePago
            chequearPermisos(6, 3);
            CuentasBancaria unBank = db.CuentasBancarias.Where(r => r.idAlojamiento == sesion.idAlojamientoActual).FirstOrDefault();
            if (unBank != null)
            {
                vm.CBU = unBank.CBU;
                vm.CUIT = unBank.CUIT;
                vm.idCuentaBancaria = unBank.idCuentaBancaria;
                vm.nombreBanco = unBank.nombreBanco;
                vm.numeroCuenta = unBank.numeroCuenta;
                vm.sucursal = unBank.sucursal;
                vm.tipoCuenta = unBank.tipoCuenta;
                vm.titular = unBank.titular;
            }
            else
            {
                CuentasBancaria cBank = new CuentasBancaria();
                cBank.CBU = ".";
                cBank.CUIT = ".";
                cBank.idAlojamiento = sesion.idAlojamientoActual;
                cBank.idTipoPago = 5;
                cBank.idTipoPagoConfiguracion = 0;
                cBank.nombreBanco = ".";
                cBank.numeroCuenta = ".";
                cBank.sucursal = ".";
                cBank.tipoCuenta = ".";
                cBank.titular = "-";

                db.CuentasBancarias.Add(cBank);
                db.SaveChanges();

                vm.CBU = unBank.CBU;
                vm.CUIT = unBank.CUIT;
                vm.idCuentaBancaria = unBank.idCuentaBancaria;
                vm.nombreBanco = unBank.nombreBanco;
                vm.numeroCuenta = unBank.numeroCuenta;
                vm.sucursal = unBank.sucursal;
                vm.tipoCuenta = unBank.tipoCuenta;
                vm.titular = unBank.titular;
            }
            return View(vm);

        }

        [HttpPost]
        public ActionResult CuentaBancaria(CuentaBancariaViewModel vm)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    MediosdePagoService sv = new MediosdePagoService(this.db);
                    sv.EditarCuentaBancaria(vm);
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "MediosdePago", new { resultado = resultado });
        }

        public ActionResult Borrar(int id)
        {
            chequearPermisos(6, 2);
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    MediosdePagoService sv = new MediosdePagoService(this.db);
                    sv.Borrar(id);
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "MediosdePago", new { resultado = resultado });
        }


        public ActionResult Test(int id)
        {
            if (id != -1)
            {
                MediosdePagoService sv = new MediosdePagoService(this.db);
                String sURL = sv.GetLinkMP(id, "titulo", 2, 3, "referencia", 1,false);
                return Redirect(sURL);
            }
            else
            {
                return RedirectToAction("index", "MediosdePago", new { resultado = "error_conf_pago" });
            }
        }
        // 
        //TODOPAGO componente de git ultima versión
        private Dictionary<String, Object> executeSendAuthorizeRequest(TPConnector connector, TodoPagoInfoViewModel info)
        {
            Dictionary<string, string> sendAuthorizeRequestParams = new Dictionary<string, string>();
            Dictionary<string, string> sendAuthorizeRequestPayload = new Dictionary<string, string>();
            Dictionary<String, Object> res = new Dictionary<String, Object>();

            initSendAuthorizeRequestParams(sendAuthorizeRequestParams, sendAuthorizeRequestPayload, info);

            try
            {
                res = connector.SendAuthorizeRequest(sendAuthorizeRequestParams, sendAuthorizeRequestPayload);

                if (res.ContainsKey("PublicRequestKey") && res["PublicRequestKey"] != null)
                {
                    info.PublicRequestKey = res["PublicRequestKey"].ToString();
                }

                if (res.ContainsKey("RequestKey") && res["RequestKey"] != null)
                {
                    info.RequestKey = res["RequestKey"].ToString();
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    WebResponse resp = ex.Response;
                    using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                    {
                        res.Add("exception", "\r\n" + sr.ReadToEnd() + " - " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                res.Add("exception", "\r\n" + ex.Message);
            }

            return res;
        }

        private void initSendAuthorizeRequestParams(Dictionary<string, string> sendAuthorizeRequestParams, Dictionary<string, string> sendAuthorizeRequestPayload, TodoPagoInfoViewModel info)
        {
            sendAuthorizeRequestParams.Add(ElementNames.SECURITY, info.Security);
            sendAuthorizeRequestParams.Add(ElementNames.SESSION, "ABCDEF-1234-12221-FDE1-00000200");
            sendAuthorizeRequestParams.Add(ElementNames.MERCHANT, info.MerchantId);
            sendAuthorizeRequestParams.Add(ElementNames.URL_OK, "http://someurl.com/ok");
            sendAuthorizeRequestParams.Add(ElementNames.URL_ERROR, "http://someurl.com/fail");
            sendAuthorizeRequestParams.Add(ElementNames.ENCODING_METHOD, "XML");

            sendAuthorizeRequestPayload.Add(ElementNames.MERCHANT, info.MerchantId);
            sendAuthorizeRequestPayload.Add(ElementNames.OPERATIONID, "2121");
            sendAuthorizeRequestPayload.Add(ElementNames.CURRENCYCODE, "032");
            sendAuthorizeRequestPayload.Add(ElementNames.AMOUNT, info.Monto);
            sendAuthorizeRequestPayload.Add(ElementNames.EMAILCLIENTE, "email_cliente@dominio.com");
            sendAuthorizeRequestPayload.Add(ElementNames.MAXINSTALLMENTS, "6"); //NO MANDATORIO, MAXIMA CANTIDAD DE CUOTAS, VALOR MAXIMO 12
            sendAuthorizeRequestPayload.Add(ElementNames.MININSTALLMENTS, "1"); //NO MANDATORIO, MINIMA CANTIDAD DE CUOTAS, VALOR MINIMO 1
            sendAuthorizeRequestPayload.Add(ElementNames.TIMEOUT, "300000"); //NO MANDATORIO, TIEMPO DE ESPERA DE 300000 (5 minutos) a 21600000 (6hs)

            sendAuthorizeRequestPayload.Add("CSBTCITY", "Villa General Belgrano"); //MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSBTCOUNTRY", "AR");//MANDATORIO. Código ISO.
            sendAuthorizeRequestPayload.Add("CSBTEMAIL", "todopago@hotmail.com"); //MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSBTFIRSTNAME", "Juan");//MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSBTLASTNAME", "Perez");//MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSBTPHONENUMBER", "541161988");//MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSBTPOSTALCODE", "1010");//MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSBTSTATE", "B");//MANDATORIO
            sendAuthorizeRequestPayload.Add("CSBTSTREET1", "Cerrito 740");//MANDATORIO.
                                                                          //sendAuthorizeRequestPayload.Add("CSBTSTREET2", "");//NO MANDATORIO

            sendAuthorizeRequestPayload.Add("CSBTCUSTOMERID", "453458"); //MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSBTIPADDRESS", "192.0.0.4"); //MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSPTCURRENCY", "ARS");//MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSPTGRANDTOTALAMOUNT", "1.00");//MANDATORIO.

            sendAuthorizeRequestPayload.Add("CSMDD6", "");//NO MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSMDD7", "");//NO MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSMDD8", ""); //NO MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSMDD9", "");//NO MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSMDD10", "");//NO MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSMDD11", "");//NO MANDATORIO.

            //retail
            sendAuthorizeRequestPayload.Add("CSSTCITY", "Villa General Belgrano"); //MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSSTCOUNTRY", "AR");//MANDATORIO. Código ISO.
            sendAuthorizeRequestPayload.Add("CSSTEMAIL", "todopago@hotmail.com"); //MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSSTFIRSTNAME", "Juan");//MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSSTLASTNAME", "Perez");//MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSSTPHONENUMBER", "541160913988");//MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSSTPOSTALCODE", "1010");//MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSSTSTATE", "B");//MANDATORIO
            sendAuthorizeRequestPayload.Add("CSSTSTREET1", "Cerrito 740");//MANDATORIO.
                                                                          //sendAuthorizeRequestPayload.Add("CSSTSTREET2", "");//NO MANDATORIO.

            sendAuthorizeRequestPayload.Add("CSITPRODUCTCODE", "electronic_good#electronic_good#electronic_good#electronic_good");//CONDICIONAL
            sendAuthorizeRequestPayload.Add("CSITPRODUCTDESCRIPTION", "Prueba desde net#Prueba desde net#Prueba desde net#tttt");//CONDICIONAL.
            sendAuthorizeRequestPayload.Add("CSITPRODUCTNAME", "netsdk#netsdk#netsdk#netsdk");//CONDICIONAL.
            sendAuthorizeRequestPayload.Add("CSITPRODUCTSKU", "nsdk123#nsdk123#nsdk123#nsdk123");//CONDICIONAL.
            sendAuthorizeRequestPayload.Add("CSITTOTALAMOUNT", "1.00#1.00#1.00#1.00");//CONDICIONAL.
            sendAuthorizeRequestPayload.Add("CSITQUANTITY", "1#1#1#1");//CONDICIONAL.
            sendAuthorizeRequestPayload.Add("CSITUNITPRICE", "1.00#1.00#1.00#1.00");

            sendAuthorizeRequestPayload.Add("CSMDD12", "");//NO MADATORIO.
            sendAuthorizeRequestPayload.Add("CSMDD13", "");//NO MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSMDD14", "");//NO MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSMDD15", "");//NO MANDATORIO.
            sendAuthorizeRequestPayload.Add("CSMDD16", "");//NO MANDATORIO.
        }

        private Dictionary<String, Object> executeGetAuthorizeAnswer(TPConnector connector, TodoPagoInfoViewModel info)
        {
            Dictionary<string, string> getAuthorizeAnswerParams = new Dictionary<string, string>();

            info.ResultadoGAA = String.Empty;

            initGetAuthorizeAnswer(getAuthorizeAnswerParams, info);

            var res = new Dictionary<String, Object>();

            try
            {
                res = connector.GetAuthorizeAnswer(getAuthorizeAnswerParams);

                if (res.ContainsKey("StatusCode") && res["StatusCode"] != null)
                {
                    info.StatusCode = res["StatusCode"].ToString();
                }

                if (res.ContainsKey("StatusMessage") && res["StatusMessage"] != null)
                {
                    info.StatusMessage = res["StatusMessage"].ToString();
                }

                foreach (var key in res.Keys)
                {
                    info.ResultadoGAA += "- " + key + ": " + res[key];
                    if (key.Equals("Payload"))
                    {
                        XmlNode[] aux = (XmlNode[])res["Payload"];
                        if (aux != null)
                        {
                            for (int i = 0; i < aux.Length; i++)
                            {
                                XmlNodeList inner = aux[i].ChildNodes;
                                for (int j = 0; j < inner.Count; j++)
                                {
                                    info.ResultadoGAA += "     " + inner.Item(j).Name + " : " + inner.Item(j).InnerText;
                                }
                            }
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    WebResponse resp = ex.Response;
                    using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                    {
                        info.ResultadoGAA = sr.ReadToEnd() + " - " + ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                info.ResultadoGAA = ex.Message + " - " + ex.InnerException.Message + " - " + ex.HelpLink;
            }

            return res;
        }

        private void initGetAuthorizeAnswer(Dictionary<string, string> getAuthorizeAnswerParams, TodoPagoInfoViewModel info)
        {
            getAuthorizeAnswerParams.Add(ElementNames.SECURITY, info.Security);
            getAuthorizeAnswerParams.Add(ElementNames.SESSION, null);
            getAuthorizeAnswerParams.Add(ElementNames.MERCHANT, info.MerchantId);
            getAuthorizeAnswerParams.Add(ElementNames.REQUESTKEY, info.RequestKey);
            getAuthorizeAnswerParams.Add(ElementNames.ANSWERKEY, info.AnswerKey);
        }

        private TPConnector initConnector(TodoPagoInfoViewModel info)
        {
            var headers = new Dictionary<String, String>();
            headers.Add("Authorization", info.ApiKey);

            return new TPConnector(TPConnector.developerEndpoint, headers);
        }
    

    // fin TODOPAGO

    protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
