﻿using ReservasWeb.Authorize;
using ReservasWeb.Models;
using ReservasWeb.Services;
using ReservasWeb.Utils;
using ReservasWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.Controllers
{
    [RequiereLogin]
    public class EstadisticasController : BaseController
    {
        // GET: TiposHabitacion
        public ActionResult Index(string fechaDesde = null, string fechaHasta = null)
        {
            chequearPermisos(4,0);
            HabitacionesService habitacionesSV = new HabitacionesService(this.db);
            ReservasService reservasSV = new ReservasService(this.db);
            EstadisticasViewModel vm = new EstadisticasViewModel();

            DateTime fechaDesdeDT, fechaHastaDT;
            if (!DateTime.TryParse(fechaDesde, out fechaDesdeDT))
                fechaDesdeDT = DateHelper.obtenerFechaActual();
            else
                vm.fechaDesde = fechaDesdeDT.ToString("dd/MM/yyyy");
            if (!DateTime.TryParse(fechaHasta, out fechaHastaDT))
                fechaHastaDT = DateHelper.obtenerFechaActual();
            else
                vm.fechaHasta = fechaHastaDT.ToString("dd/MM/yyyy");

            vm.huespedes = reservasSV.getCantidadHuespedes(fechaDesdeDT, fechaHastaDT);
            vm.reservasNuevas = reservasSV.getCantidadReservasNuevas(fechaDesdeDT, fechaHastaDT);
            vm.reservasCanceladas = reservasSV.getCantidadReservasCanceladas(fechaDesdeDT, fechaHastaDT);
            vm.checkIn = reservasSV.getCantidadCheckIn(fechaDesdeDT, fechaHastaDT);
            vm.checkOut = reservasSV.getCantidadCheckOut(fechaDesdeDT, fechaHastaDT);
            vm.checkInPendientes = reservasSV.getCantidadCheckInPendientes(fechaDesdeDT, fechaHastaDT);
            vm.checkOutPendientes = reservasSV.getCantidadCheckOutPendientes(fechaDesdeDT, fechaHastaDT);
            vm.habitacionesOcupadas = reservasSV.getCantidadHabitacionesOcupadas(fechaDesdeDT, fechaHastaDT);
            vm.habitacionesDisponibles = habitacionesSV.getCantidadTotalHabitacionesDisponibles() - vm.habitacionesOcupadas;
            return View(vm);
        }

        [HttpPost]
        public ActionResult Index(EstadisticasViewModel vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (vm.fechaDesde != "" && vm.fechaHasta != "")
                        return RedirectToAction("index", "Estadisticas", new { fechaDesde = vm.fechaDesde, fechaHasta = vm.fechaHasta });
                }
            }
            catch (Exception)
            {
            }

            return RedirectToAction("index", "Estadisticas");
        }
        protected decimal precioDecimal(string precioTexto)
        {
            decimal precioDec;
            if (precioTexto != null && Decimal.TryParse(precioTexto.Replace("$ ", ""), NumberStyles.Any, new CultureInfo("en-US"), out precioDec))
                return precioDec;
            else
                return 0;            
        }

    }
}