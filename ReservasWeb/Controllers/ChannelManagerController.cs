﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReservasWeb.ViewModels;
using ReservasWeb.ViewModels.Channel;
using ReservasWeb.Services;
using ReservasWeb.WebServices;
using ReservasWeb.Models;
using ReservasWeb.Utils;
using ReservasWeb.Authorize;
using System.Configuration;

namespace ReservasWeb.Controllers
{
    public class ChannelManagerController : BaseController
    {
        // GET: ChannelManager
        [RequiereLogin]
        public ActionResult Index()
        {
            ChannelViewModel vm = new ChannelViewModel();
            
            ChannelManagerService sv = new ChannelManagerService(this.db);
            Sesion sesion = System.Web.HttpContext.Current.Session["SesionUsuario"] as Sesion;//trae la sesion actual

            vm.idEstado = sv.estaLinkeadoHotel(sesion.idAlojamientoActual);
            vm.texto = Utils.ComboHelper.getLinkHotel(vm.idEstado);
            return View(vm);
        }

        [RequiereLogin]
        public ActionResult getHotels()
        {
            string xml = "";
            xml = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["RoomCloud_getHotels"]));
            xml = xml.Replace("##TOCKEN##", ConfigurationManager.AppSettings["RoomCloud_API_KEY"]);
            xml = xml.Replace("##USUARIO##", ConfigurationManager.AppSettings["RoomCloud_USER"]);
            xml = xml.Replace("##CLAVE##", ConfigurationManager.AppSettings["RoomCloud_PASS"]);
            string Resultado = ChannelWebService.postXMLData(ConfigurationManager.AppSettings["RoomCloud_URL_PROD"], xml);
            ChannelViewModel vm = new ChannelViewModel();
            vm.texto = Resultado;
            return View(vm);
        }

        [RequiereLogin]
        public ActionResult getRates()
        {
            string xml = "";
            xml = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["RoomCloud_getRates"]));
            xml = xml.Replace("##TOCKEN##", ConfigurationManager.AppSettings["RoomCloud_API_KEY"]);
            xml = xml.Replace("##USUARIO##", ConfigurationManager.AppSettings["RoomCloud_USER"]);
            xml = xml.Replace("##CLAVE##", ConfigurationManager.AppSettings["RoomCloud_PASS"]);
            string Resultado = ChannelWebService.postXMLData(ConfigurationManager.AppSettings["RoomCloud_URL_PROD"], xml);
            ChannelViewModel vm = new ChannelViewModel();
            vm.texto = Resultado;
            return View(vm);
        }

        [RequiereLogin]
        public ActionResult downloadReservations()
        {
            string xml = ""; 
            xml = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["RoomCloud_downloadReservations"]));
            xml = xml.Replace("##TOCKEN##", ConfigurationManager.AppSettings["RoomCloud_API_KEY"]);
            xml = xml.Replace("##USUARIO##", ConfigurationManager.AppSettings["RoomCloud_USER"]);
            xml = xml.Replace("##CLAVE##", ConfigurationManager.AppSettings["RoomCloud_PASS"]);
            string Resultado = ChannelWebService.postXMLData(ConfigurationManager.AppSettings["RoomCloud_URL_PROD"], xml);
            ChannelViewModel vm = new ChannelViewModel();
            vm.texto = Resultado;
            return View(vm);
        }

        [RequiereLogin]
        public ActionResult getRooms()
        {
            string xml = "";
            xml = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["RoomCloud_getRooms"]));
            xml = xml.Replace("##TOCKEN##", ConfigurationManager.AppSettings["RoomCloud_API_KEY"]);
            xml = xml.Replace("##USUARIO##", ConfigurationManager.AppSettings["RoomCloud_USER"]);
            xml = xml.Replace("##CLAVE##", ConfigurationManager.AppSettings["RoomCloud_PASS"]);
            string Resultado = ChannelWebService.postXMLData(ConfigurationManager.AppSettings["RoomCloud_URL_PROD"], xml);
            ChannelViewModel vm = new ChannelViewModel();
            vm.texto = Resultado;
            return View(vm);
        }



        [RequiereLogin]
        public ActionResult Modify()
        {
            string xml = "";
            xml = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["RoomCloud_modify"]));
            xml = xml.Replace("##TOCKEN##", ConfigurationManager.AppSettings["RoomCloud_API_KEY"]);
            xml = xml.Replace("##USUARIO##", ConfigurationManager.AppSettings["RoomCloud_USER"]);
            xml = xml.Replace("##CLAVE##", ConfigurationManager.AppSettings["RoomCloud_PASS"]);
            string Resultado = ChannelWebService.postXMLData(ConfigurationManager.AppSettings["RoomCloud_URL_PROD"], xml);
            ChannelViewModel vm = new ChannelViewModel();
            vm.texto = Resultado;
            return View(vm);
        }

        [RequiereLogin]
        public ActionResult Reservations()
        {
            string xml = "";
            xml = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["RoomCloud_reservations"]));
            xml = xml.Replace("##TOCKEN##", ConfigurationManager.AppSettings["RoomCloud_API_KEY"]);
            xml = xml.Replace("##USUARIO##", ConfigurationManager.AppSettings["RoomCloud_USER"]);
            xml = xml.Replace("##CLAVE##", ConfigurationManager.AppSettings["RoomCloud_PASS"]);
            string Resultado = ChannelWebService.postXMLData(ConfigurationManager.AppSettings["RoomCloud_URL_PROD"], xml);
            ChannelViewModel vm = new ChannelViewModel();
            ChannelWebService sv = new ChannelWebService(this.db);
            sv.getReservations();
            vm.texto = Resultado;
            return View(vm);
        }

        [RequiereLogin]
        public ActionResult View0()
        {
            string xml = "";
            xml = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["RoomCloud_view"]));
            xml = xml.Replace("##TOCKEN##", ConfigurationManager.AppSettings["RoomCloud_API_KEY"]);
            xml = xml.Replace("##USUARIO##", ConfigurationManager.AppSettings["RoomCloud_USER"]);
            xml = xml.Replace("##CLAVE##", ConfigurationManager.AppSettings["RoomCloud_PASS"]);
            string Resultado = ChannelWebService.postXMLData(ConfigurationManager.AppSettings["RoomCloud_URL_PROD"], xml);
            ChannelViewModel vm = new ChannelViewModel();
            vm.texto = Resultado;
            return View(vm);
        }

        [RequiereLogin]
        public ActionResult getPortals()
        {
            string xml = "";
            xml = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["RoomCloud_getPortals"]));
            xml = xml.Replace("##TOCKEN##", ConfigurationManager.AppSettings["RoomCloud_API_KEY"]);
            xml = xml.Replace("##USUARIO##", ConfigurationManager.AppSettings["RoomCloud_USER"]);
            xml = xml.Replace("##CLAVE##", ConfigurationManager.AppSettings["RoomCloud_PASS"]);
            string Resultado = ChannelWebService.postXMLData(ConfigurationManager.AppSettings["RoomCloud_URL_PROD"], xml);
            ChannelViewModel vm = new ChannelViewModel();
            vm.texto = Resultado;
            return View(vm);
        }

        // GET: ChannelManager/Details/5
        
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ChannelManager/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ChannelManager/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public JsonResult ActivarLink(int id, bool disponible)
        {
            string resultado = "error";
            try
            {
                TiposHabitacionService tipoHabService = new TiposHabitacionService(this.db);
                tipoHabService.activarLink(id, disponible);
                resultado = "ok";
                
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
            return Json(new { Resultado = resultado });
        }

        // GET: ChannelManager/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ChannelManager/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ChannelManager/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ChannelManager/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult TipoHabitaciones()
        {
            ListadoTiposHabitacionViewModel vm = new ListadoTiposHabitacionViewModel();

            TiposHabitacionService sv = new TiposHabitacionService(this.db);
            vm.listado = sv.getTiposHabitacionPorAlojamiento();

            return View(vm);
        }

        public ActionResult elegirTiposHabitaciones()
        {
            listadoChannelTipoHabViewModel vm = new listadoChannelTipoHabViewModel();
            ChannelWebService svW = new ChannelWebService(this.db);
            TiposHabitacionService sv = new TiposHabitacionService(this.db);
            vm.listado = sv.getTiposHabitacionPorAlojamiento();
            string xml = "";
            vm.comboTipoHab = svW.getComboHabFuera(xml,false);

            return View(vm);
        }

        [HttpPost]
        public ActionResult guardarTiposHabitaciones(listadoChannelTipoHabViewModel vm)
        {
            TiposHabitacionService svA = new TiposHabitacionService(this.db);
            ChannelManagerService sv = new ChannelManagerService(this.db);
            vm.listado = svA.getTiposHabitacionPorAlojamiento();
            sv.linkearHabitaciones(vm);
            return RedirectToAction("Index");
        }

        public ActionResult Hoteles()
        {
            string xml = "";
            xml = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["RoomCloud_getHotels"]));
            xml = xml.Replace("##TOCKEN##", ConfigurationManager.AppSettings["RoomCloud_API_KEY"]);
            xml = xml.Replace("##USUARIO##", ConfigurationManager.AppSettings["RoomCloud_USER"]);
            xml = xml.Replace("##CLAVE##", ConfigurationManager.AppSettings["RoomCloud_PASS"]);
            string Resultado = ChannelWebService.postXMLData(ConfigurationManager.AppSettings["RoomCloud_URL_PROD"], xml);
            ChannelViewModel vm = new ChannelViewModel();
            vm.texto = Resultado;
            
            return View(vm);
        }

        public ActionResult Linkear(int idTipoHabitacion)
        {
            EdicionTipoHabitacionViewModel vm = new EdicionTipoHabitacionViewModel();
            TiposHabitacionService sv = new TiposHabitacionService(this.db);
            vm = sv.getTipoHabitacionEdicion(idTipoHabitacion);
            return View(vm);
        }

        public ActionResult CerrarDisponibilidad()
        {
            return View();
        }

        public ActionResult Tarifas(int idHotelChannel)
        {
            string xml = "";
            xml = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["RoomCloud_getRates"]));
            xml.Replace("##HOTEL##", idHotelChannel.ToString());
            xml = xml.Replace("##TOCKEN##", ConfigurationManager.AppSettings["RoomCloud_API_KEY"]);
            xml = xml.Replace("##USUARIO##", ConfigurationManager.AppSettings["RoomCloud_USER"]);
            xml = xml.Replace("##CLAVE##", ConfigurationManager.AppSettings["RoomCloud_PASS"]);

            string Resultado = ChannelWebService.postXMLData(ConfigurationManager.AppSettings["RoomCloud_URL_PROD"], xml);
            ChannelViewModel vm = new ChannelViewModel();
            vm.texto = Resultado;

            return View(vm);
        }

        
    }
}
