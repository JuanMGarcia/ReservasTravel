﻿using ReservasWeb.Authorize;
using ReservasWeb.Models;
using ReservasWeb.Services;
using ReservasWeb.Utils;
using ReservasWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.Controllers
{
    [RequiereLogin]
    public class ItemsCuentaPorReservaController : BaseController
    {
        private object respuesta;

        // GET: Temporadas
        public ActionResult Index(string idReserva, string resultado)
        {
            chequearPermisos(8,0);
            ListadoItemsCuentaPorReservaViewModel vm = new ListadoItemsCuentaPorReservaViewModel();
            ItemsCuentaPorReservaService itemsCuentaSV = new ItemsCuentaPorReservaService(this.db);
            vm.listadoItemsCuenta = new List<CreacionEdicionItemCuentaPorReservaViewModel>();
            vm.saldo = 0;
            foreach (ItemsCuentaPorReserva itemCuenta in itemsCuentaSV.getItemsCuenta(Int32.Parse(idReserva)))
            {
                if (itemCuenta.concepto == "CH")
                {
                    vm.saldo = vm.saldo + itemCuenta.monto; 
                }
                else
                {
                    vm.saldo = vm.saldo - itemCuenta.monto;
                }
                vm.listadoItemsCuenta.Add(toViewModel(new CreacionEdicionItemCuentaPorReservaViewModel(), itemCuenta));
            }

            vm.listaTipodePago = ComboHelper.getComboTipoPagos();
            vm.ProcesarResultado(resultado);
            vm.idReserva = Int32.Parse(idReserva);
            int idRsv = Int32.Parse(idReserva);
            Reserva rsv = db.Reservas.Where(r => r.idReserva == idRsv).FirstOrDefault();
            if (rsv != null)
            {
                if(rsv.fechaEntrada != null)
                    vm.reservaFecha = rsv.fechaEntrada.ToShortDateString();
            }
            decimal precioSubTotal = 0;
            HabitacionesService svHab = new HabitacionesService(this.db);
            foreach (HabitacionesPorReserva habitacionesPR in svHab.getHabitacionesPorReservaPorId(vm.idReserva))
            {                
                precioSubTotal = precioSubTotal + habitacionesPR.precioHabitacion;
            }


            vm.reservaMonto = precioSubTotal;

            //Mauro
            AlojamientosService alojamientoService = new AlojamientosService(this.db);
            vm.simboloMoneda = ComboHelper.GetSimboloMoneda(alojamientoService.getAlojamiento().idMoneda);

            return PartialView("~/Views/Shared/_CuentaReservaPartialPage.cshtml", vm);         
        }

        public ActionResult Crear(int idReserva, string tipo)
        {
            chequearPermisos(8, 1);
            ReservasService reservasSV = new ReservasService(this.db);
            CreacionEdicionItemCuentaPorReservaViewModel vm = new CreacionEdicionItemCuentaPorReservaViewModel();
            if (reservasSV.getReserva(idReserva) != null)
            {
                ItemsCuentaPorReservaService itemsCuentaSV = new ItemsCuentaPorReservaService(this.db);
                if (tipo == "C")
                    vm.conceptoSelectList = itemsCuentaSV.getComboConceptoCargo();
                else
                    vm.conceptoSelectList = itemsCuentaSV.getComboConceptoPago();

                vm.formaDePagoSelectList = itemsCuentaSV.getComboFormasDePago();
                vm.idReserva = idReserva;
            }

            return View(vm);
        }

        [HttpPost]
        public ActionResult Crear(CreacionEdicionItemCuentaPorReservaViewModel vm)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    ItemsCuentaPorReservaService itemsCuentaSV = new ItemsCuentaPorReservaService(this.db);
                    itemsCuentaSV.Crear(vm);
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "ItemsCuenta", new { resultado = resultado });
        }

        public ActionResult Editar(int idItemCuenta, int idReserva, string tipo)
        {
            chequearPermisos(8, 3);
            ReservasService reservasSV = new ReservasService(this.db);
            CreacionEdicionItemCuentaPorReservaViewModel vm = new CreacionEdicionItemCuentaPorReservaViewModel();            
            
            if (reservasSV.getReserva(idReserva) != null)
            {
                ItemsCuentaPorReservaService itemsCuentaSV = new ItemsCuentaPorReservaService(this.db);
                ItemsCuentaPorReserva itemsCuentaPorReserva = itemsCuentaSV.get(idItemCuenta);
                if (tipo == "C")
                    vm.conceptoSelectList = itemsCuentaSV.getComboConceptoCargo();
                else
                    vm.conceptoSelectList = itemsCuentaSV.getComboConceptoPago();
                vm = toViewModel(vm, itemsCuentaPorReserva);
   
            }

            return View(vm);
        }

        [HttpPost]
        public ActionResult Editar(CreacionEdicionItemCuentaPorReservaViewModel vm)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    ItemsCuentaPorReservaService itemsCuentaSV = new ItemsCuentaPorReservaService(this.db);
                    itemsCuentaSV.Editar(vm);
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "ItemsCuenta", new { resultado = resultado });
        }

        public ActionResult Eliminar(int id)
        {
            string resultado = "ok";
            try
            {                
                ItemsCuentaPorReservaService itemsCuentaSV = new ItemsCuentaPorReservaService(this.db);
                itemsCuentaSV.Eliminar(id);
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "ItemsCuenta", new { resultado = resultado });
        }

        public CreacionEdicionItemCuentaPorReservaViewModel toViewModel(CreacionEdicionItemCuentaPorReservaViewModel vm, ItemsCuentaPorReserva itemCuenta)
        {
            vm.comentarios = itemCuenta.comentarios;
            vm.concepto = itemCuenta.concepto;
            vm.fecha = itemCuenta.fecha.HasValue ? itemCuenta.fecha.Value.ToString("dd/MM/yyyy") : "";
            vm.formaDePago = itemCuenta.formaPago;
            vm.idItemCuenta = itemCuenta.idItemCuenta;
            vm.idReserva = itemCuenta.idReserva;
            vm.monto = Convert.ToString(itemCuenta.monto, new CultureInfo("en-US"));
            return vm;
        }


        [HttpPost]
        public JsonResult obtenerMonto(int idCuenta)
        {
            decimal monto = 0;
            ItemsCuentaPorReserva unItem = db.ItemsCuentaPorReservas.Where(s=>s.idItemCuenta == idCuenta).FirstOrDefault();
            if (unItem.concepto=="CH")
            {
                monto = unItem.monto;
            } 
            else
            {
                monto = unItem.monto * (-1);
            }
 
            return Json(unItem.monto);
        }

        [HttpPost]
        public JsonResult agregarPago(decimal monto, DateTime fecha, string forma, string comentarios,int idReserva)
        {
            ItemsCuentaPorReserva unItem = new ItemsCuentaPorReserva();
            unItem.comentarios = comentarios;
            unItem.concepto = "P";
            unItem.fecha = fecha;
            unItem.fechaCreacion = DateTime.Now;
            unItem.formaPago = forma;
            unItem.idReserva = idReserva;
            unItem.monto = monto;
            db.ItemsCuentaPorReservas.Add(unItem);
            db.SaveChanges();

            return Json(unItem.idItemCuenta);

        }

        [HttpPost]
        public JsonResult agregarDescuento(decimal monto, DateTime fecha, string forma, string comentarios, int idReserva)
        {
            ItemsCuentaPorReserva unItem = new ItemsCuentaPorReserva();
            unItem.comentarios = comentarios;
            unItem.concepto = "D";
            unItem.fecha = fecha;
            unItem.fechaCreacion = DateTime.Now;
            unItem.formaPago = forma;
            unItem.idReserva = idReserva;
            unItem.monto = monto;
            db.ItemsCuentaPorReservas.Add(unItem);
            db.SaveChanges();

            return Json(unItem.idItemCuenta);

        }

        [HttpPost]
        public JsonResult agregarCargo(decimal monto, DateTime fecha, string forma, string comentarios, int idReserva)
        {
            ItemsCuentaPorReserva unItem = new ItemsCuentaPorReserva();
            unItem.comentarios = comentarios;
            unItem.concepto = "CH";
            unItem.fecha = fecha;
            unItem.fechaCreacion = DateTime.Now;
            unItem.formaPago = forma;
            unItem.idReserva = idReserva;
            unItem.monto = monto;
            db.ItemsCuentaPorReservas.Add(unItem);
            db.SaveChanges();
            
            return Json(unItem.idItemCuenta);

        }

    }
}