﻿using ReservasWeb.Authorize;
using ReservasWeb.Models;
using ReservasWeb.Services;
using ReservasWeb.Utils;
using ReservasWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.Controllers
{
    [RequiereLogin]
    public class TarifasBaseController : BaseController
    {
        // GET: TiposHabitacion
        public ActionResult Index(string resultado)
        {
            chequearPermisos(4, 0);
            ListadoTiposHabitacionViewModel vm = new ListadoTiposHabitacionViewModel();
            TiposHabitacionService svTip = new TiposHabitacionService(this.db);
            vm.listado = svTip.getTiposHabitacionPorAlojamiento();
            vm.ProcesarResultado(resultado);
            return View(vm);
        }

        public ActionResult Crear()
        {
            chequearPermisos(4, 1);
            CreacionTipoHabitacionViewModel vm = new CreacionTipoHabitacionViewModel();
            return View(vm);
        }

        [HttpPost]
        public ActionResult Crear(CreacionTipoHabitacionViewModel vm)
        {
            string resultado = "error";
            int idTipoHabitacion = 0;
            try
            {
                if (ModelState.IsValid)
                {
                    TiposHabitacionService svTip = new TiposHabitacionService(this.db);
                    idTipoHabitacion = svTip.Crear(vm);
                    resultado = "ok";
                }
            }
            catch (Exception)
            {
            }
            if (idTipoHabitacion > 0)
                return RedirectToAction("Editar", new { id = idTipoHabitacion });
            else
                return RedirectToAction("Index", new { resultado = resultado });
        }

        public ActionResult Editar(int id)
        {
            chequearPermisos(4, 3);
            TiposHabitacionService sv = new TiposHabitacionService(this.db);
            CondicionesReservaService condicionesReservaSV = new CondicionesReservaService(this.db);
            EdicionTipoHabitacionViewModel vm = sv.getTipoHabitacionEdicion(id);
            if (vm.horaCheckOutTardio == null)
                vm.horaCheckOutTardio = "10:00";
            //vm.plazasSelectList = sv.getComboPlazas(0);

            vm.camasSimpleMatrimonialSelectList = sv.getComboCamasSimplesMatrimoniales();
            vm.permiteCamasAdicionalesSelectList = sv.getComboSiNo();
            vm.permiteAdicionalesBebeSelectList = sv.getComboSiNo();
            vm.permiteAdicionalesMenorSelectList = sv.getComboSiNo();
            vm.permiteAdicionalesAdultoSelectList = sv.getComboSiNo();
            vm.camasAdicionalesSelectList = sv.getComboCamasAdicionales(0);

            CanalesService canalService = new CanalesService(this.db);
            List<Canale> canalesLista = new List<Canale>();
            canalesLista = canalService.getCanalesDeAlojamiento();



            List<CondReserva> condicionesReservas = condicionesReservaSV.getCondicionesReservaListadoAbm();

            //vm.tarifaBaseListado = new Dictionary<int, List<TarifaBaseViewModel>>();
            vm.tarifaBaseListado = new List<TarifaBaseViewModel>();
            //vm.tarifaBaseListado.Add(con)
            //CondReserva condicion;
            //foreach(condicion in condicionesReservas)
            //{

            //}

            //List<TarifaBaseViewModel> tarifaBaseListado;
            //foreach (CondReserva condicion in condicionesReservaSV.getCondicionesReservaListadoAbm())
            CondReserva condicion = null;
            vm.condicionesReservaListado = new List<SelectListItem>();
            string condicionId, condicionNombre;
            bool selected = false;
            //vm.condicionesReservaListado.Add(new SelectListItem { Value = "-1", Text = "Sin condiciones", Selected = true });
            for (int x = -1; x < condicionesReservas.Count(); x++)
            {
                if (x >= 0)
                {
                    condicion = condicionesReservas.ElementAt(x);
                    condicionId = condicion.idCondReserva.ToString();
                    condicionNombre = condicion.nombreEs;
                    selected = false;
                }
                else
                {
                    condicionId = x.ToString();
                    condicionNombre = "Sin condiciones";
                    selected = true;
                }

                vm.condicionesReservaListado.Add(new SelectListItem { Value = condicionId, Text = condicionNombre, Selected = selected });

                //tarifaBaseListado = new List<TarifaBaseViewModel>();
                TarifasBaseService tarifasBaseService = new TarifasBaseService(this.db);
                TarifaBaseViewModel tarifaBaseViewModel;
                TarifasBase tarifaBase;
                for (int i = 0; i < canalesLista.Count(); i++)
                {

                    tarifaBaseViewModel = new TarifaBaseViewModel();
                    tarifaBaseViewModel.idTipoHabitacion = vm.idTipoHabitacion;
                    tarifaBaseViewModel.idCanal = canalesLista.ElementAt(i).idCanal;
                    tarifaBaseViewModel.nombreCanalEs = canalesLista.ElementAt(i).nombreEs;
                    tarifaBaseViewModel.nombreCanalEn = canalesLista.ElementAt(i).nombreEn;
                    tarifaBaseViewModel.nombreCanalPt = canalesLista.ElementAt(i).nombrePt;


                    tarifaBase = tarifasBaseService.get(vm.idTipoHabitacion, canalesLista.ElementAt(i).idCanal, Convert.ToInt32(condicionId));
                    if (tarifaBase != null)
                    {
                        tarifaBaseViewModel.tarifaLunes = tarifaBase.tarifaLunes == 0 ? "" : Convert.ToString(tarifaBase.tarifaLunes, new CultureInfo("en-US"));
                        tarifaBaseViewModel.tarifaMartes = tarifaBase.tarifaMartes == 0 ? "" : Convert.ToString(tarifaBase.tarifaMartes, new CultureInfo("en-US"));
                        tarifaBaseViewModel.tarifaMiercoles = tarifaBase.tarifaMiercoles == 0 ? "" : Convert.ToString(tarifaBase.tarifaMiercoles, new CultureInfo("en-US"));
                        tarifaBaseViewModel.tarifaJueves = tarifaBase.tarifaJueves == 0 ? "" : Convert.ToString(tarifaBase.tarifaJueves, new CultureInfo("en-US"));
                        tarifaBaseViewModel.tarifaViernes = tarifaBase.tarifaViernes == 0 ? "" : Convert.ToString(tarifaBase.tarifaViernes, new CultureInfo("en-US"));
                        tarifaBaseViewModel.tarifaSabado = tarifaBase.tarifaSabado == 0 ? "" : Convert.ToString(tarifaBase.tarifaSabado, new CultureInfo("en-US"));
                        tarifaBaseViewModel.tarifaDomingo = tarifaBase.tarifaDomingo == 0 ? "" : Convert.ToString(tarifaBase.tarifaDomingo, new CultureInfo("en-US"));
                        tarifaBaseViewModel.descuentoPax = tarifaBase.descuentoPax == 0 ? "" : Convert.ToString(tarifaBase.descuentoPax, new CultureInfo("en-US"));
                        tarifaBaseViewModel.adicionalCuna = tarifaBase.adicionalCuna == 0 ? "" : Convert.ToString(tarifaBase.adicionalCuna, new CultureInfo("en-US"));
                        tarifaBaseViewModel.adicionalMenor = tarifaBase.adicionalMenor == 0 ? "" : Convert.ToString(tarifaBase.adicionalMenor, new CultureInfo("en-US"));
                        tarifaBaseViewModel.adicionalAdulto = tarifaBase.adicionalAdulto == 0 ? "" : Convert.ToString(tarifaBase.adicionalAdulto, new CultureInfo("en-US"));
                        tarifaBaseViewModel.estadiaMinima = tarifaBase.estadiaMinima == 0 ? "" : Convert.ToString(tarifaBase.estadiaMinima, new CultureInfo("en-US"));
                        tarifaBaseViewModel.permiteEgreso = Convert.ToBoolean(tarifaBase.permiteEgreso);
                        tarifaBaseViewModel.permiteIngreso = Convert.ToBoolean(tarifaBase.permiteIngreso);
                        tarifaBaseViewModel.aceptaMenores = Convert.ToBoolean(tarifaBase.aceptaMenores);
                        tarifaBaseViewModel.abierta = Convert.ToBoolean(tarifaBase.abierta);
                        //tarifaBaseViewModel.permiteEgreso = true;
                        //tarifaBaseViewModel.permiteIngreso = true;
                        //tarifaBaseViewModel.aceptaMenores = true;
                        //tarifaBaseViewModel.abierta = true;
                    }
                    else
                    {
                        //    tarifaBaseViewModel.tarifaLunes = "0";
                        //    tarifaBaseViewModel.tarifaMartes = "0";
                        //    tarifaBaseViewModel.tarifaMiercoles = "0";
                        //    tarifaBaseViewModel.tarifaJueves = "0";
                        //    tarifaBaseViewModel.tarifaViernes = "0";
                        //    tarifaBaseViewModel.tarifaSabado = "0";
                        //    tarifaBaseViewModel.tarifaDomingo = "0";
                        //    tarifaBaseViewModel.descuentoPax = "0";
                        //    tarifaBaseViewModel.adicionalCuna = "0";
                        //    tarifaBaseViewModel.adicionalMenor = "0";
                        //    tarifaBaseViewModel.adicionalAdulto = "0";
                        tarifaBaseViewModel.permiteEgreso = true;
                        tarifaBaseViewModel.permiteIngreso = true;
                        tarifaBaseViewModel.aceptaMenores = true;
                        tarifaBaseViewModel.estadiaMinima = "1";


                        //si no tiene condicion y es Canal Web o Mostrador, entonces abierto por default
                        if (condicionId == "-1" && (canalesLista.ElementAt(i).idCanal == Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]) || canalesLista.ElementAt(i).idCanal == Convert.ToInt32(ConfigurationManager.AppSettings["idCanalMostrador"])))
                            tarifaBaseViewModel.abierta = true;
                    }

                    vm.tarifaBaseListado.Add(tarifaBaseViewModel);


                }

                //vm.tarifaBaseListado.Add(Convert.ToInt32(condicionId), tarifaBaseListado);

            }

            GaleriasService galeriasService = new GaleriasService(this.db);
            vm.fotos = galeriasService.getFotosTipoHabitacion(id);
            vm.IdsFotosOrden = vm.GetIdsFotosOrden();

            ComodidadesService comodidadesService = new ComodidadesService(this.db);
            vm.listaComodidades = comodidadesService.getComodidadesSeleccionadasTipoHabitacion(id);





            return View(vm);
        }

        [HttpPost]
        public ActionResult Editar(EdicionTipoHabitacionViewModel vm)
        {
            string resultado = "error";
            int estadiaMinima = 0;
            int disponibilidadCanal = 0;
            if (ModelState.IsValid)
            {
                TiposHabitacionService sv = new TiposHabitacionService(this.db);
                CondicionesReservaService condicionesReservaSV = new CondicionesReservaService(this.db);
                sv.Editar(vm);
                TarifasBaseService tarifasBaseService = new TarifasBaseService(this.db);
                TarifasBase tarifaBase;
                bool esNuevo = false;






                List<CondReserva> condicionesReservas = condicionesReservaSV.getCondicionesReservaListadoAbm();

                //vm.tarifaBaseListado = new Dictionary<int, List<TarifaBaseViewModel>>();

                //List<TarifaBaseViewModel> tarifaBaseListado;
                CondReserva condicion = null;
                vm.condicionesReservaListado = new List<SelectListItem>();
                string condicionId, condicionNombre;
                int desde = 0;
                int cantCanales = vm.tarifaBaseListado.Count() / (condicionesReservas.Count() + 1);
                int hasta = cantCanales;
                for (int x = -1; x < condicionesReservas.Count(); x++)

                {
                    if (x >= 0)
                    {
                        condicion = condicionesReservas.ElementAt(x);
                        condicionId = condicion.idCondReserva.ToString();
                        condicionNombre = condicion.nombreEs;
                    }
                    else
                    {
                        condicionId = x.ToString();
                        condicionNombre = "Sin condiciones";
                    }



                    for (int i = desde; i < hasta; i++)
                    {
                        tarifaBase = tarifasBaseService.get(vm.idTipoHabitacion, vm.tarifaBaseListado[i].idCanal, Convert.ToInt32(condicionId));
                        if (tarifaBase == null)
                        {
                            esNuevo = true;
                            tarifaBase = new TarifasBase();
                            tarifaBase.idCanal = vm.tarifaBaseListado[i].idCanal;
                            tarifaBase.idTipoHabitacion = vm.idTipoHabitacion;
                            if (condicionId == "-1")
                                tarifaBase.idCondReserva = null;
                            else
                                tarifaBase.idCondReserva = Convert.ToInt32(condicionId);
                        }
                        else
                            esNuevo = false;

                        //si viene abierta en false => solo guardo ese cambio de dato, lo demas queda igual
                        if (Convert.ToBoolean(vm.tarifaBaseListado[i].abierta))
                        {
                            if (tarifaBase.tarifaLunes == null)
                                tarifaBase.tarifaLunes = 0;
                            if (tarifaBase.tarifaMartes == null)
                                tarifaBase.tarifaMartes = 0;
                            if (tarifaBase.tarifaMiercoles == null)
                                tarifaBase.tarifaMiercoles = 0;
                            if (tarifaBase.tarifaJueves == null)
                                tarifaBase.tarifaJueves = 0;
                            if (tarifaBase.tarifaViernes == null)
                                tarifaBase.tarifaViernes = 0;
                            if (tarifaBase.tarifaSabado == null)
                                tarifaBase.tarifaSabado = 0;
                            if (tarifaBase.tarifaDomingo == null)
                                tarifaBase.tarifaDomingo = 0;
                            if (tarifaBase.descuentoPax == null)
                                tarifaBase.descuentoPax = 0;
                            if (tarifaBase.adicionalCuna == null)
                                tarifaBase.adicionalCuna = 0;
                            if (tarifaBase.adicionalMenor == null)
                                tarifaBase.adicionalMenor = 0;
                            if (tarifaBase.adicionalAdulto == null)
                                tarifaBase.adicionalAdulto = 0;

                            tarifaBase.tarifaLunes = precioDecimal(vm.tarifaBaseListado[i].tarifaLunes);
                            tarifaBase.tarifaMartes = precioDecimal(vm.tarifaBaseListado[i].tarifaMartes);
                            tarifaBase.tarifaMiercoles = precioDecimal(vm.tarifaBaseListado[i].tarifaMiercoles);
                            tarifaBase.tarifaJueves = precioDecimal(vm.tarifaBaseListado[i].tarifaJueves);
                            tarifaBase.tarifaViernes = precioDecimal(vm.tarifaBaseListado[i].tarifaViernes);
                            tarifaBase.tarifaSabado = precioDecimal(vm.tarifaBaseListado[i].tarifaSabado);
                            tarifaBase.tarifaDomingo = precioDecimal(vm.tarifaBaseListado[i].tarifaDomingo);
                            tarifaBase.descuentoPax = precioDecimal(vm.tarifaBaseListado[i].descuentoPax);
                            tarifaBase.adicionalCuna = precioDecimal(vm.tarifaBaseListado[i].adicionalCuna);
                            tarifaBase.adicionalMenor = precioDecimal(vm.tarifaBaseListado[i].adicionalMenor);
                            tarifaBase.adicionalAdulto = precioDecimal(vm.tarifaBaseListado[i].adicionalAdulto);

                            if (Int32.TryParse(vm.tarifaBaseListado[i].disponibilidadCanal, out disponibilidadCanal))
                                tarifaBase.disponibilidadCanal = disponibilidadCanal;
                            else
                                tarifaBase.disponibilidadCanal = 1;
                            if (Int32.TryParse(vm.tarifaBaseListado[i].estadiaMinima, out estadiaMinima))
                                tarifaBase.estadiaMinima = estadiaMinima;
                            else
                                tarifaBase.estadiaMinima = 1;
                            //tarifaBase.permiteIngreso = Convert.ToBoolean(vm.tarifaBaseListado[i].permiteIngreso);
                            //tarifaBase.permiteEgreso = Convert.ToBoolean(vm.tarifaBaseListado[i].permiteEgreso);
                            tarifaBase.aceptaMenores = Convert.ToBoolean(vm.tarifaBaseListado[i].aceptaMenores);
                            tarifaBase.abierta = Convert.ToBoolean(vm.tarifaBaseListado[i].abierta);
                        }
                        else {
                            tarifaBase.estadiaMinima = 1;
                            tarifaBase.aceptaMenores = true;
                            tarifaBase.abierta = Convert.ToBoolean(vm.tarifaBaseListado[i].abierta);
                        }

                        if (esNuevo)
                        {
                            tarifaBase.permiteIngreso = true;
                            tarifaBase.permiteEgreso = true;
                            //tarifaBase.abierta = false;
                            tarifasBaseService.crear(tarifaBase);
                        }
                        else
                            tarifasBaseService.editar(tarifaBase);
                    }


                    desde = hasta;
                    hasta = hasta + cantCanales;

                }







                //guarda comodidades seleccionadas
                List<int> ids = new List<int>();
                foreach (var item in vm.listaComodidades)
                {
                    if (item.seleccionada)
                    {
                        ids.Add(item.idComodidad);
                    }
                }
                ComodidadesService comodidadesService = new ComodidadesService(this.db);
                comodidadesService.guardarComodidadesTipoHabitacion(ids, vm.idTipoHabitacion);

            }

            resultado = "ok";
            return RedirectToAction("index", "TiposHabitacion", new { resultado = resultado });
        }

        protected decimal precioDecimal(string precioTexto)
        {
            decimal precioDec;
            if (precioTexto != null && Decimal.TryParse(precioTexto.Replace("$ ", ""), NumberStyles.Any, new CultureInfo("en-US"), out precioDec))
                return precioDec;
            else
                return 0;
        }

        public ActionResult Eliminar(int id)
        {
            chequearPermisos(4, 2);
            string resultado = "ok";
            try
            {
                CondicionesReservaService condicionesReservaSV = new CondicionesReservaService(this.db);
                HabitacionesService habitacionesService = new HabitacionesService(this.db);
                List<Habitacione> habitaciones = habitacionesService.getHabitacionesPorTipoHabitacion(id);
                if (habitaciones != null && habitaciones.Count() > 0)
                    resultado = "error_habitaciones";
                else
                {
                    ComodidadesService comodidadesService = new ComodidadesService(this.db);
                    CanalesService canalesService = new CanalesService(this.db);
                    TarifasBaseService tarifasBaseService = new TarifasBaseService(this.db);
                    GaleriasService svGale = new GaleriasService(this.db);
                    List<Imagene> fotos = svGale.getFotosTipoHabitacion(id);
                    int idGaleria = 0;
                    foreach (Imagene foto in fotos)
                    {
                        if (idGaleria == 0)
                            idGaleria = foto.idGaleriaImagen;
                        svGale.eliminarImagen(foto.idImagen);
                    }
                    if (idGaleria > 0)
                        svGale.EliminarGaleria(idGaleria);
                    TarifasBase tarifaBase = null;
                    List<CondReserva> condicionesReservas = condicionesReservaSV.getCondicionesReservaListadoAbm();
                    List<Canale> canalesAlojamiento = canalesService.getCanalesDeAlojamiento();

                    foreach (Canale canal in canalesAlojamiento)
                    {
                        foreach (CondReserva condicion in condicionesReservas)
                        {
                            tarifaBase = tarifasBaseService.get(id, canal.idCanal, condicion.idCondReserva);
                            if (tarifaBase != null)
                                tarifasBaseService.eliminar(tarifaBase);
                        }

                        tarifaBase = tarifasBaseService.get(id, canal.idCanal, null);
                        if (tarifaBase != null)
                            tarifasBaseService.eliminar(tarifaBase);

                    }


                    comodidadesService.eliminarComdidadesDeTipoHabitacion(id);
                    TiposHabitacionService svTip = new TiposHabitacionService(this.db);
                    svTip.Eliminar(id);
                }
            }
            catch (Exception e)
            {
                resultado = "error";
            }
            return RedirectToAction("index", "TiposHabitacion", new { resultado = resultado });
        }

    }
}