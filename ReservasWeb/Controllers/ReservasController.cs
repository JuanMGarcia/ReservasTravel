﻿using ReservasWeb.Authorize;
using ReservasWeb.Services;
using ReservasWeb.Utils;
using ReservasWeb.ViewModels;
using ReservasWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Globalization;

namespace ReservasWeb.Controllers
{
    [RequiereLogin]
    public class ReservasController : BaseController
    {
        // GET: Reservas
        //public ActionResult Index()
        //{
        //    GrillaViewModel vm = new GrillaViewModel();
        //    chequearPermisos(10,0);
        //    AlojamientosService svAlojamientos = new AlojamientosService(this.db);
        //    TiposHabitacionService sv = new TiposHabitacionService(this.db);

        //    BusquedaService svb = new BusquedaService(this.db);
        //    svb.BorrarPreCargasViejas();

        //    vm.tiposHabitacion = sv.getTiposHabitacionConHabitaciones();
        //    vm.canalesHabilitados = new List<int>();
        //    foreach (CanalesPorAlojamiento canalesPorAlojamiento in svAlojamientos.getAlojamiento().CanalesPorAlojamientoes)
        //    {
        //        vm.canalesHabilitados.Add(canalesPorAlojamiento.idCanal);
        //    }
        //    return View(vm);
        //}

        public ActionResult Index(int? idReserva)
        {
            if (idReserva == null)
            {
                idReserva = 0;
            }
            GrillaViewModel vm = new GrillaViewModel();
            chequearPermisos(10, 0);
            AlojamientosService svAlojamientos = new AlojamientosService(this.db);
            TiposHabitacionService sv = new TiposHabitacionService(this.db);

            BusquedaService svb = new BusquedaService(this.db);
            svb.BorrarPreCargasViejas();

            vm.tiposHabitacion = sv.getTiposHabitacionConHabitaciones();
            vm.canalesHabilitados = new List<int>();
            foreach (CanalesPorAlojamiento canalesPorAlojamiento in svAlojamientos.getAlojamiento().CanalesPorAlojamientoes)
            {
                vm.canalesHabilitados.Add(canalesPorAlojamiento.idCanal);
            }
            vm.idReserva = idReserva.Value;
            return View(vm);
        }

        [HttpPost]
        public JsonResult ObtenerHabitacionesPorTipo(int idTipoHabitacion)
        {
            HabitacionesService svHab = new HabitacionesService(this.db);
            List<HabitacionGrilla> habs = svHab.getHabitacionesPorTipoHabitacion_Grilla(idTipoHabitacion);
            return Json(habs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string ObtenerReservas(string start, string end)
        {
            ReservasService svRes = new ReservasService(this.db);
            List<ReservaGrilla> reservas = svRes.getReservasPorAlojamientoGrilla(DateTime.Parse(start), DateTime.Parse(end));
            return JsonConvert.SerializeObject(reservas, Formatting.None, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd hh:mm:ss" });
        }        

        public ActionResult crearReservaGrilla(DateTime start, DateTime end, int resource)
        {
            CreacionEdicionReservaGrilla vm = new CreacionEdicionReservaGrilla();
            HabitacionesService svHab = new HabitacionesService(this.db);
            TiposHabitacionService svTiposHab = new TiposHabitacionService(this.db);
            Habitacione habitacion = svHab.getHabitacion(resource);
            ReservasService svReserva = new ReservasService(this.db);
            CiudadesService svCiud = new CiudadesService(this.db);
            List<int> idHabitacion = new List<int>();
            List<int> idTipoHabitacion = new List<int>();
            List<int?> idCondicion = new List<int?>();
            idHabitacion.Add(habitacion.idHabitacion);
            idTipoHabitacion.Add(habitacion.idTipoHabitacion);
            vm.condiciones = new List<List<SelectListItem>>();
            //-1 ==  SIN CONDICIONES
            idCondicion.Add(-1);
            vm.ListaEstados = svReserva.getComboEstados();
            
            vm.fechain = start.ToString("dd/MM/yyyy");
            vm.fechaout = end.ToString("dd/MM/yyyy");
            vm.idHabitacion = idHabitacion;
            vm.idHabitacionPrevio = "|" + string.Join("|", idHabitacion) + "|";
            vm.idTipoHabitacion = idTipoHabitacion;
            vm.idCondicion = idCondicion;
            vm.idCondicionPrevio = "|" + string.Join("|", idCondicion) + "|";
            vm.habitacion = habitacion.nombre;
            vm.esMostrador = true;
            vm.idCanal = Convert.ToInt32(ConfigurationManager.AppSettings["idCanalMostrador"]);
            vm.idCanalWeb = Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]);
            vm.idCanalMostrador = Convert.ToInt32(ConfigurationManager.AppSettings["idCanalMostrador"]);
            vm.paises = svCiud.getPaisesCombo();
            vm.idPais = 1;
            vm.tiposHabitaciones = svTiposHab.getComboTiposHabitacionConHabitaciones();
            vm.habitaciones = new List<SelectList>();
            vm.habitaciones.Add(svHab.getComboHabitaciones(habitacion.idTipoHabitacion));
            vm.condiciones.Add(getComboCondiciones(habitacion.idTipoHabitacion));
            vm.cambiaronHabitaciones = false;
            //pago en hotel, porque es reserva creada en mostrador.
            //Modificacion por Pedido de Diego./
            //Se genera un combo de tipos de pagos, pueden seleccionar tipos de pagos solo disponible en buscador, no hay restriccion.
            vm.idTipoPago = 1;

            vm.eligeTiposPago = this.getComboTiposPagos();
            
            vm.nombreTipoPago = Utils.ComboHelper.GetNombreTipoMediodePago(vm.idTipoPago);
            vm.tiposPagoEnHotel = this.getComboTiposPagosEnHotel();

            Sesion sesion = (Sesion)System.Web.HttpContext.Current.Session["SesionUsuario"];
            AlojamientosService alojamientosService = new AlojamientosService(this.db);
            Alojamiento unAlojamiento = alojamientosService.getByIdAlojamiento(sesion.idAlojamientoActual);
            if (unAlojamiento.impuesto.HasValue)
                vm.impuesto = unAlojamiento.impuesto.Value.ToString().Replace(",", ".");
            vm.simboloMoneda = ComboHelper.GetSimboloMoneda(unAlojamiento.Moneda.idMoneda);            
            return PartialView("CreacionEdicionReservaPartialPage", vm);
        }

        public ActionResult EditarReservaGrilla(int id)
        {
            CreacionEdicionReservaGrilla vm = new CreacionEdicionReservaGrilla();
            HabitacionesService svHab = new HabitacionesService(this.db);
            TiposHabitacionService svTiposHab = new TiposHabitacionService(this.db);
            ReservasService svReserva = new ReservasService(this.db);
            CiudadesService svCiud = new CiudadesService(this.db);                        
            Reserva reserva = svReserva.getReserva(id);

            vm.eligeTiposPago = this.getComboTiposPagos();
            vm.idCondicion = new List<int?>();
            vm.idHabitacion = new List<int>();
            vm.idTipoHabitacion = new List<int>();
            vm.habitaciones = new List<SelectList>();
            vm.condiciones = new List<List<SelectListItem>>();
            vm.ListaEstados = svReserva.getComboEstados();
            vm.idEstado = reserva.idEstado.Value;
            vm.idReserva = reserva.idReserva;
            vm.fechain = reserva.fechaEntrada.ToString("dd/MM/yyyy");
            vm.fechaout = reserva.fechaSalida.ToString("dd/MM/yyyy");
            vm.checkIn = reserva.checkIn.HasValue ? reserva.checkIn.Value.ToString("dd/MM/yyyy HH:mm") : null;
            vm.checkOut = reserva.checkOut.HasValue ? reserva.checkOut.Value.ToString("dd/MM/yyyy HH:mm") : null;
            vm.esMostrador = false;
            vm.apellido = reserva.Huespede.apellido;
            if(reserva.idCanal.HasValue)
                vm.idCanal = reserva.idCanal.Value;
            vm.idCanalWeb = Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]);
            vm.idCanalMostrador = Convert.ToInt32(ConfigurationManager.AppSettings["idCanalMostrador"]);
            vm.nombre = reserva.Huespede.nombre;
            vm.email = reserva.Huespede.email;
            vm.telefono = reserva.Huespede.telefono;
            vm.celular = reserva.Huespede.celular;
            vm.direccion = reserva.Huespede.direccion;
            vm.dni = reserva.Huespede.nroDocumento;
            vm.pasajeros = (reserva.cantidadHuespedes != null) ? reserva.cantidadHuespedes.Value : 0;
            vm.observaciones = reserva.observaciones;
            vm.comentarios = reserva.comentarios;
            vm.historialListado = toHistorialReservaViewModel(svReserva.getHistorialReserva(reserva.idReserva));

            int idPais = reserva.Huespede.idPais;
            string provincia = reserva.Huespede.provincia;
            string ciudad = reserva.Huespede.ciudad;
            vm.paises = svCiud.getPaisesCombo();
            vm.idPais = idPais;
            vm.provincia = provincia;
            vm.ciudad = ciudad;
            vm.tiposHabitaciones = svTiposHab.getComboTiposHabitacionConHabitaciones();
            vm.cambiaronHabitaciones = false;
            if (reserva.idCanal == 2) //solo editar si es por mostrador
                vm.esMostrador = true;
            vm.nombreTipoPago = Utils.ComboHelper.GetNombreTipoMediodePago(reserva.idTipoPago);
            vm.idTipoPago = reserva.idTipoPago;
            
            //si el pago es en Hotel =>  muestro el select con las opciones de pago en hotel.
            if (reserva.idTipoPago == 1 && reserva.idTipoPagoEnHotel.HasValue)
            {
                vm.idTipoPagoEnHotel = reserva.idTipoPagoEnHotel.Value;
                vm.tiposPagoEnHotel = this.getComboTiposPagosEnHotel();           
            }

            //si el pago en por transferencia => muestro los datos del comprobante si ya fue cargado.
            if (reserva.idTipoPago == 5)
            {
                Comprobante comb = db.Comprobantes.Where(c => c.idReserva == reserva.idReserva).FirstOrDefault();
                if (comb != null)
                {
                    vm.tbIdComprobante = comb.idComprobante;
                    vm.tbBanco = comb.nombreBanco;
                    vm.tbFecha = comb.fecha.Value;
                    vm.tbImagenComprobante = comb.imagenComprobante;
                    vm.tbNumeroCuenta = comb.numeroCuenta;
                    vm.tbNumeroOperacion = comb.numeroOperacion;
                    vm.tbSucursal = comb.sucursal;                    
                }
                else
                {
                    vm.tbIdComprobante = -1;
                    vm.tbBanco = "No hay datos ingresados.";
                    vm.tbFecha = DateTime.Now;
                    vm.tbImagenComprobante = ".";
                    vm.tbNumeroCuenta = ".";
                    vm.tbNumeroOperacion = ".";
                    vm.tbSucursal = ".";
                }
            }

            Sesion sesion = (Sesion)System.Web.HttpContext.Current.Session["SesionUsuario"];            
            if (reserva.nroTarjeta != null) {
                vm.tarjetaTitular = Encrypto.Desencriptar(reserva.titularTarjeta, reserva.idAlojamiento, sesion.idCliente);
                vm.tarjetaNro = Regex.Replace(Encrypto.Desencriptar(reserva.nroTarjeta, reserva.idAlojamiento, sesion.idCliente), ".{4}", "$0 ");
                vm.tarjetaFecha = Encrypto.Desencriptar(reserva.fechaTarjeta, reserva.idAlojamiento, sesion.idCliente);
                vm.tarjetaCvc = Encrypto.Desencriptar(reserva.cvcTarjeta, reserva.idAlojamiento, sesion.idCliente);
            }

            string idHabitacionUltima = "";
            vm.idHabitacionPrevio = "|";
            vm.idCondicionPrevio = "|";
            decimal precioSubTotal = 0;
            decimal precioTotal = 0;

            ItemsCuentaPorReservaService itemsCuentaSV = new ItemsCuentaPorReservaService(this.db);
            decimal saldo = 0;
            foreach (ItemsCuentaPorReserva itemCuenta in itemsCuentaSV.getItemsCuenta(vm.idReserva))
            {
                if (itemCuenta.concepto == "CH")
                {
                    saldo = saldo + itemCuenta.monto;
                }
                else
                {
                    saldo = saldo - itemCuenta.monto;
                }
            }

            //precioSubTotal = precioSubTotal + saldo;
            foreach (HabitacionesPorReserva habitacionesPR in svHab.getHabitacionesPorReservaPorId(vm.idReserva))
            {
                if (idHabitacionUltima != habitacionesPR.idHabitacion.ToString())
                {
                    idHabitacionUltima = habitacionesPR.idHabitacion.ToString();
                    vm.idTipoHabitacion.Add(habitacionesPR.Habitacione.idTipoHabitacion);
                    vm.habitaciones.Add(svHab.getComboHabitaciones(habitacionesPR.Habitacione.idTipoHabitacion));
                    vm.condiciones.Add(getComboCondiciones(habitacionesPR.Habitacione.idTipoHabitacion, habitacionesPR.idCondReserva.HasValue ? habitacionesPR.idCondReserva.Value : 0));

                    vm.idHabitacion.Add(habitacionesPR.idHabitacion);
                    vm.idHabitacionPrevio = vm.idHabitacionPrevio + habitacionesPR.idHabitacion.ToString() + "|";
                    vm.idCondicion.Add(habitacionesPR.idCondReserva);
                    if (habitacionesPR.idCondReserva == null)
                        vm.idCondicionPrevio = vm.idCondicionPrevio + "-1|";
                    else
                        vm.idCondicionPrevio = vm.idCondicionPrevio + habitacionesPR.idCondReserva.ToString() + "|";
                }
                precioSubTotal = precioSubTotal + habitacionesPR.precioHabitacion;
            }

            AlojamientosService alojamientosService = new AlojamientosService(this.db);
            Alojamiento unAlojamiento = alojamientosService.getByIdAlojamiento(reserva.idAlojamiento);
            precioSubTotal = precioSubTotal + saldo;
            precioTotal = precioTotal + sumarImpuestoAlPrecio(precioSubTotal, unAlojamiento.impuesto);

           

          


            
            vm.precioSubTotal = precioSubTotal.ToString("N", new CultureInfo("en-US"));
            vm.precioTotal = precioTotal.ToString("N", new CultureInfo("en-US"));
            if (unAlojamiento.impuesto.HasValue)
                vm.impuesto = unAlojamiento.impuesto.Value.ToString().Replace(",",".");
            vm.simboloMoneda = ComboHelper.GetSimboloMoneda(unAlojamiento.Moneda.idMoneda);
            //vm.checkIn = "";
            //vm.checkOut = "";


            return PartialView("CreacionEdicionReservaPartialPage", vm);
        }

        [ValidateAjax]
        public JsonResult GuardarReserva(CreacionEdicionReservaGrilla vm)
        {
            try
            {
                vm.eligeTiposPago = this.getComboTiposPagos();
                string idReserva = "0";
                string resultado = "OK";
                bool esPreReservaMostrador = false;
                ReservasService svReserva = new ReservasService(this.db);
                if (vm.idReserva == 0)
                {
                    idReserva = svReserva.crearReservaMostrador(vm);
                    esPreReservaMostrador = true;
                }
                else
                    idReserva = svReserva.editarReservaMostrador(vm);

                if (idReserva.Contains("OK["))
                {
                    if (esPreReservaMostrador)
                        resultado = "PRECARGA";
                    return Json(new { result = resultado, message = "Guardado", id = idReserva.Replace("OK[", "").Replace("]", "") });
                }
                else {
                    resultado = "OVERBOOKING";
                    return Json(new { result = resultado, message = "Overbooking, elija otra fecha de entrada y salida", id = idReserva.Replace("OVERBOOKING[", "").Replace("]", "") });
                }
            }
            catch (Exception e)
            {
                return Json(new { result = "FAIL" });
            }            
        }

        public JsonResult EliminarReserva(int id)
        {
            try
            {
                ReservasService svReserva = new ReservasService(this.db);
                svReserva.eliminarReservaMostrador(id);
                return Json(new { result = "OK", message = "Eliminado" });
            }
            catch (Exception e)
            {
                return Json(new { result = "FAIL" });
            }
        }

        public List<HistorialReservaViewModel> toHistorialReservaViewModel(List<HistorialReserva> historialListado)
        {
            List<HistorialReservaViewModel> historialReservasVMListado = new List<HistorialReservaViewModel>();
            HistorialReservaViewModel historialReservaVM = new HistorialReservaViewModel();
            foreach (HistorialReserva historial in historialListado)
            {
                historialReservaVM = new HistorialReservaViewModel();
                historialReservaVM.idHistorialReserva = historial.idHistorialReserva;
                if(historial.idAlojamiento.HasValue)
                    historialReservaVM.idAlojamiento = historial.idAlojamiento.Value;
                historialReservaVM.idReserva = historial.idReserva;
                if (historial.idEstado.HasValue)
                    historialReservaVM.idEstado = historial.idEstado.Value;
                if (historial.idUsuario.HasValue)
                {
                    historialReservaVM.idUsuario = historial.idUsuario.Value;
                    historialReservaVM.nombreUsuario = historial.Usuario.nombre;
                    historialReservaVM.apellidoUsuario = historial.Usuario.apellido;
                }
                if (historial.idHuesped.HasValue)
                    historialReservaVM.idHuesped = historial.idHuesped.Value;
                if (historial.checkIn.HasValue)
                    historialReservaVM.checkIn = historial.checkIn.Value.ToString("dd/MM/yyyy HH:mm");
                if (historial.checkOut.HasValue)
                    historialReservaVM.cheackout = historial.checkOut.Value.ToString("dd/MM/yyyy HH:mm");
                if (historial.fechaEntrada.HasValue)
                    historialReservaVM.fechaIn = historial.fechaEntrada.Value.ToString("dd/MM/yyyy");
                if (historial.fechaSalida.HasValue)
                    historialReservaVM.fechaOut = historial.fechaSalida.Value.ToString("dd/MM/yyyy");                
                historialReservaVM.fechaModificacion = historial.fechaModificacion.ToString("dd/MM/yyyy");
                historialReservaVM.precioPorDia = historial.precioPorDia;
                if (historial.cantidadHuespedes.HasValue)
                    historialReservaVM.cantidadHuespedes = historial.cantidadHuespedes.Value;
                if (historial.cantidadHuespedes.HasValue)
                    historialReservaVM.cantidadHuespedes = historial.cantidadHuespedes.Value;
                historialReservaVM.accion = historial.accion;
                historialReservaVM.observaciones = historial.observaciones;
                historialReservaVM.tiposHabitaciones = historial.tiposHabitaciones;
                historialReservaVM.habitaciones = historial.habitaciones;
                historialReservasVMListado.Add(historialReservaVM);
            }
            return historialReservasVMListado;
        }

        [HttpPost]
        public JsonResult obtenerHabitaciones(int idTipoHabitacion)
        {
            HabitacionesService sv = new HabitacionesService(this.db);
            var habitaciones = sv.getHabitacionesComboDinamico(idTipoHabitacion);
            return Json(habitaciones);
        }

        [HttpPost]
        public JsonResult obtenerCondiciones(int idTipoHabitacion)
        {
            HabitacionesService sv = new HabitacionesService(this.db);
            var condiciones = sv.getCondicionesComboDinamico(idTipoHabitacion);
            return Json(condiciones);
        }

        public SelectList getComboTiposPagosEnHotel()
        {
            SelectList estados = new SelectList(new[]{
                new { ID = "1", Name = ComboHelper.GetNombreTipoPagoEnHotel(1) },
                new { ID = "2", Name = ComboHelper.GetNombreTipoPagoEnHotel(2) },
                new { ID = "3", Name = ComboHelper.GetNombreTipoPagoEnHotel(3) }
            }, "ID", "Name", "");
            return estados;
        }

        public SelectList getComboTiposPagos()
        {
            SelectList estados = new SelectList(new[]{
                new { ID = "0", Name = ComboHelper.GetNombreTipoMediodePago(0) },
                new { ID = "1", Name = ComboHelper.GetNombreTipoMediodePago(1) },
                new { ID = "2", Name = ComboHelper.GetNombreTipoMediodePago(2) },
                new { ID = "3", Name = ComboHelper.GetNombreTipoMediodePago(3) },
                new { ID = "4", Name = ComboHelper.GetNombreTipoMediodePago(4) },
                new { ID = "5", Name = ComboHelper.GetNombreTipoMediodePago(5) },
                new { ID = "6", Name = ComboHelper.GetNombreTipoMediodePago(6) },
                new { ID = "7", Name = ComboHelper.GetNombreTipoMediodePago(7) }
            }, "ID", "Name", "");
            return estados;
        }

        public List<SelectListItem> getComboCondiciones(int idTipoHabitacion, int idCondicion = 0)
        {
            CondicionesReservaService condicionesReservaSV = new CondicionesReservaService(this.db);
            List<CondReserva> condicionesReservas = condicionesReservaSV.getCondicionesReservaPorTipoHabitacion(idTipoHabitacion);
            CondReserva condicion = null;
            List<SelectListItem>  condicionesReservaListado = new List<SelectListItem>();
            string condicionId, condicionNombre;
            bool selected = false;
            for (int x = -1; x < condicionesReservas.Count(); x++)
            {
                if (x >= 0)
                {
                    condicion = condicionesReservas.ElementAt(x);
                    condicionId = condicion.idCondReserva.ToString();
                    condicionNombre = condicion.nombreEs;                    
                    selected = condicion.idCondReserva == idCondicion ? true : false;
                }
                else
                {
                    condicionId = x.ToString();
                    condicionNombre = "Sin Condiciones";
                    selected = true;
                }
                condicionesReservaListado.Add(new SelectListItem { Value = condicionId, Text = condicionNombre, Selected = selected });
            }
            return condicionesReservaListado;
        }

        [HttpPost]
        public JsonResult obtenerPrecioHabitacion(int idReserva, int idTipoHabitacion, int idHabitacion, int idCondicion, string fechaDesde, string fechaHasta)
        {
            AlojamientosService alojamientosService = new AlojamientosService(this.db);
            HabitacionesService habitacionesService = new HabitacionesService(this.db);
            ReservasService reservasService = new ReservasService(this.db);
            TarifasBaseService tarifasBaseService = new TarifasBaseService(this.db);
            TarifasPorDiaService tarifasPorDiaService = new TarifasPorDiaService(this.db);
            DateTime fechaDesdeDT = DateTime.Parse(fechaDesde);
            DateTime fechaHastaDT = DateTime.Parse(fechaHasta);
            DateTime dia;
            HabitacionesPorReserva habitacionReserva;
            decimal precio = 0;
            decimal precioConImpuesto = 0;
            decimal precioBaseDia = 0;

            ItemsCuentaPorReservaService itemsCuentaSV = new ItemsCuentaPorReservaService(this.db);
            decimal saldo = 0;
            foreach (ItemsCuentaPorReserva itemCuenta in itemsCuentaSV.getItemsCuenta(idReserva))
            {
                if (itemCuenta.concepto == "CH")
                {
                    saldo = saldo + itemCuenta.monto;
                }
                else
                {
                    saldo = saldo - itemCuenta.monto;
                }
            }


            Alojamiento alojamiento = alojamientosService.getByIdAlojamiento(getSesion().idAlojamientoActual);
            Reserva reserva = reservasService.getReserva(idReserva);
            int idCanal = reserva.idCanal.HasValue ? reserva.idCanal.Value : Convert.ToInt32(ConfigurationManager.AppSettings["idCanalMostrador"]);
            precio = saldo;
            precioConImpuesto = sumarImpuestoAlPrecio(precio, alojamiento.impuesto);

            for (int i = 0; i < (fechaHastaDT - fechaDesdeDT).TotalDays; i++)
            {                
                dia = fechaDesdeDT.AddDays(i);
                if (idReserva > 0)
                    habitacionReserva = habitacionesService.getHabitacionesPorReservaPorHabitacionPorFecha(idReserva, idHabitacion, idCondicion, dia);
                else
                    habitacionReserva = null;

                if (habitacionReserva != null)
                {
                    precio = precio + habitacionReserva.precioHabitacion;
                    precioConImpuesto = precioConImpuesto + sumarImpuestoAlPrecio(habitacionReserva.precioHabitacion, habitacionReserva.impuesto);
                }
                else {

                    //TarifasBase tarifaBase = tarifasBaseService.get(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalMostrador"]), idCondicion);
                    //TarifasPorDia tarifasPorDia = tarifasPorDiaService.get(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalMostrador"]), idCondicion, dia);
                    TarifasBase tarifaBase = tarifasBaseService.get(idTipoHabitacion, idCanal, idCondicion);
                    TarifasPorDia tarifasPorDia = tarifasPorDiaService.get(idTipoHabitacion, idCanal, idCondicion, dia);

                    if (tarifasPorDia != null && tarifasPorDia.tarifaBase > 0)
                    {
                        precio = precio + tarifasPorDia.tarifaBase;
                        precioConImpuesto = precioConImpuesto + sumarImpuestoAlPrecio(tarifasPorDia.tarifaBase, alojamiento.impuesto);
                    }
                    else if (tarifaBase != null)
                    {
                        precioBaseDia = reservasService.getPrecioBasePorDia((int)dia.DayOfWeek, tarifaBase);
                        if (precioBaseDia <= 0)
                        {
                            tarifaBase = tarifasBaseService.get(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalMostrador"]), null);
                            precioBaseDia = reservasService.getPrecioBasePorDia((int)dia.DayOfWeek, tarifaBase);
                            precio = precio + precioBaseDia;
                            precioConImpuesto = precioConImpuesto + sumarImpuestoAlPrecio(precioBaseDia, alojamiento.impuesto);
                        }
                        else {
                            precio = precio + precioBaseDia;
                            precioConImpuesto = precioConImpuesto + sumarImpuestoAlPrecio(precioBaseDia, alojamiento.impuesto);
                        }
                    }

                }
            }
            return Json(Convert.ToString(precio, new CultureInfo("en-US")) + "|" + Convert.ToString(precioConImpuesto, new CultureInfo("en-US")));
        }     
        
        public decimal sumarImpuestoAlPrecio(decimal precio, decimal? impuesto)
        {
            return (precio * ((impuesto.HasValue) ? impuesto.Value : 0) / 100) + precio;
        }

        public JsonResult verificarReservaID(int id)
        {
            int idReserva = 0;
            try
            {
                ReservasService svReserva = new ReservasService(this.db);
                if (svReserva.getReserva(id) != null)
                {
                    idReserva = id;
                }
            }
            catch (Exception e)
            {               
            }
            return Json(new { idReserva = idReserva });
        }

        public JsonResult obtenerFechaHora()
        {
            string fechaHora = "";
            try
            {                
                fechaHora = DateHelper.obtenerFechaActual().ToString("dd/MM/yyyy HH:mm");
            }
            catch (Exception e)
            {
                fechaHora = "error";
            }
            return Json(new { fechaHora = fechaHora });
        }

        public ActionResult Buscar()
        {
            HojaBuscarViewModel vm = new HojaBuscarViewModel();
            TiposHabitacionService sv = new TiposHabitacionService(this.db);
            vm.comboTipoHabitaciones = sv.getComboTiposHabitacionParaBusqueda();
            vm.comboEstado = ComboHelper.getComboEstados();

            HabitacionesService svA = new HabitacionesService(this.db);
            vm.comboHabitaciones = svA.getComboHabitacionesTodasBusqueda();
            vm.fechaEntrada = DateTime.Now.AddMonths(-1);
            vm.fechaSalida = DateTime.Now.AddMonths(2);

            //vm.listadoReserva = db.Reservas.ToList();

            return View(vm);
        }

        [HttpPost]
        public ActionResult Buscar(HojaBuscarViewModel vm)
        {
            int idAlojamiento = BaseService.getIdAlojamiento();
            vm.listadoReserva = db.Reservas.ToList();
            TiposHabitacionService sv = new TiposHabitacionService(this.db);
            
            
            vm.comboTipoHabitaciones = sv.getComboTiposHabitacionParaBusqueda();
            vm.comboEstado = ComboHelper.getComboEstados();

            int? idRsv = null;
            int? idHab = null;
            int? idTipoHab = null;

            if (vm.idReserva != 0)
            {
                idRsv = vm.idReserva;
            }

            if (vm.idHabitacion !=0)
            {
                idHab = vm.idHabitacion;
            }

            if (vm.idTipoHabitacion !=0)
            {
                idTipoHab = vm.idTipoHabitacion;
            }
            var list = (from x in db.Reservas.Where(s =>s.idAlojamiento==idAlojamiento &&s.idReserva == (idRsv ?? s.idReserva))
                       from z in x.HabitacionesPorReservas.Where(h => h.idHabitacion == (idHab ?? h.idHabitacion) && h.Habitacione.idTipoHabitacion == (idTipoHab??h.Habitacione.idTipoHabitacion))
                       select x).Distinct();
            if (vm.txtHuesped!=null)
            {
                list = list.Where(r => r.Huespede.nombre.Contains(vm.txtHuesped));
            }

            if (vm.tarjeta!=null)
            {
                list = list.Where(r => r.nroTarjeta.Contains(vm.tarjeta));
            }

            if (vm.correo!=null)
            {
                list = list.Where(r => r.Huespede.email.Contains(vm.correo));
            }

            if (vm.comentarios!=null)
            {
                list = list.Where(r => r.comentarios.Contains(vm.comentarios));
            }

            if (vm.documento!=null)
            {
                list = list.Where(r => r.Huespede.nroDocumento.Contains(vm.documento));
            }

            if (vm.idEstado!=0)
            {
                list = list.Where(r => r.idEstado == vm.idEstado);
            }

            HabitacionesService svA = new HabitacionesService(this.db);
            vm.comboHabitaciones = svA.getComboHabitacionesTodasBusqueda();
            vm.listadoReserva = list.ToList();

            return View(vm);
        }
    }
}