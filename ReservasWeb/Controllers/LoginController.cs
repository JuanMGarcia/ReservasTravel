﻿using ReservasWeb.Services;
using ReservasWeb.Utils;
using ReservasWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ReservasWeb.Controllers
{
    public class LoginController : BaseController
    {
        // GET: Login
        public ActionResult Index()
        {
            //puto
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult IniciarSesion(LoginViewModel vm)
        {
            if (ModelState.IsValid)
            {
                string claveFinal = MD5Hash(vm.clave);

                LoginService svLogin = new LoginService(this.db);
                var user = svLogin.autenticarUsuario(vm.usuario, claveFinal);

                if (user != null)
                {
                    //var sesion = System.Web.HttpContext.Current.Session["SesionUsuario"] as Sesion;
                    //if (null == sesion)
                    //{
                        Sesion sesion = new Sesion();
                        sesion.authId = Guid.NewGuid().ToString();
                        sesion.idUsuario = user.idUsuario;
                        sesion.idRol = user.idRol;
                        sesion.nombre = user.nombre;
                        sesion.apellido = user.apellido;
                        sesion.usuario = user.usuario1;
                        sesion.email = user.email;
                    //sesion.listadoHoteles = new List<AlojamientoSesion>();
                    ReservasWeb.Models.Alojamiento alojamiento = user.Clientes.FirstOrDefault().Alojamientos.FirstOrDefault();
                    sesion.idAlojamientoActual = alojamiento.idAlojamiento;
                    sesion.idCliente = alojamiento.idCliente;
                    sesion.nombreAlojamiento = alojamiento.nombreEs;

                    //foreach (var item in user.Role.AlojamientosPorRols)
                    //    {
                    //        //AlojamientoSesion aloj = new AlojamientoSesion();
                    //        //aloj.idAlojamiento = item.Alojamiento.idAlojamiento;
                    //        //aloj.nombreAlojamiento = item.Alojamiento.nombreEs;
                    //        //TODO:si hay 1 solo hotel mostrar catel sino asignar idAlojamientoActual a la sesion
                    //        sesion.idAlojamientoActual = item.idAlojamiento;

                    //    //sesion.listadoHoteles.Add(aloj);
                    //}                        

                    System.Web.HttpContext.Current.Session["SesionUsuario"] = sesion;
                        //}
                        FormsAuthentication.SetAuthCookie(user.usuario1, false);
                        return Redirect("/reservas");
                }
                else
                {
                    ModelState.AddModelError("", "Usuario o contraseña inválida");
                }
            }
            return Redirect("/login");
        }

        public ActionResult LogOff()
        {
            try
            {
                var sesion = System.Web.HttpContext.Current.Session["SesionUsuario"] as Sesion;
                if (null != sesion)
                {                    
                    FormsAuthentication.SignOut();
                    System.Web.HttpContext.Current.Session["SesionUsuario"] = null;
                    System.Web.HttpContext.Current.Session.Abandon();
                }
            }
            catch (Exception)
            {
            }
            return Redirect("/");
        }
        public string MD5Hash(string input)
        {
            byte[] data = new MD5CryptoServiceProvider().ComputeHash(Encoding.Unicode.GetBytes(input));
            //byte[] data = MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(input));  
            var sb = new StringBuilder();
            for (var i = 0; i < data.Length; i++)
                sb.Append(data[i].ToString("x2"));
            return sb.ToString();
        }

        [AllowAnonymous]
        public ActionResult RecuperarClave()
        {
            //ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        public JsonResult RecuperarClaveJson(string email)
        {
            try
            {
                ClientesService clientesService = new ClientesService(this.db);
                UsuariosService usuariosService = new UsuariosService(this.db);
                ReservasWeb.Models.Cliente cliente = clientesService.get(email);
                ReservasWeb.Models.Usuario usuario = cliente.idUsuario.HasValue ? usuariosService.get(cliente.idUsuario.Value) : new Models.Usuario();
                if (cliente != null)
                {
                    string claveNueva = System.Web.Security.Membership.GeneratePassword(10, 4);
                    string claveNuevaMD5 = MD5Hash(claveNueva);
                    string fromMail = ConfigurationManager.AppSettings["fromMail"];
                    string subject = System.Net.WebUtility.HtmlDecode("Clave Recuperada desde www.reservas.travel de " + cliente.nombre);
                    string cuerpo = "Clave Recuperada enviada desde www.reservas.travel" + (char)10 + (char)10 +
                                    "Usuario: " + usuario.nombre + (char)10 +
                                    "Clave: " + claveNueva + (char)10;
                    cuerpo = System.Net.WebUtility.HtmlDecode(cuerpo);
                    enviarEmail(fromMail, fromMail, cliente.email, subject, cuerpo, false);
                    usuario.clave = claveNuevaMD5;
                    usuariosService.editar(usuario);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json(new { Resultado = "NO" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Resultado = "OK" }, JsonRequestBehavior.AllowGet);

        }
    }
}