﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReservasWeb.Authorize;

namespace ReservasWeb.Controllers
{
    public class TutorialController : Controller
    {
        [RequiereLogin]
        // GET: Tutorial
        public ActionResult Index()
        {
            return View();
        }

        [RequiereLogin]
        public ActionResult CircuitoCondiciones()
        {
            return View();
        }

    }
}