﻿using ReservasWeb.Models;
using ReservasWeb.Services;
using ReservasWeb.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.Controllers
{
    public class BaseController : Controller
    {
        public ReservasWebEntities db = new ReservasWebEntities();
        //idAccion 0 Listar 1 Alta 2 Baja 3 Modificacion
        public void chequearPermisos(int idModulo,int idAccion)
        {
            bool habilitado = true;

            var sesion = System.Web.HttpContext.Current.Session["SesionUsuario"] as Utils.Sesion;
            PermisosService svP = new PermisosService(this.db);
            switch (idAccion)
            {
                case 0:
                    habilitado = svP.listarxRol(sesion.idRol, idModulo); 
                    break;
                case 1:
                    habilitado = svP.altaxRol(sesion.idRol, idModulo);
                    break;
                case 2:
                    habilitado = svP.bajaxRol(sesion.idRol, idModulo);
                    break;
                case 3:
                    habilitado = svP.modificarxRol(sesion.idRol, idModulo);
                    break;
            }

            if (!habilitado)
            {
                Redirect("/");
            }
        }

        [HttpPost]
        public JsonResult obtenerProvincias(int idPais)
        {
            CiudadesService sv = new CiudadesService(this.db);
            var provs = sv.getProvinciasCombo(idPais);
            return Json(provs);
        }

        [HttpPost]
        public JsonResult obtenerCiudades(int idProvincia)
        {
            CiudadesService sv = new CiudadesService(this.db);
            var ciuds = sv.getCiudadesCombo(idProvincia);
            return Json(ciuds);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected Sesion getSesion()
        {
            return (ReservasWeb.Utils.Sesion)System.Web.HttpContext.Current.Session["SesionUsuario"];
        }

        public void enviarEmail(string fromMail, string replyToMail, string toMail, string subject, string body, bool isHtml)
        {

            body = System.Net.WebUtility.HtmlDecode(body);
            subject = System.Net.WebUtility.HtmlDecode(subject);

            MailMessage mensaje = new MailMessage();
            mensaje.From = new MailAddress(fromMail);

            try
            {
                mensaje.ReplyToList.Add(replyToMail);
            }
            catch (Exception)
            {
                mensaje.ReplyToList.Add(fromMail);
            }

            mensaje.To.Add(toMail);
            mensaje.Subject = subject;
            if (isHtml)
            {
                mensaje.Body = body;
            }
            else
            {
                AlternateView textoPlano = AlternateView.CreateAlternateViewFromString(body, System.Text.Encoding.Default, "text/plain");
                textoPlano.TransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;
                mensaje.AlternateViews.Add(textoPlano);
            }

            mensaje.IsBodyHtml = isHtml;
            mensaje.Priority = MailPriority.High;
            mensaje.BodyEncoding = System.Text.Encoding.Default;
            mensaje.SubjectEncoding = System.Text.Encoding.UTF8;

            SmtpClient smtp = new SmtpClient();
            smtp.Host = ConfigurationManager.AppSettings["urlSmtp"];
            smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["puertoSmtp"]);
            smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["sslSmtp"]);

            string passMail = ConfigurationManager.AppSettings["passMail"];
            //System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(fromMail, passMail);
            //smtp.UseDefaultCredentials = false;
            //smtp.Credentials = basicAuthenticationInfo;
            string usrSmtp = ConfigurationManager.AppSettings["userSmtp"];
            string passSmtp = ConfigurationManager.AppSettings["passSmtp"];
            smtp.Credentials = new System.Net.NetworkCredential(usrSmtp, passSmtp);

            smtp.Send(mensaje);
        }

        public void SetCulture(string culture)
        {
            if (culture != "")
            {
                // Validate input
                culture = CultureHelper.GetImplementedCulture(culture);

                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(culture);
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            }
        }
    }
}