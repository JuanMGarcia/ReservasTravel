﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReservasWeb.Models;
using ReservasWeb.ViewModels;
using System.Threading;
using ReservasWeb.Services;
using System.Globalization;
using ReservasWeb.Utils;
using ReservasWeb.Authorize;


namespace ReservasWeb.Controllers
{
    [RequiereLogin]
    public class CondicionesReservaController : BaseController
    {
        // GET: PoliticasCancelacion
        public ActionResult Index(string resultado)
        {
            CondicionesReservaService sv = new CondicionesReservaService(this.db);//servicio donde llama a la lista y instancio la base de datos

            CondicionesReservaViewModel vm;//instancio el view model en vacio
            List<CondicionesReservaViewModel> listado = new List<CondicionesReservaViewModel>();//instancio la lista
            foreach (CondReserva conReserva in sv.getCondicionesReservaListadoAbm())//cada elemento de la lista se guarda en una variable instanciada en poli vacia
            {
                vm = new CondicionesReservaViewModel();//en una variable vacia se instancia el view model
                vm.nombreEn = conReserva.nombreEn;
                vm.nombreEs = conReserva.nombreEs;
                vm.nombrePt = conReserva.nombrePt;
                vm.descripcionEn = conReserva.descripcionEn;
                vm.descripcionEs = conReserva.descripcionEs;
                vm.descripcionPt = conReserva.descripcionPt;
                vm.FechaCreacion = conReserva.fechaCreacion.Value;
                vm.idCondReserva = conReserva.idCondReserva;
                //vm.FechaModificacion = poli.fechaModificacion;
                listado.Add(vm);//agrega el listado al viewmodel
            }
            ListadoCondicionesReservaViewModel ListadoCondicionesReservaViewModel = new ListadoCondicionesReservaViewModel();
            ListadoCondicionesReservaViewModel.listado = listado;//se le asigna el listado creado al listado del vm
            ListadoCondicionesReservaViewModel.ProcesarResultado(resultado);
            return View(ListadoCondicionesReservaViewModel);
        }
        public ActionResult Crear()
        {
            //chequearPermisos(1, 12);
            CondicionesReservaService CondicionesReservaService = new CondicionesReservaService(this.db);
            CondicionesReservaViewModel CondicionesReservaViewModel = new CondicionesReservaViewModel();
            CondicionesReservaViewModel.listaFormaPagos = CondicionesReservaService.getFormasdePago();
            CondicionesReservaViewModel.listadoPoliticas = CondicionesReservaService.getComboPoliticas();
            CondicionesReservaViewModel.listadoPagos = CondicionesReservaService.getPagos();
            return View(CondicionesReservaViewModel);
        }

        [HttpPost]
        public ActionResult Crear(CondicionesReservaViewModel CondicionesReservaViewModel)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    CondicionesReservaService condicionreservaservice = new CondicionesReservaService(this.db);
                    CondReserva condreserva = new CondReserva();
                    condreserva.nombreEn = CondicionesReservaViewModel.nombreEn;//controlador
                    condreserva.nombreEs = CondicionesReservaViewModel.nombreEs;
                    condreserva.nombrePt = CondicionesReservaViewModel.nombrePt;
                    condreserva.descripcionEn = CondicionesReservaViewModel.descripcionEn;
                    condreserva.descripcionEs = CondicionesReservaViewModel.descripcionEs;
                    condreserva.descripcionPt = CondicionesReservaViewModel.descripcionPt;
                    List<int> ids = new List<int>();
                    foreach (var item in CondicionesReservaViewModel.listaFormaPagos)
                    {
                        if (item.seleccionada)
                        {
                            ids.Add(item.idFormadePago);
                        }
                    }
                    ComodidadesService sv = new ComodidadesService(this.db);                    
                    condreserva.fechaCreacion = DateTime.Now;
                    condreserva.fechaModificacion = condreserva.fechaCreacion;
                    Sesion sesion = System.Web.HttpContext.Current.Session["SesionUsuario"] as Sesion;//trae la sesion actual
                    condreserva.idAlojamiento = sesion.idAlojamientoActual;
                    PoliticasCancelacionService politicaCancelacionService = new PoliticasCancelacionService(this.db);
                    PoliticasCancelacion politicacancelacion = politicaCancelacionService.getPoliticasCancelacion(CondicionesReservaViewModel.idPoliticaCancelacion);
                    condreserva.TiposPagoes = condicionreservaservice.getFormasdePagoSeleccionadas(ids);
                    condreserva.PoliticasCancelacions.Add(politicacancelacion);
                    int idCondicion = condicionreservaservice.Crear(condreserva);

                    TiposHabitacionService tipHabSv = new TiposHabitacionService(this.db);
                    List<TiposHabitacione> listadoTipoHab = new List<TiposHabitacione>();
                    listadoTipoHab = tipHabSv.getTiposHabitacionPorAlojamiento();

                    CanalesService canalesService = new CanalesService(this.db);
                    List<Canale> canalesAlojamiento = canalesService.getCanalesDeAlojamiento();

                    foreach (var itemTipoHab in listadoTipoHab)
                    {
                        foreach (var itemCanal in canalesAlojamiento)
                        {                            
                            TarifasBase tarifaBase = new TarifasBase();
                            tarifaBase.abierta = true;
                            tarifaBase.aceptaMenores = true;
                            tarifaBase.adicionalAdulto = 0;
                            tarifaBase.adicionalCuna = 0;
                            tarifaBase.adicionalMenor = 0;
                            tarifaBase.descuentoPax = 0;
                            tarifaBase.disponibilidadCanal = 1;
                            tarifaBase.estadiaMinima = 1;
                            tarifaBase.idAlojamiento = BaseService.getIdAlojamiento();
                            tarifaBase.idCanal = itemCanal.idCanal;
                            tarifaBase.idCondReserva = idCondicion;
                            tarifaBase.idTipoHabitacion = itemTipoHab.idTipoHabitacion;
                            tarifaBase.permiteEgreso = true;
                            tarifaBase.permiteIngreso = true;
                            tarifaBase.tarifaLunes = 0;
                            tarifaBase.tarifaMartes = 0;
                            tarifaBase.tarifaMiercoles = 0;
                            tarifaBase.tarifaJueves = 0;
                            tarifaBase.tarifaViernes = 0;
                            tarifaBase.tarifaSabado = 0;
                            tarifaBase.tarifaDomingo = 0;
                            db.TarifasBases.Add(tarifaBase);
                            db.SaveChanges();
                        }
                    }

                }
            }
            catch (Exception)
            {
                resultado = "error";
            }
            return RedirectToAction("index", new { resultado = resultado });
        }

        public ActionResult Editar(int id)
        {
            //chequearPermisos(2, 15);
            CondicionesReservaService sv = new CondicionesReservaService(this.db);
            CondReserva poli = sv.getCondicionReserva(id);//devuelve el id para editar el seleccionado y no otro

            CondicionesReservaViewModel vm = new CondicionesReservaViewModel();
            vm.nombreEn = poli.nombreEn;
            vm.nombreEs = poli.nombreEs;
            vm.nombrePt = poli.nombrePt;
            vm.descripcionEn = poli.descripcionEn;
            vm.descripcionEs = poli.descripcionEs;
            vm.descripcionPt = poli.descripcionPt;
            if(poli.PoliticasCancelacions != null && poli.PoliticasCancelacions.Count() > 0)
                vm.idPoliticaCancelacion = poli.PoliticasCancelacions.ElementAt(0).idPoliticaCancelacion;
            vm.IdAlojamiento = poli.idAlojamiento;
            vm.idCondReserva = poli.idCondReserva;

            vm.listaFormaPagos = sv.getFormasdePagoxCondicion(id);
            vm.listadoPoliticas = sv.getComboPoliticas();
            vm.listadoPagos = sv.getPagos();
            return View(vm);
        }
        [HttpPost]
        public ActionResult Editar(CondicionesReservaViewModel vm)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    CondicionesReservaService CondicionesReservaService = new CondicionesReservaService(this.db);
                    CondicionesReservaService.Editar(vm);
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }
            return RedirectToAction("index", new { resultado = resultado });
        }
        public ActionResult Eliminar(int id)
        {

            CondicionesReservaService sv = new CondicionesReservaService(this.db);

            sv.Eliminar(id);
            return RedirectToAction("Index");
        }
    }
}