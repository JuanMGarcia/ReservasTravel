﻿using ReservasWeb.Models;
using ReservasWeb.Services;
using ReservasWeb.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.Controllers
{
    public class GaleriasController : BaseController
    {
        //private int contador_imagen = 0;
        //private int id_imagen = 0;

        public class FilesUploadedList
        {
            public List<FileUploaded> Files { get; set; }
        }

        public class FileUploaded
        {
            public int id { get; set; }
            public String url { get; set; }
            public String thumbnailUrl { get; set; }
            public String name { get; set; }            
            public String type { get; set; }
            public int size { get; set; }
            public String deleteUrl { get; set; }
            public String deleteType { get; set; }
        }

        [HttpPost]
        public ActionResult SaveFile(HttpPostedFileBase file)
        {
            chequearPermisos(5, 3);
            //some file upload magic
            // return JSON
            return Json(new
            {
                name = "picture.jpg",
                type = "image/jpeg",
                size = 123456789
            });
        }

        /// <summary>
        /// POST: /UploadImage
        /// Saves an image to the server, resizing it appropriately.
        /// </summary>
        /// <returns>A JSON response.</returns>
        [HttpPost]
        public ActionResult UploadImageBase(int idElemento, string rutaRaiz)
        {
            chequearPermisos(5, 1);
            // Validate we have a file being posted
            if (Request.Files.Count == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var file = Request.Files[0];            
            int orden_imagen = 0;
            string filename = "";

            try
            {
                string path = "";
                Random rnd = new Random();
                int id_imagen = rnd.Next(1, 999999999);
                // File we want to resize and save.
                for (int i = 0; i < Request.Files.Count; i++)
                {

                    file = Request.Files[i];

                    switch (i)
                    {
                        case 0:
                            path = "/small/";
                            break;
                        case 2:
                            path = "/medium/";
                            break;
                        default:
                            path = "/";
                            break;
                    }
                    filename = UploadFile(idElemento, id_imagen, file, "~" + rutaRaiz + idElemento.ToString() + path);

                }

                GaleriasService serviGaleria = new GaleriasService(this.db);
                List<Imagene> fotosList = serviGaleria.getFotosAlojamiento();

                if (fotosList.Count > 0)
                    orden_imagen = (int)fotosList[0].orden + 1;
                else
                    orden_imagen = orden_imagen + 1;

                int idFoto = serviGaleria.GuardarImagen(filename, orden_imagen);

                var files = new List<FileUploaded>
                    {
                        new FileUploaded {id = idFoto, url = rutaRaiz + idElemento + "/", thumbnailUrl = rutaRaiz + idElemento + "/small/" + filename, name = filename, type = "image/jpeg", size = file.ContentLength, deleteUrl = "/Galerias/DeleteImage/", deleteType = "DELETE"}
                    };

                return Json(new
                {
                    files
                });

            }
            catch (Exception)
            {
                // Log using "NLog" NuGet package
                //Logger.ErrorException(ex.ToString(), ex);
                return Json(new
                {
                    statusCode = 500,
                    status = "Error uploading image.",
                    file = string.Empty
                }, "text/html");
            }
        }

        /// <summary>
        /// POST: /UploadImage
        /// Saves an image to the server, resizing it appropriately.
        /// </summary>
        /// <returns>A JSON response.</returns>
        [HttpPost]
        public ActionResult UploadImage()
        {
            chequearPermisos(5, 1);

            // Validate we have a file being posted
            if (Request.Files.Count == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);                
            }

            var file = Request.Files[0];            
            int idAlojamiento = int.Parse(Request.Form.Get("idAlojamiento").ToString());
            int orden_imagen = 0;
            string filename = "";
            
            try
            {
                string path = "";
                Random rnd = new Random();
                int id_imagen = rnd.Next(1, 999999999);
                // File we want to resize and save.
                for (int i = 0; i < Request.Files.Count; i++)
                {

                    file = Request.Files[i];

                    switch (i)
                    {
                        case 0:
                            path = "/small/";
                            break;
                        case 2:
                            path = "/medium/";
                            break;
                        default:
                            path = "/";
                            break;
                    }
                    filename = UploadFile(idAlojamiento, id_imagen, file, "~/Files/" + idAlojamiento.ToString() + path);

                }

                //filename = "2" + filename.Substring(1);
                //icononame = "1" + filename.Substring(1);

                //var fotos = from f in db.Fotos.Where(f => f.IdInmueble == idInmueble).OrderByDescending(f => f.orden).ThenBy(f => f.IdFoto)
                //            select f;
                //List<Foto> fotosList = fotos.ToList();

                GaleriasService serviGaleria = new GaleriasService(this.db);
                List<Imagene> fotosList = serviGaleria.getFotosAlojamiento();

                if (fotosList.Count > 0)
                    orden_imagen = (int)fotosList[0].orden + 1;
                else
                    orden_imagen = orden_imagen + 1;

                //Save image DB
                //Foto foto = new Foto();
                //foto.Foto1 = filename;
                //foto.IdInmueble = idInmueble;
                ////foto.Miniatura = icononame;
                //foto.Miniatura = "";
                //foto.orden = orden_imagen;
                //db.Fotos.Add(foto);
                //db.SaveChanges();
                int idFoto = serviGaleria.GuardarImagen(filename, orden_imagen);

                //SetFotoPrincipal(idInmueble);

                var files = new List<FileUploaded>
                    {
                        //new FileUploaded {id = foto.IdFoto, url = "/Files/" + idInmobiliaria + "/", thumbnailUrl = "/Files/" + idInmobiliaria + "/" + icononame, name = icononame, type = "image/jpeg", size = file.ContentLength, deleteUrl = "http://url.to/delete /file/", deleteType = "DELETE"}
                        //new FileUploaded {id = foto.IdFoto, url = "/Files/" + idInmobiliaria + "/", thumbnailUrl = "/Files/" + idInmobiliaria + "/small/" + filename, name = filename, type = "image/jpeg", size = file.ContentLength, deleteUrl = "http://url.to/delete /file/", deleteType = "DELETE"}
                        //new FileUploaded {id = foto.IdFoto, orden = foto.orden, url = "/Files/" + idInmobiliaria + "/", thumbnailUrl = "/Files/" + idInmobiliaria + "/small/" + filename, name = filename, type = "image/jpeg", size = file.ContentLength, deleteUrl = "/Admin/Inmueble/DeleteImage/", deleteType = "DELETE"}
                        new FileUploaded {id = idFoto, url = "/Files/" + idAlojamiento + "/", thumbnailUrl = "/Files/" + idAlojamiento + "/small/" + filename, name = filename, type = "image/jpeg", size = file.ContentLength, deleteUrl = "/Galerias/DeleteImage/", deleteType = "DELETE"}
                    };

                return Json(new
                {
                    files
                });

            }
            catch (Exception)
            {
                // Log using "NLog" NuGet package
                //Logger.ErrorException(ex.ToString(), ex);
                return Json(new
                {
                    statusCode = 500,
                    status = "Error uploading image.",
                    file = string.Empty
                }, "text/html");
            }
        }

        /// <summary>
        /// POST: /UploadImage
        /// Saves an image to the server, resizing it appropriately.
        /// </summary>
        /// <returns>A JSON response.</returns>
        [HttpPost]
        public ActionResult UploadImageTipoHabitacion()
        {
            chequearPermisos(5, 1);
            // Validate we have a file being posted
            if (Request.Files.Count == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var file = Request.Files[0];
            int idTipoHabitacion = int.Parse(Request.Form.Get("idTipoHabitacion").ToString());
            int orden_imagen = 0;
            string filename = "";

            try
            {
                string path = "";
                Random rnd = new Random();
                int id_imagen = rnd.Next(1, 999999999);
                // File we want to resize and save.
                for (int i = 0; i < Request.Files.Count; i++)
                {

                    file = Request.Files[i];

                    switch (i)
                    {
                        case 0:
                            path = "/small/";
                            break;
                        case 2:
                            path = "/medium/";
                            break;
                        default:
                            path = "/";
                            break;
                    }
                    filename = UploadFile(idTipoHabitacion, id_imagen, file, "~/Files/TiposHabitacion/" + idTipoHabitacion.ToString() + path);

                }

                GaleriasService serviGaleria = new GaleriasService(this.db);
                List<Imagene> fotosList = serviGaleria.getFotosTipoHabitacion(idTipoHabitacion);

                if (fotosList.Count > 0)
                    orden_imagen = (int)fotosList[0].orden + 1;
                else
                    orden_imagen = orden_imagen + 1;

                int idFoto = serviGaleria.GuardarImagenTipoHabitacion(idTipoHabitacion, filename, orden_imagen);

                var files = new List<FileUploaded>
                    {
                        new FileUploaded {id = idFoto, url = "/Files/TiposHabitacion/" + idTipoHabitacion + "/", thumbnailUrl = "/Files/TiposHabitacion/" + idTipoHabitacion + "/small/" + filename, name = filename, type = "image/jpeg", size = file.ContentLength, deleteUrl = "/Galerias/DeleteImage/", deleteType = "DELETE"}
                    };

                return Json(new
                {
                    files
                });

            }
            catch (Exception)
            {
                // Log using "NLog" NuGet package
                //Logger.ErrorException(ex.ToString(), ex);
                return Json(new
                {
                    statusCode = 500,
                    status = "Error uploading image.",
                    file = string.Empty
                }, "text/html");
            }
        }

        ///// <summary>
        ///// POST: /UploadImage
        ///// Saves an image to the server, resizing it appropriately.
        ///// </summary>
        ///// <returns>A JSON response.</returns>
        //[HttpPost]
        //public ActionResult UploadImageTipoHabitacion()
        //{
        //    return UploadImageBase(int.Parse(Request.Form.Get("idTipoHabitacion").ToString()), "/Files/TiposHabitacion/");

        //}

        /// <summary>
        /// POST: /RotateImage
        /// Saves an image to the server, resizing it appropriately.
        /// </summary>
        /// <returns>A JSON response.</returns>
        [HttpPost]
        public ActionResult RotateImage(int idAlojamiento, string fileName)
        {
            chequearPermisos(5, 3);
            // Validate we have a file being posted
            if (fileName == null && fileName == "")
                return Json(new { statusCode = 500, status = "No image provided." }, "text/html");

            //if (CorrespondeInmueble(idInmueble) == null)
            //    return Json(new { statusCode = 500, status = "Inmueble no autorizado." }, "text/html");

            try
            {
                //fileName = "1" + fileName.Substring(1);
                //RotateImage(fileName, 90);
                //fileName = "2" + fileName.Substring(1);
                //RotateImage(fileName, 90);
                //fileName = "3" + fileName.Substring(1);
                //RotateImage(fileName, 90);

                //int idInmobiliaria = (int)Session["idInmobiliaria"];
                RotateImageFile("~/Files/" + idAlojamiento.ToString() + "/", fileName, 90);
                RotateImageFile("~/Files/" + idAlojamiento.ToString() + "/small/", fileName, 90);
                RotateImageFile("~/Files/" + idAlojamiento.ToString() + "/medium/", fileName, 90);

                return Json(new
                {
                    statusCode = 200,
                    status = "Sccess rotating image.",
                    file = string.Empty
                }, "text/html");

            }
            catch (Exception)
            {
                return Json(new
                {
                    statusCode = 500,
                    status = "Error rotating image.",
                    file = string.Empty
                }, "text/html");
            }
        }

        /// <summary>
        /// POST: /RotateImage
        /// Saves an image to the server, resizing it appropriately.
        /// </summary>
        /// <returns>A JSON response.</returns>
        [HttpPost]
        public ActionResult RotateImageBase(int idElemento, string archivo, string rutaRaiz)
        {
            chequearPermisos(5, 3);
            // Validate we have a file being posted
            if (archivo == null && archivo == "")
                return Json(new { statusCode = 500, status = "No image provided." }, "text/html");

            
            try
            {
                RotateImageFile("~" + rutaRaiz + idElemento.ToString() + "/", archivo, 90);
                RotateImageFile("~" + rutaRaiz + idElemento.ToString() + "/small/", archivo, 90);
                RotateImageFile("~" + rutaRaiz + idElemento.ToString() + "/medium/", archivo, 90);

                return Json(new
                {
                    statusCode = 200,
                    status = "Sccess rotating image.",
                    file = string.Empty
                }, "text/html");

            }
            catch (Exception)
            {
                return Json(new
                {
                    statusCode = 500,
                    status = "Error rotating image.",
                    file = string.Empty
                }, "text/html");
            }
        }


        /// <summary>
        /// method to rotate an image either clockwise or counter-clockwise
        /// </summary>
        /// <param name="img">the image to be rotated</param>
        /// <param name="rotationAngle">the angle (in degrees).
        /// NOTE: 
        /// Positive values will rotate clockwise
        /// negative values will rotate counter-clockwise
        /// </param>
        /// <returns></returns>

        private Bitmap RotateImageBitmap(Image img, float rotationAngle)
        {
            //create an empty Bitmap image
            Bitmap bmp = new Bitmap(img.Height, img.Width);

            //turn the Bitmap into a Graphics object
            Graphics gfx = Graphics.FromImage(bmp);

            //now we set the rotation point to the center of our image
            gfx.TranslateTransform((float)bmp.Width / 2, (float)bmp.Height / 2);

            //now rotate the image
            gfx.RotateTransform(rotationAngle);

            gfx.TranslateTransform(-(float)bmp.Height / 2, -(float)bmp.Width / 2);

            //set the InterpolationMode to HighQualityBicubic so to ensure a high
            //quality image once it is transformed to the specified size
            gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //now draw our new image onto the graphics object
            gfx.DrawImage(img, new Point(0, 0));

            //dispose of our Graphics object
            gfx.Dispose();

            img.Dispose();

            return bmp;
        }

        ///// <summary>
        ///// method to rotate an image either clockwise or counter-clockwise
        ///// </summary>
        ///// <param name="img">the image to be rotated</param>
        ///// <param name="rotationAngle">the angle (in degrees).
        ///// NOTE: 
        ///// Positive values will rotate clockwise
        ///// negative values will rotate counter-clockwise
        ///// </param>
        ///// <returns></returns>
        //private void RotateImage(string fileName, float rotationAngle)
        //{
        //    //int idInmobiliaria = (int)Session["idInmobiliaria"];
        //    var dir = Server.MapPath("~/Files/" + idInmobiliaria.ToString() + "/");
        //    var path = Path.Combine(dir, fileName);
        //    Bitmap image = (Bitmap)Image.FromFile(path, true);
        //    image = RotateImage(image, rotationAngle);
        //    image.Save(path);
        //}

        private void RotateImageFile(string pathFile, string fileName, float rotationAngle)
        {
            //int idInmobiliaria = (int)Session["idInmobiliaria"];
            var dir = Server.MapPath(pathFile);
            var path = Path.Combine(dir, fileName);
            if (System.IO.File.Exists(path))
            {
                Bitmap image = (Bitmap)Image.FromFile(path, true);
                image = RotateImageBitmap(image, rotationAngle);
                image.Save(path);
            }

        }

        /// <summary>
        /// POST: /ReOrderImage
        /// Re orders an image to the server
        /// </summary>
        /// <returns>A JSON response.</returns>
        [HttpPost]
        public ActionResult ReOrderImage(int idInmueble, int oldIndex, int newIndex)
        {
            chequearPermisos(5, 3);


            //return Json(new { statusCode = 500, status = "No image provided." }, "text/html");
            // Validate we have a file being posted
            if (idInmueble <= 0)
                return Json(new { statusCode = 500, status = "No image provided." }, "text/html");

            //if (CorrespondeInmueble(idInmueble) == null)
            //    return Json(new { statusCode = 500, status = "Inmueble no autorizado." }, "text/html");

            try
            {
                GaleriasService serviGale = new GaleriasService(this.db);
                serviGale.ordenarImagenes(oldIndex, newIndex);

                //if (foto.orden == 0)
                //    SetIdFotoPrincipal(idInmueble, foto.IdFoto);
                //SetFotoPrincipal(idInmueble);

                return Json(new
                {
                    statusCode = 200,
                    status = "Sccess deleting image.",
                    file = string.Empty
                }, "text/html");

            }
            catch (Exception)
            {
                return Json(new
                {
                    statusCode = 500,
                    status = "Error deleting image.",
                    file = string.Empty
                }, "text/html");
            }
        }

        /// <summary>
        /// POST: /DeleteImage
        /// Deletes an image to the server
        /// </summary>
        /// <returns>A JSON response.</returns>
        [HttpPost]
        public ActionResult DeleteImage(int idAlojamiento, int id, string fileName)
        {
            chequearPermisos(5, 2);

            // Validate we have a file being posted
            if (fileName == null && fileName == "")
                return Json(new { statusCode = 500, status = "No image provided." }, "text/html");

            //if (CorrespondeInmueble(idInmueble) == null)
            //    return Json(new { statusCode = 500, status = "Inmueble no autorizado." }, "text/html");

            try
            {
                GaleriasService serviGale = new GaleriasService(this.db);
                serviGale.eliminarImagen(id);


                if (DeleteFoto(idAlojamiento, id, fileName, "/Files/"))
                    return Json(new
                    {
                        statusCode = 200,
                        status = "Success deleting image.",
                        file = string.Empty
                    }, "text/html");
                else
                    return Json(new
                    {
                        statusCode = 500,
                        status = "Error deleting image.",
                        file = string.Empty
                    }, "text/html");

            }
            catch (Exception)
            {
                return Json(new
                {
                    statusCode = 500,
                    status = "Error deleting image.",
                    file = string.Empty
                }, "text/html");
            }
        }

        /// <summary>
        /// POST: /DeleteImage
        /// Deletes an image to the server
        /// </summary>
        /// <returns>A JSON response.</returns>
        [HttpPost]
        public ActionResult DeleteImageTipoHabitacion(int idTipoHabitacion, int id, string fileName)
        {
            chequearPermisos(5, 2);

            // Validate we have a file being posted
            if (fileName == null && fileName == "")
                return Json(new { statusCode = 500, status = "No image provided." }, "text/html");

            //if (CorrespondeInmueble(idInmueble) == null)
            //    return Json(new { statusCode = 500, status = "Inmueble no autorizado." }, "text/html");

            try
            {
                GaleriasService serviGale = new GaleriasService(this.db);
                serviGale.eliminarImagen(id);

                if (DeleteFoto(idTipoHabitacion, id, fileName, "/Files/TiposHabitacion/"))
                    return Json(new
                    {
                        statusCode = 200,
                        status = "Success deleting image.",
                        file = string.Empty
                    }, "text/html");
                else
                    return Json(new
                    {
                        statusCode = 500,
                        status = "Error deleting image.",
                        file = string.Empty
                    }, "text/html");

            }
            catch (Exception)
            {
                return Json(new
                {
                    statusCode = 500,
                    status = "Error deleting image.",
                    file = string.Empty
                }, "text/html");
            }
        }

        //private Boolean DeleteFoto(int idAlojamiento, int id, string fileName)
        //{
        //    //int idInmobiliaria = (int)Session["idInmobiliaria"];

        //    var dir = Server.MapPath("~/Files/" + idAlojamiento + "/");
        //    var path = Path.Combine(dir, fileName);

        //    if (System.IO.File.Exists(path))
        //        System.IO.File.Delete(path);

        //    dir = Server.MapPath("~/Files/" + idAlojamiento + "/small/");
        //    path = Path.Combine(dir, fileName);
        //    if (System.IO.File.Exists(path))
        //        System.IO.File.Delete(path);

        //    dir = Server.MapPath("~/Files/" + idAlojamiento + "/medium/");
        //    path = Path.Combine(dir, fileName);
        //    if (System.IO.File.Exists(path))
        //        System.IO.File.Delete(path);

        //    return true;

        //}

        private Boolean DeleteFoto(int idElemento, int id, string fileName, string fileRoute)
        {
            chequearPermisos(5, 2);

            //int idInmobiliaria = (int)Session["idInmobiliaria"];

            var dir = Server.MapPath("~" + fileRoute + idElemento + "/");
            var path = Path.Combine(dir, fileName);

            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);

            dir = Server.MapPath("~" + fileRoute + idElemento + "/small/");
            path = Path.Combine(dir, fileName);
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);

            dir = Server.MapPath("~" + fileRoute + idElemento + "/medium/");
            path = Path.Combine(dir, fileName);
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);

            return true;

        }

        /// <summary>
        /// Persist the file to disk.
        /// </summary>
        private string UploadFile(int idInmueble, int idImagen, HttpPostedFileBase file, string path)
        {
            // Initialize variables we'll need for resizing and saving
            string newFileName = "";
            //newFileName = idInmueble.ToString() + "_" + idImagen.ToString() + "_" + StringHelper.removeWhitespaces(Path.GetFileNameWithoutExtension(file.FileName).Substring(0, Path.GetFileNameWithoutExtension(file.FileName).Length / 2)) + Path.GetExtension(file.FileName);
            newFileName = idInmueble.ToString() + "_" + idImagen.ToString() + Path.GetExtension(file.FileName);

            // Build absolute path
            var absPath = Server.MapPath(path);
            var absFileAndPath = Server.MapPath(path + "/" + newFileName);

            // Create directory as necessary and save image on server
            if (!Directory.Exists(absPath))
                Directory.CreateDirectory(absPath);
            file.SaveAs(absFileAndPath);

            return newFileName;
        }
    }
}