﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Net;
using System.Web;
using ReservasWeb.Services;
using ReservasWeb.Authorize;
using ReservasWeb.Utils;
using ReservasWeb.Models;
using ReservasWeb.ViewModels;
using System.Web.Mvc;
using System.Configuration;
using System.IO;
using System.Data.Entity;
using System.Globalization;

namespace ReservasWeb.Controllers
{
    [AllowAnonymous]

    public class BusquedaController : BaseController
    {
        // GET: Busqueda

        public ActionResult Buscar(string idHotel, string fechaIni, string fechaFin, string pasajeros, string habitaciones, string idioma)
        {
            System.Web.HttpContext.Current.Session.Remove("SesionInvitado" + idHotel);
            int cRestriccionDias = 90;
            bool limiteFecha = false;

            ResultadoBusquedaViewModel vm = new ResultadoBusquedaViewModel();
            AlojamientosService sv = new AlojamientosService(this.db);
            Alojamiento unAlojamiento = sv.getByIdAlojamiento(Int32.Parse(idHotel));


            vm.campoHabitacion = @Resources.Views.Busqueda.Buscar.Habitaciones;
            vm.colorBarra = unAlojamiento.colorBarra;
            vm.colorBoton = unAlojamiento.colorBotones;
            vm.colorTextoBoton = unAlojamiento.colorTextoBotones;
            string nombreHotel = "";
            string descripcionHotel = "";
            BusquedaService svb = new BusquedaService(this.db);

            switch (idioma)
            {
                case "es":
                    nombreHotel = unAlojamiento.nombreEs;
                    descripcionHotel = unAlojamiento.descripcionEs;
                    vm.cancelaciones = unAlojamiento.cancelacionesEs;
                    vm.mascotas = unAlojamiento.mascotasEs;
                    if (unAlojamiento.idTipoAlojamiento==4 || unAlojamiento.idAlojamiento == 2)
                    {
                        vm.campoHabitacion = unAlojamiento.habitacionEs;
                    }

                    break;

                case "pt":
                    nombreHotel = (unAlojamiento.nombrePt != null && unAlojamiento.nombrePt != "") ? unAlojamiento.nombrePt : unAlojamiento.nombreEs;
                    descripcionHotel = unAlojamiento.descripcionPt;
                    vm.cancelaciones = unAlojamiento.cancelacionesPt;
                    vm.mascotas = unAlojamiento.mascotasPt;
                    if (unAlojamiento.idTipoAlojamiento == 4 || unAlojamiento.idAlojamiento == 2)
                    {
                        vm.campoHabitacion = unAlojamiento.habitacionPt;
                    }
                    break;

                case "en":
                    nombreHotel = (unAlojamiento.nombreEn != null && unAlojamiento.nombreEn != "") ? unAlojamiento.nombreEn : unAlojamiento.nombreEs;
                    descripcionHotel = unAlojamiento.descripcionEn;
                    vm.cancelaciones = unAlojamiento.cancelacionesEn;
                    vm.mascotas = unAlojamiento.mascotasEn;
                    if (unAlojamiento.idTipoAlojamiento == 4 || unAlojamiento.idAlojamiento == 2)
                    {
                        vm.campoHabitacion = unAlojamiento.habitacionEn;
                    }
                    break;

                default:
                    nombreHotel = unAlojamiento.nombreEs;
                    descripcionHotel = unAlojamiento.descripcionEs;
                    break;

            }
            
            TimeSpan dNoches = DateTime.Parse(fechaFin) - DateTime.Parse(fechaIni);
            TimeSpan limitDias = DateTime.Parse(fechaFin) - DateTime.Now;

            if (limitDias.Days > cRestriccionDias) {
                limiteFecha = true;
            }
            if (dNoches.Days == 0)
            {
                limiteFecha = true;
            }

            if (DateTime.Parse(fechaIni) < DateTime.Now)
            {
                limiteFecha = true;
            }

            vm.nombreHotel = nombreHotel;
            vm.descripcionHotel = descripcionHotel;
            vm.emailHotel = unAlojamiento.emailReserva;
            vm.fechaIni = DateTime.Parse(fechaIni);
            vm.fechaFin = DateTime.Parse(fechaFin);
            vm.pasajeros = Int32.Parse(pasajeros);
            vm.cantPasajeros = Int32.Parse(pasajeros);
            vm.idioma = idioma;
            vm.habitaciones = Int32.Parse(habitaciones);
            vm.sitioWeb = unAlojamiento.website;
            vm.telefonoHotel = unAlojamiento.telefonos;
            vm.idAlojamiento = Int32.Parse(idHotel);
            vm.backurlHotel = (unAlojamiento.website.IndexOf("http") > -1 ? unAlojamiento.website : "http://" + unAlojamiento.website);
            vm.tieneLogo = unAlojamiento.urlLogo != null;
            if (vm.tieneLogo)
            {
                vm.urlLogoHotel = unAlojamiento.urlLogo;
            }
            else
            {
                vm.urlLogoHotel = "/Content/resources/img/reservaslogo141x20_gris.png";
            }
            vm.colorBarra = unAlojamiento.colorBarra;
            vm.colorBoton = unAlojamiento.colorBotones;
            vm.colorTextoBoton = unAlojamiento.colorTextoBotones;
            vm.direccionHotel = unAlojamiento.direccion;

            if (vm.colorBoton == null)
            {
                vm.colorBoton = "#dbbd8d";
            }

            if (vm.colorBarra == null)
            {
                vm.colorBarra = "#F0F0F0";
            }

            vm.urlLogo = unAlojamiento.urlLogo;
            vm.paisHotel = unAlojamiento.paisPiePagina;
            vm.ciudadHotel = unAlojamiento.ciudadPiePagina;

            vm.noches = dNoches.Days.ToString();
            vm.cantidadpersonas = pasajeros;
            vm.checkin = fechaIni;
            vm.checkout = fechaFin;

            if (unAlojamiento.GaleriaImagene != null)
            {
                vm.fotosHotel = unAlojamiento.GaleriaImagene.Imagenes.ToList();
            }

            vm.impuesto = (unAlojamiento.impuesto != null) ? unAlojamiento.impuesto.Value : 0;
            vm.horaCheckin = unAlojamiento.horaCheckin;
            vm.horaCheckout = unAlojamiento.horaCheckout;


            vm.listadoMedios = unAlojamiento.TiposPagoConfiguracions.Where(r => r.standBy == true).ToList();
            svb.Ejecutar(vm, limiteFecha);

            SetCulture(idioma);

            string urlBusqueda = "/busqueda/buscar?idioma={0}&idHotel={1}&fechaIni={2}&fechaFin={3}&habitaciones={4}&pasajeros={5}";

            vm.urlEspanol = String.Format(urlBusqueda, "es", idHotel, fechaIni, fechaFin, habitaciones, pasajeros);
            vm.urlIngles = String.Format(urlBusqueda, "en", idHotel, fechaIni, fechaFin, habitaciones, pasajeros);
            vm.urlPortugues = String.Format(urlBusqueda, "pt", idHotel, fechaIni, fechaFin, habitaciones, pasajeros);
            vm.idioma = idioma;
            

            var sesion = System.Web.HttpContext.Current.Session["SesionInvitado" + idHotel] as SesionInvitado;
            if (sesion != null)
            {
                sesion.impuesto = vm.impuesto;
                sesion.nombreHotel = vm.nombreHotel;
                sesion.idioma = idioma;
                sesion.listado = vm.listado;
                sesion.tieneLogo = vm.tieneLogo;
                sesion.noches = vm.noches.ToString();
                sesion.emailHotel = vm.emailHotel;
                sesion.reservaConfirmada = false;
                sesion.habitaciones = Int32.Parse(habitaciones);
            }
            

            vm.simboloMoneda = ComboHelper.GetSimboloMoneda(unAlojamiento.Moneda.idMoneda);
            vm.contadorSesion = "00:10:00";
            int minutos = Convert.ToInt32(ConfigurationManager.AppSettings["MinutosSesionBusqueda"]);
            vm.contadorSegundos = minutos *60;
            return View(vm);
        }

        [HttpPost]
        public ActionResult Buscar2(ResultadoBusquedaViewModel vm)
        {
            string idHotel = vm.idAlojamiento.ToString();
            var sesion = System.Web.HttpContext.Current.Session["SesionInvitado" + idHotel] as SesionInvitado;
            if (ModelState.IsValid && sesion != null && !sesion.reservaConfirmada)
            {
                sesion.listado = vm.listado;
                sesion.listadoElige = vm.listadoElige;
                sesion.listadoEligeAdulto = vm.listadoEligeAdulto;
                sesion.listadoEligeNino = vm.listadoEligeNino;
                sesion.listadoCondicionPorHab = vm.listadoCondicionesPorHab;
                //sesion.listadoNombCondicionPorHab = vm.listarsv;

                vm.urlLogo = sesion.urlLogo;
                vm.urlLogoHotel = sesion.urlLogo;
                vm.backurlHotel = sesion.backurl;
                vm.colorBarra = sesion.colorBarra;
                vm.colorBoton = sesion.colorBoton;
                vm.tieneLogo = sesion.tieneLogo;
                sesion.contadorSesion = vm.contadorSesion;
                sesion.contadorSegundos = vm.contadorSegundos;
                sesion.habitaciones = vm.habitaciones;
            }
            else
                Response.Redirect(vm.backurlHotel);

            return RedirectToAction("Prereserva", "Busqueda", new { hotel = idHotel });
        }

        [HttpPost]
        public ActionResult Buscar3(ResultadoBusquedaViewModel vm)
        {
            string idHotel = vm.idAlojamiento.ToString();
            var sesion = System.Web.HttpContext.Current.Session["SesionInvitado" + idHotel] as SesionInvitado;
            if (ModelState.IsValid && sesion != null && !sesion.reservaConfirmada)
            {
                vm.habitaciones = sesion.habitaciones;
                vm.contadorSegundos = vm.contadorSegundos2;
                vm.contadorSesion = vm.contadorSesion2;
                sesion.listadoElige = sesion.combinaElige;
                sesion.listado = sesion.combina;
                vm.urlLogo = sesion.urlLogo;
                vm.urlLogoHotel = sesion.urlLogo;
                vm.backurlHotel = sesion.backurl;
                vm.colorBarra = sesion.colorBarra;
                vm.colorBoton = sesion.colorBoton;
                vm.tieneLogo = sesion.tieneLogo;
                sesion.contadorSesion = vm.contadorSesion;
                sesion.contadorSegundos = vm.contadorSegundos;
                sesion.habitaciones = vm.habitaciones;
            }
            else
                Response.Redirect(sesion.backurl);

            return RedirectToAction("Prereserva", "Busqueda", new { hotel = idHotel });
        }

        public ActionResult Prereserva(string hotel)
        {
            
            string idHotel = hotel;
            var sesion = System.Web.HttpContext.Current.Session["SesionInvitado" + idHotel] as SesionInvitado;
            PrereservaViewModel vm = new PrereservaViewModel();
            AlojamientosService sva = new AlojamientosService(this.db);
            Alojamiento unAlojamiento;

            vm.contadorSesion = sesion.contadorSesion;
            vm.contadorSegundos = sesion.contadorSegundos;
            vm.listadoCondicionesPorHab = sesion.listadoCondicionPorHab;

            if (sesion != null && sesion.reservaConfirmada)
            {
                Response.Redirect(sesion.backurl);
                return null;
            }
            else if (sesion == null)
            {
                int idHotelInt = 0;
                Int32.TryParse(hotel, out idHotelInt);
                if (idHotelInt > 0)
                {
                    unAlojamiento = sva.getByIdAlojamiento(idHotelInt);
                    Response.Redirect((unAlojamiento.website.IndexOf("http") > 0 ? unAlojamiento.website : "http://" + unAlojamiento.website));
                }
                else
                    return null;
            }

            unAlojamiento = sva.getByIdAlojamiento(sesion.idAlojamiento);
            vm.tieneLogo = sesion.tieneLogo;
            if (unAlojamiento.GaleriaImagene != null)
            {
                vm.fotosHotel = unAlojamiento.GaleriaImagene.Imagenes.ToList();
            }
            vm.urlLogo = sesion.urlLogo;
            vm.urlLogoHotel = sesion.urlLogo;
            vm.backurlHotel = sesion.backurl;

            vm.nombreHotel = sesion.nombreHotel;
            vm.direccionHotel = sesion.direccionHotel;
            vm.telefonoHotel = sesion.telHotel;
            vm.fechaEntrada = sesion.fechaEntrada;
            vm.fechaSalida = sesion.fechaSalida;
            vm.cantPasajeros = sesion.pasajeros;
            vm.cantidadpersonas = sesion.pasajeros.ToString();
            vm.ninios = "0";
            vm.imgHotelmini = sesion.urlImagenMini;

            ReservasService sv = new ReservasService(this.db);

            vm.idAlojamiento = sesion.idAlojamiento;

            vm.checkin = sesion.fechaEntrada.ToString("dd/MM/yyyy");
            vm.checkout = sesion.fechaSalida.ToString("dd/MM/yyyy");
            vm.horaCheckin = unAlojamiento.horaCheckin;
            vm.horaCheckout = unAlojamiento.horaCheckout;
            vm.noches = vm.noches;
            vm.backurlHotel = "";

            vm.listaTipo = sv.getComboTipo();
            vm.listaTipoPago = sv.getComboTiposPagoPorHotel(sesion.idAlojamiento);
            vm.listaTipoDocumento = sv.getComboTipoDocumento();
            vm.listaPais = sv.getComboPaises();

            int tb = vm.listaTipoPago.Where(s => s.Value == "5").Count();
            if (tb == 1)
            {
                CuentasBancaria unBank = db.CuentasBancarias.Where(b => b.idAlojamiento == sesion.idAlojamiento).FirstOrDefault();
                if (unBank != null)
                { 
                    vm.tbCBU = unBank.CBU;
                    vm.tbCUIT = unBank.CUIT;
                    vm.tbNombreBanco = unBank.nombreBanco;
                    vm.tbNumeroCuenta = unBank.numeroCuenta;
                    vm.tbSucursal = unBank.sucursal;
                    vm.tbTipoCuenta = unBank.tipoCuenta;
                    vm.tbTitular = unBank.titular;
                }
            }
            else
            {
                vm.tbCBU = ".";
                vm.tbCUIT = ".";
                vm.tbNombreBanco = ".";
                vm.tbNumeroCuenta = ".";
                vm.tbSucursal = ".";
                vm.tbTipoCuenta = ".";
                vm.tbTitular = ".";               
            }

            sesion.tbCBU = vm.tbCBU;
            sesion.tbCUIT = vm.tbCUIT;
            sesion.tbNombreBanco = vm.tbNombreBanco;
            sesion.tbNumeroCuenta = vm.tbNumeroCuenta;
            sesion.tbSucursal = vm.tbSucursal;
            sesion.tbTipoCuenta = vm.tbTipoCuenta;
            sesion.tbTitular = vm.tbTitular;
            
            vm.urlLogo = sesion.urlLogo;
            vm.backurlHotel = sesion.backurl;
            vm.colorBarra = unAlojamiento.colorBarra;
            vm.colorBoton = unAlojamiento.colorBotones;
            vm.colorTextoBoton = unAlojamiento.colorTextoBotones;

            vm.listaTarjetaCredito = sv.getComboTarjeta();

            vm.cvcTarjeta = "0";

            SetCulture(sesion.idioma);
            vm.idioma = sesion.idioma;

            vm.listadoElige = new List<int>();
            vm.listadoEligeAdulto = new List<int>();
            vm.listadoEligeNino = new List<int>();
            vm.listado = new List<TiposHabitacione>();
            vm.listadoTarifa = new List<decimal>();
            vm.listadoTarifaAdulto = new List<AdicionalItem>();
            vm.listadoTarifaNino = new List<AdicionalItem>();
            decimal total = 0;

            int i = -1;
            int contHab = 0;

            //MAURO
            //sesion.listadoTarifa.Reverse();

            foreach (int item in sesion.listadoElige)
            {
                i++;
                if (item != 0)
                {
                    contHab++;
                    vm.listadoTarifa.Add(item * sesion.listadoTarifa[i]);
                    vm.listadoElige.Add(item);
                    vm.listado.Add(sesion.listado[i]);
                    total = total +item * sesion.listadoTarifa[i];
                }
            }

            

          vm.habitaciones = contHab;

            if (sesion.listadoEligeAdulto != null)
            {
                vm.tieneAdulto = true;
                i = -1;

                //MAURO
                sesion.listadoTarifaAdulto.Reverse();

                foreach (int itemA in sesion.listadoEligeAdulto)
                {
                    i++;
                    AdicionalItem auxAd = new AdicionalItem();
                    auxAd.precio = itemA * sesion.listadoTarifaAdulto[i].precio;
                    vm.listadoTarifaAdulto.Add(auxAd);
                    vm.listadoEligeAdulto.Add(itemA);
                    total = +itemA * sesion.listadoTarifaAdulto[i].precio;
                }
            }
            else
            {
                vm.tieneAdulto = false;
            }

            if (sesion.listadoEligeNino != null)
            {
                vm.tieneNino = true;
                i = -1;
                foreach (int itemN in sesion.listadoEligeNino)
                {
                    i++;
                    AdicionalItem auxAd = new AdicionalItem();
                    auxAd.precio = itemN * sesion.listadoTarifaNino[i].precio;
                    vm.listadoTarifaNino.Add(auxAd);
                    vm.listadoEligeNino.Add(itemN);
                    total = +itemN * sesion.listadoTarifaNino[i].precio;
                }
            }
            else
            {
                vm.tieneNino = false;
            }

            vm.impuesto = total * (sesion.impuesto / 100);
            total = total + vm.impuesto;
            vm.total = total;

            //Datos NN a fin de crear un huesped temporal para la reserva
            vm.nombre = "Invitado";
            vm.apellido = "Internet";
            vm.tipoDocumento = "DNI";
            vm.nroDocumento = "1";
            vm.celular = "1";
            vm.idPais = 1;
            vm.email = "invitado@internet.com";
            vm.tipopago = 1;

            sesion.idReserva = sv.GuardarReserva(vm, true, 0);

            //    MediosdePagoService svm = new MediosdePagoService(this.db);
            ///     int id = svm.getIdMercadoPago(sesion.idAlojamiento, 1);
            //     string sURL = "s";
            //     if (id != -1)
            //     {
            //         sURL = svm.GetLinkMP(id, "Reserva " + vm.nombreHotel, 1, total, "referencia", sesion.idReserva);
            //     }
            //     vm.linkMP = sURL;

            //Blanqueo de Datos NN a fin de que no lo vea el cliente
            vm.nombre = "";
            vm.apellido = "";
            vm.tipoDocumento = "";
            vm.nroDocumento = "";
            vm.celular = "";
            vm.idPais = 1;
            vm.email = "";
            vm.tipopago = 0;
            vm.idAlojamiento = sesion.idAlojamiento;
            vm.noches = (sesion.fechaSalida - sesion.fechaEntrada).Days.ToString();
            vm.simboloMoneda = ComboHelper.GetSimboloMoneda(unAlojamiento.Moneda.idMoneda);

            
            vm.tipopago = -1;
            return View(vm);
        }

        [HttpPost]
        public ActionResult Prereserva(PrereservaViewModel vm)
        {
            vm.contadorSegundos = 100;
            vm.contadorSesion   = "10";
            vm.colorBoton = "FFFF";
            vm.colorTextoBoton = "black";
            
            if (ModelState.IsValid)
            {
                ReservasService sv = new ReservasService(this.db);
                AlojamientosService asv = new AlojamientosService(this.db);
                Alojamiento unAloja = asv.getByIdAlojamiento(vm.idAlojamiento);
                var sesion = System.Web.HttpContext.Current.Session["SesionInvitado"+vm.idAlojamiento.ToString()] as SesionInvitado;
                if (sesion != null && !sesion.reservaConfirmada && sv.GuardarReserva(vm, false, sesion.idHuesped) > 0)
                {

                    //datos para el form detalle 
                    vm.telefonoHotel = sesion.telHotel;
                    vm.imgHotelmini = sesion.urlImagenMini;
                    vm.direccionHotel = sesion.direccionHotel;
                    vm.imgHotelmini = "1";

                    vm.contadorSesion = sesion.contadorSesion;
                    vm.contadorSegundos = sesion.contadorSegundos;

                    vm.colorBarra = unAloja.colorBarra;
                    vm.colorBoton = unAloja.colorBotones;
                    vm.colorTextoBoton = unAloja.colorTextoBotones;


                    vm.nombreHotel = unAloja.nombreEs;
                    vm.fechaEntrada = sesion.fechaEntrada;
                    vm.fechaSalida = sesion.fechaSalida;
                    vm.cantPasajeros = sesion.pasajeros; // cantidad pasajeros adultos                
                    vm.ninios = ""; // cantidad de pasajeros menores
                    vm.Comodidades = "";

                    string fromMail = ConfigurationManager.AppSettings["fromMail"];
                    string toMail = vm.email;
                    string subject = "Reservas Travel ";
                    string listaHabs = "";
                    string listaTarifas = "";
                    int i = -1;
                    string simboloMoneda = ComboHelper.GetSimboloMoneda(unAloja.idMoneda);
                    decimal subtotal = 0;
                    decimal total = 0;
                    int count = 0;
                    foreach (int itemE in sesion.listadoElige)
                    {
                        i++;
                        if (itemE != 0)
                        {
                            decimal auxPrecio = sesion.listadoTarifa[i] * itemE;
                            listaHabs = listaHabs + " " + itemE.ToString() + "-" + sesion.listado[i].nombreEs + " " + sesion.listadoNombCondicionPorHab[i] + " <br>";
                            listaTarifas = listaTarifas + " " + simboloMoneda + " " + auxPrecio.ToString("N", new CultureInfo("en-US")) + " <br>";
                            total = total + auxPrecio;
                            count++;
                        }
                    }
                    if (count == 1)
                    {
                        listaTarifas = listaTarifas.Replace("<br>", "");
                        listaHabs = listaHabs.Replace("<br>", "");
                    }

                    decimal impuesto = 1;

                    if (unAloja.impuesto.HasValue)
                        impuesto = unAloja.impuesto.Value;
                    ReservasController reservaController = new ReservasController();
                    subtotal = total;
                    total = reservaController.sumarImpuestoAlPrecio(total, impuesto);

                    sesion.total = total;
                
                    if (vm.tipopago == 6)
                    {
                        return RedirectToAction("MercadoPago", "Busqueda", new { hotel = sesion.idAlojamiento });
                    }
                    else
                    {
                        BusquedaService svb = new BusquedaService(db);
                        if (vm.tipopago == 1)
                        {
                            int resultado = svb.GuardarGarantia(vm);
                        }
                        else
                        {
                            if (vm.tipopago == 5)
                            {

                            }
                        }
                        
                        string html = "";
                        if (vm.tipopago == 1)
                        {
                            html = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Content/plantillaEmailConfirmacion.html"));
                        }
                        else
                        {
                            if(vm.tipopago == 2)
                            {
                                html = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Content/plantillaEmailMercadoPago.html"));
                            }
                            else
                            {
                                html = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Content/plantillaEmailTransferencia.html"));
                            }
                            
                        }
                        string strLogo = "http://pms.reservas.travel/" + @"/Files/" + vm.idAlojamiento + @"/" + sesion.urlLogo;

                        html = html.Replace("##NUMRSV##", sesion.idReserva.ToString());
                        html = html.Replace("##APELLIDO##", vm.apellido);
                        html = html.Replace("##NOMBRE##", vm.nombre);
                        html = html.Replace("##NOMBREHOTEL##", vm.nombreHotel);
                        html = html.Replace("##FECHAENTRADA##", sesion.fechaEntrada.ToString("dd/MM/yyyy"));
                        html = html.Replace("##FECHASALIDA##", sesion.fechaSalida.ToString("dd/MM/yyyy"));
                        html = html.Replace("##NOCHES##", sesion.noches);
                        html = html.Replace("##PASAJEROS##", vm.cantPasajeros.ToString());
                        html = html.Replace("##URLLOGO##", strLogo);
                        html = html.Replace("##DIRECCIONHOTEL##", vm.direccionHotel);
                        html = html.Replace("##HABITACIONES##", listaHabs);
                        html = html.Replace("##MINIIMAGEN##", strLogo);
                        html = html.Replace("##TOTAL##", simboloMoneda + " " + total.ToString("N", new CultureInfo("en-US")));
                        html = html.Replace("##IMPUESTO##", simboloMoneda + " " + (total - subtotal).ToString("N", new CultureInfo("en-US")));
                        html = html.Replace("##TARIFAS##", listaTarifas);

                        if (vm.tipopago == 5)
                        {
                            vm.tbCBU = sesion.tbCBU;
                            vm.tbCUIT = sesion.tbCUIT;
                            vm.tbNombreBanco = sesion.tbNombreBanco;
                            vm.tbNumeroCuenta = sesion.tbNumeroCuenta;
                            vm.tbSucursal = sesion.tbSucursal;
                            vm.tbTipoCuenta = sesion.tbTipoCuenta;
                            vm.tbTitular = sesion.tbTitular;

                            html = html.Replace("##HORAS##", "72");
                            html = html.Replace("##CBU##", vm.tbCBU);
                            html = html.Replace("##NOMBREBANCO##", vm.tbNombreBanco);
                            html = html.Replace("##TIPOCUENTA##", vm.tbTipoCuenta);
                            html = html.Replace("##NUMEROCUENTA##", vm.tbNumeroCuenta);
                            html = html.Replace("##TBTITULAR##", vm.tbTitular);
                            html = html.Replace("##TBCUIT##", vm.tbCUIT);
                            html = html.Replace("##SUCURSAL##", vm.tbSucursal);
                            string strComprobante = "http://pms.reservas.travel/busqueda/comprobante?idReserva=" + sesion.idReserva.ToString();
                            html = html.Replace("##COMPROBANTE##", strComprobante);

                        }


                        if (vm.tipopago == 2)
                        {

                            MediosdePagoService svm = new MediosdePagoService(this.db);
                            int id = svm.getIdMercadoPago(sesion.idAlojamiento, 2);
                            sesion.idMP = id;
                            string sURL = "s";
                            if (id != -1)
                            {
                                sURL = svm.GetLinkMP(id, "Reserva " + vm.nombreHotel, 1, sesion.total, sesion.idReserva.ToString(), sesion.idReserva,false);
                            }
                            vm.linkMP = sURL;
                            sesion.linkMP = sURL;

                            html = html.Replace("##HORAS##", "72");
                            html = html.Replace("##CBU##", vm.tbCBU);
                            html = html.Replace("##LINKMP##", sURL);

                        }


                        if (unAloja.cancelacionesEs != null)
                            html = html.Replace("##POLITICAHOTEL##", unAloja.cancelacionesEs);
                        else
                            html = html.Replace("##POLITICAHOTEL##", " ");

                        enviarEmail(fromMail, sesion.emailHotel, toMail, subject, html, true);

                        ///email para el hotel
                        html = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Content/plantillaEmailParaHotel.html"));
                        html = html.Replace("##NUMRSV##", sesion.idReserva.ToString());
                        html = html.Replace("##APELLIDO##", vm.apellido);
                        html = html.Replace("##NOMBRE##", vm.nombre);
                        html = html.Replace("##NOMBREHOTEL##", vm.nombreHotel);
                        html = html.Replace("##FECHAENTRADA##", sesion.fechaEntrada.ToString("dd/MM/yyyy"));
                        html = html.Replace("##FECHASALIDA##", sesion.fechaSalida.ToString("dd/MM/yyyy"));
                        html = html.Replace("##NOCHES##", sesion.noches);
                        html = html.Replace("##PASAJEROS##", vm.cantPasajeros.ToString());
                        html = html.Replace("##URLLOGO##", strLogo);
                        html = html.Replace("##DIRECCIONHOTEL##", vm.direccionHotel);
                        html = html.Replace("##HABITACIONES##", listaHabs);
                        html = html.Replace("##MINIIMAGEN##", strLogo);
                        html = html.Replace("##TOTAL##", simboloMoneda + " " + total.ToString("N", new CultureInfo("en-US")));
                        html = html.Replace("##IMPUESTO##", simboloMoneda + " " + (total - subtotal).ToString("N", new CultureInfo("en-US")));
                        html = html.Replace("##TARIFAS##", listaTarifas);
                        html = html.Replace("##CELULAR##", vm.celular);
                        html = html.Replace("##EMAILCONTACTO##", vm.email);


                        enviarEmail(fromMail, toMail, sesion.emailHotel, subject, html, true);
                        string idHotel = vm.idAlojamiento.ToString();

                        if (vm.tipopago == 5)
                        {
                            return RedirectToAction("TransferenciaBancaria", "Busqueda", new { hotel = idHotel });
                        }
                        else
                        {
                            if (vm.tipopago == 2)
                            {
                                return RedirectToAction("MercadoPago", "Busqueda", new { hotel = sesion.idAlojamiento });
                            }
                            else
                            { 
                                return RedirectToAction("Confirmado", "Busqueda", new { hotel = idHotel });
                            }
                        }
                    }

                }
                else
                {
                        Response.Redirect(vm.backurlHotel);
                    return View();
         
                }

            }
            else
            {
                return View(); //Error
            }
        }


        [HttpPost]
        public ActionResult ValidarTarjeta(string nrotarjeta)
        {
            try
            {                   
               return Json(BusquedaService.IsValidNumber(nrotarjeta));
            }
            catch (Exception)
            {
                return Json(false);
            }
        }


        [HttpPost]
        public ActionResult MandarSesion()
        {
            try
            {
                return Json(true);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }


        public ActionResult Garantia()
        {
            var sesion = System.Web.HttpContext.Current.Session["SesionInvitado"] as SesionInvitado;
            

            GarantiaViewModel vm = new GarantiaViewModel();

            vm.urlLogo = sesion.urlLogo;
            vm.backurlHotel = sesion.backurl;
            vm.colorBarra = sesion.colorBarra;
            vm.colorBoton = sesion.colorBoton;

            ReservasService sv = new ReservasService(this.db);
            vm.listaTarjetaCredito = sv.getComboTarjeta();
            return View(vm);
        }

        [HttpPost]
        public ActionResult Garantia(GarantiaViewModel vm)
        {
            if (ModelState.IsValid)
            {
                BusquedaService sv = new BusquedaService(this.db);
                return RedirectToAction("Confirmado", "Busqueda");//success
            }
            else
            {
                return View(); // error
            }
        }

        public ActionResult Comprobante(string idReserva)
        {
            CargarComprobanteViewModel vm = new CargarComprobanteViewModel();
            Reserva Rsv = db.Reservas.Find(Int32.Parse(idReserva));

            int idHotel = Rsv.idAlojamiento;

            AlojamientosService sv = new AlojamientosService(this.db);
            Alojamiento unAlojamiento = sv.getByIdAlojamiento(idHotel);

            BusquedaService svb = new BusquedaService(this.db);
            
            vm.checkin = Rsv.fechaEntrada.ToString("dd/MM/yyyy");
            vm.checkout = Rsv.fechaSalida.ToString("dd/MM/yyyy");

            vm.colorBarra = unAlojamiento.colorBarra;
            vm.colorBoton = unAlojamiento.colorBotones;
            vm.colorTextoBoton = unAlojamiento.colorTextoBotones;
            string nombreHotel = unAlojamiento.nombreEs;
            string descripcionHotel = unAlojamiento.descripcionEs;
            vm.urlLogo = "corbelhotel.com.ar";
            vm.urlLogoHotel = unAlojamiento.urlLogo;
            vm.backurlHotel = unAlojamiento.website;
            vm.colorBarra = unAlojamiento.colorBarra;
            vm.colorBoton = unAlojamiento.colorBotones;
            vm.tieneLogo = true;
            vm.observaciones = ".";

            vm.idAlojamiento = idHotel;



            if (unAlojamiento.GaleriaImagene != null)
            {
                vm.fotosHotel = unAlojamiento.GaleriaImagene.Imagenes.ToList();
            }
            vm.urlLogo = unAlojamiento.urlLogo;
            vm.urlLogoHotel = unAlojamiento.urlLogo;
            vm.backurlHotel = unAlojamiento.website;

            vm.nombreHotel = unAlojamiento.nombreEs;
            vm.direccionHotel = unAlojamiento.direccion;
            vm.telefonoHotel = unAlojamiento.telefonos;
            vm.fechaEntrada = Rsv.fechaEntrada;
            vm.fechaSalida = Rsv.fechaSalida;
            vm.cantPasajeros = 0;
            vm.cantidadpersonas = "0";
            vm.ninios = "0";
            vm.imgHotelmini = unAlojamiento.urlLogo;

           

            vm.checkin = Rsv.fechaEntrada.ToString("dd/MM/yyyy");
            vm.checkout = Rsv.fechaSalida.ToString("dd/MM/yyyy");
            vm.horaCheckin = unAlojamiento.horaCheckin;
            vm.horaCheckout = unAlojamiento.horaCheckout;
            vm.noches = vm.noches;
            vm.backurlHotel = "";

          
            vm.colorBarra = unAlojamiento.colorBarra;
            vm.colorBoton = unAlojamiento.colorBotones;
            vm.colorTextoBoton = unAlojamiento.colorTextoBotones;

            string idioma = "es";
            SetCulture(idioma);
            vm.idioma = idioma;

            vm.listadoElige = new List<int>();
            vm.listadoEligeAdulto = new List<int>();
            vm.listadoEligeNino = new List<int>();
            vm.listado = new List<TiposHabitacione>();
            //            vm.listadoTarifaAdulto = sesion.listadoTarifaAdulto;
            //            vm.listadoTarifaNino = sesion.listadoTarifaNino;
            vm.listadoTarifa = new List<decimal>();
            vm.listadoTarifaAdulto = new List<AdicionalItem>();
            vm.listadoTarifaNino = new List<AdicionalItem>();

            svb.CargarDetalleReserva(vm, Int32.Parse(idReserva));
            vm.idReserva = Int32.Parse(idReserva);

            decimal total = 0;

            int i = -1;
            int contHab = 0;
            foreach (int item in vm.listadoElige)
            {
                i++;
                if (item != 0)
                {
                    contHab++;
            //        vm.listadoTarifa.Add(item * sesion.listadoTarifa[i]);
            //        vm.listadoElige.Add(item);
            //        vm.listado.Add(sesion.listado[i]);
                    total =  total + vm.listadoTarifa[i];
                }
            }

            vm.habitaciones = contHab;

            //if (sesion.listadoEligeAdulto != null)
            //{
            //    vm.tieneAdulto = true;
            //    i = -1;
            //    foreach (int itemA in sesion.listadoEligeAdulto)
            //    {
            //        i++;
            //        AdicionalItem auxAd = new AdicionalItem();
            //        auxAd.precio = itemA * sesion.listadoTarifaAdulto[i].precio;
            //        vm.listadoTarifaAdulto.Add(auxAd);
            //        vm.listadoEligeAdulto.Add(itemA);
            //        total = +itemA * sesion.listadoTarifaAdulto[i].precio;
            //    }
            //}
            //else
            //{
            //    vm.tieneAdulto = false;
            //}

            //if (sesion.listadoEligeNino != null)
            //{
            //    vm.tieneNino = true;
            //    i = -1;
            //    foreach (int itemN in sesion.listadoEligeNino)
            //    {
            //        i++;
            //        AdicionalItem auxAd = new AdicionalItem();
            //        auxAd.precio = itemN * sesion.listadoTarifaNino[i].precio;
            //        vm.listadoTarifaNino.Add(auxAd);
            //        vm.listadoEligeNino.Add(itemN);
            //        total = +itemN * sesion.listadoTarifaNino[i].precio;
            //    }
            //}
            //else
            //{
            //    vm.tieneNino = false;
            //}
            decimal imp = vm.impuesto;
            vm.fecha = DateTime.Now;
            vm.impuesto = (total * imp);
            vm.impuesto =  vm.impuesto / 100;
            total = total + vm.impuesto;
            vm.total = total;

            //Datos NN a fin de crear un huesped temporal para la reserva
   
            //sesion.idReserva = sv.GuardarReserva(vm, true, 0);

                //MediosdePagoService svm = new MediosdePagoService(this.db);
                // int id = svm.getIdMercadoPago(sesion.idAlojamiento, 1);
                // string sURL = "s";
                // if (id != -1)
                // {
                //     sURL = svm.GetLinkMP(id, "Reserva " + vm.nombreHotel, 1, total, "referencia", sesion.idReserva);
                // }
                // vm.linkMP = sURL;

            //Blanqueo de Datos NN a fin de que no lo vea el cliente
            vm.noches = (Rsv.fechaSalida - Rsv.fechaEntrada).Days.ToString();
            vm.simboloMoneda = ComboHelper.GetSimboloMoneda(unAlojamiento.Moneda.idMoneda);


            return View(vm);
        }


        [HttpPost]
        public ActionResult Comprobante(CargarComprobanteViewModel vm)
        {

            Reserva Rsv = db.Reservas.Find(vm.idReserva);

            int idHotel = Rsv.idAlojamiento;

            AlojamientosService sv = new AlojamientosService(this.db);
            Alojamiento unAlojamiento = sv.getByIdAlojamiento(idHotel);

            BusquedaService svb = new BusquedaService(this.db);

            vm.checkin = Rsv.fechaEntrada.ToString("dd/MM/yyyy");
            vm.checkout = Rsv.fechaSalida.ToString("dd/MM/yyyy");

            vm.colorBarra = unAlojamiento.colorBarra;
            vm.colorBoton = unAlojamiento.colorBotones;
            vm.colorTextoBoton = unAlojamiento.colorTextoBotones;
            string nombreHotel = unAlojamiento.nombreEs;
            string descripcionHotel = unAlojamiento.descripcionEs;
            vm.urlLogo = "corbelhotel.com.ar";
            vm.urlLogoHotel = unAlojamiento.urlLogo;
            vm.backurlHotel = unAlojamiento.website;
            vm.colorBarra = unAlojamiento.colorBarra;
            vm.colorBoton = unAlojamiento.colorBotones;
            vm.tieneLogo = true;
            vm.observaciones = ".";

            vm.idAlojamiento = idHotel;



            if (unAlojamiento.GaleriaImagene != null)
            {
                vm.fotosHotel = unAlojamiento.GaleriaImagene.Imagenes.ToList();
            }
            vm.urlLogo = unAlojamiento.urlLogo;
            vm.urlLogoHotel = unAlojamiento.urlLogo;
            vm.backurlHotel = unAlojamiento.website;

            vm.nombreHotel = unAlojamiento.nombreEs;
            vm.direccionHotel = unAlojamiento.direccion;
            vm.telefonoHotel = unAlojamiento.telefonos;
            vm.fechaEntrada = Rsv.fechaEntrada;
            vm.fechaSalida = Rsv.fechaSalida;
            vm.cantPasajeros = 0;
            vm.cantidadpersonas = "0";
            vm.ninios = "0";
            vm.imgHotelmini = unAlojamiento.urlLogo;
            
            vm.checkin = Rsv.fechaEntrada.ToString("dd/MM/yyyy");
            vm.checkout = Rsv.fechaSalida.ToString("dd/MM/yyyy");
            vm.horaCheckin = unAlojamiento.horaCheckin;
            vm.horaCheckout = unAlojamiento.horaCheckout;
            vm.noches = vm.noches;
            vm.backurlHotel = "";
            
            vm.colorBarra = unAlojamiento.colorBarra;
            vm.colorBoton = unAlojamiento.colorBotones;
            vm.colorTextoBoton = unAlojamiento.colorTextoBotones;

            string idioma = "es";
            SetCulture(idioma);
            vm.idioma = idioma;

            vm.listadoElige = new List<int>();
            vm.listadoEligeAdulto = new List<int>();
            vm.listadoEligeNino = new List<int>();
            vm.listado = new List<TiposHabitacione>();
            //            vm.listadoTarifaAdulto = sesion.listadoTarifaAdulto;
            //            vm.listadoTarifaNino = sesion.listadoTarifaNino;
            vm.listadoTarifa = new List<decimal>();
            vm.listadoTarifaAdulto = new List<AdicionalItem>();
            vm.listadoTarifaNino = new List<AdicionalItem>();

            svb.CargarDetalleReserva(vm, (vm.idReserva));
     
            decimal total = 0;

            int i = -1;
            int contHab = 0;
            foreach (int item in vm.listadoElige)
            {
                i++;
                if (item != 0)
                {
                    contHab++;
                    //        vm.listadoTarifa.Add(item * sesion.listadoTarifa[i]);
                    //        vm.listadoElige.Add(item);
                    //        vm.listado.Add(sesion.listado[i]);
                    total = total + vm.listadoTarifa[i];
                }
            }

            vm.habitaciones = contHab;

            //if (sesion.listadoEligeAdulto != null)
            //{
            //    vm.tieneAdulto = true;
            //    i = -1;
            //    foreach (int itemA in sesion.listadoEligeAdulto)
            //    {
            //        i++;
            //        AdicionalItem auxAd = new AdicionalItem();
            //        auxAd.precio = itemA * sesion.listadoTarifaAdulto[i].precio;
            //        vm.listadoTarifaAdulto.Add(auxAd);
            //        vm.listadoEligeAdulto.Add(itemA);
            //        total = +itemA * sesion.listadoTarifaAdulto[i].precio;
            //    }
            //}
            //else
            //{
            //    vm.tieneAdulto = false;
            //}

            //if (sesion.listadoEligeNino != null)
            //{
            //    vm.tieneNino = true;
            //    i = -1;
            //    foreach (int itemN in sesion.listadoEligeNino)
            //    {
            //        i++;
            //        AdicionalItem auxAd = new AdicionalItem();
            //        auxAd.precio = itemN * sesion.listadoTarifaNino[i].precio;
            //        vm.listadoTarifaNino.Add(auxAd);
            //        vm.listadoEligeNino.Add(itemN);
            //        total = +itemN * sesion.listadoTarifaNino[i].precio;
            //    }
            //}
            //else
            //{
            //    vm.tieneNino = false;
            //}
            decimal imp = vm.impuesto;
            vm.fecha = DateTime.Now;
            vm.impuesto = (total * imp);
            vm.impuesto = vm.impuesto / 100;
            total = total + vm.impuesto;
            vm.total = total;            

            //Blanqueo de Datos NN a fin de que no lo vea el cliente
            vm.noches = (Rsv.fechaSalida - Rsv.fechaEntrada).Days.ToString();
            vm.simboloMoneda = ComboHelper.GetSimboloMoneda(unAlojamiento.Moneda.idMoneda);

            vm.ProcesarResultado("Comprobante Cargado");
            BusquedaService sva = new BusquedaService(db);
            sva.CrearComprobante(vm);
            vm.MensajeExito = "Comprobante cargado exitosamente.";
            return View(vm);
        }

        public ActionResult Confirmado(string hotel)
        {
            string idHotel = hotel;
            var sesion = System.Web.HttpContext.Current.Session["SesionInvitado"+idHotel] as SesionInvitado;

            AlojamientosService asv = new AlojamientosService(this.db);
            ReservasService svReserva = new ReservasService(this.db);
            Alojamiento unAl;
            ConfirmadoViewModel vm = new ConfirmadoViewModel();

            try {

                unAl = asv.getByIdAlojamiento(sesion.idAlojamiento);
                int idReserva = sesion.idReserva;

                Reserva rsv = svReserva.getReservaAny(idReserva,sesion.idAlojamiento);

                if (rsv.HabitacionesPorReservas != null && rsv.HabitacionesPorReservas.Count() == 0)
                {
                    svReserva.eliminar(idReserva);
                    throw new Exception();
                }
                else
                {
                    rsv.idEstado = 2;
                    db.Entry(rsv).State = EntityState.Modified;
                    db.SaveChanges();

                    //vm.habitaciones = new List<Habitacione>();

                    //foreach (HabitacionesPorReserva itemHPR in rsv.HabitacionesPorReservas)
                    //{
                    //    Habitacione unHab = db.Habitaciones.Find(itemHPR.idHabitacion);
                    //    vm.habitaciones.Add(unHab);
                    //}

                    vm.listaHabPorRsv = rsv.HabitacionesPorReservas.ToList();

                    vm.nombreHotel = unAl.nombreEs;
                    vm.direccionHotel = unAl.direccion;
                    vm.telefonoHotel = sesion.telHotel;
                    vm.ciudadHotel = unAl.ciudadPiePagina;
                    vm.emailHotel = unAl.emailContacto;
                    vm.paisHotel = unAl.paisPiePagina;

                    vm.fechaEntrada = sesion.fechaEntrada;
                    vm.fechaSalida = sesion.fechaSalida;
                    vm.cantPasajeros = sesion.pasajeros;

                    vm.urlLogo = sesion.urlLogo;
                    vm.urlLogoHotel = sesion.urlLogo;
                    vm.backurlHotel = sesion.backurl;
                    vm.idAlojamiento = Int32.Parse(hotel);
                    vm.colorBarra = unAl.colorBarra;
                    vm.colorBoton = unAl.colorBotones;
                    vm.colorTextoBoton = unAl.colorTextoBotones;
                    vm.tieneLogo = true;

                    SetCulture(sesion.idioma);
                    vm.idioma = sesion.idioma;
                }
            }
            catch (Exception)
            {
                int idHotelInt;
                Int32.TryParse(idHotel, out idHotelInt);
                if (idHotelInt > 0)
                {
                    unAl = asv.getByIdAlojamiento(idHotelInt);
                    Response.Redirect(unAl.website);
                }
            }

            sesion.reservaConfirmada = true;

            return View(vm);
        }


        public ActionResult TransferenciaBancaria(string hotel)
        {
            string idHotel = hotel;
            var sesion = System.Web.HttpContext.Current.Session["SesionInvitado" + idHotel] as SesionInvitado;

            AlojamientosService asv = new AlojamientosService(this.db);
            ReservasService svReserva = new ReservasService(this.db);
            Alojamiento unAl;
            ConfirmadoViewModel vm = new ConfirmadoViewModel();

            try
            {

                unAl = asv.getByIdAlojamiento(sesion.idAlojamiento);
                int idReserva = sesion.idReserva;

                Reserva rsv = svReserva.getReservaAny(idReserva, sesion.idAlojamiento);

                if (rsv.HabitacionesPorReservas != null && rsv.HabitacionesPorReservas.Count() == 0)
                {
                    svReserva.eliminar(idReserva);
                    throw new Exception();
                }
                else
                {
                    rsv.idEstado = 2;
                    db.Entry(rsv).State = EntityState.Modified;
                    db.SaveChanges();

                    //vm.habitaciones = new List<Habitacione>();

                    //foreach (HabitacionesPorReserva itemHPR in rsv.HabitacionesPorReservas)
                    //{
                    //    Habitacione unHab = db.Habitaciones.Find(itemHPR.idHabitacion);
                    //    vm.habitaciones.Add(unHab);
                    //}


                    vm.nombreHotel = unAl.nombreEs;
                    vm.direccionHotel = unAl.direccion;
                    vm.telefonoHotel = sesion.telHotel;
                    vm.ciudadHotel = unAl.ciudadPiePagina;
                    vm.emailHotel = unAl.emailContacto;
                    vm.paisHotel = unAl.paisPiePagina;

                    vm.fechaEntrada = sesion.fechaEntrada;
                    vm.fechaSalida = sesion.fechaSalida;
                    vm.cantPasajeros = sesion.pasajeros;

                    vm.urlLogo = sesion.urlLogo;
                    vm.urlLogoHotel = sesion.urlLogo;
                    vm.backurlHotel = sesion.backurl;
                    vm.idAlojamiento = Int32.Parse(hotel);
                    vm.colorBarra = unAl.colorBarra;
                    vm.colorBoton = unAl.colorBotones;
                    vm.colorTextoBoton = unAl.colorTextoBotones;
                    vm.tieneLogo = true;

                    SetCulture(sesion.idioma);
                    vm.idioma = sesion.idioma;
                }
            }
            catch (Exception)
            {
                int idHotelInt;
                Int32.TryParse(idHotel, out idHotelInt);
                if (idHotelInt > 0)
                {
                    unAl = asv.getByIdAlojamiento(idHotelInt);
                    Response.Redirect(unAl.website);
                }
            }

            sesion.reservaConfirmada = true;

            return View(vm);
        }

        // son titulos que se toman de mercadopago al volver.
        public ActionResult MercadoPago(string hotel)
        {
            BaseBusquedaViewModel vm = new BaseBusquedaViewModel();
            BusquedaService sv = new BusquedaService(db);
            string title;
            int quantity;
            decimal price;
            string reference;
            int idMP;
            title = "Reserva X";
            quantity = 1;
            price = 400;
            reference = "1002";
            idMP = 10;

            string idHotel = hotel;
            var sesion = System.Web.HttpContext.Current.Session["SesionInvitado" + idHotel] as SesionInvitado;

            AlojamientosService sva = new AlojamientosService(db);
            Alojamiento unAlojamiento = sva.getByIdAlojamiento(Int32.Parse(idHotel));

            vm.colorBarra = unAlojamiento.colorBarra;
            vm.colorBoton = unAlojamiento.colorBotones;
            vm.colorTextoBoton = unAlojamiento.colorTextoBotones;
            vm.nombreHotel = unAlojamiento.nombreEs;
            vm.idAlojamiento = Int32.Parse(idHotel);
            vm.tieneLogo = unAlojamiento.urlLogo != null;
            if (vm.tieneLogo)
            {
                vm.urlLogoHotel = unAlojamiento.urlLogo;
            }
            else
            {
                vm.urlLogoHotel = "/Content/resources/img/reservaslogo141x20_gris.png";
            }
            vm.colorBarra = unAlojamiento.colorBarra;
            vm.colorBoton = unAlojamiento.colorBotones;
            vm.colorTextoBoton = unAlojamiento.colorTextoBotones;
            vm.direccionHotel = unAlojamiento.direccion;
            vm.telefonoHotel = unAlojamiento.telefonos;
            vm.emailHotel = unAlojamiento.emailContacto;

            if (vm.colorBoton == null)
            {
                vm.colorBoton = "#dbbd8d";
            }

            if (vm.colorBarra == null)
            {
                vm.colorBarra = "#F0F0F0";
            }

            vm.urlLogo = unAlojamiento.urlLogo;
            //vm.backurlHotel = backUrlHotel;
            vm.paisHotel = unAlojamiento.paisPiePagina;
            vm.ciudadHotel = unAlojamiento.ciudadPiePagina;

            vm.urlLogo = sesion.urlLogo;
            vm.backurlHotel = sesion.backurl;
            vm.colorBarra = sesion.colorBarra;
            vm.colorBoton = sesion.colorBoton;

            //CargarComprobanteViewModel cvm = new CargarComprobanteViewModel();
            //sv.CargarDetalleReserva(cvm, sesion.idReserva);



            MediosdePagoService svm = new MediosdePagoService(this.db);
            int id = sesion.idMP;
           // int id = svm.getIdMercadoPago(sesion.idAlojamiento, 1);
            string sURL = sesion.linkMP;
            //if (id != -1)
            //{
                sURL = svm.GetLinkMP(id, "Reserva " + vm.nombreHotel, 1, sesion.total, sesion.idReserva.ToString(), sesion.idReserva,false);
            //}
            vm.linkMP = sURL;


      //      MediosdePagoService sv = new MediosdePagoService(this.db);            
      ////      var id = sv.getIdMercadoPago(sesion.idAlojamiento,1); 
      //      String sURL = sv.GetLinkMP(5, "name", 2, 3, "reference", 1);
      //      //   String aURL = sv.GetLinkMP(idMP, title, quantity, price, reference, id);
      //      vm.linkMP = sURL;
      // // se genera el link de compra y se muestra por link y iframe.

            return View(vm);
        }

        public ActionResult TodoPago()
        {
            BaseBusquedaViewModel vm = new BaseBusquedaViewModel();
            var sesion = System.Web.HttpContext.Current.Session["SesionInvitado"] as SesionInvitado;
            vm.urlLogo = sesion.urlLogo;
            vm.backurlHotel = sesion.backurl;
            vm.colorBarra = sesion.colorBarra;
            vm.colorBoton = sesion.colorBoton;

            MediosdePagoService sv = new MediosdePagoService(this.db);
            var id = sv.getIdMercadoPago(sesion.idAlojamiento, 1);
            String sURL = sv.GetLinkMP(id, "name", 2, 3, "reference", 1,true);
            vm.linkMP = sURL;
            // se genera el link de compra y se muestra por link y iframe.

            return View(vm);
        }

        public ActionResult PayPal()
        {
            BaseBusquedaViewModel vm = new BaseBusquedaViewModel();

            var sesion = System.Web.HttpContext.Current.Session["SesionInvitado"] as SesionInvitado;
            vm.urlLogo = sesion.urlLogo;
            vm.backurlHotel = sesion.backurl;
            vm.colorBarra = sesion.colorBarra;
            vm.colorBoton = sesion.colorBoton;
            

            MediosdePagoService sv = new MediosdePagoService(this.db);
            var id = sv.getIdMercadoPago(sesion.idAlojamiento, 1);
            String sURL = sv.GetLinkMP(id, "name", 2, 3, "reference", 1,true);
            vm.linkMP = sURL;
            // se genera el link de compra y se muestra por link y iframe.

            return View(vm);
        }

        public ActionResult MPPendiente(string collection_id, string collection_status, string preference_id, string external_reference, string payment_type, string merchant_order_id)
        {
            // se produce cuando la compra queda pendiente por que el cliente 
            //quiere comprar por rapipago o algun medio de pago 
            //que requiera una acreditacion que no es instantanea.
            var ReservaId = external_reference;

            BaseBusquedaViewModel vm = new BaseBusquedaViewModel();
            //vm.collection_id = collection_id;
            //vm.collection_status = collection_status;
            //vm.external_reference = external_reference;
            //vm.preference_id = preference_id;
            //vm.payment_type = payment_type;
            //vm.merchant_order_id = merchant_order_id;

            if (ReservaId != null)
            {
                ReservasService svReservas = new ReservasService(this.db);
                AlojamientosService sv = new AlojamientosService(db);
                int resId = Int32.Parse(ReservaId);
                Reserva rsv = db.Reservas.Where(s => s.idReserva == resId).FirstOrDefault();
                int idHotel = rsv.idAlojamiento;
                Alojamiento unAlojamiento = sv.getByIdAlojamiento(idHotel);

                vm.colorBarra = unAlojamiento.colorBarra;
                vm.colorBoton = unAlojamiento.colorBotones;
                vm.colorTextoBoton = unAlojamiento.colorTextoBotones;
                vm.nombreHotel = unAlojamiento.nombreEs;
                vm.idAlojamiento = idHotel;
                vm.tieneLogo = unAlojamiento.urlLogo != null;
                if (vm.tieneLogo)
                {
                    vm.urlLogoHotel = unAlojamiento.urlLogo;
                }
                else
                {
                    vm.urlLogoHotel = "/Content/resources/img/reservaslogo141x20_gris.png";
                }
                vm.colorBarra = unAlojamiento.colorBarra;
                vm.colorBoton = unAlojamiento.colorBotones;
                vm.colorTextoBoton = unAlojamiento.colorTextoBotones;
                vm.direccionHotel = unAlojamiento.direccion;

                if (vm.colorBoton == null)
                {
                    vm.colorBoton = "#dbbd8d";
                }

                if (vm.colorBarra == null)
                {
                    vm.colorBarra = "#F0F0F0";
                }

                vm.urlLogo = unAlojamiento.urlLogo;
                vm.paisHotel = unAlojamiento.paisPiePagina;
                vm.ciudadHotel = unAlojamiento.ciudadPiePagina;
                if (svReservas.ReservaPendiente(ReservaId))
                {
//                    vm.estado = "Pago Pendiente";
                    return View(vm);
                }
            }
  //          vm.estado = "Error en el pago";
            return View(vm);
        }

        public ActionResult MPConfirma(string collection_id, string collection_status, string preference_id, string external_reference, string payment_type, string merchant_order_id)
        {
            //de momento no se utiliza un GUID, se devuelve la referencia externa cn el Id de la reserva.
            //var ReservaId = ReservasService.GetByGuid(external_reference);
            var ReservaId = external_reference;

            BaseBusquedaViewModel vm = new BaseBusquedaViewModel();
            //vm.collection_id = collection_id;
            //vm.collection_status = collection_status;
            //vm.preference_id = preference_id;
            //vm.external_reference = external_reference;
            //vm.payment_type = payment_type;
            //vm.merchant_order_id = merchant_order_id;


            if (ReservaId != null)
            {
                ReservasService svReservas = new ReservasService(this.db);

                AlojamientosService sv = new AlojamientosService(db);
                int resId = Int32.Parse(ReservaId);
                Reserva rsv = db.Reservas.Where(s => s.idReserva == resId).FirstOrDefault();
                int idHotel = rsv.idAlojamiento;
                Alojamiento unAlojamiento = sv.getByIdAlojamiento(idHotel);

                vm.colorBarra = unAlojamiento.colorBarra;
                vm.colorBoton = unAlojamiento.colorBotones;
                vm.colorTextoBoton = unAlojamiento.colorTextoBotones;
                vm.nombreHotel = unAlojamiento.nombreEs;
                vm.idAlojamiento = idHotel;
                vm.tieneLogo = unAlojamiento.urlLogo != null;
                if (vm.tieneLogo)
                {
                    vm.urlLogoHotel = unAlojamiento.urlLogo;
                }
                else
                {
                    vm.urlLogoHotel = "/Content/resources/img/reservaslogo141x20_gris.png";
                }
                vm.colorBarra = unAlojamiento.colorBarra;
                vm.colorBoton = unAlojamiento.colorBotones;
                vm.colorTextoBoton = unAlojamiento.colorTextoBotones;
                vm.direccionHotel = unAlojamiento.direccion;

                if (vm.colorBoton == null)
                {
                    vm.colorBoton = "#dbbd8d";
                }

                if (vm.colorBarra == null)
                {
                    vm.colorBarra = "#F0F0F0";
                }

                vm.urlLogo = unAlojamiento.urlLogo;
                //vm.backurlHotel = backUrlHotel;
                vm.paisHotel = unAlojamiento.paisPiePagina;
                vm.ciudadHotel = unAlojamiento.ciudadPiePagina;


                if (svReservas.ConfirmarReserva(ReservaId))
                {
                    //vm.estado = "Pago Realizado";
                    return View(vm);
                }
            }
            //vm.estado = "Error en el pago";
            return View(vm);
        }

        
        public ActionResult MPCancela(string collection_id, string collection_status, string preference_id, string external_reference, string payment_type, string merchant_order_id)
        {
            //de momento no se utiliza un GUID, se devuelve la referencia externa cn el Id de la reserva.
            //var ReservaId = ReservasService.GetByGuid(external_reference);
            var ReservaId = (external_reference);
            BaseBusquedaViewModel vm = new BaseBusquedaViewModel();

            if (ReservaId != null)
            {
                ReservasService svReservas = new ReservasService(this.db);
                AlojamientosService sv = new AlojamientosService(db);
                int resId = Int32.Parse(ReservaId);
                Reserva rsv = db.Reservas.Where(s => s.idReserva == resId).FirstOrDefault();
                int idHotel = rsv.idAlojamiento;
                Alojamiento unAlojamiento = sv.getByIdAlojamiento(idHotel);

                vm.colorBarra = unAlojamiento.colorBarra;
                vm.colorBoton = unAlojamiento.colorBotones;
                vm.colorTextoBoton = unAlojamiento.colorTextoBotones;
                vm.nombreHotel = unAlojamiento.nombreEs;
                vm.idAlojamiento = idHotel;
                vm.tieneLogo = unAlojamiento.urlLogo != null;
                if (vm.tieneLogo)
                {
                    vm.urlLogoHotel = unAlojamiento.urlLogo;
                }
                else
                {
                    vm.urlLogoHotel = "/Content/resources/img/reservaslogo141x20_gris.png";
                }
                vm.colorBarra = unAlojamiento.colorBarra;
                vm.colorBoton = unAlojamiento.colorBotones;
                vm.colorTextoBoton = unAlojamiento.colorTextoBotones;
                vm.direccionHotel = unAlojamiento.direccion;

                if (vm.colorBoton == null)
                {
                    vm.colorBoton = "#dbbd8d";
                }

                if (vm.colorBarra == null)
                {
                    vm.colorBarra = "#F0F0F0";
                }

                vm.urlLogo = unAlojamiento.urlLogo;
                //vm.backurlHotel = backUrlHotel;
                vm.paisHotel = unAlojamiento.paisPiePagina;
                vm.ciudadHotel = unAlojamiento.ciudadPiePagina;

                if (svReservas.CancelarReserva(ReservaId))
                {
                 //   vm.estado = "Pago Cancelado";
                    return View(vm);
                }
            }
//            vm.estado = "Error en el pago";
            return View(vm);
        }

        public ActionResult Reserva()
        {
            var sesion = System.Web.HttpContext.Current.Session["SesionInvitado"] as SesionInvitado;
            ReservaViewModel vm = new ReservaViewModel();

            vm.urlLogo = sesion.urlLogo;
            vm.backurlHotel = sesion.backurl;
            vm.colorBarra = sesion.colorBarra;
            vm.colorBoton = sesion.colorBoton;


            vm.nombreHotel = sesion.nombreHotel;
            vm.direccionHotel = sesion.direccionHotel;
            vm.telefonoHotel = sesion.telHotel;
            vm.fechaEntrada = sesion.fechaEntrada;
            vm.fechaSalida = sesion.fechaSalida;
            vm.cantPasajeros = sesion.pasajeros;            
            vm.ninios = "0";
            vm.imgHotelmini = sesion.urlImagenMini;
            vm.estrellas = sesion.estrellas;




            //ReservaViewModel vm = new ReservaViewModel();
            //Prereserva
            ReservasService sv = new ReservasService(this.db);
            vm.listaTipo = sv.getComboTipo();
            vm.listaTipoPago = sv.getComboTiposPago();
            vm.listaTipoDocumento = sv.getComboTipoDocumento();
            vm.listaCiudad = sv.getComboPaises();
            //Garantia
            vm.listaTarjetaCredito = sv.getComboTarjeta();

            

            //MEDIO PAGO
            string title;
            int quantity;
            decimal price;
            string reference;
            int idMP;
            title = "Reserva X";
            quantity = 1;
            price = 400;
            reference = "1002";
            //idMP = 10;

            vm.urlLogo = sesion.urlLogo;
            vm.backurlHotel = sesion.backurl;
            vm.colorBarra = sesion.colorBarra;
            vm.colorBoton = sesion.colorBoton;

            MediosdePagoService svMP = new MediosdePagoService(this.db);
                  var id = svMP.getIdMercadoPago(sesion.idAlojamiento,1); 

            String sURL = svMP.GetLinkMP(id, "name", 2, 3, "reference", 1,false);
            //   String aURL = sv.GetLinkMP(idMP, title, quantity, price, reference, id);
            vm.linkMP = sURL;
            // se genera el link de compra y se muestra por link y iframe.

            return View(vm);
        }

    }    
}