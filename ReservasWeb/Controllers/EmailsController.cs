﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReservasWeb.ViewModels;
using ReservasWeb.Services;
using ReservasWeb.Authorize;
using ReservasWeb.Models;

namespace ReservasWeb.Controllers
{
    //// will be applied to all actions in MyController, unless those actions override with their own decoration
    //[OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)] 
    [RequiereLogin]
    public class EmailsController : BaseController
    {
        // GET: Emails
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index(string resultado)
        {
            chequearPermisos(1, 0);

            

            ListadoEmailsViewModel vm = new ListadoEmailsViewModel();
            EmailsService emailsService = new EmailsService(this.db);
            //vm.listado = toEmailViewModel(emailsService.getEmails());
            vm.listado = Utils.MailKit.getEmails();
            vm.ProcesarResultado(resultado);
            return View(vm);
        }

        private void getEmails()
        {
            throw new NotImplementedException();
        }

        public ActionResult Crear()
        {
            chequearPermisos(1, 1);
            EmailsService emailsService = new EmailsService(this.db);
            CreacionEdicionEmailsViewModel vm = new CreacionEdicionEmailsViewModel();
            return View("Email", vm);
        }

        [HttpPost]
        public ActionResult Crear(CreacionEdicionEmailsViewModel vm)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    EmailsService emailsService = new EmailsService(this.db);                    
                    vm.email.de = getSesion().email;                     
                    Email email = toEmail(vm.email);
                    emailsService.crear(email);
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "Emails", new { resultado = resultado });
        }

        public ActionResult Editar(int id)
        {
            chequearPermisos(1, 3);
            EmailsService sv = new EmailsService(this.db);
            CreacionEdicionEmailsViewModel vm = new CreacionEdicionEmailsViewModel();
            vm.email = toEmailViewModel(sv.getEmail(id));
            vm.modoEdicion = true;
            return View("Email", vm);
        }

        [HttpPost]
        public ActionResult Editar(CreacionEdicionEmailsViewModel vm)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    EmailsService sv = new EmailsService(this.db);
                    Email email = sv.getEmail(vm.email.idEmail);                    
                    email.para = vm.email.para;
                    email.cc = vm.email.cc;
                    email.cco = vm.email.cco;
                    email.asunto = vm.email.asunto;
                    email.cuerpo = vm.email.cuerpo;
                    sv.editar(email);
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "Emails", new { resultado = resultado });
        }

        public ActionResult Eliminar(int id)
        {
            chequearPermisos(1, 2);
            string resultado = "ok";
            try
            {
                EmailsService sv = new EmailsService(this.db);
                sv.Eliminar(id);
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "Emails", new { resultado = resultado });
        }

        private List<EmailViewModel> toEmailViewModel(List<Email> emails)
        {
            List<EmailViewModel> listado = new List<EmailViewModel>();
            try
            {
                EmailViewModel emailVM = null;
                foreach (Email email in emails)
                {
                    emailVM = new EmailViewModel();
                    emailVM.idAlojamiento = email.idAlojamiento;
                    emailVM.idEmail = email.idEmail;
                    emailVM.de = email.de;
                    emailVM.para = email.para;
                    emailVM.cc = email.cc;
                    emailVM.cco = email.cco;
                    emailVM.asunto = email.asunto;
                    emailVM.cuerpo = email.cuerpo;
                    listado.Add(emailVM);
                }
            }
            catch (Exception)
            {                
            }
            return listado;
        }

        private EmailViewModel toEmailViewModel(Email email)
        {
            EmailViewModel emailVM = new EmailViewModel();
            try
            {                
                emailVM.idAlojamiento = email.idAlojamiento;
                emailVM.idEmail = email.idEmail;
                emailVM.de = email.de;
                emailVM.para = email.para;
                emailVM.cc = email.cc;
                emailVM.cco = email.cco;
                emailVM.asunto = email.asunto;
                emailVM.cuerpo = email.cuerpo;
            }
            catch (Exception)
            {
            }
            return emailVM;
        }

        private Email toEmail(EmailViewModel emailVM)
        {
            Email email = new Email();
            try
            {                
                email.idAlojamiento = emailVM.idAlojamiento;
                email.idEmail = emailVM.idEmail;
                email.de = emailVM.de;
                email.para = emailVM.para;
                email.cc = emailVM.cc;
                email.cco = emailVM.cco;
                email.asunto = emailVM.asunto;
                email.cuerpo = emailVM.cuerpo;
            }
            catch (Exception)
            {
            }
            return email;
        }
    }
}