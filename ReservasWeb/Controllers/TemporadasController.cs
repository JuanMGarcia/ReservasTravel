﻿using ReservasWeb.Authorize;
using ReservasWeb.Services;
using ReservasWeb.Utils;
using ReservasWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.Controllers
{
    [RequiereLogin]
    public class TemporadasController : BaseController
    {
        // GET: Temporadas
        public ActionResult Index(string resultado)
        {
            chequearPermisos(8,0);
            ListadoTemporadasViewModel vm = new ListadoTemporadasViewModel();
            TemporadasService temporadasService = new TemporadasService(this.db);
            vm.listadoTemporadas = temporadasService.getTemporadasPorAlojamiento(ConfigurationManager.AppSettings["tipoTemporada"]);
            vm.listadoFechasEspeciales = temporadasService.getTemporadasPorAlojamiento(ConfigurationManager.AppSettings["tipoFechaEspecial"]);
            vm.ProcesarResultado(resultado);
            return View(vm);
        }

        public ActionResult Crear(string tipo)
        {
            chequearPermisos(8, 1);
            TemporadasService temporadasService = new TemporadasService(this.db);
            CreacionEdicionTemporadaViewModel vm = new CreacionEdicionTemporadaViewModel();
            vm.tipo = tipo;
            vm.tiposSelectList = temporadasService.getComboTipo();
            //selecciono un color al azar para la nueva Temporada/Fecha Especial
            Random r = new Random();
            int rInt = r.Next(1, 24);
            vm.cssColor = ConfigurationManager.AppSettings["color" + (rInt).ToString()];
            vm.cssColorSelectList = temporadasService.getComboCssColor(vm.cssColor);
            return View(vm);
        }

        [HttpPost]
        public ActionResult Crear(CreacionEdicionTemporadaViewModel vm)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    TemporadasService temporadasService = new TemporadasService(this.db);
                    int idTemp = temporadasService.Crear(vm);
                    if (idTemp==0)
                    {
                        resultado = "error";
                    }
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "Temporadas", new { resultado = resultado });
        }

        public ActionResult Editar(int id)
        {
            chequearPermisos(8, 3);
            TemporadasService temporadasService = new TemporadasService(this.db);
            CreacionEdicionTemporadaViewModel vm = temporadasService.getTemporadaEdicion(id);            
            vm.tiposSelectList = temporadasService.getComboTipo();
            vm.cssColorSelectList = temporadasService.getComboCssColor(vm.cssColor);
            return View(vm);
        }

        [HttpPost]
        public ActionResult Editar(CreacionEdicionTemporadaViewModel vm)
        {
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    TemporadasService temporadasService = new TemporadasService(this.db);
                    temporadasService.Editar(vm);
                }
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "Temporadas", new { resultado = resultado });
        }

        public ActionResult Eliminar(int id)
        {
            chequearPermisos(8, 2);
            string resultado = "ok";
            try
            {
                TemporadasService temporadasService = new TemporadasService(this.db);
                temporadasService.Eliminar(id);
            }
            catch (Exception)
            {
                resultado = "error";
            }

            return RedirectToAction("index", "Temporadas", new { resultado = resultado });
        }
    }
}