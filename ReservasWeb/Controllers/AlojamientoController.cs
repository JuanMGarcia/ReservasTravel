﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReservasWeb.ViewModels;
using ReservasWeb.Services;
using ReservasWeb.Authorize;

namespace ReservasWeb.Controllers
{
    [RequiereLogin]
    public class AlojamientoController : BaseController
    {
        // GET: Alojamiento
        public ActionResult Index(string resultado)
        {
            chequearPermisos(2, 0);
            AlojamientosService sv = new AlojamientosService(this.db);
            DatosAlojamientoViewModel vm = sv.getDatosAlojamiento();
            vm.ProcesarResultado(resultado);
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(DatosAlojamientoViewModel vm, HttpPostedFileBase logoUpload)
        {
            string resultado = "ok";

            try
            {
                if (ModelState.IsValid)
                {
                    chequearPermisos(2, 3);
                    AlojamientosService sv = new AlojamientosService(this.db);
                    vm.logoUpload = logoUpload;
                    sv.Editar(vm);
                }                
            }
            catch (Exception)
            {
                resultado = "error";
            }
            return RedirectToAction("index", "Alojamiento", new { resultado = resultado });
        }

        // GET: Alojamiento
        public ActionResult Comodidades(string resultado)
        {
            chequearPermisos(3, 0);
            ComodidadesService sv = new ComodidadesService(this.db);
            ComodidadesAlojamientoViewModel vm = new ComodidadesAlojamientoViewModel();
            vm.listaComodidades = sv.getComodidades();            
            vm.ProcesarResultado(resultado);
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Comodidades(ComodidadesAlojamientoViewModel vm)
        {
            string resultado = "ok";

            try
            {
                if (ModelState.IsValid)
                {
                    chequearPermisos(3, 3);
                    List<int> ids = new List<int>();
                    foreach (var item in vm.listaComodidades)
                    {
                        if (item.seleccionada)
                        {
                            ids.Add(item.idComodidad);
                        }
                    }
                    ComodidadesService sv = new ComodidadesService(this.db);
                    sv.guardarComodidades(ids);

                }
            }
            catch (Exception)
            {
                resultado = "error";
            }
            return RedirectToAction("Comodidades", "Alojamiento", new { resultado = resultado });
        }
        

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Crear(DatosAlojamientoViewModel vm, HttpPostedFileBase imagenUpload)
        //{
        //    string resultado = "ok";

        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            AlojamientosService sv = new AlojamientosService(db);
        //            vm.logoUpload = imagenUpload;
        //            sv.subirLogo(vm);
        //        }
        //        else
        //        {
        //            resultado = "error";
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        resultado = "error";
        //    }
        //    return RedirectToAction("Index", "Obras", new { resultado = resultado });
        //}

        [HttpPost]
        public JsonResult eliminarLogo()
        {
            AlojamientosService sv = new AlojamientosService(this.db);
            sv.quitarLogo();
            return Json(new { Resultado = "OK" }, JsonRequestBehavior.AllowGet);
        }


    }
}