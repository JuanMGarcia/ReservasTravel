﻿using ReservasWeb.Models;
using ReservasWeb.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.Controllers
{
    public class ContactoController : BaseController
    {        

        [HttpPost]
        public JsonResult EnviarMailContacto(string nombre, string email, string telefono, string desde, string hasta, string pasajeros, string mensaje, int idAlojamiento)
        {
            //try
            //{

            AlojamientosService sv = new AlojamientosService(this.db);
            Alojamiento unAloj = sv.getByIdAlojamiento(idAlojamiento);

            string fromMail = ConfigurationManager.AppSettings["fromMail"];
            string toMail = unAloj.emailContacto;

            string subject = "Consulta Via Reservas Travel";

            string cuerpo = "Consulta enviada desde Reservas.Travel" + (char)10 + (char)10 +                            
                            "Nombre y Apellido: " + nombre + (char)10 +                            
                            "Email: " + email + (char)10 +
                            "Teléfono: " + telefono + (char)10 +                            
                            "Desde: " + desde + (char)10 +
                            "Hasta: " + hasta + (char)10 +
                            "Cant. Pasajeros: " + pasajeros + (char)10 +
                            "Mensaje: " + mensaje;

            enviarEmail(fromMail, email, toMail, subject, cuerpo, false);


            //}
            //catch (Exception e)
            //{
            //    return Json(new { Resultado = "NO" }, JsonRequestBehavior.AllowGet);
            //}

            return Json(new { Resultado = "OK" }, JsonRequestBehavior.AllowGet);

        }
    }
}