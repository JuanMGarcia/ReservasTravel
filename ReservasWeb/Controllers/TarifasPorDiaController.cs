﻿using ReservasWeb.Authorize;
using ReservasWeb.Models;
using ReservasWeb.Services;
using ReservasWeb.Utils;
using ReservasWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.Controllers
{
    [RequiereLogin]
    public class TarifasPorDiaController : BaseController
    {
        // GET: TiposHabitacion
        public async Task<ActionResult> Index(string resultado, string idTipoHabitacion = "0", string idCanal = "0", string idCondicionReserva = null, string fechaDesde = null)
        {
            chequearPermisos(9,0);
            //EdicionTarifasPorDiaViewModel vm = await this.ObtenerEdicionTarifasPorDiaViewModel(0, 0, null, null, null);
            int idTipoHabitacionInt, idCanalInt, idCondicionReservaInt;
            int? idCondicionReservaIntFinal;
            if (!Int32.TryParse(idTipoHabitacion, out idTipoHabitacionInt))
                idTipoHabitacionInt = 0;
            if (!Int32.TryParse(idCanal, out idCanalInt))
                idCanalInt = 0;
            if (!Int32.TryParse(idCondicionReserva, out idCondicionReservaInt) || idCondicionReserva == "0" || idCondicionReserva == "-1")
                idCondicionReservaIntFinal = null;
            else
                idCondicionReservaIntFinal = idCondicionReservaInt;
            
            EdicionTarifasPorDiaViewModel vm = await this.ObtenerEdicionTarifasPorDiaViewModel(idTipoHabitacionInt, idCanalInt, idCondicionReservaIntFinal, fechaDesde, null);
            vm.ProcesarResultado(resultado);

            //// T A R I F A S   B A S E S
            CanalesService canalService = new CanalesService(this.db);
            List<Canale> canalesLista = new List<Canale>();
            canalesLista = canalService.getCanalesDeAlojamiento();

            CondicionesReservaService condicionesReservaSV = new CondicionesReservaService(this.db);

            List<CondReserva> condicionesReservas = condicionesReservaSV.getCondicionesReservaListadoAbm();

            vm.tarifaBaseListado = new List<TarifaBaseViewModel>();
          
            CondReserva condicion = null;
            vm.condicionesReservaListado = new List<SelectListItem>();
            string condicionId, condicionNombre;
            bool selected = false;
            for (int x = -1; x < condicionesReservas.Count(); x++)
            {
                if (x >= 0)
                {
                    condicion = condicionesReservas.ElementAt(x);
                    condicionId = condicion.idCondReserva.ToString();
                    condicionNombre = condicion.nombreEs;
                    selected = false;
                }
                else
                {
                    condicionId = x.ToString();
                    condicionNombre = "Sin condiciones";
                    selected = true;
                }

                vm.condicionesReservaListado.Add(new SelectListItem { Value = condicionId, Text = condicionNombre, Selected = selected });

                //tarifaBaseListado = new List<TarifaBaseViewModel>();
                TarifasBaseService tarifasBaseService = new TarifasBaseService(this.db);
                TarifaBaseViewModel tarifaBaseViewModel;
                TarifasBase tarifaBase;
                for (int i = 0; i < canalesLista.Count(); i++)
                {

                    tarifaBaseViewModel = new TarifaBaseViewModel();
                    tarifaBaseViewModel.idTipoHabitacion = vm.idTipoHabitacion;
                    tarifaBaseViewModel.idCanal = canalesLista.ElementAt(i).idCanal;
                    tarifaBaseViewModel.nombreCanalEs = canalesLista.ElementAt(i).nombreEs;
                    tarifaBaseViewModel.nombreCanalEn = canalesLista.ElementAt(i).nombreEn;
                    tarifaBaseViewModel.nombreCanalPt = canalesLista.ElementAt(i).nombrePt;


                    tarifaBase = tarifasBaseService.get(vm.idTipoHabitacion, canalesLista.ElementAt(i).idCanal, Convert.ToInt32(condicionId));
                    if (tarifaBase != null)
                    {
                        tarifaBaseViewModel.tarifaLunes = tarifaBase.tarifaLunes == 0 ? "" : Convert.ToString(tarifaBase.tarifaLunes, new CultureInfo("en-US"));
                        tarifaBaseViewModel.tarifaMartes = tarifaBase.tarifaMartes == 0 ? "" : Convert.ToString(tarifaBase.tarifaMartes, new CultureInfo("en-US"));
                        tarifaBaseViewModel.tarifaMiercoles = tarifaBase.tarifaMiercoles == 0 ? "" : Convert.ToString(tarifaBase.tarifaMiercoles, new CultureInfo("en-US"));
                        tarifaBaseViewModel.tarifaJueves = tarifaBase.tarifaJueves == 0 ? "" : Convert.ToString(tarifaBase.tarifaJueves, new CultureInfo("en-US"));
                        tarifaBaseViewModel.tarifaViernes = tarifaBase.tarifaViernes == 0 ? "" : Convert.ToString(tarifaBase.tarifaViernes, new CultureInfo("en-US"));
                        tarifaBaseViewModel.tarifaSabado = tarifaBase.tarifaSabado == 0 ? "" : Convert.ToString(tarifaBase.tarifaSabado, new CultureInfo("en-US"));
                        tarifaBaseViewModel.tarifaDomingo = tarifaBase.tarifaDomingo == 0 ? "" : Convert.ToString(tarifaBase.tarifaDomingo, new CultureInfo("en-US"));
                        tarifaBaseViewModel.descuentoPax = tarifaBase.descuentoPax == 0 ? "" : Convert.ToString(tarifaBase.descuentoPax, new CultureInfo("en-US"));
                        tarifaBaseViewModel.adicionalCuna = tarifaBase.adicionalCuna == 0 ? "" : Convert.ToString(tarifaBase.adicionalCuna, new CultureInfo("en-US"));
                        tarifaBaseViewModel.adicionalMenor = tarifaBase.adicionalMenor == 0 ? "" : Convert.ToString(tarifaBase.adicionalMenor, new CultureInfo("en-US"));
                        tarifaBaseViewModel.adicionalAdulto = tarifaBase.adicionalAdulto == 0 ? "" : Convert.ToString(tarifaBase.adicionalAdulto, new CultureInfo("en-US"));
                        tarifaBaseViewModel.estadiaMinima = tarifaBase.estadiaMinima == 0 ? "" : Convert.ToString(tarifaBase.estadiaMinima, new CultureInfo("en-US"));
                        tarifaBaseViewModel.permiteEgreso = Convert.ToBoolean(tarifaBase.permiteEgreso);
                        tarifaBaseViewModel.permiteIngreso = Convert.ToBoolean(tarifaBase.permiteIngreso);
                        tarifaBaseViewModel.aceptaMenores = Convert.ToBoolean(tarifaBase.aceptaMenores);
                        tarifaBaseViewModel.abierta = Convert.ToBoolean(tarifaBase.abierta);
                        //tarifaBaseViewModel.permiteEgreso = true;
                        //tarifaBaseViewModel.permiteIngreso = true;
                        //tarifaBaseViewModel.aceptaMenores = true;
                        //tarifaBaseViewModel.abierta = true;
                    }
                    else
                    {
                        //    tarifaBaseViewModel.tarifaLunes = "0";
                        //    tarifaBaseViewModel.tarifaMartes = "0";
                        //    tarifaBaseViewModel.tarifaMiercoles = "0";
                        //    tarifaBaseViewModel.tarifaJueves = "0";
                        //    tarifaBaseViewModel.tarifaViernes = "0";
                        //    tarifaBaseViewModel.tarifaSabado = "0";
                        //    tarifaBaseViewModel.tarifaDomingo = "0";
                        //    tarifaBaseViewModel.descuentoPax = "0";
                        //    tarifaBaseViewModel.adicionalCuna = "0";
                        //    tarifaBaseViewModel.adicionalMenor = "0";
                        //    tarifaBaseViewModel.adicionalAdulto = "0";
                        tarifaBaseViewModel.permiteEgreso = true;
                        tarifaBaseViewModel.permiteIngreso = true;
                        tarifaBaseViewModel.aceptaMenores = true;
                        tarifaBaseViewModel.estadiaMinima = "1";


                        //si no tiene condicion y es Canal Web o Mostrador, entonces abierto por default
                        if (condicionId == "-1" && (canalesLista.ElementAt(i).idCanal == Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]) || canalesLista.ElementAt(i).idCanal == Convert.ToInt32(ConfigurationManager.AppSettings["idCanalMostrador"])))
                            tarifaBaseViewModel.abierta = true;
                    }

                    vm.tarifaBaseListado.Add(tarifaBaseViewModel);


                }

                //vm.tarifaBaseListado.Add(Convert.ToInt32(condicionId), tarifaBaseListado);

            }


            ////
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<ActionResult> Index(EdicionTarifasPorDiaViewModel vm)
        public ActionResult Index(EdicionTarifasPorDiaViewModel vm)        
        {
            chequearPermisos(9,0);
            string resultado = "ok";
            try
            {
                if (ModelState.IsValid)
                {
                    string[] diasEditados = vm.diasEditados.Split(',');
                    TarifaPorDiaViewModel tarifaPorDiaVM;
                    TarifasPorDiaService sv = new TarifasPorDiaService(this.db);
                    for (int i = 1; i < diasEditados.Length-1; i++)
                    {
                        tarifaPorDiaVM = vm.tarifaPorDiaListado.ElementAt(Int32.Parse(diasEditados[i]));
                        sv.editar(tarifaPorDiaVM);
                    }



                    //vm = await this.ObtenerEdicionTarifasPorDiaViewModel(vm.idTipoHabitacion, vm.idCanal, vm.idCondicionReserva, vm.tarifaPorDiaListado[0].fecha.ToString("dd/MM/yyyy"), null);
                    //vm.ProcesarResultado(resultado);
                    //return View(vm);
                    //// T A R I F A S   B A S E S
                    CanalesService canalService = new CanalesService(this.db);
                    List<Canale> canalesLista = new List<Canale>();
                    canalesLista = canalService.getCanalesDeAlojamiento();

                    CondicionesReservaService condicionesReservaSV = new CondicionesReservaService(this.db);

                    List<CondReserva> condicionesReservas = condicionesReservaSV.getCondicionesReservaListadoAbm();

                    vm.tarifaBaseListado = new List<TarifaBaseViewModel>();

                    CondReserva condicion = null;
                    vm.condicionesReservaListado = new List<SelectListItem>();
                    string condicionId, condicionNombre;
                    bool selected = false;
                    for (int x = -1; x < condicionesReservas.Count(); x++)
                    {
                        if (x >= 0)
                        {
                            condicion = condicionesReservas.ElementAt(x);
                            condicionId = condicion.idCondReserva.ToString();
                            condicionNombre = condicion.nombreEs;
                            selected = false;
                        }
                        else
                        {
                            condicionId = x.ToString();
                            condicionNombre = "Sin condiciones";
                            selected = true;
                        }

                        vm.condicionesReservaListado.Add(new SelectListItem { Value = condicionId, Text = condicionNombre, Selected = selected });

                        //tarifaBaseListado = new List<TarifaBaseViewModel>();
                        TarifasBaseService tarifasBaseService = new TarifasBaseService(this.db);
                        TarifaBaseViewModel tarifaBaseViewModel;
                        TarifasBase tarifaBase;
                        for (int i = 0; i < canalesLista.Count(); i++)
                        {

                            tarifaBaseViewModel = new TarifaBaseViewModel();
                            tarifaBaseViewModel.idTipoHabitacion = vm.idTipoHabitacion;
                            tarifaBaseViewModel.idCanal = canalesLista.ElementAt(i).idCanal;
                            tarifaBaseViewModel.nombreCanalEs = canalesLista.ElementAt(i).nombreEs;
                            tarifaBaseViewModel.nombreCanalEn = canalesLista.ElementAt(i).nombreEn;
                            tarifaBaseViewModel.nombreCanalPt = canalesLista.ElementAt(i).nombrePt;


                            tarifaBase = tarifasBaseService.get(vm.idTipoHabitacion, canalesLista.ElementAt(i).idCanal, Convert.ToInt32(condicionId));
                            if (tarifaBase != null)
                            {
                                tarifaBaseViewModel.tarifaLunes = tarifaBase.tarifaLunes == 0 ? "" : Convert.ToString(tarifaBase.tarifaLunes, new CultureInfo("en-US"));
                                tarifaBaseViewModel.tarifaMartes = tarifaBase.tarifaMartes == 0 ? "" : Convert.ToString(tarifaBase.tarifaMartes, new CultureInfo("en-US"));
                                tarifaBaseViewModel.tarifaMiercoles = tarifaBase.tarifaMiercoles == 0 ? "" : Convert.ToString(tarifaBase.tarifaMiercoles, new CultureInfo("en-US"));
                                tarifaBaseViewModel.tarifaJueves = tarifaBase.tarifaJueves == 0 ? "" : Convert.ToString(tarifaBase.tarifaJueves, new CultureInfo("en-US"));
                                tarifaBaseViewModel.tarifaViernes = tarifaBase.tarifaViernes == 0 ? "" : Convert.ToString(tarifaBase.tarifaViernes, new CultureInfo("en-US"));
                                tarifaBaseViewModel.tarifaSabado = tarifaBase.tarifaSabado == 0 ? "" : Convert.ToString(tarifaBase.tarifaSabado, new CultureInfo("en-US"));
                                tarifaBaseViewModel.tarifaDomingo = tarifaBase.tarifaDomingo == 0 ? "" : Convert.ToString(tarifaBase.tarifaDomingo, new CultureInfo("en-US"));
                                tarifaBaseViewModel.descuentoPax = tarifaBase.descuentoPax == 0 ? "" : Convert.ToString(tarifaBase.descuentoPax, new CultureInfo("en-US"));
                                tarifaBaseViewModel.adicionalCuna = tarifaBase.adicionalCuna == 0 ? "" : Convert.ToString(tarifaBase.adicionalCuna, new CultureInfo("en-US"));
                                tarifaBaseViewModel.adicionalMenor = tarifaBase.adicionalMenor == 0 ? "" : Convert.ToString(tarifaBase.adicionalMenor, new CultureInfo("en-US"));
                                tarifaBaseViewModel.adicionalAdulto = tarifaBase.adicionalAdulto == 0 ? "" : Convert.ToString(tarifaBase.adicionalAdulto, new CultureInfo("en-US"));
                                tarifaBaseViewModel.estadiaMinima = tarifaBase.estadiaMinima == 0 ? "" : Convert.ToString(tarifaBase.estadiaMinima, new CultureInfo("en-US"));
                                tarifaBaseViewModel.permiteEgreso = Convert.ToBoolean(tarifaBase.permiteEgreso);
                                tarifaBaseViewModel.permiteIngreso = Convert.ToBoolean(tarifaBase.permiteIngreso);
                                tarifaBaseViewModel.aceptaMenores = Convert.ToBoolean(tarifaBase.aceptaMenores);
                                tarifaBaseViewModel.abierta = Convert.ToBoolean(tarifaBase.abierta);
                                //tarifaBaseViewModel.permiteEgreso = true;
                                //tarifaBaseViewModel.permiteIngreso = true;
                                //tarifaBaseViewModel.aceptaMenores = true;
                                //tarifaBaseViewModel.abierta = true;
                            }
                            else
                            {
                                //    tarifaBaseViewModel.tarifaLunes = "0";
                                //    tarifaBaseViewModel.tarifaMartes = "0";
                                //    tarifaBaseViewModel.tarifaMiercoles = "0";
                                //    tarifaBaseViewModel.tarifaJueves = "0";
                                //    tarifaBaseViewModel.tarifaViernes = "0";
                                //    tarifaBaseViewModel.tarifaSabado = "0";
                                //    tarifaBaseViewModel.tarifaDomingo = "0";
                                //    tarifaBaseViewModel.descuentoPax = "0";
                                //    tarifaBaseViewModel.adicionalCuna = "0";
                                //    tarifaBaseViewModel.adicionalMenor = "0";
                                //    tarifaBaseViewModel.adicionalAdulto = "0";
                                tarifaBaseViewModel.permiteEgreso = true;
                                tarifaBaseViewModel.permiteIngreso = true;
                                tarifaBaseViewModel.aceptaMenores = true;
                                tarifaBaseViewModel.estadiaMinima = "1";


                                //si no tiene condicion y es Canal Web o Mostrador, entonces abierto por default
                                if (condicionId == "-1" && (canalesLista.ElementAt(i).idCanal == Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]) || canalesLista.ElementAt(i).idCanal == Convert.ToInt32(ConfigurationManager.AppSettings["idCanalMostrador"])))
                                    tarifaBaseViewModel.abierta = true;
                            }

                            vm.tarifaBaseListado.Add(tarifaBaseViewModel);


                        }

                        //vm.tarifaBaseListado.Add(Convert.ToInt32(condicionId), tarifaBaseListado);

                    }


                    ////

                }
            }
            catch (Exception)
            {
                resultado = "error";
            }
            //return RedirectToAction("index", "TarifasPorDia", new { resultado = resultado });
            return RedirectToAction("index", "TarifasPorDia", new { resultado = resultado, idTipoHabitacion  = vm.idTipoHabitacion , idCanal = vm.idCanal , idCondicionReserva = vm.idCondicionReserva , fechaDesde = vm.tarifaPorDiaListado[0].fecha.ToString("dd/MM/yyyy") });
        }

        [HttpGet]
        public async Task<ActionResult> TarifasPorDiaListado(int idHabitcion, int idCanal, int? idCondicionReserva, string fechaDesde = "")
        {
            EdicionTarifasPorDiaViewModel model = await this.ObtenerEdicionTarifasPorDiaViewModel(idHabitcion, idCanal, idCondicionReserva, fechaDesde, null);
            return PartialView("TarifaPorDiaListado", model);
        }

        private async Task<EdicionTarifasPorDiaViewModel> ObtenerEdicionTarifasPorDiaViewModel(int idTipoHabitacion = 0, int idCanal = 0, int? idCondicionReserva = null, string fechaDesde = "", string resultado = "")  
        {
            EdicionTarifasPorDiaViewModel vm = new EdicionTarifasPorDiaViewModel();
            TiposHabitacionService svTip = new TiposHabitacionService(this.db);
            HabitacionesService svHabitaciones = new HabitacionesService(this.db);            
            CanalesService svCanales = new CanalesService(this.db);
            CondicionesReservaService svCondiciones = new CondicionesReservaService(this.db);

            List<TiposHabitacione> tiposHabitacion = svTip.getTiposHabitacionConDatosCompletos();
            List<Canale> canales = svCanales.getCanalesDeAlojamiento();

            vm.diasEditados = ",";
            vm.tiposHabitacion = new SelectList(tiposHabitacion, "IdTipoHabitacion", "NombreEs");
            vm.canales = new SelectList(canales, "IdCanal", "NombreEs");

            
            List<CondReserva> condicionesReservas = svCondiciones.getCondicionesReservaListadoAbm();
            //vm.condicionesReservas = new SelectList(svCondiciones.getCondicionesReservaListadoAbm(), "IdCondReserva", "Nombre");
            //vm.condicionesReservas.Aggregate<SelectListItem>(new SelectListItem { Value = "-1", Text = "Sin condiciones", Selected = true });
            string condicionId, condicionNombre;
            bool selected = false;

            vm.condicionesReservas = new List<SelectListItem>();
            if (condicionesReservas != null && condicionesReservas.Count() > 0)
            {                
                for (int x = -1; x < condicionesReservas.Count(); x++)
                {
                    if (x >= 0)
                    {
                        condicionId = condicionesReservas.ElementAt(x).idCondReserva.ToString();
                        condicionNombre = condicionesReservas.ElementAt(x).nombreEs;
                        selected = false;
                    }
                    else
                    {
                        condicionId = x.ToString();
                        condicionNombre = "Sin condiciones";
                        selected = true;
                    }

                    vm.condicionesReservas.Add(new SelectListItem { Value = condicionId, Text = condicionNombre, Selected = selected });
                }
            }else
                vm.condicionesReservas.Add(new SelectListItem { Value = "-1", Text = "Sin condiciones", Selected = true });




            DateTime fechaDesdeDT;            

            if (fechaDesde != null)
            {
                fechaDesdeDT = DateHelper.stringToDateTime(fechaDesde);
                vm.fechaDesde = fechaDesdeDT.ToString("dd/MM/yyyy");
            }
            else
            {
                fechaDesdeDT = DateTime.Now;
                vm.fechaDesde = fechaDesdeDT.ToString("dd/MM/yyyy");
            }

            TarifasBaseService tarifasBaseService = new TarifasBaseService(this.db);
            TarifasPorDiaService tarifasPorDiaService = new TarifasPorDiaService(this.db);
            TarifaPorDiaViewModel tarifaPorDiaViewModel;
            TarifasBase tarifaBase;
            TarifasPorDia tarifaPorDia;

            if (idTipoHabitacion == 0 && tiposHabitacion.Count > 0)
                idTipoHabitacion = Int32.Parse(tiposHabitacion.ElementAt(0).idTipoHabitacion.ToString());

            if (idCanal == 0 && canales.Count > 0)
                idCanal = Int32.Parse(canales.ElementAt(0).idCanal.ToString());

            int cantidadDisponible = 0;
            List<Habitacione> habitaciones = svHabitaciones.getHabitacionesPorTipoHabitacion(idTipoHabitacion);
            if (habitaciones != null)
                cantidadDisponible = habitaciones.Count();
            
            vm.tarifaPorDiaListado = new List<TarifaPorDiaViewModel>();
            tarifaBase = tarifasBaseService.get(idTipoHabitacion, idCanal, idCondicionReserva);

            for (int i = 0; i < 14; i++)
            {
                tarifaPorDiaViewModel                    = new TarifaPorDiaViewModel();
                tarifaPorDiaViewModel.idTipoHabitacion   = idTipoHabitacion;
                tarifaPorDiaViewModel.idCanal            = idCanal;
                tarifaPorDiaViewModel.idCondicionReserva = idCondicionReserva;
                tarifaPorDiaViewModel.fecha = fechaDesdeDT.AddDays(i);

                tarifaPorDia = tarifasPorDiaService.get(idTipoHabitacion, idCanal, idCondicionReserva, tarifaPorDiaViewModel.fecha);
                if (tarifaPorDia != null)
                {
                    tarifaPorDiaViewModel.tarifaBase = Convert.ToString(tarifaPorDia.tarifaBase, new CultureInfo("en-US"));
                    tarifaPorDiaViewModel.disponibilidadCanal = tarifaPorDia.disponibilidadCanal.ToString();
                    if (tarifaPorDiaViewModel.disponibilidadCanal == "")
                        tarifaPorDiaViewModel.disponibilidadCanal = cantidadDisponible.ToString();
                    tarifaPorDiaViewModel.disponibilidadReal = cantidadDisponible.ToString();
                    if (tarifaPorDia.estadiaMinima.HasValue)
                        tarifaPorDiaViewModel.estadiaMinima = tarifaPorDia.estadiaMinima.Value.ToString();
                    else
                        tarifaPorDiaViewModel.estadiaMinima = "1";
                    tarifaPorDiaViewModel.descuentoPax      = Convert.ToString(tarifaPorDia.descuentoPorPax, new CultureInfo("en-US"));
                    tarifaPorDiaViewModel.adicionalCuna     = Convert.ToString(tarifaPorDia.adicionalCuna, new CultureInfo("en-US"));
                    tarifaPorDiaViewModel.adicionalMenor    = Convert.ToString(tarifaPorDia.adicionalMenor, new CultureInfo("en-US"));
                    tarifaPorDiaViewModel.adicionalAdulto   = Convert.ToString(tarifaPorDia.adicionalAdulto, new CultureInfo("en-US"));
                    tarifaPorDiaViewModel.permiteEgreso     = Convert.ToBoolean(tarifaPorDia.permiteEgreso);
                    tarifaPorDiaViewModel.permiteIngreso    = Convert.ToBoolean(tarifaPorDia.permiteIngreso);
                    tarifaPorDiaViewModel.aceptaMenores     = Convert.ToBoolean(tarifaPorDia.aceptaMenores);
                    tarifaPorDiaViewModel.abierta           = tarifaPorDia.abierta.Value;
                }
                else
                {
                    if (tarifaBase != null)
                    {
                        switch((int)tarifaPorDiaViewModel.fecha.DayOfWeek)
                        {
                            case 0:
                                tarifaPorDiaViewModel.tarifaBase = Convert.ToString(tarifaBase.tarifaDomingo, new CultureInfo("en-US"));
                                break;
                            case 1:
                                tarifaPorDiaViewModel.tarifaBase = Convert.ToString(tarifaBase.tarifaLunes, new CultureInfo("en-US"));
                                break;
                            case 2:
                                tarifaPorDiaViewModel.tarifaBase = Convert.ToString(tarifaBase.tarifaMartes, new CultureInfo("en-US"));
                                break;
                            case 3:
                                tarifaPorDiaViewModel.tarifaBase = Convert.ToString(tarifaBase.tarifaMiercoles, new CultureInfo("en-US"));
                                break;
                            case 4:
                                tarifaPorDiaViewModel.tarifaBase = Convert.ToString(tarifaBase.tarifaJueves, new CultureInfo("en-US"));
                                break;
                            case 5:
                                tarifaPorDiaViewModel.tarifaBase = Convert.ToString(tarifaBase.tarifaViernes, new CultureInfo("en-US"));
                                break;
                            case 6:
                                tarifaPorDiaViewModel.tarifaBase = Convert.ToString(tarifaBase.tarifaSabado, new CultureInfo("en-US"));
                                break;
                        }                                                                           
                        tarifaPorDiaViewModel.disponibilidadCanal = tarifaBase.disponibilidadCanal.ToString();
                        if (tarifaPorDiaViewModel.disponibilidadCanal == "")
                            tarifaPorDiaViewModel.disponibilidadCanal = cantidadDisponible.ToString();
                        tarifaPorDiaViewModel.disponibilidadReal = cantidadDisponible.ToString();
                        if (tarifaBase.estadiaMinima.HasValue)
                            tarifaPorDiaViewModel.estadiaMinima = tarifaBase.estadiaMinima.Value.ToString();
                        else
                            tarifaPorDiaViewModel.estadiaMinima = "1";
                        tarifaPorDiaViewModel.descuentoPax      = Convert.ToString(tarifaBase.descuentoPax, new CultureInfo("en-US"));
                        tarifaPorDiaViewModel.adicionalCuna     = Convert.ToString(tarifaBase.adicionalCuna, new CultureInfo("en-US"));
                        tarifaPorDiaViewModel.adicionalMenor    = Convert.ToString(tarifaBase.adicionalMenor, new CultureInfo("en-US"));
                        tarifaPorDiaViewModel.adicionalAdulto   = Convert.ToString(tarifaBase.adicionalAdulto, new CultureInfo("en-US"));
                        tarifaPorDiaViewModel.permiteEgreso     = Convert.ToBoolean(tarifaBase.permiteEgreso);
                        tarifaPorDiaViewModel.permiteIngreso    = Convert.ToBoolean(tarifaBase.permiteIngreso);
                        tarifaPorDiaViewModel.aceptaMenores     = Convert.ToBoolean(tarifaBase.aceptaMenores);
                        if (tarifaBase.abierta.HasValue)
                            tarifaPorDiaViewModel.abierta = tarifaBase.abierta.Value;
                    }
                    else
                    {
                        tarifaPorDiaViewModel.tarifaBase = "0.00";
                        tarifaPorDiaViewModel.disponibilidadCanal = "0";
                        tarifaPorDiaViewModel.disponibilidadReal = "0";
                        tarifaPorDiaViewModel.estadiaMinima = "1";
                        tarifaPorDiaViewModel.descuentoPax = "0.00";
                        tarifaPorDiaViewModel.adicionalCuna = "0.00";
                        tarifaPorDiaViewModel.adicionalMenor = "0.00";
                        tarifaPorDiaViewModel.adicionalAdulto = "0.00";
                        tarifaPorDiaViewModel.permiteEgreso = true;
                        tarifaPorDiaViewModel.permiteIngreso = true;
                        tarifaPorDiaViewModel.aceptaMenores = true;
                        tarifaPorDiaViewModel.abierta = true;
                    }
                }
                
                vm.tarifaPorDiaListado.Add(tarifaPorDiaViewModel);
            }
            vm.ProcesarResultado(resultado);
            return vm;
        }

        public ActionResult Crear()
        {
            CreacionTipoHabitacionViewModel vm = new CreacionTipoHabitacionViewModel();
            return View(vm);
        }

        [HttpPost]
        public ActionResult Crear(CreacionTipoHabitacionViewModel vm)
        {
            int idTipoHabitacion = 0;
            if (ModelState.IsValid)
            {
                TiposHabitacionService svTip = new TiposHabitacionService(this.db);
                idTipoHabitacion = svTip.Crear(vm);                
            }
            if (idTipoHabitacion > 0)
                return RedirectToAction("Editar", new { id = idTipoHabitacion });
            else
                return RedirectToAction("Index");
        }

        public ActionResult EditarMasivo()
        {
            chequearPermisos(9,3);
            EdicionTarifasPorDiaMasivaViewModel vm = new EdicionTarifasPorDiaMasivaViewModel();
            TiposHabitacionService svTip = new TiposHabitacionService(this.db);
            HabitacionesService svHabitaciones = new HabitacionesService(this.db);
            CanalesService svCanales = new CanalesService(this.db);
            TemporadasService temporadasService = new TemporadasService(this.db);
            CondicionesReservaService svCondiciones = new CondicionesReservaService(this.db);

            List<Temporada> temporadas = temporadasService.getTemporadasPorAlojamiento(ConfigurationManager.AppSettings["tipoTemporada"]);
            List<Temporada> fechasEspeciales = temporadasService.getTemporadasPorAlojamiento(ConfigurationManager.AppSettings["tipoFechaEspecial"]);

            vm.temporadas = new SelectList(temporadas, "IdTemporada", "Nombre");
            CheckBoxItemViewModel checkBoxItemViewModel = null;

            vm.fechasEspeciales = new SelectList(fechasEspeciales, "IdTemporada", "Nombre");
            List<TiposHabitacione> tiposHabitacion = svTip.getTiposHabitacionConDatosCompletos();
            List<Canale> canales = svCanales.getCanalesDeAlojamiento();
            List<CondReserva> condicionesReservas = svCondiciones.getCondicionesReservaListadoAbm();

            vm.tiposHabitacion = new List<CheckBoxItemViewModel>();
            checkBoxItemViewModel = null;
            foreach (TiposHabitacione tipoHabitacion in tiposHabitacion)
            {
                checkBoxItemViewModel = new CheckBoxItemViewModel();
                checkBoxItemViewModel.nombre = tipoHabitacion.nombreEs;
                checkBoxItemViewModel.valor = tipoHabitacion.idTipoHabitacion.ToString();
                checkBoxItemViewModel.seleccionado = false;
                vm.tiposHabitacion.Add(checkBoxItemViewModel);
            }

            vm.canales = new List<CheckBoxItemViewModel>();
            checkBoxItemViewModel = null;
            foreach (Canale canal in canales)
            {
                checkBoxItemViewModel = new CheckBoxItemViewModel();
                checkBoxItemViewModel.nombre = canal.nombreEs;
                checkBoxItemViewModel.valor = canal.idCanal.ToString();
                checkBoxItemViewModel.seleccionado = false;
                vm.canales.Add(checkBoxItemViewModel);
            }

            vm.condicionesReservas = new List<CheckBoxItemViewModel>();
            checkBoxItemViewModel = new CheckBoxItemViewModel();

            checkBoxItemViewModel.nombre = "Sin condiciones";
            checkBoxItemViewModel.valor = "-1";
            checkBoxItemViewModel.seleccionado = false;
            vm.condicionesReservas.Add(checkBoxItemViewModel);

            foreach (CondReserva condicion in condicionesReservas)
            {
                checkBoxItemViewModel = new CheckBoxItemViewModel();
                checkBoxItemViewModel.nombre = condicion.nombreEs;
                checkBoxItemViewModel.valor = condicion.idCondReserva.ToString();
                checkBoxItemViewModel.seleccionado = false;
                vm.condicionesReservas.Add(checkBoxItemViewModel);
            }

            TarifaPorDiaMasivoViewModel tarifaPorDia = new TarifaPorDiaMasivoViewModel();            
            vm.tarifaPorDia = tarifaPorDia;

            return View(vm);
        }


        [HttpPost]
        public ActionResult EditarMasivo(EdicionTarifasPorDiaMasivaViewModel vm)
        {
            chequearPermisos(9,3);
            string resultado = "ok";
            TarifasPorDiaService tarifasPorDiaService = new TarifasPorDiaService(this.db);
            TarifasBaseService tarifasBaseService = new TarifasBaseService(this.db);
            HabitacionesService svHabitaciones = new HabitacionesService(this.db);
            TemporadasService temporadasService = null;
            //try
            //{
                //if (ModelState.IsValid)
                //{
                    int cantidadDisponible = 0;
                    List<Habitacione> habitaciones = null;
                    if (habitaciones != null)
                        cantidadDisponible = habitaciones.Count();

                    string[] idCanales = vm.canalesSeleccionados.Trim(',').Split(',');
                    string[] idsTiposHabitacion = vm.tiposHabitacionSeleccionados.Trim(',').Split(',');
                    string[] idCondicionesReservas = vm.condicionesReservasSeleccionados.Trim(',').Split(',');
                    string[] diasSemana = vm.diasSemanaSeleccionados.Trim(',').Split(',');
                    DateTime fecha, fechaDesde, fechaHasta;
                    TarifaPorDiaViewModel tarifaPorDiaVM;
                    tarifaPorDiaVM = new TarifaPorDiaViewModel();
                    TarifasPorDia tarifaPorDia = null;
                    TarifasBase tarifaBase = null;
                    Temporada temporada = null;

                    if (vm.desdeFecha != null && vm.hastaFecha != null)
                    {
                        fechaDesde = DateHelper.stringToDateTime(vm.desdeFecha);
                        fechaHasta = DateHelper.stringToDateTime(vm.hastaFecha);
                    }
                    else if (vm.idTemporada != null)
                        {
                            temporadasService = new TemporadasService(this.db);
                            temporada = temporadasService.getTemporada(vm.idTemporada.Value);
                            fechaHasta = temporada.fechaHasta;
                            fechaDesde = temporada.fechaDesde;
                        }
                    else if (vm.idFechaEspecial != null)
                    {
                        temporadasService = new TemporadasService(this.db);
                        temporada = temporadasService.getTemporada(vm.idFechaEspecial.Value);
                        fechaHasta = temporada.fechaHasta;
                        fechaDesde = temporada.fechaDesde;
                    }
                    else
                    {
                        //esta asignacion es para que no se ejecute el for, en el caso excepcional que se haga commit sin datos de fecha.
                        fechaDesde = DateTime.Now;
                        fechaHasta = DateTime.Now.AddDays(-1);
                    }

                    if (fechaHasta != null && fechaDesde != null)
                    {
                        for (int i = 0; i <= (fechaHasta - fechaDesde).TotalDays; i++)
                        {
                            fecha = fechaDesde.AddDays(i);

                            tarifaPorDiaVM.fecha = fecha;

                            if (Array.IndexOf(diasSemana, ((int)tarifaPorDiaVM.fecha.DayOfWeek).ToString()) >= 0)
                            {
                                foreach (string idCondicionReserva in idCondicionesReservas)
                                {
                                    if(idCondicionReserva == "-1")
                                        tarifaPorDiaVM.idCondicionReserva = null;
                                    else
                                        tarifaPorDiaVM.idCondicionReserva = Int32.Parse(idCondicionReserva);


                        
                                    foreach (string idCanal in idCanales)
                                    {
                                        tarifaPorDiaVM.idCanal = Int32.Parse(idCanal);
                                        foreach (string idTipoHabitacion in idsTiposHabitacion)
                                        {
                                            habitaciones = svHabitaciones.getHabitacionesPorTipoHabitacion(Int32.Parse(idTipoHabitacion));
                                            if (habitaciones != null)
                                                cantidadDisponible = habitaciones.Count();
                                            else
                                                cantidadDisponible = 0;

                                            tarifaPorDiaVM.idTipoHabitacion = Int32.Parse(idTipoHabitacion);

                                            tarifaPorDia = tarifasPorDiaService.get(tarifaPorDiaVM.idTipoHabitacion, tarifaPorDiaVM.idCanal, tarifaPorDiaVM.idCondicionReserva, tarifaPorDiaVM.fecha);
                                            if (tarifaPorDia == null)
                                                tarifaBase = tarifasBaseService.get(tarifaPorDiaVM.idTipoHabitacion, tarifaPorDiaVM.idCanal, tarifaPorDiaVM.idCondicionReserva);

                                            if (vm.tarifaPorDia.disponibilidadCanal != null && vm.tarifaPorDia.disponibilidadCanal != "")
                                                if (Int32.Parse(vm.tarifaPorDia.disponibilidadCanal) > cantidadDisponible)
                                                    tarifaPorDiaVM.disponibilidadCanal = cantidadDisponible.ToString();
                                                else
                                                    tarifaPorDiaVM.disponibilidadCanal = vm.tarifaPorDia.disponibilidadCanal;
                                            else if (tarifaPorDia != null && tarifaPorDia.disponibilidadCanal.HasValue)
                                                tarifaPorDiaVM.disponibilidadCanal = tarifaPorDia.disponibilidadCanal.Value.ToString();
                                            else if(tarifaBase != null && tarifaBase.disponibilidadCanal.HasValue)
                                                tarifaPorDiaVM.disponibilidadCanal = tarifaBase.disponibilidadCanal.Value.ToString();



                                            if ((vm.tarifaPorDia.tarifaBase == null || vm.tarifaPorDia.tarifaBase == "") && tarifaPorDia == null)
                                            {
                                                    switch ((int)tarifaPorDiaVM.fecha.DayOfWeek)
                                                    {
                                                        case 0:
                                                            if (tarifaBase.tarifaDomingo != null)
                                                                tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaBase.tarifaDomingo, new CultureInfo("en-US"));
                                                            break;
                                                        case 1:
                                                            if (tarifaBase.tarifaLunes != null)
                                                                tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaBase.tarifaLunes, new CultureInfo("en-US"));
                                                            break;
                                                        case 2:
                                                            if (tarifaBase.tarifaMartes != null)
                                                                tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaBase.tarifaMartes, new CultureInfo("en-US"));
                                                            break;
                                                        case 3:
                                                            if (tarifaBase.tarifaMiercoles != null)
                                                                tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaBase.tarifaMiercoles, new CultureInfo("en-US"));
                                                            break;
                                                        case 4:
                                                            if (tarifaBase.tarifaJueves != null)
                                                                tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaBase.tarifaJueves, new CultureInfo("en-US"));
                                                            break;
                                                        case 5:
                                                            if (tarifaBase.tarifaViernes != null)
                                                                tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaBase.tarifaViernes, new CultureInfo("en-US"));
                                                            break;
                                                        case 6:
                                                            if (tarifaBase.tarifaSabado != null)
                                                                tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaBase.tarifaSabado, new CultureInfo("en-US"));
                                                            break;
                                                    }

                                            }
                                            else if (vm.tarifaPorDia.tarifaBase != null && vm.tarifaPorDia.tarifaBase != "")
                                            {
                                                tarifaPorDiaVM.tarifaBase = vm.tarifaPorDia.tarifaBase;
                                            }
                                            else if (tarifaPorDia != null && tarifaPorDia.tarifaBase != null)
                                                tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaPorDia.tarifaBase, new CultureInfo("en-US"));


                                            //if (vm.tarifaPorDia.tarifaBase != "")
                                            //    tarifaPorDiaVM.tarifaBase = vm.tarifaPorDia.tarifaBase;
                                            //else
                                            //{
                                            //    if (tarifaPorDia != null && tarifaPorDia.tarifaBase != null)
                                            //        tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaPorDia.tarifaBase, new CultureInfo("en-US"));
                                            //    else
                                            //    {
                                            //        switch ((int)tarifaPorDiaVM.fecha.DayOfWeek)
                                            //        {
                                            //            case 0:
                                            //                if (tarifaBase.tarifaDomingo != null)
                                            //                    tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaBase.tarifaDomingo, new CultureInfo("en-US"));
                                            //                break;
                                            //            case 1:
                                            //                if (tarifaBase.tarifaLunes != null)
                                            //                    tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaBase.tarifaLunes, new CultureInfo("en-US"));
                                            //                break;
                                            //            case 2:
                                            //                if (tarifaBase.tarifaMartes != null)
                                            //                    tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaBase.tarifaMartes, new CultureInfo("en-US"));
                                            //                break;
                                            //            case 3:
                                            //                if (tarifaBase.tarifaMiercoles != null)
                                            //                    tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaBase.tarifaMiercoles, new CultureInfo("en-US"));
                                            //                break;
                                            //            case 4:
                                            //                if (tarifaBase.tarifaJueves != null)
                                            //                    tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaBase.tarifaJueves, new CultureInfo("en-US"));
                                            //                break;
                                            //            case 5:
                                            //                if (tarifaBase.tarifaViernes != null)
                                            //                    tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaBase.tarifaViernes, new CultureInfo("en-US"));
                                            //                break;
                                            //            case 6:
                                            //                if (tarifaBase.tarifaSabado != null)
                                            //                    tarifaPorDiaVM.tarifaBase = Convert.ToString(tarifaBase.tarifaSabado, new CultureInfo("en-US"));
                                            //                break;
                                            //        }
                                            //    }
                                            //}

                                            if (vm.tarifaPorDia.estadiaMinima != null && vm.tarifaPorDia.estadiaMinima != "")
                                                tarifaPorDiaVM.estadiaMinima = vm.tarifaPorDia.estadiaMinima;
                                            else
                                            {
                                                if (tarifaPorDia != null)
                                                    tarifaPorDiaVM.estadiaMinima = tarifaPorDia.estadiaMinima.Value.ToString();
                                                else if (tarifaBase.estadiaMinima.HasValue)
                                                    tarifaPorDiaVM.estadiaMinima = tarifaBase.estadiaMinima.Value.ToString();
                                            }

                                            if (vm.tarifaPorDia.descuentoPax != null)
                                                tarifaPorDiaVM.descuentoPax = vm.tarifaPorDia.descuentoPax;
                                            else
                                            {
                                                if (tarifaPorDia != null)
                                                    tarifaPorDiaVM.descuentoPax = Convert.ToString(tarifaPorDia.descuentoPorPax, new CultureInfo("en-US"));
                                                else if (tarifaBase.descuentoPax != null)
                                                    tarifaPorDiaVM.descuentoPax = Convert.ToString(tarifaBase.descuentoPax, new CultureInfo("en-US"));
                                            }

                                            if (vm.tarifaPorDia.adicionalCuna != null)
                                                tarifaPorDiaVM.adicionalCuna = vm.tarifaPorDia.adicionalCuna;
                                            else
                                            {
                                                if (tarifaPorDia != null)
                                                    tarifaPorDiaVM.adicionalCuna = Convert.ToString(tarifaPorDia.adicionalCuna, new CultureInfo("en-US"));
                                                else if (tarifaBase.adicionalCuna != null)
                                                    tarifaPorDiaVM.adicionalCuna = Convert.ToString(tarifaBase.adicionalCuna, new CultureInfo("en-US"));
                                            }

                                            if (vm.tarifaPorDia.adicionalMenor != null)
                                                tarifaPorDiaVM.adicionalMenor = vm.tarifaPorDia.adicionalMenor;
                                            else
                                            {
                                                if (tarifaPorDia != null)
                                                    tarifaPorDiaVM.adicionalMenor = Convert.ToString(tarifaPorDia.adicionalMenor, new CultureInfo("en-US"));
                                                else if (tarifaBase.adicionalMenor != null)
                                                    tarifaPorDiaVM.adicionalMenor = Convert.ToString(tarifaBase.adicionalMenor, new CultureInfo("en-US"));
                                            }

                                            if (vm.tarifaPorDia.adicionalAdulto != null)
                                                tarifaPorDiaVM.adicionalAdulto = vm.tarifaPorDia.adicionalAdulto;
                                            else
                                            {
                                                if (tarifaPorDia != null)
                                                    tarifaPorDiaVM.adicionalAdulto = Convert.ToString(tarifaPorDia.adicionalAdulto, new CultureInfo("en-US"));
                                                else if (tarifaBase.adicionalAdulto != null)
                                                    tarifaPorDiaVM.adicionalAdulto = Convert.ToString(tarifaBase.adicionalAdulto, new CultureInfo("en-US"));
                                            }

                                            if (vm.tarifaPorDia.permiteIngreso != null)
                                                tarifaPorDiaVM.permiteIngreso = Convert.ToBoolean(vm.tarifaPorDia.permiteIngreso);
                                            else
                                            {
                                                if (tarifaPorDia != null)
                                                    tarifaPorDiaVM.permiteIngreso = Convert.ToBoolean(tarifaPorDia.permiteIngreso);
                                                else if (tarifaBase.permiteIngreso != null)
                                                    tarifaPorDiaVM.permiteIngreso = Convert.ToBoolean(tarifaBase.permiteIngreso);
                                            }

                                            if (vm.tarifaPorDia.permiteEgreso != null)
                                                tarifaPorDiaVM.permiteEgreso = Convert.ToBoolean(vm.tarifaPorDia.permiteEgreso);
                                            else
                                            {
                                                if (tarifaPorDia != null)
                                                    tarifaPorDiaVM.permiteEgreso = Convert.ToBoolean(tarifaPorDia.permiteEgreso);
                                                else if (tarifaBase.permiteEgreso != null)
                                                    tarifaPorDiaVM.permiteEgreso = Convert.ToBoolean(tarifaBase.permiteEgreso);
                                            }

                                            if (vm.tarifaPorDia.aceptaMenores != null)
                                                tarifaPorDiaVM.aceptaMenores = Convert.ToBoolean(vm.tarifaPorDia.aceptaMenores);
                                            else
                                            {
                                                if (tarifaPorDia != null)
                                                    tarifaPorDiaVM.aceptaMenores = Convert.ToBoolean(tarifaPorDia.aceptaMenores);
                                                else if (tarifaBase.aceptaMenores != null)
                                                    tarifaPorDiaVM.aceptaMenores = Convert.ToBoolean(tarifaBase.aceptaMenores);
                                            }

                                            if (vm.tarifaPorDia.abierta != null)
                                                tarifaPorDiaVM.abierta = Convert.ToBoolean(vm.tarifaPorDia.abierta);
                                            else
                                            {
                                                if (tarifaPorDia != null)
                                                    tarifaPorDiaVM.abierta = Convert.ToBoolean(tarifaPorDia.abierta);
                                                else if (tarifaBase.aceptaMenores != null)
                                                    tarifaPorDiaVM.abierta = Convert.ToBoolean(tarifaBase.abierta);
                                            }

                                            tarifasPorDiaService.editar(tarifaPorDiaVM);
                                        }

                                    }
                                }
                            }

                        }

                    }

                //}
            //}
            //catch (Exception)
            //{
            //    resultado = "error";
            //}
            return RedirectToAction("index", "TarifasPorDia", new { resultado = resultado });
        }

    }
}