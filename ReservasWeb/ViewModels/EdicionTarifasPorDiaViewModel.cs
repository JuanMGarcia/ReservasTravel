﻿using ReservasWeb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.ViewModels
{
    public class EdicionTarifasPorDiaViewModel : BaseViewModel
    {
        [Display(Name = "Habitacion")]
        public SelectList tiposHabitacion { get; set; }
        
        [Display(Name = "Canal")]
        public SelectList canales { get; set; }

        [Display(Name = "Condicion Reserva")]
        public List<SelectListItem> condicionesReservas { get; set; }

        public String fechaDesde { get; set; }
        public String diasEditados { get; set; }

        public List<TarifaPorDiaViewModel> tarifaPorDiaListado { get; set; }

        public int idTipoHabitacion { get; set; }
        public int idCanal { get; set; }
        public int idCondicionReserva { get; set; }

//agregado desde tipohabitacion, se mueve tarifas base a tarifas por dia.
        public List<TarifaBaseViewModel> tarifaBaseListado { get; set; }
        public List<SelectListItem> condicionesReservaListado { get; set; }

    }
}