﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ReservasWeb.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Campo requerido")]
        public string usuario { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        public string clave { get; set; }
    }
}