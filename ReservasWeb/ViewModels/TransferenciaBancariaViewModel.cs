﻿using ReservasWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.ViewModels
{
    public class TransferenciaBancariaViewModel : BaseBusquedaViewModel
    {
        public List<HabitacionesPorReserva> listaHabPorRsv { get; set; }
    }
}