﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.ViewModels
{
    public class EdicionHabitacionesViewModel : BaseViewModel
    {
        public int idHabitacion { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [System.Web.Mvc.Remote("VerificarNombreHabitacion", "Habitaciones", AdditionalFields = "idHabitacion", HttpMethod = "POST", ErrorMessage = "El nombre ya existe")]
        [Display(Name = "Nombre")]
        public string nombre { get; set; }
        [Display(Name = "Descripción")]
        public string descripcion { get; set; }
        public SelectList listaEstados { get; set; }
        public SelectList listaTiposHabitacion { get; set; }
        [Display(Name = "Estado")]
        public int idEstado { get; set; }
        [Display(Name = "Tipo de Habitación")]
        public int idTipoHabitacion { get; set; }
        [Display(Name = "Disponible Web")]
        public bool disponibleAbonoHabitacion { get; set; }
    }
}