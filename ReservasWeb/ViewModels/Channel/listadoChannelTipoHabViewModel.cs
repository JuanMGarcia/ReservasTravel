﻿using ReservasWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.ViewModels.Channel
{
    public class listadoChannelTipoHabViewModel: BaseViewModel
    {
        public List<TiposHabitacione> listado { get; set; }
        public List<int> linkList { get; set; }
        public SelectList comboTipoHab { get; set; }
    }
}


