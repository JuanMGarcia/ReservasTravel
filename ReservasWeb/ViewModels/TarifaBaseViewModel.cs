﻿using ExpressiveAnnotations.Attributes;
using ReservasWeb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.ViewModels
{
    public class TarifaBaseViewModel : BaseViewModel
    {
        public int idTipoHabitacion { get; set; }
        public int idCanal { get; set; }
        public string nombreCanalEs { get; set; }
        public string nombreCanalEn { get; set; }
        public string nombreCanalPt { get; set; }

        //[RequiredIf("abierta == true & idCanal == -1",
        //ErrorMessage = "Requerido")]
        //[Required(ErrorMessage = "Requerido")]
        [Display(Name = "Lunes")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        public string tarifaLunes { get; set; }

        //[RequiredIf("abierta == true & idCanal == -1")]
        //[Required(ErrorMessage = "Requerido")]
        [Display(Name = "Martes")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        public string tarifaMartes { get; set; }

        //[RequiredIf("abierta == true & idCanal == -1")]
        //[Required(ErrorMessage = "Requerido")]
        [Display(Name = "Miercoles")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        public string tarifaMiercoles { get; set; }

        //[RequiredIf("abierta == true & idCanal == -1")]
        //[Required(ErrorMessage = "Requerido")]
        [Display(Name = "Jueves")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        public string tarifaJueves { get; set; }

        //[RequiredIf("abierta == true & idCanal == -1")]
        //[Required(ErrorMessage = "Requerido")]
        [Display(Name = "Viernes")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        public string tarifaViernes { get; set; }

        //[RequiredIf("abierta == true & idCanal == -1")]
        //[Required(ErrorMessage = "Requerido")]
        [Display(Name = "Sabado")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        public string tarifaSabado { get; set; }

        //[RequiredIf("abierta == true & idCanal == -1")]
        //[Required(ErrorMessage = "Requerido")]
        [Display(Name = "Domingo")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        public string tarifaDomingo { get; set; }

        //descuento por pasajero que falte del maximo de la habitacion
        [Display(Name = "Descuento Por Pax")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        public string descuentoPax { get; set; }

        [Display(Name = "Adicional Cuna")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        public string adicionalCuna { get; set; }

        [Display(Name = "Adicional Menor")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        public string adicionalMenor { get; set; }

        [Display(Name = "Adicional Adulto")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        public string adicionalAdulto { get; set; }

        [Display(Name = "Estadia Mínima")]
        public string estadiaMinima { get; set; }

        [Display(Name = "Permite Ingreso")]
        public bool permiteIngreso { get; set; }

        [Display(Name = "Permite Egreso")]
        public bool permiteEgreso { get; set; }

        [Display(Name = "Acepta Menores")]
        public bool aceptaMenores { get; set; }

        [Display(Name = "Abierta")]
        public bool abierta { get; set; }

        [Display(Name = "Disponibilidad para Canal")]
        public string disponibilidadCanal { get; set; } 
    }
}