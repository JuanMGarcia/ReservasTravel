﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.ComponentModel.DataAnnotations;

namespace ReservasWeb.ViewModels
{
    public class ListadoTemporadasViewModel : BaseViewModel
    {
        [Display(Name = "Temporadas")]
        public List<Temporada> listadoTemporadas { get; set; }

        [Display(Name = "Fechas Especiales")]
        public List<Temporada> listadoFechasEspeciales { get; set; }
    }
}