﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.ViewModels
{
    public class MercadoPagoViewModel : BaseViewModel
    {
        public string clientStatus { get; set; }
        public string clientSecret { get; set; }
        public Boolean standBy { get; set; }
        public DateTime fechaCreacion { get; set; }
        public DateTime fechaModificacion { get; set; }
        public string clientId { get; set; }
        public int idAlojamiento { get; set; }
        public int idMercadoPago { get; set; }
        public int tipo { get; set; }


    }
}