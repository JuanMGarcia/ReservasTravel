﻿using ReservasWeb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.ViewModels
{
    public class EdicionTipoHabitacionViewModel : BaseViewModel
    {
        public int idTipoHabitacion { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Nombre")]        
        public string nombreEs { get; set; }
        [Display(Name = "Nombre Ingles")]
        public string nombreEn { get; set; }
        [Display(Name = "Nombre Portugues")]
        public string nombrePt { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Plazas")]
        [Range(1, 40, ErrorMessage ="Seleccione cantidad de camas")]
        public int plazas { get; set; }

        [Display(Name = "Camas Matrimoniales")]        
        public int camaMatrimonial { get; set; }

        [Display(Name = "Camas Simples")]        
        public int camasSimples { get; set; }

        [Display(Name = "Hora Checkout Tardío")]        
        public string horaCheckOutTardio { get; set; }

        [Display(Name = "Permite Camas Adicionales")]
        public int permiteCamasAdicionales { get; set; }

        [Display(Name = "Camas Adicionales")]
        public int camasAdicionales { get; set; }

        [Display(Name = "Descripción")]
        public String descripcionEs { get; set; }
        [Display(Name = "Descripción Ingles")]
        public String descripcionEn { get; set; }
        [Display(Name = "Descripción Portugues")]
        public String descripcionPt { get; set; }

        [Display(Name = "Habitación Compartida")]
        public bool compartida { get; set; }
        
        [Display(Name = "Bebés en cunas")]
        public int permiteAdicionalCunasBebes { get; set; }

        [Display(Name = "Niños")]
        public int permiteAdicionalMenores { get; set; }
        
        [Display(Name = "Adultos")]
        public int permiteAdicionalAdultos { get; set; }
        
        //public SelectList tiposSelectList { get; set; }
        public List<SelectListItem> plazasSelectList { get; set; }
        public List<SelectListItem> camasSimpleMatrimonialSelectList { get; set; }
        public SelectList permiteCamasAdicionalesSelectList { get; set; }
        public List<SelectListItem> camasAdicionalesSelectList { get; set; }
        public SelectList permiteAdicionalesBebeSelectList { get; set; }
        public SelectList permiteAdicionalesMenorSelectList { get; set; }
        public SelectList permiteAdicionalesAdultoSelectList { get; set; }

        public List<TarifaBaseViewModel> tarifaBaseListado { get; set; }
        //public Dictionary<int,List<TarifaBaseViewModel>> tarifaBaseListado { get; set; }
        public List<HabitacionViewModel> habitacionesListado { get; set; }
        public List<ComodidadSeleccionada> listaComodidades { get; set; }
        public List<SelectListItem> condicionesReservaListado { get; set; }
        public List<Imagene> fotos { get; set; }
        public string IdsFotosOrden { get; set; }
        public string GetIdsFotosOrden()
        {
            string idsFotos = "";
            for (int i = 0; i < fotos.Count(); i++)
            {
                if (i == 0)
                    idsFotos = fotos.ElementAt(i).idImagen.ToString();
                else
                    idsFotos = idsFotos + "," + fotos.ElementAt(i).idImagen.ToString();
            }
            return idsFotos;
        }
    }
}