﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.ViewModels
{
    public class CreacionEdicionReservaGrilla : BaseViewModel
    {
        public int idReserva { get; set; }

        public int idTipoPago { get; set; }
        public string nombreTipoPago { get; set; }
        public int idTipoPagoEnHotel { get; set; }
        public List<int> idTipoHabitacion { get; set; }
        [Required(ErrorMessage = "Requerido")]
        public List<int> idHabitacion { get; set; }
        public string idHabitacionPrevio { get; set; }
        [Required(ErrorMessage = "Requerido")]
        public List<int?> idCondicion { get; set; }
        public string idCondicionPrevio { get; set; }
        public string apellido { get; set; }
        public string nombre { get; set; }
        public string email { get; set; }
        public string dni { get; set; }
        public string habitacion { get; set; }
        public int pasajeros { get; set; }
        [Required(ErrorMessage = "Requerido")]
        public string fechain { get; set; }
        [Required(ErrorMessage = "Requerido")]
        public string fechaout { get; set; }
        //public string fechainPrevio { get; set; }
        //public string fechaoutPrevio { get; set; }
        public bool esMostrador { get; set; }
        public int idCanal { get; set; }
        public int idCanalWeb { get; set; }
        public int idCanalMostrador { get; set; }
        [Required(ErrorMessage = "Requerido")]
        public int idEstado { get; set; }
        public SelectList ListaEstados { get; set; }
        public SelectList paises { get; set; }
        public SelectList tiposHabitaciones { get; set; }
        public SelectList tiposPagoEnHotel { get; set; }
        public SelectList eligeTiposPago { get; set; }
        public List<List<SelectListItem>> condiciones { get; set; }
        public List<SelectList> habitaciones { get; set; }
        [Required(ErrorMessage = "Requerido")]
        public int idPais { get; set; }
        public string provincia { get; set; }
        public string ciudad { get; set; }
        public string observaciones { get; set; }
        public string comentarios { get; set; }
        public string telefono { get; set; }
        public string celular { get; set; }
        public List<HistorialReservaViewModel> historialListado { get; set; }
        public bool cambiaronHabitaciones { get; set; }
        public string tarjetaTitular { get; set; }
        public string tarjetaNro { get; set; }
        public string tarjetaCvc { get; set; }
        public string tarjetaFecha { get; set; }
        public string precioSubTotal { get; set; }
        public string precioTotal { get; set; }
        public string impuesto { get; set; }
        public string simboloMoneda { get; set; }
        public string checkIn { get; set; }
        public string checkOut { get; set; }

        public string direccion { get; set; }

        ///Transferencia Bancaria
        [Display(Name = "Número de Cuenta")]
        public string tbNumeroCuenta { get; set; }
        [Display(Name = "Numero de Sucursal")]
        public string tbSucursal { get; set; }
        [Display(Name = "Monto Transferido")]
        public decimal mtbMnto { get; set; }
        [Display(Name = "Número de Operación")]
        public string tbNumeroOperacion { get; set; }
        [Display(Name = "Imagen del Comprobante(Opcional)")]
        public string tbImagenComprobante { get; set; }
        [Display(Name = "Número de Comprobante")]
        public int tbIdComprobante { get; set; }
        [Display(Name = "Observaciones")]

        public DateTime tbFecha { get; set; }
        public string tbBanco { get; set; }
    }
}