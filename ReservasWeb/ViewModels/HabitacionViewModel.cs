﻿using System.ComponentModel.DataAnnotations;

namespace ReservasWeb.ViewModels
{
    public class HabitacionViewModel : BaseViewModel
    {
        public int idHabitacion { get; set; }
        public int idTipoHabitacion { get; set; }

        [Display(Name = "Nombre")]
        public string nombre { get; set; }
        
        [Display(Name = "Descripción")]
        public string descripcion { get; set; }

        [Display(Name = "Estado")]
        public string estado { get; set; }

        [Display(Name = "Disponible Web/Mostrador")]
        public bool disponibleAbonoHabitacion { get; set; }
    }
}