﻿using ReservasWeb.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.ViewModels
{
    public class BaseViewModel
    {
        private const string RESULTADO_OK = "ok";
        private const string RESULTADO_ERROR = "error";
        private const string RESULTADO_ERROR_HTML = "error_html";
        private const string RESULTADO_ERROR_HABITACIONES = "error_habitaciones";
        //private const string RESULTADO_ERROR_HABITACION_DUPLICADA = "error_habitacion_duplicada";
        private const string RESULTADO_ERROR_RESERVAS = "error_reservas";
        private const string RESULTADO_ERROR_ABONO_HABITACIONES = "error_abono_habitaciones";
        private const string RESULTADO_ERROR_CONFIGURACION_PAGO = "error_conf_pago";
        public string MensajeExito { get; set; }
        public string MensajeError { get; set; }
        public void ProcesarResultado(string resultado)
        {
            switch (resultado)
            {
                case RESULTADO_OK:
                    MensajeExito = "Acción procesada correctamente";
                    break;
                case RESULTADO_ERROR:
                    MensajeError = "Se produjo un error";
                    break;
                case RESULTADO_ERROR_HTML:
                    MensajeError = "No puede instroducirse etiquetas HTML en los campos de texto";
                    break;
                case RESULTADO_ERROR_HABITACIONES:
                    MensajeError = "No puede eliminarse, porque existen habitaciones relacionadas";
                    break;
                //case RESULTADO_ERROR_HABITACION_DUPLICADA:
                //    MensajeError = "Ya existe una habitación con ese nombre, para el tipo de habitación seleccionado";
                //    break;
                case RESULTADO_ERROR_RESERVAS:
                    MensajeError = "No puede eliminarse, porque existen reservas relacionadas";
                    break;
                case RESULTADO_ERROR_ABONO_HABITACIONES:
                    MensajeError = "Se alcanzó el máximo de habitaciones para el abono adquirido";
                    break;
                case RESULTADO_ERROR_CONFIGURACION_PAGO:
                    MensajeError = "Se debe configurar antes el medio de pago.";
                    break;
            }
        }
    }
}