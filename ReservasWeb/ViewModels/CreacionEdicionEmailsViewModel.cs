﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.ViewModels
{
    public class CreacionEdicionEmailsViewModel : BaseViewModel
    {
        public EmailViewModel email { get; set; }
        public bool modoEdicion = false;
    }
}