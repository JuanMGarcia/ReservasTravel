﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.ViewModels
{
    public class MPResultadoViewModel : BaseViewModel
    {
        public string collection_id { get; set; }
        public string collection_status { get; set; }
        public string preference_id { get; set; }
        public string external_reference { get; set; }
        public string payment_type { get; set; }
        public string merchant_order_id { get; set; }
        public string estado { get; set;}
    }
}