﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.ViewModels
{
    public class ListadoCondicionesReservaViewModel : BaseViewModel
    {
        public List<CondicionesReservaViewModel> listado { get; set; }        
    }
}