﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;

namespace ReservasWeb.ViewModels
{
    public class ListadoHabitacionesViewModel : BaseViewModel
    {
        public int cantidadDisponiblesWebMostrador { get; set; }
        public int cantidadMaximaWebMostrador { get; set; }
        public List<Habitacione> listado { get; set; }
    }
}