﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace ReservasWeb.ViewModels
{
    public class CuentaBancariaViewModel : BaseViewModel
    {
        public int idCuentaBancaria { get; set; }
        [Display(Name = "Nombre del Banco")]
        public string nombreBanco { get; set; }
        [Display(Name = "Tipo de Cuenta")]
        public string tipoCuenta { get; set; }
        [Display(Name = "Titular")]
        public string titular { get; set; }
        [Display(Name = "Número de Cuenta")]
        public string numeroCuenta { get; set; }
        [Display(Name = "CBU")]
        public string CBU { get; set; }
        [Display(Name = "Cuit")]
        public string CUIT { get; set; }
        [Display(Name = "Número de Sucursal")]
        public string sucursal { get; set; }

    }
}