﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using System.Web;

namespace ReservasWeb.ViewModels
{
    public class GarantiaViewModel : BaseBusquedaViewModel
    {
        public string tarjetaCredito { get; set; }
        [Display(Name ="Tipo")]
        public SelectList listaTarjetaCredito { get; set; }
        [Display(Name="Número de Tarjeta")]
        public string nroTarjeta { get; set; }
        [Display(Name ="Titular de la Tarjeta")]
        public string titularTarjeta { get; set; }
        [Display(Name ="Fecha(MES/AÑO)")]
        public string mesTarjeta { get; set; }
        public string anioTarjeta { get; set; }
        [Display(Name = "CVC")]
        public string cvcTarjeta { get; set; }
        //public int cantPasajeros { get; set; }
        //public string ninios { get; set; }
        //public DateTime fechaEntrada { get; set; }
        //public DateTime fechaSalida { get; set; }
    }
}