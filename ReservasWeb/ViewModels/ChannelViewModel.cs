﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.ViewModels
{
    public class ChannelViewModel:BaseViewModel
    {
        public string texto { get; set; }
        public bool linkeado { get; set; }
        public int idEstado { get; set; } 
    }
}