﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ReservasWeb.Models;
using ReservasWeb.Utils;

namespace ReservasWeb.ViewModels
{
    public class PrereservaViewModel : BaseBusquedaViewModel
    {
        public PrereservaViewModel()
        {
            this.listado = new List<TiposHabitacione>();

        }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Ingrese su Nombre")]
        public string nombre { get; set; }
        [Display(Name = "Apellido")]
        [Required(ErrorMessage = "Ingrese su Apellido")]
        public string apellido { get; set; }
        [Display(Name = "Tipo de Documento")]
        public string tipoDocumento { get; set; }
        [Display(Name = "Número de Documento")]
        [Required(ErrorMessage = "Ingrese su Nro de documento")]
        public string nroDocumento { get; set; }
        [Display(Name = "Teléfono")]
        public string telefono { get; set; }
        [Display(Name = "Celular")]
        public string celular { get; set; }
        [Display(Name = "Correo Electronico")]
        [Required(ErrorMessage = "Ingrese su email")]
        public string email { get; set; }
        [Display(Name = "Domicilio")]
        public String direccion { get; set; }
        public SelectList listaTipo { get; set; }
        [Display(Name ="Tipo y Número de Documento")]
        public SelectList listaTipoDocumento { get; set; }
        [Display(Name = "Razón Social")]
        public string razonSocial { get; set; }
        [Display(Name = "Responsable")]
        public string responsable { get; set; }
        [Display(Name = "Email de la Empresa")]
        public string emailAdmin { get; set; }
        [Display(Name = "Sitio Web")]
        public string url { get; set; }
        [Display(Name = "CUIT")]
        public string cuit { get; set; }
        [Display(Name = "Fax")]
        public string fax { get; set; }
        public SelectList listaTipoPago { get; set; }
        [Display(Name ="Tipo")]
        public string tipo { get; set; }
        [Display(Name = "Tipo de Pago")]
        public int tipopago { get; set; }
 
        [Display(Name ="País")]
        public int idPais { get; set; }
        [Display(Name ="Elija el país")]
        public SelectList listaPais { get; set; }
        [Display(Name ="Provincia")]
        public string provincia { get; set; }
        [Display(Name = "Ciudad")]
        public string ciudad { get; set; }

        public string tarjetaCredito { get; set; }
        [Display(Name = "Tipo")]
        public SelectList listaTarjetaCredito { get; set; }
        [Display(Name = "Número de Tarjeta")]
  //      [Required(ErrorMessage = "Ingrese su nro de tarjeta")]
        //[RegularExpression(@"^\S*$", ErrorMessage = "No puede contener espacios")]
        [System.Web.Mvc.Remote("ValidarTarjeta", "Busqueda", HttpMethod = "POST", ErrorMessage = "Número de Tarjeta Incorrecto")]

        public string nroTarjeta { get; set; }
        [Display(Name = "Titular de la Tarjeta")]
//        [Required(ErrorMessage = "Ingrese Titular de la Tarjeta")]
        public string titularTarjeta { get; set; }
        [Display(Name = "Fecha(MES/AÑO)")]
//        [Required(ErrorMessage = "Ingrese Mes")]
        public string mesTarjeta { get; set; }
 //       [Required(ErrorMessage = "Ingrese Año")]
        public string anioTarjeta { get; set; }
        [Display(Name = "CVC")]
 //       [Required(ErrorMessage = "Ingrese CVC")]
        public string cvcTarjeta { get; set; }
        public string cantidadpersonas { get; set; }
        public string checkin { get; set; }
        public string checkout { get; set; }
        public string noches { get; set; }
 //       public int habitaciones { get; set; }
        public List<TiposHabitacione> listado { get; set; }
        public List<decimal> listadoTarifa { get; set; }
        public List<AdicionalItem> listadoTarifaAdulto { get; set; }
        public List<AdicionalItem> listadoTarifaNino { get; set; }
        //public decimal impuesto { get; set; }
//        public string linkMP { get; set; }
//        public DateTime fechaEntrada { get; set; }
//        public DateTime fechaSalida { get; set; }
//        public int cantPasajeros { get; set; }
//        public string ninios { get; set; }
     //   public string imgHotelmini { get; set; }
        public string Comodidades { get; set; }
        public List<int> listadoElige { get; set; }
        public List<int> listadoCondicionesPorHab { get; set; }
        public List<int> listadoEligeAdulto { get; set; }
        public List<int> listadoEligeNino { get; set; }
        public bool tieneAdulto { get; set; }
        public bool tieneNino { get; set; }
        public string simboloMoneda { get; set; }
    }
}