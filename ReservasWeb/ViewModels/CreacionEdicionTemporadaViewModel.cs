﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.ViewModels
{
    public class CreacionEdicionTemporadaViewModel : BaseViewModel
    {
        public int idTemporada { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Nombre")]
        public string nombre { get; set; }

        [Display(Name = "Descripción")]
        public string descripcion { get; set; }

        [Display(Name = "Tipo")]
        [Required(ErrorMessage = "Campo requerido")]
        public string tipo { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Fecha Desde")]
        public String fechaDesde { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Fecha Hasta")]
        public String fechaHasta { get; set; }

        [Display(Name = "Color")]
        public String cssColor { get; set; }

        public SelectList tiposSelectList { get; set; }
        public List<SelectListItem> cssColorSelectList { get; set; }
    }
}