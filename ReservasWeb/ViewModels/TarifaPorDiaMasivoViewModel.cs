﻿using ExpressiveAnnotations.Attributes;
using ReservasWeb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.ViewModels
{
    public class TarifaPorDiaMasivoViewModel : BaseViewModel
    {
        public int idTipoHabitacion { get; set; }
        public int idCanal { get; set; }
        public string nombreCanal { get; set; }

        public DateTime fecha { get; set; }

        [Display(Name = "Tarifa Base")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Base")]
        public string tarifaBase { get; set; }

        //descuento por pasajero que falte del maximo de la habitacion
        [Display(Name = "Descuento Por Pax")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Pax")]
        public string descuentoPax { get; set; }

        [Display(Name = "Adicional Cuna")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Cuna")]
        public string adicionalCuna { get; set; }

        [Display(Name = "Adicional Menor")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Menor")]
        public string adicionalMenor { get; set; }

        [Display(Name = "Adicional Adulto")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tarifa inválida")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Adulto")]
        public string adicionalAdulto { get; set; }

        [Display(Name = "Estadia Mínima")]
        public string estadiaMinima { get; set;}

        [Display(Name = "Disponibilidad por Canal")]
        public string disponibilidadCanal { get; set; }

        [Display(Name = "Disponibilidad Real")]
        public string disponibilidadReal { get; set; } 

        [Display(Name = "Permite Ingreso")]
        public int? permiteIngreso { get; set; }

        [Display(Name = "Permite Egreso")]
        public int? permiteEgreso { get; set; }

        [Display(Name = "Acepta Menores")]
        public int? aceptaMenores { get; set; }

        [Display(Name = "Abierta")]
        public int? abierta { get; set; }        

    }
}