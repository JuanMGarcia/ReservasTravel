﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web;
using ReservasWeb.Models;
using ReservasWeb.Utils;

using System.Web;

namespace ReservasWeb.ViewModels
{
    public class CargarComprobanteViewModel : BaseBusquedaViewModel
    {
        [Display(Name ="Número de Cuenta")]
        public string numeroCuenta { get; set; }
        [Display(Name ="Numero de Sucursal")]
        public string sucursal { get; set; }
        [Display(Name ="Monto Transferido")]
        public decimal monto { get; set; }
        [Display(Name ="Número de Operación")]
        public string numeroOperacion { get; set; }
        [Display(Name ="Imagen del Comprobante(Opcional)")]
        public string imagenComprobante { get; set; }
        [Display(Name ="Número de Comprobante")]
        public int idComprobante { get; set; }
        [Display(Name = "Observaciones")]
        public string observaciones { get; set; }

        public DateTime fecha { get; set; }

        public string cantidadpersonas { get; set; }
        public string checkin { get; set; }
        public string checkout { get; set; }
        public string noches { get; set; }
        //       public int habitaciones { get; set; }
        public List<TiposHabitacione> listado { get; set; }
        public List<decimal> listadoTarifa { get; set; }
        public List<AdicionalItem> listadoTarifaAdulto { get; set; }
        public List<AdicionalItem> listadoTarifaNino { get; set; }
        //public decimal impuesto { get; set; }
        //        public string linkMP { get; set; }
        //        public DateTime fechaEntrada { get; set; }
        //        public DateTime fechaSalida { get; set; }
        //        public int cantPasajeros { get; set; }
        //        public string ninios { get; set; }
        //   public string imgHotelmini { get; set; }
        public string Comodidades { get; set; }
        public List<int> listadoElige { get; set; }
        public List<int> listadoEligeAdulto { get; set; }
        public List<int> listadoEligeNino { get; set; }
        public bool tieneAdulto { get; set; }
        public bool tieneNino { get; set; }
        public string simboloMoneda { get; set; }
        public int idReserva { get; set; }


    }
}