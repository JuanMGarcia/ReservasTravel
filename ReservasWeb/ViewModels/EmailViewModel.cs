﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using System.Web;

namespace ReservasWeb.ViewModels
{
    public class EmailViewModel : BaseViewModel
    {
        public int idEmail { get; set; }
        public int idAlojamiento { get; set; }
        //ID del mensaje dentro del servidor de email
        public string idMensaje { get; set; }
        [Display(Name = "De")]
        public string de { get; set; }
        [Display(Name = "Para")]
        public string para { get; set; }
        [Display(Name = "CC")]
        public string cc { get; set; }
        [Display(Name = "CCO")]
        public string cco { get; set; }
        [Display(Name = "Asunto")]
        public string asunto { get; set; }
        [Display(Name = "Cuerpo")]
        public string cuerpo { get; set; }
    }
}