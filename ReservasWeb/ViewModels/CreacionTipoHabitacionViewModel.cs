﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReservasWeb.ViewModels
{
    public class CreacionTipoHabitacionViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Nombre")]
        public string nombre { get; set; }
    }
}