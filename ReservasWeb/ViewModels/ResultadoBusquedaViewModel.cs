﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReservasWeb.Models;
using System.ComponentModel.DataAnnotations;
using ReservasWeb.Utils;

namespace ReservasWeb.ViewModels
{
    public class ResultadoBusquedaViewModel : BaseBusquedaViewModel
    {
        public List<string> oferta;
        public List<OfertaItem> ofertaBooking;
        public string totalOferta;
        public string impuestoOferta;

        public ResultadoBusquedaViewModel()
        {
            this.listado = new List<TiposHabitacione>();
            this.combina = new List<TiposHabitacione>();
        }

        [Display(Name = "Descripcion")]
        public string descripcionHotel { get; set; }
        [Display(Name = "Sitio Web")]
        public string sitioWeb { get; set; }
        public List<TiposHabitacione> listado { get; set; }
        public List<decimal> listadoTarifa { get; set; }
        public List<AdicionalItem> listadoTarifaAdulto { get; set; }
        public List<AdicionalItem> listadoTarifaNino { get; set; }
        public List<int> listadoCant {get; set;} 
        public List<int> listadoCantAdulto { get; set; }
        public List<int> listadoCantNino { get; set; }
        public List<string> listarsv { get; set; }
        public List<string> listarsvdes { get; set; }
        public List<string> listapoli { get; set; }  
        public List<string> listapolides { get; set; } 
        public List<int> listadoElige { get; set; }      
        public List<int> listadoEligeAdulto { get; set; }
        public List<int> listadoEligeNino { get; set; } 
        [Display(Name = "Fecha de Entrada")]    
        public DateTime fechaIni { get; set; }
        [Display(Name = "Fecha de Salida")]
        public DateTime fechaFin { get; set; }
        [Display(Name = "Pasajeros")]
        public int pasajeros { get; set; }
        [Display(Name = "Cantidad de Pasajeros")]
        public int cantidadHab {get;set;}
        public string idioma { get; set; }
        public int habitaciones { get; set; }
        public string urlimagenhotel { get; set; } 
        public string checkin { get; set; }
        public string checkout { get; set; }

        public string noches { get; set; }

        public string cantidadpersonas { get; set; }
        public string simboloMoneda { get; set; }

        public List<CondReserva> listaCondiciones { get; set; }
        //
        public List<TiposHabitacione> combina { get; set; }
        public List<int> combinaElige { get; set; }
        public List<int> combinaEligeAdulto { get; set; }
        public List<int> combinaEligeNino { get; set; }

        public int contadorSegundos { get; set; }
        public List<int> listadoCondicionesPorHab { get; set; }


    }
}