﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace ReservasWeb.ViewModels
{
    public class ListadoItemsCuentaPorReservaViewModel : BaseViewModel
    {
        [Display(Name = "Items Cuenta")]
        public List<CreacionEdicionItemCuentaPorReservaViewModel> listadoItemsCuenta { get; set; }
        public bool cambiaronCuentas { get; set; }
        public SelectList listaTipodePago { get; set; }
        public int idTipoPago { get; set; }
        public int idReserva { get; set; }
        public decimal saldo { get; set; }
        public string reservaFecha { get; set; }
        public decimal reservaMonto { get; set; }
        public string simboloMoneda { get; set; }
    }
}