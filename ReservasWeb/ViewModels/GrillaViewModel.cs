﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReservasWeb.Models;

namespace ReservasWeb.ViewModels
{
    public class GrillaViewModel : BaseViewModel
    {
        /*public string habitaciones { get; set; }
        public string reservas { get; set; }
        public SelectList tiposHabitacion { get; set; }*/
        public int idReserva { get; set; }
        public List<TiposHabitacione> tiposHabitacion { get; set; }

        public List<int> canalesHabilitados { get; set; }

        
    }
}