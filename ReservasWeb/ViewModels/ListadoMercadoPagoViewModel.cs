﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;

namespace ReservasWeb.ViewModels
{
    public class ListadoMercadoPagoViewModel : BaseViewModel
    {
        public List<TiposPagoConfiguracion> listado { get; set; }
    }
}