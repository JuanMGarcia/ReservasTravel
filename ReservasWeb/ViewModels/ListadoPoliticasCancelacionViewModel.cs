﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.ViewModels
{
    public class ListadoPoliticasCancelacionViewModel : BaseViewModel
    {
        public List<PoliticasCancelacionViewModel> listado { get; set; }
    }
}