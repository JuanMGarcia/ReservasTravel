﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReservasWeb.Models;

namespace ReservasWeb.ViewModels
{
    public class HojaBuscarViewModel:BaseViewModel
    {
        [Display(Name = "Reserva")]
        public int idReserva { get; set; }
        [Display(Name = "Alojamiento")]
        public int idAlojamiento { get; set; }
        [Display(Name = "Tipo de Pago")]
        public int idTipoPago { get; set; }
        [Display(Name = "Huesped:")]
        public int idHuesped { get; set; }
        [Display(Name = "Check In:")]
        public DateTime checkIn { get; set; }
        [Display(Name = "Check Out:")]
        public DateTime checkOut { get; set; }
        [Display(Name = "Entrada:")]
        public DateTime fechaEntrada { get; set; }
        [Display(Name = "Salida:")]
        public DateTime fechaSalida { get; set; }
        [Display(Name = "Canal:")]
        public int idCanal { get; set; }
        [Display(Name = "Estado:")]
        public int idEstado { get; set; }
        [Display(Name = "Comentarios:")]
        public string comentarios { get; set; }
        [Display(Name = "Estado:")]
        public string txtEstado { get; set; }
        [Display(Name = "Canal:")]
        public string txtCanal { get; set; }
        [Display(Name = "Huesped:")]
        public string txtHuesped { get; set; }
        [Display(Name = "Tipo de Pago:")]
        public string txtTipoPago { get; set; }
        [Display(Name = "Documento:")]

        public string documento { get; set; }
        [Display(Name = "Tipo de Habitación:")]
        public int idTipoHabitacion { get; set; }
        [Display(Name = "Habitación:")]
        public int idHabitacion { get; set; }
        [Display(Name = "Tarjeta:")]
        public string tarjeta { get; set; }
        [Display(Name = "Email:")]
        public string correo { get; set; }
        public SelectList comboHabitaciones { get; set; }
        public SelectList comboTipoHabitaciones { get; set; }
        public List<Reserva> listadoReserva { get; set; }
        public List<int> listadoEstado { get; set; }
        public List<string> listadoHuesped { get; set; }
        public SelectList comboEstado { get; set; }

    }
}