﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReservasWeb.Models;
using ReservasWeb.Utils;
using System.ComponentModel.DataAnnotations;

namespace ReservasWeb.ViewModels
{
    public class DatosAlojamientoViewModel : BaseViewModel
    {
        public DatosAlojamientoViewModel()
        {
            this.fotos = new List<Imagene>();
        }
        public int idAlojamiento { get; set; }
        [Display(Name = "Zona")]
        public int idZona { get; set; }
        public SelectList listaZonas { get; set; }
        public GaleriaImagene galeria  { get; set; }
        [Display(Name = "Tipo de Alojamiento")]
        public int idTipoAlojamiento { get; set; }
        public SelectList listaTiposAlojamiento { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Moneda")]
        public int idMoneda { get; set; }
        public SelectList listaMonedas { get; set; }
        [Display(Name = "Estrellas")]
        public int estrellas { get; set; }
        public SelectList listaEstrellas { get; set; }
        public GoogleMap ubicacion { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Nombre")]
        public string nombreEs { get; set; }
        [Display(Name = "Nombre Ingles")]
        public string nombreEn { get; set; }
        [Display(Name = "Nombre Portugues")]
        public string nombrePt { get; set; }
        [Display(Name = "Descripción")]
        public string descripcionEs { get; set; }
        [Display(Name = "Descripción Ingles")]
        public string descripcionEn { get; set; }
        [Display(Name = "Desc. Portugues")]
        public string descripcionPt { get; set; }
        [Display(Name = "Email Reserva")]
        public string emailReserva { get; set; }
        [Display(Name = "Email Contacto")]
        public string emailContacto { get; set; }
        [Display(Name = "Teléfonos")]
        public string telefonos { get; set; }
        public string logo { get; set; }
        [Display(Name = "Impuesto")]        
        public string impuesto { get; set; }
        [Display(Name = "Ciudad")]
        public string ciudadPiePagina { get; set; }
        [Display(Name = "País")]
        public string paisPiePagina { get; set; }
        [Display(Name = "Hora Check-In")]
        public string horaCheckin { get; set; }
        [Display(Name = "Hora Check-Out")]
        public string horaCheckout { get; set; }
        [Display(Name = "Cancelaciones Español")]
        public string cancelacionesEs { get; set; }
        [Display(Name = "Cancelaciones Ingles")]
        public string cancelacionesEn { get; set; }
        [Display(Name = "Cancelaciones Portugues")]
        public string cancelacionesPt { get; set; }
        [Display(Name = "Campo Habitación Español")]
        public string habitacionEs { get; set; }
        [Display(Name = "Campo Habitación Ingles")]
        public string habitacionEn { get; set; }
        [Display(Name = "Campo Habitación Portugues")]
        public string habitacionPt { get; set; }
        [Display(Name = "Mascotas Español")]
        public string mascotasEs { get; set; }
        [Display(Name = "Mascotas Ingles")]
        public string mascotasEn { get; set; }
        [Display(Name = "Mascotas Portugues")]
        public string mascotasPt { get; set; }
        [Display(Name = "Logo del Hotel")]
        public HttpPostedFileBase logoUpload { get; set; }
        public List<Imagene> fotos { get; set; }
        public string IdsFotosOrden { get; set; }
        [Display(Name = "Color Barra Buscador")]
        public string colorBarra { get; set; }
        [Display(Name = "Color Botones Buscador")]
        public string colorBotones { get; set; }
        [Display(Name = "Color Texto Botones Buscador")]
        public string colorTextoBotones { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Sitio Web")]
        public string website { get; set; }
        public string GetIdsFotosOrden()
        {
            string idsFotos = "";
            for (int i = 0; i < fotos.Count(); i++)
            {
                if (i == 0)
                    idsFotos = fotos.ElementAt(i).idImagen.ToString();
                else
                    idsFotos = idsFotos + "," + fotos.ElementAt(i).idImagen.ToString();
            }
            return idsFotos;
        }
    }
}