﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ReservasWeb.ViewModels
{
    public class RecuperarClaveViewModel
    {
        [Required]
        public string Email { get; set; }
    }
}