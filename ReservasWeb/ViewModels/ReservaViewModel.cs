﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ReservasWeb.ViewModels
{
    public class ReservaViewModel : BaseBusquedaViewModel
    {
        [Display(Name = "Nombre")]
        public string nombre { get; set; }
        [Display(Name = "Apellido")]
        public string apellido { get; set; }
        [Display(Name = "Tipo de Documento")]
        public string tipoDocumento { get; set; }
        [Display(Name = "Número de Documento")]
        public string nroDocumento { get; set; }
        [Display(Name = "Telefono")]
        public string telefono { get; set; }
        [Display(Name = "Celular")]
        public string celular { get; set; }
        [Display(Name = "Correo Electronico")]
        public string email { get; set; }
        [Display(Name = "Domicilio")]
        public String direccion { get; set; }
        public SelectList listaTipo { get; set; }
        [Display(Name ="Tipo y Numero de Documento")]
        public SelectList listaTipoDocumento { get; set; }
        [Display(Name = "Razon Social")]
        public string razonSocial { get; set; }
        [Display(Name = "Responsable")]
        public string responsable { get; set; }
        [Display(Name = "Email de la Empresa")]
        public string emailAdmin { get; set; }
        [Display(Name = "Sitio Web")]
        public string url { get; set; }
        [Display(Name = "CUIT")]
        public string cuit { get; set; }
        [Display(Name = "Fax")]
        public string fax { get; set; }
        public SelectList listaTipoPago { get; set; }
        [Display(Name ="Tipo")]
        public string tipo { get; set; }
        [Display(Name = "Tipo de Pago")]
        public int tipopago { get; set; }
 
        [Display(Name ="Ciudad")]
        public int ciudad { get; set; }
        [Display(Name ="Elija la ciudad")]
        public SelectList listaCiudad { get; set; }

        public string tarjetaCredito { get; set; }
        [Display(Name = "Tipo")]
        public SelectList listaTarjetaCredito { get; set; }
        [Display(Name = "Número de Tarjeta")]
        public string nroTarjeta { get; set; }
        [Display(Name = "Titular de la Tarjeta")]
        public string titularTarjeta { get; set; }
        [Display(Name = "Fecha(MES/AÑO)")]
        public string mesTarjeta { get; set; }
        public string anioTarjeta { get; set; }
        [Display(Name = "CVC")]
        public string cvcTarjeta { get; set; }
        public string clientStatus { get; set; }
        public string clientSecret { get; set; }
        public Boolean standBy { get; set; }
        public DateTime fechaCreacion { get; set; }
        public DateTime fechaModificacion { get; set; }
        public string clientId { get; set; }
        //public int idAlojamiento { get; set; }
        public int idMercadoPago { get; set; }
        public int tipoPago { get; set; }
        //public int cantPasajeros { get; set; }
        //public string ninios { get; set; }
        //public DateTime fechaEntrada { get; set; }
        //public DateTime fechaSalida { get; set; }
        

    }
}