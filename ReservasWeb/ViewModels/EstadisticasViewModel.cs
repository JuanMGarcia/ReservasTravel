﻿using ExpressiveAnnotations.Attributes;
using ReservasWeb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.ViewModels
{
    public class EstadisticasViewModel : BaseViewModel
    {
        public int huespedes { get; set; }
        public int reservasNuevas { get; set; }
        public int reservasCanceladas { get; set; }
        public int checkIn { get; set; }
        public int checkOut { get; set; }
        public int checkInPendientes { get; set; }
        public int checkOutPendientes { get; set; }
        public decimal ventasAyer { get; set; }
        public decimal ventasHoy { get; set; }
        public decimal ventasMañana { get; set; }
        public int habitacionesOcupadas { get; set; }
        public int habitacionesDisponibles { get; set; }

        //[RequiredIf("fechaHasta != ''", ErrorMessage = "Requerido")]
        [Required(ErrorMessage = "Requerido")]
        public string fechaDesde { get; set; }
        //[RequiredIf("fechaDesde != ''", ErrorMessage = "Requerido")]
        [Required(ErrorMessage = "Requerido")]
        public string fechaHasta { get; set; }

    }
}