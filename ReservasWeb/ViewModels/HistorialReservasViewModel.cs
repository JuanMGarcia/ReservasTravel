﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using System.Web;

namespace ReservasWeb.ViewModels
{
    public class HistorialReservaViewModel : BaseViewModel
    {
        public int idHistorialReserva { get; set; }
        public int idAlojamiento { get; set; }
        public int idReserva { get; set; }
        public int idEstado { get; set; }
        public int idUsuario { get; set; }
        public int idHuesped { get; set; }
        public string checkIn { get; set; }
        public string cheackout { get; set; }
        public string fechaIn { get; set; }
        public string fechaOut { get; set; }
        public string fechaModificacion { get; set; }
        public string precioPorDia { get; set; }
        public int cantidadHuespedes { get; set; }
        public string accion { get; set; }
        public string observaciones { get; set; }
        public string nombreUsuario { get; set; }
        public string apellidoUsuario { get; set; }
        public string tiposHabitaciones { get; set; }
        public string habitaciones { get; set; }
    }
}
