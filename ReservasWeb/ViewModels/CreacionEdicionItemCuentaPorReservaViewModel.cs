﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.ViewModels
{
    public class CreacionEdicionItemCuentaPorReservaViewModel : BaseViewModel
    {
        public int idItemCuenta { get; set; }
        public int idReserva { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Fecha")]
        public string fecha { get; set; }

        [Display(Name = "Concepto")]
        [Required(ErrorMessage = "Campo requerido")]
        public string concepto { get; set; }

        [Display(Name = "Forma de Pago")]
        [Required(ErrorMessage = "Campo requerido")]
        public string formaDePago { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Monto")]
        public string monto { get; set; }

        [Display(Name = "Comentarios")]
        public string comentarios { get; set; }

        public SelectList conceptoSelectList { get; set; }
        public SelectList formaDePagoSelectList { get; set; }        
    }
}