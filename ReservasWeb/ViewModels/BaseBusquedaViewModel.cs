﻿using ReservasWeb.Models;
using ReservasWeb.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.ViewModels
{
    public class BaseBusquedaViewModel : BaseViewModel
    {
        public int idAlojamiento { get; set; }
        public string nombreHotel { get; set; }
        public string colorBarra { get; set; }
        public string colorBoton { get; set; }
        public string colorTextoBoton { get; set; }
        public string urlLogo { get; set; }
        public int habitaciones { get; set; }
        public string backurlHotel { get; set; }
        public string urlLogoHotel { get; set; }        
        public string telefonoHotel { get; set; }
        public string emailHotel { get; set; }
        public string paisHotel { get; set; } 
        public string ciudadHotel { get; set; }
        public GoogleMap ubicacion { get; set; }
        public List<Imagene> fotosHotel { get; set; }
        public decimal impuesto { get; set; }
        public decimal total { get; set; }
        public string horaCheckin { get; set; }
        public string horaCheckout { get; set; }
        public string cancelaciones { get; set; }
        public string mascotas { get; set; }
        public List<TiposPagoConfiguracion> listadoMedios { get; set; }
        public string estrellas { get; set; }
        public string direccionHotel { get; set; }
        public string imgHotelmini { get; set; }
        public string linkMP { get; set; }
        public int cantPasajeros { get; set; }
        public string ninios { get; set; }
        public DateTime fechaEntrada { get; set; }
        public DateTime fechaSalida { get; set; }
        public string politica { get; set; }
        public string urlEspanol { get; set; }
        public string urlIngles { get; set; }
        public string urlPortugues { get; set; }
        public string idioma { get; set; }
        public bool tieneLogo { get; set; }
        public List<LHabDisponibles> listadoDisponible { get; set; }
        public string contadorSesion { get; set; }
        public int contadorSegundos { get; set; }
        public string contadorSesion2 { get; set; }
        public int contadorSegundos2 { get; set; }

        //Transferencia Bancaria
        public string tbNombreBanco { get; set; }
        public string tbTipoCuenta { get; set; }
        public string tbTitular { get; set; }
        public string tbNumeroCuenta { get; set; }
        public string tbCBU { get; set; }
        public string tbCUIT { get; set; }
        public string tbSucursal { get; set; }

        public string campoHabitacion { get; set; }

    }

}
