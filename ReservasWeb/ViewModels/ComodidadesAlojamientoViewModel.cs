﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;

namespace ReservasWeb.ViewModels
{
    public class ComodidadesAlojamientoViewModel : BaseViewModel
    {        
        public List<ComodidadSeleccionada> listaComodidades { get; set; }
    }

    public class ComodidadSeleccionada
    {
        public int idComodidad { get; set; }
        public string nombre { get; set; }
        public string icono { get; set; }
        public bool seleccionada { get; set; }
    }


}