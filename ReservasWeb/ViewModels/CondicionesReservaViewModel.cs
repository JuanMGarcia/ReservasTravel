﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ReservasWeb.ViewModels
{
    public class CondicionesReservaViewModel : BaseViewModel
    {
        public int idCondReserva { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Nombre")]
        public string nombreEs { get; set; }
        [Display(Name = "Nombre Pt")]
        public string nombrePt { get; set; }
        [Display(Name = "Nombre En")]
        public string nombreEn { get; set; }
        [Display(Name = "Descripción")]
        public string descripcionEs { get; set; }
        [Display(Name = "Descripción En")]
        public string descripcionEn { get; set; }
        [Display(Name = "Descripción Pt")]
        public string descripcionPt { get; set; }
        public int IdAlojamiento { get; set; }
        public int idPoliticaCancelacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public SelectList listadoPoliticas { get; set; }
        public SelectList listadoPagos { get; set; }
        public SelectList getPagos { get; set; }
        public int IdFormasDePago { get; set; }
        public List<FormasDePagoSeleccionadas> listaFormaPagos { get; set; }
    }

    public class FormasDePagoSeleccionadas
    {
        public int idFormadePago { get; set; }
        public string nombre { get; set; }
        public string icono { get; set; }
        public bool seleccionada { get; set; }
    }
}
    