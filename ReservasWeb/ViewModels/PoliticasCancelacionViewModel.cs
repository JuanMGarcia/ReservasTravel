﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ReservasWeb.ViewModels
{
    public class PoliticasCancelacionViewModel : BaseViewModel
    {
        public int idPoliticaCancelacion { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Nombre")]
        public string nombreEs { get; set; }
        [Display(Name = "Nombre Pt")]
        public string nombrePt { get; set; }

        [Display(Name = "Nombre En")]
        public string nombreEn { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Descripción")]
        public string descripcionEs { get; set; }
        [Display(Name = "Descripción En")]
        public string descripcionEn { get; set; }

        [Display(Name = "Descripción Pt")]
        public string descripcionPt { get; set; }
        public int IdAlojamiento { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
    }
}
    