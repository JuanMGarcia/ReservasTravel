﻿using ExpressiveAnnotations.Attributes;
using ReservasWeb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.ViewModels
{
    public class EdicionTarifasPorDiaMasivaViewModel : BaseViewModel
    {
        public EdicionTarifasPorDiaMasivaViewModel() { }

        public bool? porFecha { get; set; }
        public bool? porTemporada { get; set; }
        public bool? porFechaEspecial { get; set; }
        
        [Display(Name = "Habitaciones")]        
        public List<CheckBoxItemViewModel> tiposHabitacion { get; set; }
        
        [Required(ErrorMessage = "Seleccione al menos un Tipo de Habitación")]
        public string tiposHabitacionSeleccionados { get; set; }        
        
        [Display(Name = "Canales")]        
        public List<CheckBoxItemViewModel> canales { get; set; }
        [Required(ErrorMessage = "Seleccione al menos un Canal")]
        public string canalesSeleccionados { get; set; }

        [Display(Name = "Condiciones")]
        public List<CheckBoxItemViewModel> condicionesReservas { get; set; }
        [Required(ErrorMessage = "Seleccione al menos una Condición")]
        public string condicionesReservasSeleccionados { get; set; }

        [RequiredIf("porFecha == true")]
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Fecha Desde")]
        public string desdeFecha { get; set; }
        
        [RequiredIf("porFecha == true")]
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Fecha Hasta")]
        public string hastaFecha { get; set; }        
        
        [Display(Name = "Dias")]
        [Required(ErrorMessage = "Seleccione al menos un Día")]
        public string diasSemanaSeleccionados { get; set; }

        [Display(Name = "Temporadas")]
        public SelectList temporadas { get; set; }
        
        [RequiredIf("porTemporada == true")]
        [Required(ErrorMessage = "Campo requerido")]
        public int? idTemporada { get; set; }

        [Display(Name = "Fechas Especiales")]
        public SelectList fechasEspeciales { get; set; }
        
        [RequiredIf("porFechaEspecial == true")]
        [Required(ErrorMessage = "Campo requerido")]
        public int? idFechaEspecial { get; set; }
        
        public TarifaPorDiaMasivoViewModel tarifaPorDia { get; set; }
        
        [Required(ErrorMessage = "Ingrese al menos un dato")]
        public string tarifaPorDiaDatosIngresados { get; set; }
    }

    public class CheckBoxItemViewModel
    {
        public string nombre { get; set; }
        public string valor { get; set; }
        public bool seleccionado { get; set; }
    }
}