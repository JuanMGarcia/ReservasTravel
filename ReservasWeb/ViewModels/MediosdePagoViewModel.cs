﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.ViewModels
{
    public class MediosdePagoViewModel:BaseViewModel
    {
        //Paga en Hotel Tipo = 0 solo importa el campo Stand By
      
        public Boolean PHstandBy { get; set; }
        public int PHidMercadoPago { get; set; }
        //Mercado Pago Tipo = 1
        public string MPclientStatus { get; set; }
        public string MPclientSecret { get; set; }
        public Boolean MPstandBy { get; set; }
        public DateTime MPfechaCreacion { get; set; }
        public DateTime MPfechaModificacion { get; set; }
        public string MPclientId { get; set; }
        public int idAlojamiento { get; set; }
        public int MPidMercadoPago { get; set; }
        public int MPtipo { get; set; }

        //TODOPAGO Tipo = 2
        public string TPclientStatus { get; set; }
        public string TPclientSecret { get; set; }
        public Boolean TPstandBy { get; set; }
        public DateTime TPfechaCreacion { get; set; }
        public DateTime TPfechaModificacion { get; set; }
        public string TPclientId { get; set; }
        public int TPidMercadoPago { get; set; }
        public int TPtipo { get; set; }

        //PayPal Tipo = 3
        public string PPclientStatus { get; set; }
        public string PPclientSecret { get; set; }
        public Boolean PPstandBy { get; set; }
        public DateTime PPfechaCreacion { get; set; }
        public DateTime PPfechaModificacion { get; set; }
        public string PPclientId { get; set; }
        public int PPidMercadoPago { get; set; }
        public int PPtipo { get; set; }

        public int TBidMercadoPago { get; set; }
        public bool TBstandBy { get; set; }



    }
}