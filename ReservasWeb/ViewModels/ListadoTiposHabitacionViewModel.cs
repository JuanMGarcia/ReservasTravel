﻿using ReservasWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.ViewModels
{
    public class ListadoTiposHabitacionViewModel : BaseViewModel
    {
        public List<TiposHabitacione> listado { get; set; }
    }
}