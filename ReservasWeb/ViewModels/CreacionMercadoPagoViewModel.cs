﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReservasWeb.ViewModels
{
    public class CreacionMercadoPagoViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Client ID")]
        public String clientId { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Client Secret")]
        public String clientSecret { get; set; }
        [Display(Name = "Client Status")]
        public String clientStatus { get; set; }
        [Display(Name = "Stand By")]
        public Boolean standBy { get; set; }
        public int tipo { get; set; }
    }
}