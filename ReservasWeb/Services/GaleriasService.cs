﻿using ReservasWeb.Models;
using ReservasWeb.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ReservasWeb.Services
{
    public class GaleriasService : BaseService
    {
        public GaleriasService(ReservasWebEntities db) : base(db)
        {
        }

        public List<Imagene> getFotosAlojamiento()
        {
            int idAlojamiento = getIdAlojamiento();
            Alojamiento aloj = DB.Alojamientos.Find(idAlojamiento);
            //return aloj.GaleriaImagene.Imagenes.OrderBy(t => t.orden).ThenBy(t => t.idImagen).ToList();
            List<Imagene> lista;
            if (aloj.idGaleriaImagen != null)
            {
                lista = aloj.GaleriaImagene.Imagenes.OrderBy(t => t.orden).ThenByDescending(t => t.idImagen).ToList();
            }
            else
            {
                lista = new List<Imagene>();
            }
            return lista;
        }

        public List<Imagene> getFotosTipoHabitacion(int idTipoHabitacion)
        {
            int idAlojamiento = getIdAlojamiento();            
            List<TiposHabitacione> tipoHabitacionList = DB.TiposHabitaciones.Where(t => t.idTipoHabitacion == idTipoHabitacion && t.idAlojamiento == idAlojamiento).ToList();
            List<Imagene> lista;
            TiposHabitacione tipoHabitacion = null;

            if (tipoHabitacionList.Count > 0)
                tipoHabitacion = tipoHabitacionList.ElementAt(0);

            if (tipoHabitacion != null && tipoHabitacion.idGaleriaImagen != null)
            {
                lista = tipoHabitacion.GaleriaImagene.Imagenes.OrderBy(t => t.orden).ThenByDescending(t => t.idImagen).ToList();
            }
            else
            {
                lista = new List<Imagene>();
            }
            return lista;
        }

        public int CrearGaleria(string nombre, string descripcion)
        {
            GaleriaImagene galeria = new GaleriaImagene();
            galeria.nombre = nombre;
            galeria.descripcion = descripcion;
            DB.GaleriaImagenes.Add(galeria);
            DB.SaveChanges();
            return galeria.idGaleriaImagen;
        }

        public void EliminarGaleria(int idGaleria)
        {
            int idAlojamiento = getIdAlojamiento();
            GaleriaImagene galeria = DB.GaleriaImagenes.Where(t => t.TiposHabitaciones.FirstOrDefault().idAlojamiento == idAlojamiento && t.idGaleriaImagen == idGaleria).FirstOrDefault();
            //GaleriaImagene galeria = DB.GaleriaImagenes.Find(idGaleria);
            DB.GaleriaImagenes.Remove(galeria);
            DB.SaveChanges();
        }

        public int GuardarImagen(string nombreArchivo, int orden)
        {
            int idAlojamiento = getIdAlojamiento();
            Alojamiento aloj = DB.Alojamientos.Find(idAlojamiento);

            Imagene foto = new Imagene();
            foto.archivo = nombreArchivo;
            foto.orden = orden;

            if (aloj.idGaleriaImagen != null)
            {
                foto.idGaleriaImagen = aloj.idGaleriaImagen.Value;
            }
            else
            {
                int idGaleria = CrearGaleria("Galeria Alojamiento", "Galeria de Fotos");
                aloj.idGaleriaImagen = idGaleria;
                foto.idGaleriaImagen = idGaleria;
            }
            DB.Imagenes.Add(foto);
            DB.Entry(aloj).State = EntityState.Modified;
            DB.SaveChanges();
            return foto.idImagen;
        }

        public int GuardarImagenTipoHabitacion(int idTipoHabitacion, string nombreArchivo, int orden)
        {
            int idAlojamiento = getIdAlojamiento();
            List<TiposHabitacione> tipoHabitacionList = DB.TiposHabitaciones.Where(t => t.idTipoHabitacion == idTipoHabitacion && t.idAlojamiento == idAlojamiento).ToList();
            TiposHabitacione tipoHabitacion = null;

            Imagene foto = new Imagene();
            foto.archivo = nombreArchivo;
            foto.orden = orden;

            if (tipoHabitacionList.Count > 0)
                tipoHabitacion = tipoHabitacionList.ElementAt(0);

            if (tipoHabitacion != null && tipoHabitacion.idGaleriaImagen != null)
            {
                foto.idGaleriaImagen = tipoHabitacion.idGaleriaImagen.Value;
            }
            else
            {
                int idGaleria = CrearGaleria("Galeria Tipo Habitación", "Galeria de Fotos");
                tipoHabitacion.idGaleriaImagen = idGaleria;
                foto.idGaleriaImagen = idGaleria;
            }
            DB.Imagenes.Add(foto);
            DB.Entry(tipoHabitacion).State = EntityState.Modified;
            DB.SaveChanges();
            return foto.idImagen;
        }
        public void ordenarImagenes(int oldIndex, int newIndex)
        {
            List<Imagene> fotosList = getFotosAlojamiento();
            Imagene foto;

            if (oldIndex < newIndex)
            {
                for (int i = newIndex; i > oldIndex; i--)
                {
                    foto = fotosList[i];
                    foto.orden = i - 1;
                    DB.Entry(foto).State = EntityState.Modified;
                    DB.SaveChanges();
                    //if (foto.orden == 0)
                    //    SetIdFotoPrincipal(idInmueble, foto.IdFoto);
                }
            }
            else
            {
                for (int i = newIndex; i < oldIndex; i++)
                {
                    foto = fotosList[i];
                    foto.orden = i + 1;
                    DB.Entry(foto).State = EntityState.Modified;
                    DB.SaveChanges();
                    //if (foto.orden == 0)
                    //    SetIdFotoPrincipal(idInmueble, foto.IdFoto);
                }
            }
            foto = fotosList[oldIndex];
            foto.orden = newIndex;
            DB.Entry(foto).State = EntityState.Modified;
            DB.SaveChanges();
        }

        public void eliminarImagen(int idImagen)
        {
            int idAlojamiento = getIdAlojamiento();
            Imagene foto = DB.Imagenes.Find(idImagen);
            DB.Imagenes.Remove(foto);
            DB.SaveChanges();
        }

        public void SetFotoOrden(Imagene foto, int orden)
        {
            foto.orden = orden;
            DB.Entry(foto).State = EntityState.Modified;
            DB.SaveChanges();
        }
    }
}