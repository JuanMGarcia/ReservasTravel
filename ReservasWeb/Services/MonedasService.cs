﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.Data.Entity;
using ReservasWeb.ViewModels;
using System.Web.Mvc;

namespace ReservasWeb.Services
{
    public class MonedasService : BaseService
    {
        public MonedasService(ReservasWebEntities db) : base(db)
        {
        }

        public Moneda get(int id)
        {
            Moneda moneda = null;
            List<Moneda> monedaList = (from i in DB.Monedas.Where(i => i.idMoneda == id)
                                                               select i).ToList<Moneda>();

            if (monedaList.Count() > 0)
            {
                moneda = monedaList.ElementAt(0);
            }

            return moneda;
        }

        public List<Moneda> getMonedas()
        {
            return DB.Monedas.ToList();
        }

        public SelectList getComboMonedas()
        {
            List<Moneda> lista = getMonedas();
            return new SelectList(lista, "idMoneda", "nombreEs");
        }

        public void crear(Moneda moneda)
        {
            DB.Monedas.Add(moneda);
            DB.SaveChanges();
        }

        public void editar(Moneda moneda)
        {
            DB.Entry(moneda).State = EntityState.Modified;
            DB.SaveChanges();
        }
    }
}