﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using ReservasWeb.ViewModels;
using ReservasWeb.Utils;
using System.Data.Entity;
using System.Web.Mvc;

namespace ReservasWeb.Services
{
    public class TiposHabitacionService : BaseService
    {
        public TiposHabitacionService(ReservasWebEntities db) : base(db)
        {
        }

        public TiposHabitacione get(int id)
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.TiposHabitaciones.Where(t => t.idTipoHabitacion == id && t.idAlojamiento == idAlojamiento).FirstOrDefault();
        }

        public List<TiposHabitacione> getTiposHabitacionPorAlojamiento()
        {
            int idAlojamiento = getIdAlojamiento();
            List<TiposHabitacione> lista = DB.TiposHabitaciones.Where(t => t.idAlojamiento == idAlojamiento).ToList();
            return lista;
        }

        public List<TiposHabitacione> getTiposHabitacionConDatosCompletos()
        {
            int idAlojamiento = getIdAlojamiento();
            List<TiposHabitacione> lista = DB.TiposHabitaciones.Where(t => t.idAlojamiento == idAlojamiento && t.TarifasBases.Count() > 0 && t.plazas > 0).ToList();
            return lista;
        }

        public List<TiposHabitacione> getTiposHabitacionConHabitaciones()
        {
            int idAlojamiento = getIdAlojamiento();
            List<TiposHabitacione> lista = DB.TiposHabitaciones.Where(t => t.idAlojamiento == idAlojamiento && t.Habitaciones.Count > 0).ToList();
            return lista;
        }

        public SelectList getComboTiposHabitacion()
        {            
            return new SelectList(getTiposHabitacionConDatosCompletos(), "idTipoHabitacion", "nombreEs");
        }

        public SelectList getComboTiposHabitacionParaBusqueda()
        {
            List<TiposHabitacione> lst = getTiposHabitacionConDatosCompletos();
            TiposHabitacione item = new TiposHabitacione();
            item.idTipoHabitacion = 0;
            item.nombreEs = "Todos";
            lst.Add(item);
            return new SelectList(lst, "idTipoHabitacion", "nombreEs");
        }

        public SelectList getComboTiposHabitacionConHabitaciones()
        {
            return new SelectList(getTiposHabitacionConHabitaciones(), "idTipoHabitacion", "nombreEs");
        }

        public EdicionTipoHabitacionViewModel getTipoHabitacionEdicion(int idTipoHabitacion)
        {
            TiposHabitacione tipoHabitacion = DB.TiposHabitaciones.Find(idTipoHabitacion);
            EdicionTipoHabitacionViewModel vm = new EdicionTipoHabitacionViewModel();
            //vm.idTipoHabitacion = tipoHabitacion.idTipoHabitacion;
            vm.idTipoHabitacion = idTipoHabitacion;
            vm.nombreEs = tipoHabitacion.nombreEs;
            vm.nombreEn = tipoHabitacion.nombreEn;
            vm.nombrePt = tipoHabitacion.nombrePt;
            //if(tipoHabitacion.plazas.HasValue)
            //    vm.plazas = tipoHabitacion.plazas.Value;

            if (tipoHabitacion.camaMatrimonial.HasValue)
                vm.camaMatrimonial = tipoHabitacion.camaMatrimonial.Value;
            if (tipoHabitacion.camasSimples.HasValue)
                vm.camasSimples = tipoHabitacion.camasSimples.Value;

            int plazas = 0;
            if (vm.camasSimples > 0)
                plazas = vm.camasSimples;
            if (vm.camaMatrimonial > 0)
                plazas = plazas + vm.camaMatrimonial * 2;
                vm.plazas = plazas;
                //vm.plazas = plazas.ToString();


            vm.horaCheckOutTardio = tipoHabitacion.horaCheckOutTardio;
            if (tipoHabitacion.camasAdicionales.HasValue && tipoHabitacion.camasAdicionales.Value > 0)
            {
                vm.permiteCamasAdicionales = 1;
                vm.camasAdicionales = tipoHabitacion.camasAdicionales.Value;
                vm.permiteAdicionalCunasBebes = Convert.ToInt32(tipoHabitacion.permiteAdicionalCunasBebes.Value);
                vm.permiteAdicionalMenores = Convert.ToInt32(tipoHabitacion.permiteAdicionalMenores.Value);
                vm.permiteAdicionalAdultos = Convert.ToInt32(tipoHabitacion.permiteAdicionalAdultos.Value);
            }
            else
            {
                vm.permiteCamasAdicionales = 0;
                vm.camasAdicionales = 0;
                vm.permiteAdicionalCunasBebes = 0;
                vm.permiteAdicionalMenores = 0;
                vm.permiteAdicionalAdultos = 0;
            }
            vm.descripcionEs = (tipoHabitacion.descripcionEs != null) ? tipoHabitacion.descripcionEs.Replace("<br/>", System.Environment.NewLine) : "";
            vm.descripcionEn = (tipoHabitacion.descripcionEn != null) ? tipoHabitacion.descripcionEn.Replace("<br/>", System.Environment.NewLine) : "";
            vm.descripcionPt = (tipoHabitacion.descripcionPt != null) ? tipoHabitacion.descripcionPt.Replace("<br/>", System.Environment.NewLine) : "";


            return vm;
        }

        public int Crear(CreacionTipoHabitacionViewModel vm)
        {
            TiposHabitacione tipo = new TiposHabitacione();
            tipo.nombreEs = vm.nombre;
            tipo.idAlojamiento = getIdAlojamiento();
            crear(tipo);
            return tipo.idTipoHabitacion;
        }

        public void Eliminar(int idTipoHabitacion)
        {
            TiposHabitacione tipo = this.get(idTipoHabitacion);
            if (tipo != null)
            {
                DB.TiposHabitaciones.Remove(tipo);
                DB.SaveChanges();
            }
        }

        public void Editar(EdicionTipoHabitacionViewModel vm)
        {
            TiposHabitacione tipo = DB.TiposHabitaciones.Find(vm.idTipoHabitacion);
            tipo.nombreEs = vm.nombreEs;
            tipo.nombreEn = vm.nombreEn;
            tipo.nombrePt = vm.nombrePt;
            tipo.plazas = Convert.ToInt32(vm.plazas);
            tipo.camaMatrimonial = vm.camaMatrimonial;
            tipo.camasSimples = vm.camasSimples;
            tipo.horaCheckOutTardio = vm.horaCheckOutTardio;
            tipo.descripcionEs = (vm.descripcionEs != null) ? vm.descripcionEs.Replace(System.Environment.NewLine, "<br/>") : "";
            tipo.descripcionEn = (vm.descripcionEn != null) ? vm.descripcionEn.Replace(System.Environment.NewLine, "<br/>") : "";
            tipo.descripcionPt = (vm.descripcionPt != null) ? vm.descripcionPt.Replace(System.Environment.NewLine, "<br/>") : "";
            if (vm.permiteCamasAdicionales == 1)
            {
                tipo.camasAdicionales = vm.camasAdicionales;
                tipo.permiteAdicionalCunasBebes = Convert.ToBoolean(vm.permiteAdicionalCunasBebes);
                tipo.permiteAdicionalMenores = Convert.ToBoolean(vm.permiteAdicionalMenores);
                tipo.permiteAdicionalAdultos = Convert.ToBoolean(vm.permiteAdicionalAdultos);
            }
            else {
                tipo.camasAdicionales = 0;
                tipo.permiteAdicionalCunasBebes = false;
                tipo.permiteAdicionalMenores = false;
                tipo.permiteAdicionalAdultos = false;
            }

            GaleriasService svGale = new GaleriasService(this.DB);

            if (vm.IdsFotosOrden != null)
            {
                string[] idsFotosSplit = vm.IdsFotosOrden.Split(',');
                Imagene foto;
                int idFotoInt;
                for (int i = 0; i < idsFotosSplit.Count(); i++)
                {
                    if (idsFotosSplit[i] != null || idsFotosSplit[i] != "" )
                    {
                        idFotoInt = Int32.Parse(idsFotosSplit[i]);
                        var fotoLinq = from f in DB.Imagenes.Where(f => f.idImagen == idFotoInt)
                                       select f;
                        if (fotoLinq.ToList().Count() > 0)
                        {
                            foto = fotoLinq.ToList().ElementAt(0);
                            svGale.SetFotoOrden(foto, i);
                        }
                    }
                }
            }
            editar(tipo);
        }

        public List<SelectListItem> getComboPlazas(int plazaSeleccionada)
        {
            bool selected = false;
            List<SelectListItem> plazasList = new List<SelectListItem>();
            if (plazaSeleccionada == 0)
                plazaSeleccionada = 2;
            selected = false;
            for (int i = 1; i < 21; i++)
            {
                if (plazaSeleccionada == i)
                    selected = true;

                plazasList.Add(new SelectListItem { Value = i.ToString(), Text = i.ToString(), Selected = selected });
                selected = false;
            }
            return plazasList;
        }

        public List<SelectListItem> getComboCamasAdicionales(int camaAdicionalSeleccionada)
        {
            bool selected = false;
            List<SelectListItem> camasAdicionalesList = new List<SelectListItem>();
            if (camaAdicionalSeleccionada == 0)
                camaAdicionalSeleccionada = 2;
            selected = false;
            for (int i = 1; i < 21; i++)
            {
                if (camaAdicionalSeleccionada == i)
                    selected = true;

                camasAdicionalesList.Add(new SelectListItem { Value = i.ToString(), Text = i.ToString(), Selected = selected });
                selected = false;
            }
            return camasAdicionalesList;
        }

        public List<SelectListItem> getComboCamasSimplesMatrimoniales()
        {
            //bool selected = false;
            List<SelectListItem> camasList = new List<SelectListItem>();
            //if (camaAdicionalSeleccionada == 0)
            //    camaAdicionalSeleccionada = 2;
            //selected = false;
            //for (int i = 1; i < 21; i++)
            //{
            //    if (camaAdicionalSeleccionada == i)
            //        selected = true;

            //    camasAdicionalesList.Add(new SelectListItem { Value = i.ToString(), Text = i.ToString(), Selected = selected });
            //    selected = false;
            //}

            for (int i = 0; i < 21; i++)
            {
                camasList.Add(new SelectListItem { Value = i.ToString(), Text = i.ToString(), Selected = false });
            }
            return camasList;
        }

        public SelectList getComboSiNo()
        {
            return new SelectList(new[]
            {
                new { ID = "0", Name = "No" },
                new { ID = "1", Name = "Si" }
            },
            "ID", "Name", "");
        }

        public void crear(TiposHabitacione tipoHabitacion)
        {
            tipoHabitacion.fechaCreacion = DateTime.Now;
            DB.TiposHabitaciones.Add(tipoHabitacion);
            DB.SaveChanges();
        }

        public void editar(TiposHabitacione tipoHabitacion)
        {
            tipoHabitacion.fechaModificacion = DateTime.Now;
            DB.Entry(tipoHabitacion).State = EntityState.Modified;
            DB.SaveChanges();
        }

        public void activarLink(int id, bool disponible)
        {
            TiposHabitacione tipoHab = DB.TiposHabitaciones.Where(s=>s.idTipoHabitacion == id).FirstOrDefault();
            tipoHab.activoLink = disponible;
            DB.SaveChanges();
        }
    }
}