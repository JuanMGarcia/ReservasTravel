﻿using ReservasWeb.Models;
using ReservasWeb.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Mvc;
using System.Data.Entity;
using ReservasWeb.ViewModels;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace ReservasWeb.Services
{
    public class ReservasService : BaseService
    {
        public ReservasService(ReservasWebEntities db) : base(db)
        {
        }

        public List<HistorialReserva> getHistorialReserva(int idReserva)
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.HistorialReservas.Where(t => t.idAlojamiento == idAlojamiento && t.idReserva == idReserva).ToList();
        }

        public List<ReservaGrilla> getReservasPorAlojamientoGrilla(DateTime inicio, DateTime fin)
        {
            int idAlojamiento = getIdAlojamiento();
            var consulta = (from x in DB.Reservas.Where(t => t.idAlojamiento == idAlojamiento && 
                                (
                                    (t.fechaEntrada > inicio && t.fechaEntrada < fin && t.fechaSalida > inicio && t.fechaSalida < fin) || 
                                    (t.fechaEntrada.Month == inicio.Month && t.fechaEntrada.Year == inicio.Year) || 
                                    (t.fechaEntrada.Month == fin.Month && t.fechaEntrada.Year == fin.Year) ||
                                    (t.fechaSalida.Month == inicio.Month && t.fechaSalida.Year == inicio.Year) ||
                                    (t.fechaSalida.Month == fin.Month && t.fechaSalida.Year == fin.Year) ||
                                    (t.fechaEntrada.Month < inicio.Month && t.fechaEntrada.Year == inicio.Year && t.fechaSalida.Month > inicio.Month && t.fechaSalida.Year == inicio.Year) ||
                                    (t.fechaEntrada.Year < inicio.Year && t.fechaSalida.Month > inicio.Month && t.fechaSalida.Year == inicio.Year) ||
                                    (t.fechaEntrada.Month < inicio.Month && t.fechaEntrada.Year == inicio.Year && t.fechaSalida.Year > inicio.Year)
                                    
                                )           
                            )                 
                            from z in x.HabitacionesPorReservas.Select(z => z.idHabitacion).Distinct()
                            select new ReservaGrilla{
                                id = x.idReserva,
                                text = x.Huespede.apellido + ", " + x.Huespede.nombre,
                                start = x.fechaEntrada,
                                end = x.fechaSalida,
                                resource = z,
                                bubbleHtml = "",
                                status = x.idEstado.Value.ToString(),
                                idCanal = x.idCanal.Value
                            }).ToList();
            return consulta;
        }

        public int getCantidadReservasPorHabitacion(int idHabitacion)
        {
            int idAlojamiento = getIdAlojamiento();
            int cantidad = (from x in DB.Reservas.Where(t => t.idAlojamiento == idAlojamiento)
                            from z in x.HabitacionesPorReservas.Where(h => h.idHabitacion == idHabitacion)
                            select new ReservaGrilla
                            {
                                id = x.idReserva,
                                text = x.Huespede.apellido + ", " + x.Huespede.nombre,
                                start = x.fechaEntrada,
                                end = x.fechaSalida,
                                resource = z.idHabitacion,
                                bubbleHtml = "",
                                status = x.idEstado.Value.ToString()
                            }).ToList().Count();
            return cantidad;
        }

        public int getCantidadReservasPorHabitacion(int idHabitacion, int idReserva, DateTime inicio, DateTime fin)
        {
            int idAlojamiento = getIdAlojamiento();
            //no considero las reservas con estado = 4 => CANCELADA
            int cantidad = (from x in DB.Reservas.Where(t => t.idAlojamiento == idAlojamiento && t.idReserva != idReserva && t.idEstado != 4 && ((t.fechaEntrada >= inicio && t.fechaEntrada < fin) || (t.fechaSalida > inicio && t.fechaSalida <= fin)))
                            from z in x.HabitacionesPorReservas.Where(h => h.idHabitacion == idHabitacion)
                            select new ReservaGrilla
                            {
                                id = x.idReserva,
                                text = x.Huespede.apellido + ", " + x.Huespede.nombre,
                                start = x.fechaEntrada,
                                end = x.fechaSalida,
                                resource = z.idHabitacion,
                                bubbleHtml = "",
                                status = x.idEstado.Value.ToString()
                            }).ToList().Count();
            return cantidad;
        }

        public Reserva getReserva(int idReserva)
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.Reservas.Where(r => r.idReserva == idReserva && r.idAlojamiento == idAlojamiento).FirstOrDefault();
        }

        public Reserva getReservaAny(int idReserva,int idAlojamiento)
        {
            return DB.Reservas.Where(r => r.idReserva == idReserva && r.idAlojamiento == idAlojamiento).FirstOrDefault();
        }

        public bool ConfirmarReserva(String IdReserva)
        {
            if (IdReserva != null)
            { 
                Reserva unaRsv = DB.Reservas.Find(Int32.Parse(IdReserva));
                unaRsv.idEstado = 3;
                DB.SaveChanges();
                return true; //confirma reserva despues de pagar en MP
            }
            else
            {
                return false;
            }
        }

        public bool ReservaPendiente(String IdReserva)
        {
            if (IdReserva != null)
            {
                Reserva unaRsv = DB.Reservas.Find(Int32.Parse(IdReserva));
                unaRsv.idEstado = 2;
                DB.SaveChanges();
                return true; //confirma reserva despues de pagar en MP
            }
            else
            {
                return false;
            }
        }

        public bool CancelarReserva(String IdReserva)
        {
            if (IdReserva != null)
            {            
                Reserva unaRsv = DB.Reservas.Find(Int32.Parse(IdReserva));
                unaRsv.idEstado = 4;
                DB.SaveChanges();
                return true; //cancela reserva despues de pagar en MP
            }
            else
            {
                return false;
            }

        }

        public SelectList getComboTipo()
        {
            List<string> estados = new List<string>();
            estados.Add("Persona");
            estados.Add("Agencia");
            return new SelectList(estados);
        }

        public SelectList getComboTipoDocumento()
        {
            List<string> estados = new List<string>();
            estados.Add("DNI");
            estados.Add("CI");
            estados.Add("LE");
            estados.Add("LI");
            return new SelectList(estados);
        }

        public SelectList getComboTarjeta()
        {
            List<string> estados = new List<string>();
            estados.Add("Visa");
            estados.Add("Master Card");
            estados.Add("American Express");
            return new SelectList(estados);
        }

        public SelectList getComboEstados()
        {
            SelectList estados = new SelectList(new[]{
                new { ID = "1", Name = ComboHelper.GetNombreEstadoReserva(1) },
                new { ID = "2", Name = ComboHelper.GetNombreEstadoReserva(2) },
                new { ID = "3", Name = ComboHelper.GetNombreEstadoReserva(3) },
                new { ID = "4", Name = ComboHelper.GetNombreEstadoReserva(4) },
                new { ID = "5", Name = ComboHelper.GetNombreEstadoReserva(5) }
            }, "ID", "Name", "");
            return estados;
        }

        public List<TiposPago> getFormasdePagos()
        {
            List<TiposPago> lista = DB.TiposPagoes.ToList();
            return lista;
        }

        public List<Pais> getPaises()
        {
            List<Pais> lista = DB.Paises.ToList();
            return lista;
        }

        public SelectList getComboTiposPago()
        {
            return new SelectList(getFormasdePagos(), "idTipoPago", "nombreEs");
        }

        public List<TiposPago> getFormasdePagosPorHotel(int idAlojamiento)
        {
            var lista = from c in DB.TiposPagoes
                                    from e in c.TiposPagoConfiguracions.Where(h => h.idAlojamiento == idAlojamiento && h.standBy).ToList()
                                    select c;
            return lista.ToList<TiposPago>();
        }

        public SelectList getComboTiposPagoPorHotel(int idAlojamiento)
        {
            return new SelectList(getFormasdePagosPorHotel(idAlojamiento), "idTipoPago", "nombreEs");
        }


        public SelectList getComboPaises()
        {
            return new SelectList(getPaises(), "idPais", "nombreES");
        }

        public string crearReservaMostrador(CreacionEdicionReservaGrilla vm)
        {
            HuespedesService sv = new HuespedesService(this.DB);
            Huespede unHues = sv.crearHuespedMostrador(vm.apellido, vm.nombre, vm.dni, vm.email, vm.idPais, vm.provincia, vm.ciudad,vm.telefono,vm.direccion,vm.celular);
            HabitacionesService svHab = new HabitacionesService(this.DB);
            Habitacione habitacion;
            DateTime fechaIn = DateTime.ParseExact(vm.fechain, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime fechaOut = DateTime.ParseExact(vm.fechaout, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime? checkIn = vm.checkIn != null && vm.checkIn != "" ? DateTime.ParseExact(vm.checkIn, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) : (DateTime?)null;
            DateTime? checkOut = vm.checkOut != null && vm.checkOut != "" ? DateTime.ParseExact(vm.checkOut, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) : (DateTime?)null;

            string resultado = "";
            bool habitacionesLibres = true;
            List<Habitacione> habitacionesListado = new List<Habitacione>();

            foreach (int idHabitacion in vm.idHabitacion)
            {
                habitacion = svHab.getHabitacion(idHabitacion);
                if (this.getCantidadReservasPorHabitacion(idHabitacion, 0, fechaIn, fechaOut) > 0)
                {   //cargo el listado con habitaciones en OVERBOOKING
                    if (habitacionesLibres && habitacionesListado.Count > 0)
                        habitacionesListado.Clear();
                    habitacionesLibres = false;
                    habitacionesListado.Add(habitacion);
                    if (resultado == "")
                        resultado = habitacion.idHabitacion.ToString();
                    else
                        resultado = resultado + "," + habitacion.idHabitacion.ToString();
                }
                //cargo el listado con habitaciones de la reserva
                else if (habitacionesLibres)
                    habitacionesListado.Add(habitacion);
            }

            if (habitacionesLibres)
            {
                Reserva unRsv = new Reserva();
                unRsv.idAlojamiento = getIdAlojamiento();
                unRsv.fechaEntrada = fechaIn;
                unRsv.fechaSalida = fechaOut;
                unRsv.checkIn = checkIn;
                unRsv.checkOut = checkOut;
                unRsv.idEstado = vm.idEstado;
                unRsv.fechaEstado = DateTime.Now;
                unRsv.fechaCreacion = DateTime.Now;
                unRsv.fechaModificacion = DateTime.Now;
                unRsv.idTipoPago = vm.idTipoPago;
                unRsv.idHuesped = unHues.idHuesped;
                unRsv.HabitacionesPorReservas = new List<HabitacionesPorReserva>();
                unRsv.HabitacionesPorReservas = new List<HabitacionesPorReserva>();
                for (int i = 0; i < habitacionesListado.Count(); i++)
                {
                    foreach(HabitacionesPorReserva habitacionPR in crearHabitacionesPorReserva(fechaIn, fechaOut, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalMostrador"]), vm.idCondicion[i], habitacionesListado[i]))
                    {
                        unRsv.HabitacionesPorReservas.Add(habitacionPR);
                    }
                }
                unRsv.cantidadHuespedes = vm.pasajeros;
                unRsv.observaciones = vm.observaciones;
                unRsv.comentarios = vm.comentarios;
                if (vm.idTipoPago == 1)
                    unRsv.idTipoPagoEnHotel = vm.idTipoPagoEnHotel;
                unRsv.Canale = DB.Canales.Find(2);
                DB.Reservas.Add(unRsv);
                DB.SaveChanges();
                resultado = "OK[" + unRsv.idReserva.ToString() + "]";
            }
            else {
                resultado = "OVERBOOKING[" + resultado + "]";
            }
            return resultado;
        }

        public void eliminarReservaMostrador(int idReserva)
        {
            HabitacionesService habitacionesService = new HabitacionesService(this.DB);
            Reserva unRsv = getReserva(idReserva);
            if (unRsv.idCanal != 2 && unRsv.idCanal!=3)//no es mostrador
            {
                throw new ArgumentException("No se borra");
            }
            else
            {
                habitacionesService.EliminarHabitacionesPorReserva(idReserva);
                unRsv.HabitacionesPorReservas.Clear();
                DB.Reservas.Remove(unRsv);
                DB.SaveChanges();
            }
        }

        public void eliminar(int idReserva)
        {
            HabitacionesService habitacionesService = new HabitacionesService(this.DB);
            Reserva unRsv = getReserva(idReserva);            
            habitacionesService.EliminarHabitacionesPorReserva(idReserva);
            unRsv.HabitacionesPorReservas.Clear();
            DB.Reservas.Remove(unRsv);
            DB.SaveChanges();
        }

        public string editarReservaMostrador(CreacionEdicionReservaGrilla vm)
        {
            HuespedesService sv = new HuespedesService(this.DB);            
            Reserva unRsv = getReserva(vm.idReserva);
            DateTime fechaIn = DateTime.ParseExact(vm.fechain, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime fechaOut = DateTime.ParseExact(vm.fechaout, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime? checkIn = vm.checkIn != null && vm.checkIn != "" ? DateTime.ParseExact(vm.checkIn, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) : (DateTime?)null;
            DateTime? checkOut = vm.checkOut != null && vm.checkOut != "" ? DateTime.ParseExact(vm.checkOut, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) : (DateTime?)null;            
            
            Huespede unHues = sv.editarHuespedMostrador(unRsv.idHuesped, vm.apellido, vm.nombre, vm.dni, vm.email, vm.idPais, vm.provincia, vm.ciudad,vm.telefono,vm.direccion,vm.celular);
            HabitacionesService svHab = new HabitacionesService(this.DB);
            Habitacione habitacion;
            string resultado = "";

            int idCanal = unRsv.idCanal.HasValue ? unRsv.idCanal.Value : Convert.ToInt32(ConfigurationManager.AppSettings["idCanalMostrador"]);
            bool habitacionesLibres = true;
            List<Habitacione> habitacionesListado = new List<Habitacione>();

            foreach (int idHabitacion in vm.idHabitacion)
            {
                habitacion = svHab.getHabitacion(idHabitacion);
                if (this.getCantidadReservasPorHabitacion(idHabitacion, unRsv.idReserva, fechaIn, fechaOut) > 0)
                {   //cargo el listado con habitaciones en OVERBOOKING
                    if (habitacionesLibres && habitacionesListado.Count > 0)
                        habitacionesListado.Clear();
                    habitacionesLibres = false;
                    habitacionesListado.Add(habitacion);
                    if (resultado == "")
                        resultado = habitacion.idHabitacion.ToString();
                    else
                        resultado = resultado + "," + habitacion.idHabitacion.ToString();
                }
                //cargo el listado con habitaciones de la reserva
                else if (habitacionesLibres)
                    habitacionesListado.Add(habitacion);
            }

            if (habitacionesLibres)
            {
                if ((vm.idEstado != unRsv.idEstado) || (vm.pasajeros != unRsv.cantidadHuespedes) || (fechaIn != unRsv.fechaEntrada) || (fechaOut != unRsv.fechaSalida) || (vm.comentarios != unRsv.comentarios) || (vm.observaciones != unRsv.observaciones) || (vm.cambiaronHabitaciones))
                {
                    InsertarHistorialReserva(unRsv, "Modificación");
                }
                unRsv.idEstado = vm.idEstado;                
                unRsv.fechaEstado = DateTime.Now;
                unRsv.cantidadHuespedes = vm.pasajeros;
                unRsv.fechaCreacion = DateTime.Now;
                unRsv.observaciones = vm.observaciones;
                unRsv.comentarios = vm.comentarios;
                if(vm.idTipoPago == 1)
                    unRsv.idTipoPagoEnHotel = vm.idTipoPagoEnHotel;
                HabitacionesService habitacionesService = new HabitacionesService(this.DB);
                string[] idHabitacionPrevio = vm.idHabitacionPrevio.Trim('|').Split('|');
                string[] idCondicionPrevio = vm.idCondicionPrevio.Trim('|').Split('|');
                unRsv.HabitacionesPorReservas = svHab.getHabitacionesPorReservaPorId(vm.idReserva);

                //elimino las habitaciones que ya no figuran en el listado vm.idHabitacion
                List<HabitacionesPorReserva> habitacionesEliminar = unRsv.HabitacionesPorReservas.Where(h => !vm.idHabitacion.Contains(h.idHabitacion)).ToList();
                foreach (HabitacionesPorReserva habitacionPR in habitacionesEliminar)
                {
                    habitacionesService.EliminarHabitacionesPorReserva(vm.idReserva, habitacionPR.idHabitacion);                    
                }

                int? idCondicionPrevioInt, idHabitacionPrevioInt;                
                for (int i = 0; i < vm.idHabitacion.Count(); i++)
                {
                    if (i >= idCondicionPrevio.Count() || idCondicionPrevio[i] == null || idCondicionPrevio[i] == "")
                        idCondicionPrevioInt = null;
                    else
                        idCondicionPrevioInt = Convert.ToInt32(idCondicionPrevio[i]);

                    if (i >= idHabitacionPrevio.Count() || idHabitacionPrevio[i] == null || idHabitacionPrevio[i] == "")
                        idHabitacionPrevioInt = null;
                    else
                        idHabitacionPrevioInt = Convert.ToInt32(idHabitacionPrevio[i]);

                    if (vm.idHabitacion[i] != idHabitacionPrevioInt || vm.idCondicion[i] != idCondicionPrevioInt)
                    {
                        if (vm.idCondicion[i] == -1)
                            vm.idCondicion[i] = null;

                        if (idHabitacionPrevioInt != null)
                        {
                            habitacionesService.EliminarHabitacionesPorReserva(vm.idReserva, idHabitacionPrevioInt.Value);
                        }

                        foreach (HabitacionesPorReserva habitacionPR in crearHabitacionesPorReserva(fechaIn, fechaOut, idCanal, vm.idCondicion[i], habitacionesListado[i]))
                        {
                            unRsv.HabitacionesPorReservas.Add(habitacionPR);
                        }
                    }else if (unRsv.fechaEntrada != fechaIn || unRsv.fechaSalida != fechaOut)
                    {
                        //si se cambio la fecha de entrada o salida, de modo que no contempla 
                        //ninguno de los dias cargados a la reserva ya existente
                        if (fechaIn > unRsv.fechaSalida || fechaOut < unRsv.fechaEntrada)
                        {
                            habitacionesService.EliminarHabitacionesPorReserva(vm.idReserva, idHabitacionPrevioInt.Value);

                            foreach (HabitacionesPorReserva habitacionPR in crearHabitacionesPorReserva(fechaIn, fechaOut, idCanal, vm.idCondicion[i], habitacionesListado[i]))
                            {
                                unRsv.HabitacionesPorReservas.Add(habitacionPR);
                            }
                        }
                        //si se corrio la reserva pocos dias, manteniendo algunos dias de la reserva original
                        //si se corrio el bloque de fechas hacia la DERECHA
                        else if (fechaIn > unRsv.fechaEntrada || fechaOut > unRsv.fechaSalida)
                        {
                            for (int x = 0; x < (fechaIn - unRsv.fechaEntrada).Days; x++)
                            {
                                habitacionesService.EliminarHabitacionesPorReservaPorFecha(vm.idReserva, idHabitacionPrevioInt.Value, unRsv.fechaEntrada.AddDays(x));
                            }

                            if (fechaOut > unRsv.fechaSalida)
                            {
                                foreach (HabitacionesPorReserva habitacionPR in crearHabitacionesPorReserva(unRsv.fechaSalida, fechaOut, idCanal, vm.idCondicion[i], habitacionesListado[i]))
                                {
                                    unRsv.HabitacionesPorReservas.Add(habitacionPR);
                                }
                            }
                        }
                        //si se corrio el bloque de fechas hacia la IZQUIERDA
                        else if (fechaOut < unRsv.fechaSalida || fechaIn < unRsv.fechaEntrada)
                        {
                            for (int x = 1; x <= (unRsv.fechaSalida - fechaOut).Days; x++)
                            {
                                habitacionesService.EliminarHabitacionesPorReservaPorFecha(vm.idReserva, idHabitacionPrevioInt.Value, unRsv.fechaSalida.AddDays(-x));
                            }

                            if (fechaIn < unRsv.fechaEntrada)
                            {
                                foreach (HabitacionesPorReserva habitacionPR in crearHabitacionesPorReserva(fechaIn, unRsv.fechaEntrada, idCanal, vm.idCondicion[i], habitacionesListado[i]))
                                {
                                    unRsv.HabitacionesPorReservas.Add(habitacionPR);
                                }
                            }
                        }
                    }
                }

                unRsv.fechaEntrada = fechaIn;
                unRsv.fechaSalida = fechaOut;
                unRsv.checkIn = checkIn;
                unRsv.checkOut = checkOut;
                unRsv.idTipoPago = vm.idTipoPago;
                unRsv.fechaModificacion = DateTime.Now;
                DB.Entry(unRsv).State = EntityState.Modified;
                DB.SaveChanges();
                resultado = "OK[" + unRsv.idReserva.ToString() + "]";
            }
            else {
                resultado = "OVERBOOKING[" + resultado + "]";
            }
            return resultado;
        }
		
		public void InsertarHistorialReserva(Reserva unRsv, string accion)
		{
            Sesion sesion = getSesion();
			HistorialReserva log = new HistorialReserva();
			log.idReserva = unRsv.idReserva;
			log.accion = accion;
			log.cantidadHuespedes = unRsv.cantidadHuespedes;
			log.fechaEntrada = unRsv.fechaEntrada;
			log.fechaSalida = unRsv.fechaSalida;
			log.idHuesped = unRsv.idHuesped;
			log.idAlojamiento = getIdAlojamiento();
			log.idEstado = unRsv.idEstado;
			log.checkIn = unRsv.checkIn;
			log.checkOut = unRsv.checkOut;
			log.fechaModificacion = DateTime.Now;
			log.idUsuario = sesion.idUsuario;
			log.precioPorDia = unRsv.precioPorDia;
            log.observaciones = unRsv.observaciones;
            foreach (HabitacionesPorReserva habitacionPorReserva in unRsv.HabitacionesPorReservas)
            {
                if (log.tiposHabitaciones == null)
                {
                    log.tiposHabitaciones = habitacionPorReserva.Habitacione.TiposHabitacione.nombreEs;
                    log.habitaciones = habitacionPorReserva.Habitacione.nombre;
                }
                else {
                    log.tiposHabitaciones = log.tiposHabitaciones + "|" + habitacionPorReserva.Habitacione.TiposHabitacione.nombreEs;
                    log.habitaciones = log.habitaciones + "|" + habitacionPorReserva.Habitacione.nombre;
                }
            }
            DB.HistorialReservas.Add(log);
			DB.SaveChanges();
		}

        public List<HabitacionesPorReserva> crearHabitacionesPorReserva(DateTime fechaIn, DateTime fechaOut, int idCanal, int? idCondicion, Habitacione habitacionItem)
        {
            TarifasBase tarifaBase;
            TarifasPorDia tarifasPorDia;
            HabitacionesPorReserva habitacionPR;
            TarifasBaseService tarifasBaseService = new TarifasBaseService(this.DB);
            TarifasPorDiaService tarifasPorDiaService = new TarifasPorDiaService(this.DB);
            List<HabitacionesPorReserva> habitacionesPorReservas = new List<HabitacionesPorReserva>();

            Sesion sesion = getSesion();
            AlojamientosService alojamientosService = new AlojamientosService(this.DB);
            Alojamiento unAlojamiento = alojamientosService.getByIdAlojamiento(sesion.idAlojamientoActual);
            decimal? impuesto = unAlojamiento.impuesto;

            if (idCondicion == -1)
                idCondicion = null;
                bool actualizarTarifaBase = true;
                tarifaBase = null;
                for (int i = 0; i < (fechaOut - fechaIn).TotalDays; i++)
                {
                    if (actualizarTarifaBase)
                    {
                        tarifaBase = tarifasBaseService.get(habitacionItem.idTipoHabitacion, idCanal, idCondicion);
                        actualizarTarifaBase = false;
                    }
                    habitacionPR = new HabitacionesPorReserva();
                    habitacionPR.dia = fechaIn.AddDays(i);
                    habitacionPR.idHabitacion = habitacionItem.idHabitacion;
                    habitacionPR.idCondReserva = idCondicion;

                    tarifasPorDia = tarifasPorDiaService.get(habitacionItem.idTipoHabitacion, idCanal, habitacionPR.idCondReserva, habitacionPR.dia);
                    if (tarifasPorDia != null && tarifasPorDia.tarifaBase > 0)
                        habitacionPR.precioHabitacion = tarifasPorDia.tarifaBase;
                    else {
                        habitacionPR.precioHabitacion = getPrecioBasePorDia((int)habitacionPR.dia.DayOfWeek, tarifaBase);
                    }
                    if (habitacionPR.precioHabitacion <= 0)
                    {
                        tarifaBase = tarifasBaseService.get(habitacionItem.idTipoHabitacion, idCanal, null);
                        habitacionPR.precioHabitacion = getPrecioBasePorDia((int)habitacionPR.dia.DayOfWeek, tarifaBase);
                        actualizarTarifaBase = true;
                    }                    
                    habitacionPR.impuesto = impuesto;
                    habitacionPR.fechaCreacion = DateTime.Now;
                    habitacionesPorReservas.Add(habitacionPR);
                }
            return habitacionesPorReservas;
        }

        //MAURO
        public decimal getPrecioBasePorDia(int dia, TarifasBase tarifaBase)
        {
            decimal precio = 0;
            switch (dia)
            {
                case 0:
                    precio = tarifaBase.tarifaDomingo.HasValue ? tarifaBase.tarifaDomingo.Value : 0;
                    break;
                case 1:
                    precio = tarifaBase.tarifaLunes;
                    break;
                case 2:
                    precio = tarifaBase.tarifaMartes.HasValue ? tarifaBase.tarifaMartes.Value : 0;
                    break;
                case 3:
                    precio = tarifaBase.tarifaMiercoles.HasValue ? tarifaBase.tarifaMiercoles.Value : 0;
                    break;
                case 4:
                    precio = tarifaBase.tarifaJueves.HasValue ? tarifaBase.tarifaJueves.Value : 0;
                    break;
                case 5:
                    precio = tarifaBase.tarifaViernes.HasValue ? tarifaBase.tarifaViernes.Value : 0;
                    break;
                case 6:
                    precio = tarifaBase.tarifaSabado.HasValue ? tarifaBase.tarifaSabado.Value : 0;
                    break;
            }
            return precio;
        }

        public int GuardarReserva(PrereservaViewModel vm,bool esPrecarga,int idHuesped)
        {
            List<Habitacione> LDispo;
            string idHotel = vm.idAlojamiento.ToString();
            var sesion = System.Web.HttpContext.Current.Session["SesionInvitado"+idHotel] as SesionInvitado;
            int countHab = 0;
            if (null == sesion || sesion.reservaConfirmada)
            {
                return 0; //error , pierde la sesion o nunca ingreso.
            }
            else
            {
                HuespedesService hsv = new HuespedesService(this.DB);
                Huespede unHues;
                if (esPrecarga)
                { 
                    unHues = hsv.crearHuespedMostrador(vm.apellido,vm.nombre,vm.nroDocumento,vm.email,vm.idPais,vm.provincia,vm.ciudad,vm.telefono,vm.direccion,vm.celular);
                    sesion.idHuesped = unHues.idHuesped;
                }
                else
                {
                    unHues = DB.Huespedes.Find(idHuesped);
                    unHues.apellido = vm.apellido;
                    unHues.nombre = vm.nombre;
                    unHues.tipoDocumento = vm.tipoDocumento;
                    unHues.nroDocumento = vm.nroDocumento;
                    unHues.telefono = vm.telefono;
                    unHues.email = vm.email;
                    unHues.direccion = vm.direccion;
                    unHues.celular = vm.celular;
                    unHues.razonSocial = vm.razonSocial;
                    unHues.responsable = vm.responsable;
                    unHues.emailAdmin = vm.emailAdmin;
                    if(vm.url != null)
                        unHues.url = vm.url;
                    unHues.cuit = vm.cuit;
                    unHues.fax = vm.fax;
                    unHues.fechaCrecion = DateTime.Now;
                    unHues.idPais = vm.idPais;
                    unHues.provincia = vm.provincia;
                    unHues.ciudad = vm.ciudad;
                    DB.Entry(unHues).State = EntityState.Modified;
                    DB.SaveChanges();
                }

                //if (esPrecarga && sesion.idReserva > 0)
                if (esPrecarga)
                {
                    Reserva unRsv = new Reserva();
                    unRsv.idAlojamiento = sesion.idAlojamiento;
                    unRsv.fechaEntrada = sesion.fechaEntrada;
                    unRsv.fechaSalida = sesion.fechaSalida;
                    //estado 1 = PRECARGA
                    unRsv.idEstado = 1;
                    unRsv.cantidadHuespedes = sesion.pasajeros;
                    unRsv.fechaEstado = DateTime.Now;
                    unRsv.fechaCreacion = DateTime.Now;
                    unRsv.idTipoPago = vm.tipopago;
                    unRsv.idHuesped = unHues.idHuesped;
                    unRsv.idCanal = 1;
                    unRsv.idTipoPagoEnHotel = 0;
                    unRsv.Canale = DB.Canales.Find(1);
                    int i = -1;
                    int j = -1;
                    foreach (TiposHabitacione itemTH in sesion.listado)
                    {
                        i++;
                        j++;
                        List<Habitacione> itemTH1 = new List<Habitacione>();
                        LDispo = new List<Habitacione>();
                        countHab = 0;
                        if (sesion.listadoElige[j] != 0)
                        {
                            itemTH1 = DB.Habitaciones.Where(h => h.idTipoHabitacion == itemTH.idTipoHabitacion).ToList();
                            foreach (Habitacione itemH in itemTH1)
                            {
                                var itemR = from x in DB.Reservas.Where(r => r.idAlojamiento == vm.idAlojamiento &&
                                ((r.fechaEntrada >= vm.fechaEntrada && r.fechaEntrada < vm.fechaSalida) || (r.fechaSalida > vm.fechaEntrada && r.fechaSalida <= vm.fechaSalida))
                                && r.idEstado != 4)
                                            select x;

                                var itemR0 = from s in itemR
                                             from x in s.HabitacionesPorReservas.Where(l => l.idHabitacion == itemH.idHabitacion)
                                             select x;

                                if (itemR0.Count() == 0)
                                {
                                    countHab++;
                                    LDispo.Add(itemH);
                                }

                            }
                        }
                        if (countHab != 0)
                        {
                            int iHab = sesion.listadoElige[j];
                            int idCond = sesion.listadoCondicionPorHab[j];
                            for (int z = 0; z < iHab; z++)
                            {
                                foreach (DiaRsv itemD in sesion.listadoDias[j].listado)
                                {
                                    HabitacionesPorReserva habPR = new HabitacionesPorReserva();
                                    if (vm.tieneAdulto)
                                    {
                                        habPR.adicionalesAdultos = sesion.listadoEligeAdulto[i];
                                    }
                                    if (vm.tieneNino)
                                    {
                                        habPR.adicionalesMenores = sesion.listadoEligeNino[i];
                                    }
                                    habPR.camasMatrimoniales = itemTH.camaMatrimonial;
                                    habPR.camasSimples = itemTH.camasSimples;
                                    habPR.dia = itemD.dia;
                                    habPR.fechaCreacion = DateTime.Now;
                                    habPR.precioAdicionalAdulto = itemD.precioAdicionalAdulto;
                                    habPR.precioAdicionalMenor = itemD.precioAdicionalMenor;
                                    habPR.precioHabitacion = itemD.precioHabitacion;
                                    habPR.idHabitacion = LDispo[z].idHabitacion;
                                    habPR.impuesto = sesion.impuesto;
                                    habPR.idCondReserva = idCond;
                                    unRsv.HabitacionesPorReservas.Add(habPR);
                                }
                            }
                        }
                    }
                    DB.Reservas.Add(unRsv);

                    try
                    {
                        DB.SaveChanges();
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        if (unRsv != null && unRsv.idReserva > 0)
                            eliminar(unRsv.idReserva);

                        return 0;
                        
                    }
                    sesion.idReserva = unRsv.idReserva;
                    return unRsv.idReserva;
                }
                else
                {
                    Reserva unRsv = DB.Reservas.Find(sesion.idReserva);
                    unRsv.idTipoPago = vm.tipopago;
                    DB.Entry(unRsv).State = EntityState.Modified;
                    DB.SaveChanges();
                 
                    return sesion.idReserva;
                }
            }
            
        }

        public int getCantidadHuespedes(DateTime inicio, DateTime fin)
        {
            int idAlojamiento = getIdAlojamiento();
            int? huespedes =  DB.Reservas.Where(t => t.idAlojamiento == idAlojamiento && t.idEstado != 4 && t.checkIn.HasValue && !t.checkOut.HasValue &&
                                     (
                                       (
                                        DbFunctions.TruncateTime(t.fechaEntrada) <= DbFunctions.TruncateTime(inicio) &&
                                        DbFunctions.TruncateTime(t.fechaSalida) >= DbFunctions.TruncateTime(fin)
                                        )
                                        ||
                                        (DbFunctions.TruncateTime(t.fechaEntrada) >= DbFunctions.TruncateTime(inicio) &&
                                        DbFunctions.TruncateTime(t.fechaEntrada) < DbFunctions.TruncateTime(fin)
                                        ) || (
                                        DbFunctions.TruncateTime(t.fechaSalida) > DbFunctions.TruncateTime(inicio) &&
                                        DbFunctions.TruncateTime(t.fechaSalida) <= DbFunctions.TruncateTime(fin)
                                        )
                                     )).Select(t => t.cantidadHuespedes).Sum();

            if (huespedes.HasValue)
                return huespedes.Value;
            else
                return 0;
        }

        public int getCantidadReservasNuevas(DateTime inicio, DateTime fin)
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.Reservas.Where(t => t.idAlojamiento == idAlojamiento && t.idEstado != 4 &&
                                      (
                                       (DbFunctions.TruncateTime(t.fechaCreacion) >= DbFunctions.TruncateTime(inicio) &&
                                       DbFunctions.TruncateTime(t.fechaCreacion) <= DbFunctions.TruncateTime(fin)
                                       )
                                    )).Count();
        }

        public int getCantidadReservasCanceladas(DateTime inicio, DateTime fin)
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.Reservas.Where(t => t.idAlojamiento == idAlojamiento && t.idEstado == 4 && t.fechaModificacion.HasValue &&                                                                             
                                       DbFunctions.TruncateTime(t.fechaModificacion) >= DbFunctions.TruncateTime(inicio) &&
                                       DbFunctions.TruncateTime(t.fechaModificacion) <= DbFunctions.TruncateTime(fin)
                                       ).Count();
        }

        public int getCantidadCheckIn(DateTime inicio, DateTime fin)
        {
            int idAlojamiento = getIdAlojamiento();
            List<Reserva> reservas = DB.Reservas.Where(t => t.idAlojamiento == idAlojamiento && t.idEstado != 4 && t.checkIn.HasValue &&
                                      DbFunctions.TruncateTime(t.checkIn) >= DbFunctions.TruncateTime(inicio) &&
                                      DbFunctions.TruncateTime(t.checkIn) <= DbFunctions.TruncateTime(fin)
                                    ).ToList();

            return DB.Reservas.Where(t => t.idAlojamiento == idAlojamiento && t.idEstado != 4 && t.checkIn.HasValue &&
                                      DbFunctions.TruncateTime(t.checkIn) >= DbFunctions.TruncateTime(inicio) && 
                                      DbFunctions.TruncateTime(t.checkIn) <= DbFunctions.TruncateTime(fin)
                                    ).Count();
        }

        public int getCantidadCheckOut(DateTime inicio, DateTime fin)
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.Reservas.Where(t => t.idAlojamiento == idAlojamiento && t.idEstado != 4 && t.checkOut.HasValue &&
                                       DbFunctions.TruncateTime(t.checkOut) >= DbFunctions.TruncateTime(inicio) &&
                                       DbFunctions.TruncateTime(t.checkOut) <= DbFunctions.TruncateTime(fin)
                                       ).Count();
        }

        public int getCantidadCheckInPendientes(DateTime inicio, DateTime fin)
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.Reservas.Where(t => t.idAlojamiento == idAlojamiento && t.idEstado != 4 && !t.checkIn.HasValue &&
                                       DbFunctions.TruncateTime(t.fechaEntrada) >= DbFunctions.TruncateTime(inicio) &&
                                       DbFunctions.TruncateTime(t.fechaEntrada) <= DbFunctions.TruncateTime(fin)                                      
                                    ).Count();
        }

        public int getCantidadCheckOutPendientes(DateTime inicio, DateTime fin)
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.Reservas.Where(t => t.idAlojamiento == idAlojamiento && t.idEstado != 4 && !t.checkOut.HasValue &&
                                      DbFunctions.TruncateTime(t.fechaSalida) >= DbFunctions.TruncateTime(inicio) &&
                                      DbFunctions.TruncateTime(t.fechaSalida) <= DbFunctions.TruncateTime(fin)
                                     ).Count();
        }

        public int getCantidadHabitacionesOcupadas(DateTime inicio, DateTime fin)
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.HabitacionesPorReservas.Where(t => t.Reserva.idAlojamiento == idAlojamiento && t.Reserva.idEstado != 4 && t.Reserva.checkIn.HasValue
                                      //(
                                       
                                       &&
                                       (                                       
                                        (
                                        DbFunctions.TruncateTime(t.Reserva.fechaEntrada) <= DbFunctions.TruncateTime(inicio) &&
                                        DbFunctions.TruncateTime(t.Reserva.fechaSalida) >= DbFunctions.TruncateTime(fin)
                                        )
                                        ||
                                        (
                                        DbFunctions.TruncateTime(t.Reserva.fechaEntrada) >= DbFunctions.TruncateTime(inicio) &&
                                        DbFunctions.TruncateTime(t.Reserva.fechaEntrada) < DbFunctions.TruncateTime(fin)
                                        ) 
                                        || (
                                        DbFunctions.TruncateTime(t.Reserva.fechaSalida) > DbFunctions.TruncateTime(inicio) &&
                                        DbFunctions.TruncateTime(t.Reserva.fechaSalida) <= DbFunctions.TruncateTime(fin)
                                        )
                                      )
                                    //)
                                    ).Select(t => t.Habitacione.idHabitacion).Distinct().Count();
        }
    }
}