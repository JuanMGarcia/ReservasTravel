﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.Data.Entity;
using ReservasWeb.ViewModels;
using ReservasWeb.Utils;
using System.Globalization;

namespace ReservasWeb.Services
{
    public class TarifasPorDiaService : BaseService
    {
        public TarifasPorDiaService(ReservasWebEntities db) : base(db)
        {
        }

        public TarifasPorDia get(int idTipoHabitacion, int idCanal, int? idCondicionReserva, DateTime fecha)
        {
            int idAlojemiento = getIdAlojamiento();
            TarifasPorDia tarifaPorDia = null;
            try
            {
                tarifaPorDia = (from i in DB.TarifasPorDias.Where(i => i.idAlojamiento == idAlojemiento && i.idTipoHabitacion == idTipoHabitacion && i.idCanal == idCanal && idCondicionReserva == i.idCondReserva && i.fecha == DbFunctions.TruncateTime(fecha))
                                           select i).ToList().ElementAt(0);
            }
            catch (Exception)
            {                
            }


            return tarifaPorDia;
        }

        public TarifasPorDia getInvitado(int idTipoHabitacion, int idCanal, int? idCondicionReserva, DateTime fecha, int idAlojamiento)
        {
            TarifasPorDia tarifaPorDia = null;
            try
            {
                tarifaPorDia = (from i in DB.TarifasPorDias.Where(i => i.idAlojamiento == idAlojamiento && i.idTipoHabitacion == idTipoHabitacion && i.idCanal == idCanal && idCondicionReserva == i.idCondReserva && i.fecha == DbFunctions.TruncateTime(fecha))
                                select i).ToList().ElementAt(0);
            }
            catch (Exception)
            {
            }


            return tarifaPorDia;
        }

        public void crear(TarifasPorDia tarifaPorDia)
        {
            tarifaPorDia.idAlojamiento = getIdAlojamiento();
            DB.TarifasPorDias.Add(tarifaPorDia);
            DB.SaveChanges();
        }

        public void editar(TarifasPorDia tarifaPorDia)
        {
            DB.Entry(tarifaPorDia).State = EntityState.Modified;
            DB.SaveChanges();
        }

        public void editar(TarifaPorDiaViewModel vm)
        {
            bool crearBool = false;
            TarifasPorDia tarifasPorDia = get(vm.idTipoHabitacion, vm.idCanal, vm.idCondicionReserva, vm.fecha);
            if (tarifasPorDia == null)
            {
                crearBool = true;
                tarifasPorDia = new TarifasPorDia();
                tarifasPorDia.idTipoHabitacion = vm.idTipoHabitacion;
                tarifasPorDia.idCanal = vm.idCanal;
                if(vm.idCondicionReserva.HasValue)
                    tarifasPorDia.idCondReserva = vm.idCondicionReserva.Value;
                tarifasPorDia.fecha = vm.fecha;
            }

            if (vm.disponibilidadCanal != null && vm.disponibilidadCanal != "")
                tarifasPorDia.disponibilidadCanal = Int32.Parse(vm.disponibilidadCanal);
            if (vm.tarifaBase != null && vm.tarifaBase != "")
                tarifasPorDia.tarifaBase = Convert.ToDecimal(vm.tarifaBase.Replace("$ ", ""), new CultureInfo("en-US"));
            if (vm.estadiaMinima != null && vm.estadiaMinima != "")
                tarifasPorDia.estadiaMinima = Int32.Parse(vm.estadiaMinima);
            else
                tarifasPorDia.estadiaMinima = 1;
            if (vm.descuentoPax != null && vm.descuentoPax != "" && vm.descuentoPax != "$ ")
                tarifasPorDia.descuentoPorPax = Convert.ToDecimal(vm.descuentoPax.Replace("$ ", ""), new CultureInfo("en-US"));
            else
                tarifasPorDia.descuentoPorPax = Convert.ToDecimal("0.00", new CultureInfo("en-US"));
            if (vm.adicionalCuna != null && vm.adicionalCuna != "" && vm.adicionalCuna != "$ ")
                tarifasPorDia.adicionalCuna = Convert.ToDecimal(vm.adicionalCuna.Replace("$ ", ""), new CultureInfo("en-US"));
            else
                tarifasPorDia.adicionalCuna = Convert.ToDecimal("0.00", new CultureInfo("en-US")); ;
            if (vm.adicionalMenor != null && vm.adicionalMenor != "" && vm.adicionalMenor != "$ ")
                tarifasPorDia.adicionalMenor = Convert.ToDecimal(vm.adicionalMenor.Replace("$ ", ""), new CultureInfo("en-US"));
            else
                tarifasPorDia.adicionalMenor = Convert.ToDecimal("0.00", new CultureInfo("en-US")); ;
            if (vm.adicionalAdulto != null && vm.adicionalAdulto != "" && vm.adicionalAdulto != "$ ")
                tarifasPorDia.adicionalAdulto = Convert.ToDecimal(vm.adicionalAdulto.Replace("$ ", ""), new CultureInfo("en-US"));
            else
                tarifasPorDia.adicionalAdulto = Convert.ToDecimal("0.00", new CultureInfo("en-US")); ;

            tarifasPorDia.permiteIngreso = vm.permiteIngreso;            
            tarifasPorDia.permiteEgreso = vm.permiteEgreso;            
            tarifasPorDia.aceptaMenores = vm.aceptaMenores;            
            tarifasPorDia.abierta = vm.abierta;

            if(crearBool)
                crear(tarifasPorDia);
            else
                editar(tarifasPorDia);
        }

    }
}