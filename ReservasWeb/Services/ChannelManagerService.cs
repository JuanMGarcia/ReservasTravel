﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.Xml;
using ReservasWeb.ViewModels;
using ReservasWeb.ViewModels.Channel;
using ReservasWeb.Services;
using System.Data.Entity;

namespace ReservasWeb.Services
{
    public class ChannelManagerService: BaseService
    {
        public ChannelManagerService(ReservasWebEntities db) : base(db)
        {
        }

        public int estaLinkeadoHotel(int idAlojamiento)
        {
            Alojamiento aloja = DB.Alojamientos.Where(s => s.idAlojamiento == idAlojamiento).FirstOrDefault();
            if (aloja != null)
            {
                if (aloja.idHotelAfuera.HasValue)
                {
                    return aloja.idHotelAfuera.Value;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return -1;
            }
        }

        public int obtenerLinkAlojamiento(int idHotelFuera)
        {
            Alojamiento unAloja = DB.Alojamientos.Where(s => s.idHotelAfuera == idHotelFuera).FirstOrDefault();
            if (unAloja!=null)
            {
                return unAloja.idAlojamiento;
            }
            else
            { 
                return -1;
            }
        }

        public void linkearHabitaciones(listadoChannelTipoHabViewModel vm)
        {
            TiposHabitacionService sv = new TiposHabitacionService(this.DB);
            int i = -1;
            foreach (var item in vm.listado)
            {
                i++;
                item.idLink = vm.linkList[i];
                TiposHabitacione hab = sv.get(item.idTipoHabitacion);
                hab.idLink = item.idLink;
                
                DB.Entry(hab).State = EntityState.Modified;
                DB.SaveChanges();
            }

        }

        public void ProcesarHotel(string xmlString)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlString);

            listadoHotelViewModel vmList = new listadoHotelViewModel();

            vmList.listado = new List<hotelViewModel>();

            foreach (XmlNode item in doc.DocumentElement.ChildNodes)
            {
                hotelViewModel vm = new hotelViewModel();
                string hotelID = item.Attributes["id"].Value;
                string hotelDescripcion = item.Attributes["description"].Value;
                vm.idHotelFuera = Int32.Parse(hotelID);
                vm.description = hotelDescripcion;
                vm.idAlojamiento = obtenerLinkAlojamiento(vm.idHotelFuera);
                vmList.listado.Add(vm);
            }

            

           

            //XmlNode hotelNode = doc.DocumentElement.SelectSingleNode("/Response/");

            //string hotelID          = hotelNode.Attributes["id"].Value;
            //string hotelDescripcion = hotelNode.Attributes["descripcion"].Value;

//            <? xml version = "1.0" encoding = "UTF-8" ?>< Response >< hotel id = "3766" description = "testReservasTravel" /></ Response >
//           < !--UUID  c5c8bca8 - 12bb - 4fb2 - 96c8 - 0a59391dc724-- >

//                < DataChunk >
//    < ResponseChunk >
//        < errors >
//            < error code =\"0\">
//                Something happened here: Line 1, position 1.
//            </ error >
//        </ errors >
//    </ ResponseChunk >
//</ DataChunk >

        }
    }
}