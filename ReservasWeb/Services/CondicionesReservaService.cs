﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.ViewModels;
using ReservasWeb.Models;
using ReservasWeb.Utils;
using System.Web.Mvc;
using System.Data.Entity;
using Newtonsoft.Json;

namespace ReservasWeb.Services
{
    public class CondicionesReservaService : BaseService
    {
        public CondicionesReservaService(ReservasWebEntities db) : base(db)
        {
        }

        public CondReserva getCondicionReserva(int id)
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.CondReservas.Where(c => c.idAlojamiento == idAlojamiento && c.idCondReserva == id).FirstOrDefault();
        }

        public List<CondReserva> getCondicionesReservaListadoAbm()
        {
            int idAlojamiento = getIdAlojamiento();            
            return DB.CondReservas.Where(c => c.idAlojamiento == idAlojamiento).ToList();
        }

        public List<CondReserva> getCondicionesReservaPorTipoHabitacion(int idTipoHabitacion)
        {
            int idAlojamiento = getIdAlojamiento();
            return (from i in DB.CondReservas
                    from z in i.TarifasBases
                    where z.idTipoHabitacion == idTipoHabitacion && z.abierta == true
                    select i
                    ).Distinct().ToList();
        }

        public List<CondReserva> getCondicionesReservaInvitado(int idAlojamiento)
        {
            return DB.CondReservas.Where(c => c.idAlojamiento == idAlojamiento).ToList();
        }
        public void Eliminar(int id)
        {
            int idAlojamiento = getIdAlojamiento();
            CondReserva condicion = DB.CondReservas.Where(t => t.idCondReserva == id && t.idAlojamiento == idAlojamiento).FirstOrDefault();//borra el seleecionado y no otro
            condicion.PoliticasCancelacions.Clear();
            condicion.TarifasBases.Clear();
            condicion.TarifasPorDias.Clear();
            condicion.TiposPagoes.Clear();
            condicion.TiposHabitaciones.Clear();
            DB.CondReservas.Remove(condicion);
            DB.SaveChanges();
        }

        public int Crear(CondReserva condicion)
        {
            condicion.fechaCreacion = DateTime.Now;
            condicion.fechaModificacion = DateTime.Now;
            DB.CondReservas.Add(condicion);
            DB.SaveChanges();
            return condicion.idCondReserva;
        }

        public void Editar(CondicionesReservaViewModel vm)
        {
            CondReserva CondReserva = DB.CondReservas.Find(vm.idCondReserva);
            CondReserva.nombreEn = vm.nombreEn;
            CondReserva.nombreEs = vm.nombreEs;
            CondReserva.nombrePt = vm.nombrePt;
            CondReserva.descripcionEn = vm.descripcionEn;
            CondReserva.descripcionEs = vm.descripcionEs;
            CondReserva.descripcionPt = vm.descripcionPt;
            CondReserva.idAlojamiento = vm.IdAlojamiento;
            CondReserva.fechaModificacion = DateTime.Now;
            CondReserva.TiposPagoes.Clear();
            foreach (FormasDePagoSeleccionadas item in vm.listaFormaPagos)
            {
                if (item.seleccionada)
                { 
                    TiposPago unTP = DB.TiposPagoes.Find(item.idFormadePago);
                    CondReserva.TiposPagoes.Add(unTP);
                }
            }
            PoliticasCancelacionService politicaCancelacionService = new PoliticasCancelacionService(this.DB);
            PoliticasCancelacion politicacancelacion = politicaCancelacionService.getPoliticasCancelacion(vm.idPoliticaCancelacion);
            CondReserva.PoliticasCancelacions.Clear();
            CondReserva.PoliticasCancelacions.Add(politicacancelacion);


            CondReserva.fechaModificacion = DateTime.Now;
            DB.Entry(CondReserva).State = EntityState.Modified;
            DB.SaveChanges();
        }

        public List<PoliticasCancelacion> getPoliticaPorAlojamiento()
        {
            int idAlojamiento = getIdAlojamiento();
            List<PoliticasCancelacion> lista = DB.PoliticasCancelacions.Where(t => t.idAlojamiento == idAlojamiento).ToList();
            return lista;
        }
      
        public SelectList getComboPoliticas()
        {
            return new SelectList(getPoliticaPorAlojamiento(), "idPoliticaCancelacion", "nombreEs");//devuelve la lista de politicas con el id y el nombre
        }

        public SelectList getPagos()
        {
            int idAlojamiento = getIdAlojamiento();
            List<TiposPago> listapagos = new List<TiposPago>();
            return new SelectList (DB.TiposPagoes.ToList());//trae la lista de pagos
        }

        public SelectList getComboPagos()
        {
            return new SelectList(getPagos(), "idTipoPago", "nombreEs");//devuelve la lista de pagos con el id y el nombre
        }

        public List<FormasDePagoSeleccionadas> getFormasdePago()
        {
            int idAlojamiento = getIdAlojamiento();
            var query = (from g in DB.TiposPagoes  orderby g.idTipoPago
                         from m in g.CondReservas.Where(x => x.idCondReserva == idAlojamiento).DefaultIfEmpty()
                         select new FormasDePagoSeleccionadas
                             {
                             seleccionada = (g == null) ? false : false,
                             idFormadePago = g.idTipoPago,
                             nombre = g.nombreEs,
                             icono = g.icono
                         });

           return query.ToList();
        }

        public List<FormasDePagoSeleccionadas> getFormasdePagoxCondicion(int idCondReserva)
        {
            int idAlojamiento = getIdAlojamiento();
            var query = (from g in DB.TiposPagoes
                         orderby g.idTipoPago
                         from m in g.CondReservas.Where(x => x.idCondReserva == idCondReserva).DefaultIfEmpty()
                         select new FormasDePagoSeleccionadas
                         {
                             seleccionada = (m == null) ? false : true,
                             idFormadePago = g.idTipoPago,
                             nombre = g.nombreEs,
                             icono = g.icono
                         });

            return query.ToList();
        }

        public List<TiposPago> getFormasdePagoSeleccionadas(List<int> ids)
        {
            return DB.TiposPagoes.Where(x => ids.Contains(x.idTipoPago)).ToList();
        }
    }
}