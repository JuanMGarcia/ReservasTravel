﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.Data.Entity;
using ReservasWeb.ViewModels;
using ReservasWeb.Utils;
using System.Web.Mvc;
using System.Globalization;
using System.IO;

namespace ReservasWeb.Services
{
    public class AlojamientosService : BaseService
    {
        public AlojamientosService(ReservasWebEntities db) : base(db)
        {               
        }

        public Alojamiento getByIdAlojamiento(int idAlojamiento)
        {
            Alojamiento unAlojamiento = DB.Alojamientos.Find(idAlojamiento);
            return unAlojamiento;
        }

        public Alojamiento getAlojamiento()
        {            
            return getByIdAlojamiento(getIdAlojamiento());
        }

        public Alojamiento getByIdProductoAdmin(int idProductoAdmin)
        {

            Alojamiento alojamiento = null;
            try
            {
                List<Alojamiento> alojamientoList = (from i in DB.Alojamientos.Where(i => i.idProductoAdmin == idProductoAdmin)
                                                     select i).ToList<Alojamiento>();
                if (alojamientoList.Count() > 0)
                {
                    alojamiento = alojamientoList.ElementAt(0);
                }
            }
            catch (Exception e)
            {
            }
            return alojamiento;
        }

        public void crear(Alojamiento alojamiento)
        {
            alojamiento.fechaCreacion = DateTime.Now;
            DB.Alojamientos.Add(alojamiento);
            DB.SaveChanges();                           
        }

        public void editar(Alojamiento alojamiento)
        {
            alojamiento.fechaModificacion = DateTime.Now;
            DB.Entry(alojamiento).State = EntityState.Modified;
            DB.SaveChanges();
        }

        public DatosAlojamientoViewModel getDatosAlojamiento()
        {
            int idAlojamiento = getIdAlojamiento();
            TiposAlojamientoService svTipoAloj = new TiposAlojamientoService(this.DB);
            MonedasService svMoneda = new MonedasService(this.DB);
            DatosAlojamientoViewModel vm = new DatosAlojamientoViewModel();
            Alojamiento aloj = DB.Alojamientos.Find(idAlojamiento);

            vm.idAlojamiento = aloj.idAlojamiento;
            vm.listaEstrellas = getComboEstrellas();
            //vm.listaZonas = new SelectList(aloj.Ciudade.Zonas, "idZona", "nombreEs");
            vm.listaMonedas = svMoneda.getComboMonedas();
            vm.listaTiposAlojamiento = svTipoAloj.getComboTiposAlojamientos();

            //if (aloj.idZona.HasValue)
            //    vm.idZona = aloj.idZona.Value;
            if (aloj.idTipoAlojamiento.HasValue)
                vm.idTipoAlojamiento = aloj.idTipoAlojamiento.Value;
            vm.idMoneda = aloj.idMoneda;            
            vm.nombreEs = aloj.nombreEs;
            vm.nombreEn = aloj.nombreEn;
            vm.nombrePt = aloj.nombrePt;
            vm.descripcionEs = (aloj.descripcionEs != null) ? aloj.descripcionEs.Replace("<br/>", System.Environment.NewLine) : "";
            vm.descripcionEn = (aloj.descripcionEn != null) ? aloj.descripcionEn.Replace("<br/>", System.Environment.NewLine) : "";
            vm.descripcionPt = (aloj.descripcionPt != null) ? aloj.descripcionPt.Replace("<br/>", System.Environment.NewLine) : "";
            vm.emailReserva = aloj.emailReserva;
            vm.emailContacto = aloj.emailContacto;
            vm.telefonos = aloj.telefonos;
            vm.ciudadPiePagina = aloj.ciudadPiePagina;
            vm.paisPiePagina = aloj.paisPiePagina;

            if (aloj.habitacionEs != null)
            {
                vm.habitacionEs = aloj.habitacionEs;                    ;
            }

            if (aloj.habitacionEn != null)
            {
                vm.habitacionEn = aloj.habitacionEn;
            }

            if (aloj.habitacionPt != null)
            {
                vm.habitacionPt = aloj.habitacionPt;
            }

            if (aloj.estrellas.HasValue)
                vm.estrellas = aloj.estrellas.Value;

            vm.ubicacion = new GoogleMap();
            vm.ubicacion.direccion = aloj.direccion;
            vm.ubicacion.zoomMapa = aloj.zoomMapa;
            vm.ubicacion.latitud = aloj.latitud;
            vm.ubicacion.longitud = aloj.longitud;
            vm.ubicacion.tipoMapa = aloj.tipoMapa;
            vm.ubicacion.pais = aloj.Ciudade.Provincia.Pais.nombreEs;
            vm.ubicacion.provincia = aloj.Ciudade.Provincia.nombreEs;
            vm.ubicacion.ciudad = aloj.Ciudade.nombreEs;
            vm.impuesto = (aloj.impuesto != null) ? aloj.impuesto.Value.ToString() : "0";
            vm.website = aloj.website;

            vm.horaCheckin = aloj.horaCheckin;
            vm.horaCheckout = aloj.horaCheckout;

            vm.cancelacionesEs = (aloj.cancelacionesEs != null) ? aloj.cancelacionesEs.Replace("<br/>", System.Environment.NewLine) : "";
            vm.cancelacionesEn = (aloj.cancelacionesEn != null) ? aloj.cancelacionesEn.Replace("<br/>", System.Environment.NewLine) : "";
            vm.cancelacionesPt = (aloj.cancelacionesPt != null) ? aloj.cancelacionesPt.Replace("<br/>", System.Environment.NewLine) : "";

            vm.mascotasEs = (aloj.mascotasEs != null) ? aloj.mascotasEs.Replace("<br/>", System.Environment.NewLine) : "";
            vm.mascotasEn = (aloj.mascotasEn != null) ? aloj.mascotasEn.Replace("<br/>", System.Environment.NewLine) : "";
            vm.mascotasPt = (aloj.mascotasPt != null) ? aloj.mascotasPt.Replace("<br/>", System.Environment.NewLine) : "";

            vm.logo = (aloj.urlLogo != null) ? aloj.urlLogo : "";
            vm.colorBarra = (aloj.colorBarra != null) ? aloj.colorBarra : "";
            vm.colorBotones = (aloj.colorBotones != null) ? aloj.colorBotones : "";
            vm.colorTextoBotones = (aloj.colorTextoBotones != null) ? aloj.colorTextoBotones : "";

            try
            {                
                vm.fotos = aloj.GaleriaImagene.Imagenes.OrderBy(t => t.orden).ThenByDescending(t => t.idImagen).ToList();

                vm.IdsFotosOrden = vm.GetIdsFotosOrden();
            }
            catch (Exception)
            {
            }

            return vm;
        }

        public SelectList getComboEstrellas()
        {
            List<int> estrellas = new List<int>();
            estrellas.Add(1);
            estrellas.Add(2);
            estrellas.Add(3);
            estrellas.Add(4);
            estrellas.Add(5);            
            return new SelectList(estrellas);
        }

        public void Editar(DatosAlojamientoViewModel vm)
        {
            int idAlojamiento = getIdAlojamiento();
            Alojamiento aloj = DB.Alojamientos.Find(idAlojamiento);
            //aloj.idZona = vm.idZona;
            aloj.idTipoAlojamiento = vm.idTipoAlojamiento;
            aloj.idMoneda = vm.idMoneda;
            aloj.direccion = vm.ubicacion.direccion;
            aloj.latitud = vm.ubicacion.latitud;
            aloj.longitud = vm.ubicacion.longitud;
            aloj.zoomMapa = vm.ubicacion.zoomMapa;
            aloj.tipoMapa = vm.ubicacion.tipoMapa;
            aloj.nombreEs = vm.nombreEs;
            aloj.nombreEn = vm.nombreEn;
            aloj.nombrePt = vm.nombrePt;
            aloj.descripcionEs = (vm.descripcionEs != null) ? vm.descripcionEs.Replace(System.Environment.NewLine, "<br/>") : "";
            aloj.descripcionEn = (vm.descripcionEn != null) ? vm.descripcionEn.Replace(System.Environment.NewLine, "<br/>") : "";
            aloj.descripcionPt = (vm.descripcionPt != null) ? vm.descripcionPt.Replace(System.Environment.NewLine, "<br/>") : "";
            aloj.emailReserva = vm.emailReserva;
            aloj.emailContacto = vm.emailContacto;
            aloj.telefonos = vm.telefonos;
            aloj.fechaModificacion = DateTime.Now;
            aloj.estrellas = vm.estrellas;
            aloj.ciudadPiePagina = vm.ciudadPiePagina;
            aloj.paisPiePagina = vm.paisPiePagina;
            aloj.impuesto = Convert.ToDecimal(vm.impuesto, new CultureInfo("en-US"));

            aloj.horaCheckin = vm.horaCheckin;
            aloj.horaCheckout = vm.horaCheckout;

            if (vm.habitacionEs != null)
            {
                aloj.habitacionEs = vm.habitacionEs;
            }

            if (vm.habitacionEn != null)
            {
                aloj.habitacionEn = vm.habitacionEn;
            }

            if (vm.habitacionPt != null)
            {
                aloj.habitacionPt = vm.habitacionPt;
            }

            aloj.cancelacionesEs = (vm.cancelacionesEs != null) ? vm.cancelacionesEs.Replace(System.Environment.NewLine, "<br/>") : "";
            aloj.cancelacionesEn = (vm.cancelacionesEn != null) ? vm.cancelacionesEn.Replace(System.Environment.NewLine, "<br/>") : "";
            aloj.cancelacionesPt = (vm.cancelacionesPt != null) ? vm.cancelacionesPt.Replace(System.Environment.NewLine, "<br/>") : "";

            aloj.mascotasEs = (vm.mascotasEs != null) ? vm.mascotasEs.Replace(System.Environment.NewLine, "<br/>") : "";
            aloj.mascotasEn = (vm.mascotasEn != null) ? vm.mascotasEn.Replace(System.Environment.NewLine, "<br/>") : "";
            aloj.mascotasPt = (vm.mascotasPt != null) ? vm.mascotasPt.Replace(System.Environment.NewLine, "<br/>") : "";

            aloj.website = vm.website;

            if (vm.logoUpload != null)
            {
                string ruta = System.Web.HttpContext.Current.Server.MapPath("~/Files/" + idAlojamiento.ToString() + "/");

                if (!Directory.Exists(ruta))
                    Directory.CreateDirectory(ruta);

                string extension = new FileInfo(vm.logoUpload.FileName).Extension;

                string nombreArchivo = "logo_" + new Random().Next().ToString() + "_" + idAlojamiento.ToString();

                while (File.Exists(ruta + nombreArchivo + extension))
                {
                    nombreArchivo = "logo_" + new Random().Next().ToString() + "_" + idAlojamiento.ToString();
                }

                if (aloj.urlLogo != null && aloj.urlLogo != "")
                {
                    if (File.Exists(ruta + aloj.urlLogo))
                    {
                        File.Delete(ruta + aloj.urlLogo);
                    }
                }

                vm.logoUpload.SaveAs(ruta + nombreArchivo + extension);

                aloj.urlLogo = nombreArchivo + extension;
            }

            aloj.colorBarra = vm.colorBarra;
            aloj.colorBotones = vm.colorBotones;
            aloj.colorTextoBotones = vm.colorTextoBotones;

            GaleriasService svGale = new GaleriasService(this.DB);

            if (vm.IdsFotosOrden != null)
            {
                string[] idsFotosSplit = vm.IdsFotosOrden.Split(',');
                Imagene foto;
                int idFotoInt;
                for (int i = 0; i < idsFotosSplit.Count(); i++)
                {
                    idFotoInt = Int32.Parse(idsFotosSplit[i]);
                    var fotoLinq = from f in DB.Imagenes.Where(f => f.idImagen == idFotoInt)
                                   select f;
                    if (fotoLinq.ToList().Count() > 0)
                    {
                        foto = fotoLinq.ToList().ElementAt(0);
                        svGale.SetFotoOrden(foto, i);
                    }
                }
            }

            DB.Entry(aloj).State = EntityState.Modified;
            DB.SaveChanges();
        }

        //public int subirLogo(DatosAlojamientoViewModel vm)
        //{
        //    int idAlojamiento = 0;
        //    Alojamiento alo = DB.Alojamientos.Find(idAlojamiento);

        //    alo.urlLogo = vm.logo;

        //    DB.SaveChanges();
        //    if (vm.logoUpload != null) 
        //    {
        //        DB.Entry(alo).State = EntityState.Modified;
        //        DB.SaveChanges();
        //    }
        //    return alo.idAlojamiento;
        //}

        public void quitarLogo()
        {
            int idAlojamiento = getIdAlojamiento();
            Alojamiento aloj = DB.Alojamientos.Find(idAlojamiento);
            string ruta = System.Web.HttpContext.Current.Server.MapPath("~/Files/" + idAlojamiento.ToString() + "/");
            if (File.Exists(ruta + aloj.urlLogo))
            {
                File.Delete(ruta + aloj.urlLogo);
            }
            aloj.urlLogo = "";
            DB.Entry(aloj).State = EntityState.Modified;
            DB.SaveChanges();
        }


    }
}