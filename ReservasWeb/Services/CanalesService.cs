﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.Data.Entity;

namespace ReservasWeb.Services
{
    public class CanalesService : BaseService
    {

        public CanalesService(ReservasWebEntities db) : base(db)
        {
        }

        public List<Canale> getCanalesDeAlojamiento()
        {
            int idAlojemiento = getIdAlojamiento();
            List<Canale> canalesList = (from i in DB.CanalesPorAlojamientoes.Where(i => i.idAlojamiento == idAlojemiento)
                                                 select i.Canale).ToList<Canale>();

            return canalesList;
        }

        public Canale getCanal(int idCanal)
        {
            return DB.Canales.Find(idCanal);
        }

        public void crear(CanalesPorAlojamiento canalPorAlojamiento)
        {
            DB.CanalesPorAlojamientoes.Add(canalPorAlojamiento);
            DB.SaveChanges();
        }

        //public void editar(CanalesPorAlojamiento canalPorAlojamiento)
        //{
        //    DB.Entry(canalPorAlojamiento).State = EntityState.Modified;
        //    DB.SaveChanges();
        //}
    }
}