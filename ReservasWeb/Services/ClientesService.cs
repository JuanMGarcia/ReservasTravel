﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.Data.Entity;

namespace ReservasWeb.Services
{
    public class ClientesService : BaseService
    {
        public ClientesService(ReservasWebEntities db) : base(db)
        {
        }

        public Cliente get(int id)
        {
            Cliente cliente = null;
            List<Cliente> clienteList = (from i in DB.Clientes.Where(i => i.idCliente == id)
                                                 select i).ToList<Cliente>();
            if (clienteList.Count() > 0)
            {
                cliente = clienteList.ElementAt(0);
            }

            return cliente;
        }

        public Cliente get(string email)
        {
            return (from i in DB.Clientes.Where(i => i.email == email)
                    select i).FirstOrDefault();
        }

        public Cliente crear(int idCliente, string nombre, string email, bool standBy)
        {
            Cliente cliente = new Cliente();
            cliente.idCliente = idCliente;
            cliente.nombre = nombre;
            cliente.email = email;
            cliente.standBy = standBy;
            crear(cliente);
            return cliente;
        }

        public void crear(Cliente cliente)
        {
            cliente.fechaCreacion = DateTime.Now;
            DB.Clientes.Add(cliente);
            DB.SaveChanges();
        }

        public void editar(Cliente cliente)
        {
            DB.Entry(cliente).State = EntityState.Modified;
            DB.SaveChanges();
        }
    }
}