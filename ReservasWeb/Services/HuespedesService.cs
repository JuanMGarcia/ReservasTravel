﻿using ReservasWeb.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ReservasWeb.Services
{
    public class HuespedesService : BaseService
    {
        public HuespedesService(ReservasWebEntities db) : base(db)
        {
        }

        public Huespede getHuesped(int idHuesped)
        {
            return DB.Huespedes.Find(idHuesped);
        }

        //public Huespede crearHuespedMostrador(string apellido, string nombre, string nroDoc, string email, int idCiudad)
        public Huespede crearHuespedMostrador(string apellido, string nombre, string nroDoc, string email, int idPais, string provincia, string ciudad,string telefono, string direccion, string celular)
        {
            if (apellido == null)
            {
                apellido = "";
                nombre = "";
                nroDoc = "";
                email = "";
            }
            Huespede unHues = new Huespede();
            unHues.apellido = apellido;
            unHues.nombre = nombre;
            unHues.tipoDocumento = "DNI";
            unHues.nroDocumento = nroDoc;
            unHues.email = email;
            unHues.tipo = "Persona";
            unHues.fechaCrecion = DateTime.Now;
            unHues.idPais = idPais;
            unHues.provincia = provincia;
            unHues.ciudad = ciudad;
            unHues.celular = celular;
            unHues.direccion = direccion;
            unHues.telefono = telefono;
            DB.Huespedes.Add(unHues);
            DB.SaveChanges();
            return unHues;
        }

        public Huespede editarHuespedMostrador(int idHuesped, string apellido, string nombre, string nroDoc, string email, int idPais, string provincia, string ciudad,string telefono,string direccion,string celular)
        {
            if (apellido == null)
            {
                apellido = "";
                nombre = "";
                nroDoc = "";
                email = "";
            }

            if (nombre == null || nombre == "")
            {
                nombre = "Falta Completar";
            }

            if (apellido == null || apellido == "")
            {
                apellido = "Falta Completar";
            }


            if (email == null || email == "")
            {
                email = "Falta Completar";
            }

            if (nroDoc == null || nroDoc == "")
            {
                nroDoc = "Falta Completar";
            }

            Huespede unHues = getHuesped(idHuesped);
            unHues.apellido = apellido;
            unHues.nombre = nombre;
            unHues.nroDocumento = nroDoc;
            unHues.email = email;
            unHues.idPais = idPais;
            unHues.provincia = provincia;
            unHues.ciudad = ciudad;
            unHues.telefono = telefono;
            unHues.celular = celular;
            unHues.direccion = direccion;
            unHues.idPais = 1;
            DB.Entry(unHues).State = EntityState.Modified;            
            DB.SaveChanges();
            return unHues;
        }

    }
}