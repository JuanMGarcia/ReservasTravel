﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using Newtonsoft.Json;
using mercadopago;
using ReservasWeb.Models;
using ReservasWeb.ViewModels;
using ReservasWeb.Utils;
using System.Web.Mvc;
using System.Data.Entity;


namespace ReservasWeb.Services
{
    public class MediosdePagoService : BaseService
    {
//        private static readonly bool mercadoPagoTestMode = true;

        private static readonly string testUrlKey = "sandbox_init_point";

        private static readonly string ProductionUrlKey = "init_point";

        private static readonly string BackUrlRoot = "http://pms.reservas.travel/Busqueda/";
    //    private static readonly string BackUrlRoot = "http://localhost:48601/Busqueda/";

        public MediosdePagoService(ReservasWebEntities db) : base(db)
        {
        }

        public TiposPagoConfiguracion obtenerConf(int idAlojamiento,int idTipo)
        {
            TiposPagoConfiguracion unMercadoPago = DB.TiposPagoConfiguracions.Where(T => T.idAlojamiento == idAlojamiento && T.tipo == idTipo).FirstOrDefault();
            return unMercadoPago;
        }

        public int getIdMercadoPago(int idAlojamiento,int idTipo)
        {
            TiposPagoConfiguracion unMercadoPago = DB.TiposPagoConfiguracions.Where(T => T.idAlojamiento == idAlojamiento && T.tipo == idTipo ).FirstOrDefault();
            if (unMercadoPago!= null) {
                return unMercadoPago.idTipoPagoConfiguracion;
            }
            else
            {
                return -1; //no existe ese medio de pago para ese Hotel.
            }
            
        }

        
        public TiposPagoConfiguracion getMedioPago(int idAlojamiento,int idTipo)
        {
            TiposPagoConfiguracion unMP = DB.TiposPagoConfiguracions.Where(S => S.idAlojamiento == idAlojamiento && S.tipo == idTipo).FirstOrDefault();
            return unMP;
        }

        public List<TiposPagoConfiguracion> getMercadoPagoAbm()
        {
            int idAlojamiento = ((Sesion)System.Web.HttpContext.Current.Session["SesionUsuario"]).idAlojamientoActual;
            List<TiposPagoConfiguracion> listado = DB.TiposPagoConfiguracions.Where(s => s.idAlojamiento == idAlojamiento).ToList();
            return listado;
        }

        public int Crear(CreacionMercadoPagoViewModel vm)
        {
            int idAlojamiento = ((Sesion)System.Web.HttpContext.Current.Session["SesionUsuario"]).idAlojamientoActual;
            TiposPagoConfiguracion unMercadoPago = new TiposPagoConfiguracion();
            unMercadoPago.clientId = vm.clientId;
            unMercadoPago.clientSecret = vm.clientSecret;
            unMercadoPago.clientStatus = vm.clientStatus;
            unMercadoPago.fechaCreacion = DateTime.Now;
            unMercadoPago.fechaModificacion = DateTime.Now;
            unMercadoPago.standBy = vm.standBy;
            unMercadoPago.idAlojamiento = idAlojamiento;
            unMercadoPago.tipo = vm.tipo;
            DB.TiposPagoConfiguracions.Add(unMercadoPago);
            DB.SaveChanges();
            return unMercadoPago.idTipoPagoConfiguracion;
        }

        public EdicionMercadoPagoViewModel getMercadoPagoEdicion(int idMercadoPago)
        {
            TiposPagoConfiguracion unMercadoPago = DB.TiposPagoConfiguracions.Find(idMercadoPago);
            EdicionMercadoPagoViewModel vm = new EdicionMercadoPagoViewModel();
            vm.idMercadoPago = unMercadoPago.idTipoPagoConfiguracion;
            vm.clientId = unMercadoPago.clientId;
            vm.clientSecret = unMercadoPago.clientSecret;
            vm.clientStatus = unMercadoPago.clientStatus;
            vm.standBy = unMercadoPago.standBy;
            vm.nombreTipo = ComboHelper.GetNombreTipoMediodePago(unMercadoPago.tipo); 

            return vm;
        }
        public void Editar(EdicionMercadoPagoViewModel vm)
        {
            TiposPagoConfiguracion unMercadoPago = DB.TiposPagoConfiguracions.Find(vm.idMercadoPago);
            unMercadoPago.clientId = vm.clientId;
            unMercadoPago.clientSecret = vm.clientSecret;
            unMercadoPago.clientStatus = vm.clientStatus;
            unMercadoPago.standBy = vm.standBy;
            unMercadoPago.fechaModificacion = DateTime.Now;
            DB.SaveChanges();
        }

        public void EditarCuentaBancaria(CuentaBancariaViewModel vm)
        {
            CuentasBancaria unBank = DB.CuentasBancarias.Where(s=>s.idCuentaBancaria==vm.idCuentaBancaria).FirstOrDefault();
            unBank.nombreBanco = vm.nombreBanco;
            unBank.numeroCuenta = vm.numeroCuenta;
            unBank.sucursal = vm.sucursal;
            unBank.tipoCuenta = vm.tipoCuenta;
            unBank.titular = vm.titular;
            unBank.CBU = vm.CBU;
            unBank.CUIT = vm.CUIT;
            DB.SaveChanges();
        }

        public void Borrar(int idMercadoPago)
        {
            TiposPagoConfiguracion unMercadoPago = DB.TiposPagoConfiguracions.Find(idMercadoPago);
            DB.TiposPagoConfiguracions.Remove(unMercadoPago);
            DB.SaveChanges();
        }

        //GetLink Solo para MercadoPago
        public string GetLinkMP(int idMercadoPago,string title, int quantity, decimal price, string reference, int id, bool mercadoPagoTestMode)
        {
        //    mercadoPagoTestMode = true;
            TiposPagoConfiguracion unMercadoPago = DB.TiposPagoConfiguracions.Find(idMercadoPago);
            string clientId = unMercadoPago.clientId;
            string clientSecret = unMercadoPago.clientSecret;
            MP mp = new MP(clientId, clientSecret);
            
            
            Hashtable Order = new Hashtable();
            var back_urls = new
            {
                success = $"{BackUrlRoot}MPConfirma",
                failure = $"{BackUrlRoot}MPCancela",
                pending = $"{BackUrlRoot}MPPendiente"
            };

            var items = new[]
            {
                new { title = title,
                quantity = quantity,
                currency_id = "ARS",
                unit_price = price
                }
            };

            Order.Add("auto_return", "all");
            Order.Add("back_urls", back_urls);
            Order.Add("items", items);
            Order.Add("external_reference", reference);

            
            //segun la configuracion del hotel que haya una politica de expiración de la reserva
            // FALTA Agregar en ADMIN configuracion de SETEO.
            Order.Add("expires", true);
            Order.Add("expiration_date_from", DateTime.Now.ToString("yyyy-MM-ddTH:mm:ss.fffzzz"));
            //Order.Add("expiration_date_to", DateTime.Now.AddMinutes(5).ToString("yyyy-MM-ddTH:mm:ss.fffzzz"));
            Order.Add("expiration_date_to", DateTime.Now.AddDays(3).ToString("yyyy-MM-ddTH:mm:ss.fffzzz"));


            string output = JsonConvert.SerializeObject(Order);
            Hashtable preference = mp.createPreference(output);

            if ((int)preference["status"] == 201)
            {
                Hashtable response = (Hashtable)preference["response"];

                var key = mercadoPagoTestMode ? testUrlKey : ProductionUrlKey;

                return response[key].ToString();
            }


            return null;
        }
    }

 
}