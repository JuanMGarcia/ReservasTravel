﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.Data.Entity;

namespace ReservasWeb.Services
{
    public class RolesService : BaseService
    {
        public RolesService(ReservasWebEntities db) : base(db)
        {
        }

        public Role get(int id)
        {
            Role rol = null;
            List<Role> rolList = (from i in DB.Roles.Where(i => i.idRol == id)
                                       select i).ToList<Role>();

            if (rolList.Count() > 0)
            {
                rol = rolList.ElementAt(0);
            }

            return rol;
        }

        public Role crear(string nombre)
        {
            Role rol = new Role();
            rol.nombre = nombre;
            rol.fechaCreacion = DateTime.Now;
            crear(rol);
            return rol;
        }

        public void crear(Role rol)
        {
            rol.fechaCreacion = DateTime.Now;
            DB.Roles.Add(rol);
            DB.SaveChanges();
        }

        public void editar(Role rol)
        {
            rol.fechaModificacion = DateTime.Now;
            DB.Entry(rol).State = EntityState.Modified;
            DB.SaveChanges();
        }
    }
}