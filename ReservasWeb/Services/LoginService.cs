﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;

namespace ReservasWeb.Services
{
    public class LoginService : BaseService
    {
        public LoginService(ReservasWebEntities db) : base(db)
        {
        }
        public Usuario autenticarUsuario(string usuario, string clave)
        {
            Usuario unUser = DB.Usuarios.Where(T => T.usuario1 == usuario && T.clave == clave && T.Clientes.FirstOrDefault().standBy != true && T.Clientes.FirstOrDefault().Alojamientos.FirstOrDefault().standBy != true).FirstOrDefault();
            return unUser;
        }
    }
}