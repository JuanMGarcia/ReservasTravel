﻿using ReservasWeb.Models;
using ReservasWeb.ViewModels;
using ReservasWeb.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ReservasWeb.Services
{
    public class ComodidadesService : BaseService
    {
        public ComodidadesService(ReservasWebEntities db) : base(db)
        {
        }

        public List<ComodidadSeleccionada> getComodidades()
        {
            //return DB.Comodidades.ToList();
            int idAlojamiento = getIdAlojamiento();
            var query = (from g in DB.Comodidades.Where(c => c.tipo.Equals("Hotel") || c.tipo == null)
                        from m in g.Alojamientos.Where(x => x.idAlojamiento == idAlojamiento).DefaultIfEmpty()
                        orderby g.nombreEs
                        select new ComodidadSeleccionada
                        {
                            seleccionada = (m == null) ? false : true,
                            idComodidad = g.idComodidad,
                            nombre = g.nombreEs,
                            icono = g.imagen
                        });

            return query.ToList();
        }

        public List<ComodidadSeleccionada> getComodidadesSeleccionadasTipoHabitacion(int idTipoHabitacion)
        {
            int idAlojamiento = getIdAlojamiento();
            var query = (from g in DB.Comodidades.Where(c => c.tipo.Equals("Habitacion") || c.tipo == null)
                         from m in g.TiposHabitaciones.Where(x => x.idAlojamiento == idAlojamiento && x.idTipoHabitacion == idTipoHabitacion).DefaultIfEmpty()
                         orderby g.nombreEs
                         select new ComodidadSeleccionada
                         {
                             seleccionada = (m == null) ? false : true,
                             idComodidad = g.idComodidad,
                             nombre = g.nombreEs,
                             icono = g.imagen
                         });

            return query.ToList();
        }

        public List<Comodidade> getComodidadesPorAlojamiento()
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.Alojamientos.Find(idAlojamiento).Comodidades.ToList();
        }

        public List<Comodidade> getComodidadesPorTipoDeHabitacion(int idTipoHabitacion)
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.TiposHabitaciones.Find(idTipoHabitacion).Comodidades.ToList();
            //return new List<Comodidade>();
        }

        public void eliminarComdidadesDeTipoHabitacion(int idTipoHabitacion)
        {
            int idAlojamiento = getIdAlojamiento();
            TiposHabitacione tipoHabitacion = DB.TiposHabitaciones.Where(t => t.idTipoHabitacion == idTipoHabitacion && t.idAlojamiento == idAlojamiento).FirstOrDefault();
            tipoHabitacion.Comodidades.Clear();            
            DB.Entry(tipoHabitacion).State = EntityState.Modified;
            DB.SaveChanges();
        }

        public List<Comodidade> getComodiadesSeleccionadas(List<int> ids)
        {
            return DB.Comodidades.Where(x => ids.Contains(x.idComodidad)).ToList();
        }

        public void guardarComodidades(List<int> ids)
        {            
            int idAlojamiento = getIdAlojamiento();
            Alojamiento aloj = DB.Alojamientos.Find(idAlojamiento);
            aloj.Comodidades.Clear();
            aloj.Comodidades = getComodiadesSeleccionadas(ids);
            DB.Entry(aloj).State = EntityState.Modified;
            DB.SaveChanges();
        }

        public void guardarComodidadesTipoHabitacion(List<int> ids, int idTipoHabitacion)
        {
            try { 
                int idAlojamiento = getIdAlojamiento();
                TiposHabitacione tipoHabitacion = DB.TiposHabitaciones.Where(x => x.idAlojamiento == idAlojamiento && x.idTipoHabitacion == idTipoHabitacion).ToList().ElementAt(0);
                tipoHabitacion.Comodidades.Clear();
                tipoHabitacion.Comodidades = getComodiadesSeleccionadas(ids);
                DB.Entry(tipoHabitacion).State = EntityState.Modified;
                DB.SaveChanges();
            }
            catch (Exception)
            {
            }
        }
    }
}