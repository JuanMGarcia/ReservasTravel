﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.Data.Entity;

namespace ReservasWeb.Services
{
    public class AlojamientosPorRolService : BaseService
    {
        public AlojamientosPorRolService(ReservasWebEntities db) : base(db)
        {
        }

        public AlojamientosPorRol getByIdAlojamientoAndNombreRol(int idAlojamiento, int idRol)
        {
            AlojamientosPorRol alojamientosPorRol = null;
            List<AlojamientosPorRol> alojamientosPorRolList = (from i in DB.AlojamientosPorRols.Where(i => i.idAlojamiento == idAlojamiento && i.Role.idRol == idRol)
                                                               select i).ToList<AlojamientosPorRol>();

            if (alojamientosPorRolList.Count() > 0)
            {
                alojamientosPorRol = alojamientosPorRolList.ElementAt(0);
            }

            return alojamientosPorRol;
        }

        public void crear(Alojamiento alojamiento, Role rol)
        {
            AlojamientosPorRol alojamientosPorRol = new AlojamientosPorRol();
            alojamientosPorRol.Alojamiento = alojamiento;
            alojamientosPorRol.Role = rol;
            crear(alojamientosPorRol);
        }

        public void crear(AlojamientosPorRol alojamientosPorRol)
        {
            alojamientosPorRol.fechaCreacion = DateTime.Now;
            DB.AlojamientosPorRols.Add(alojamientosPorRol);
            DB.SaveChanges();
        }

        public void editar(AlojamientosPorRol alojamientosPorRol)
        {
            DB.Entry(alojamientosPorRol).State = EntityState.Modified;
            DB.SaveChanges();
        }
    }
}