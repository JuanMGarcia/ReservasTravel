﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.Data.Entity;

namespace ReservasWeb.Services
{
    public class TarifasBaseService : BaseService
    {
        public TarifasBaseService(ReservasWebEntities db) : base(db)
        {
        }

        public TarifasBase get(int idTipoHabitacion, int idCanal, int? idCondicionReserva)
        {
            int idAlojemiento = getIdAlojamiento();
            TarifasBase tarifaBase = null;
            try
            {
                if (idCondicionReserva < 0)
                    idCondicionReserva = null;

                tarifaBase = (from i in DB.TarifasBases.Where(i => i.idAlojamiento == idAlojemiento && i.idTipoHabitacion == idTipoHabitacion && i.idCanal == idCanal && i.idCondReserva == idCondicionReserva)
                                           select i).ToList().ElementAt(0);
            }
            catch
            {                
            }


            return tarifaBase;
        }

        public TarifasBase getInvitado(int idTipoHabitacion, int idCanal, int? idCondicionReserva,int idAlojamiento)
        {
            TarifasBase tarifaBase = null;
            try
            {
                if (idCondicionReserva < 0)
                    idCondicionReserva = null;

                tarifaBase = (from i in DB.TarifasBases.Where(i => i.idAlojamiento == idAlojamiento && i.idTipoHabitacion == idTipoHabitacion && i.idCanal == idCanal && i.idCondReserva == idCondicionReserva)
                              select i).ToList().ElementAt(0);
            }
            catch
            {
            }


            return tarifaBase;
        }

        public void crear(TarifasBase tarifaBase)
        {
            tarifaBase.idAlojamiento = getIdAlojamiento();
            DB.TarifasBases.Add(tarifaBase);
            DB.SaveChanges();
        }

        public void editar(TarifasBase tarifaBase)
        {
            DB.Entry(tarifaBase).State = EntityState.Modified;
            DB.SaveChanges();
        }

        public void eliminar(TarifasBase tarifaBase)
        {
            DB.TarifasBases.Remove(tarifaBase);
            DB.SaveChanges();
        }
    }
}