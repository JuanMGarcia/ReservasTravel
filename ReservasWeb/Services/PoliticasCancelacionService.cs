﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.ViewModels;
using ReservasWeb.Models;
using ReservasWeb.Utils;
using System.Web.Mvc;
using System.Data.Entity;
using Newtonsoft.Json;

namespace ReservasWeb.Services
{
    public class PoliticasCancelacionService : BaseService
    {
        public PoliticasCancelacionService(ReservasWebEntities db) : base(db)
        {
        }

        public PoliticasCancelacion getPoliticasCancelacion(int id)
        {
           int idAlojamiento = getIdAlojamiento();
             return DB.PoliticasCancelacions.Where(t => t.idPoliticaCancelacion == id && t.idAlojamiento == idAlojamiento).FirstOrDefault();
            //return DB.PoliticasCancelacions.Find(id);
            //trae el id alojamiento para luego comparar que el quiero borrar sea el seleccionado y no otro pasado por la url
         //  var datos= from t in (DB.PoliticasCancelacions)
              //        where (t.idPoliticaCancelacion == idAlojamiento)
               //       select t;
            
            //return datos.FirstOrDefault();

        }

        public List<PoliticasCancelacion> getPoliticaCancelacionListadoAbm()
        {
            int idAlojamiento = getIdAlojamiento();
            List<PoliticasCancelacion> listado = DB.PoliticasCancelacions.Where(t => t.idAlojamiento == idAlojamiento).ToList();
            return listado;//trae el listado de politicas de cancelacion
        }


        public int Crear(PoliticasCancelacion politicascancelacion)
          {

            //politicascancelacion.fechaCreacion = DateTime.Now;
            //politicascancelacion.fechaModificacion = DateTime.Now;
            
            DB.PoliticasCancelacions.Add(politicascancelacion);//solo se agrega y se guardan los cambios
            DB.SaveChanges();
            return politicascancelacion.idPoliticaCancelacion;
        }





        public void Editar(PoliticasCancelacion politicas)
        {
            politicas.fechaModificacion = DateTime.Now;
            DB.Entry(politicas).State = EntityState.Modified;
            DB.SaveChanges();
        
        }

   
        public void Eliminar(int idPoliticaCancelacion)
        {

            if (getPoliticasCancelacion(idPoliticaCancelacion)!= null)
            {
                int idAlojamiento = getIdAlojamiento();
                PoliticasCancelacion pol = DB.PoliticasCancelacions.Where(t => t.idPoliticaCancelacion == idPoliticaCancelacion && t.idAlojamiento == idAlojamiento).FirstOrDefault();
                DB.PoliticasCancelacions.Remove(pol);
                DB.SaveChanges();
            }
        }

       

       
    }
}