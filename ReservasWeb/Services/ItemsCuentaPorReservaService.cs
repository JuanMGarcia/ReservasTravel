﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using ReservasWeb.ViewModels;
using ReservasWeb.Utils;
using System.Data.Entity;
using System.Web.Mvc;
using System.Configuration;
using System.Globalization;
using System.Threading;

namespace ReservasWeb.Services
{
    public class ItemsCuentaPorReservaService : BaseService
    {
        public ItemsCuentaPorReservaService(ReservasWebEntities db) : base(db)
        {
        }

        public List<ItemsCuentaPorReserva> getItemsCuenta(int idReserva)
        {
            int idAlojamiento = getIdAlojamiento();
            List<ItemsCuentaPorReserva> lista = DB.ItemsCuentaPorReservas.Where(t => t.Reserva.idAlojamiento == idAlojamiento && t.idReserva == idReserva).ToList();
            return lista;
        }

        public ItemsCuentaPorReserva get(int idItemCuenta)
        {
            int idAlojamiento = getIdAlojamiento();
            ItemsCuentaPorReserva lista = DB.ItemsCuentaPorReservas.Where(t => t.Reserva.idAlojamiento == idAlojamiento && t.idItemCuenta == idItemCuenta).FirstOrDefault();
            return lista;
        }

        public SelectList getComboConceptoCargo()
        {
            return new SelectList(new[]
            {                
                new { ID = "CH", Name = "Cargo por Habitación" },
                new { ID = "RC", Name = "Recargo por cancelación" },
                new { ID = "R", Name = "Recargo" },
                new { ID = "O", Name = "Otros" }
            },
            "ID", "Name", "");
        }

        public SelectList getComboConceptoPago()
        {
            return new SelectList(new[]
            {
                new { ID = "RA", Name = "Reserva" },
                new { ID = "P", Name = "Pago" },
                new { ID = "DN", Name = "Devolución" },
                new { ID = "DO", Name = "Descuento" },
                new { ID = "DM", Name = "Devolución por Modificación" }
            },
            "ID", "Name", "");
        }

        public SelectList getComboFormasDePago()
        {
            return new SelectList(new[]
            {
                new { ID = "E", Name = "Efectivo" },
                new { ID = "TC", Name = "Tarjeta de Crédito" },
                new { ID = "TD", Name = "Tarjeta de Débito" },
                new { ID = "T", Name = "Transferencia" },
                new { ID = "MP", Name = "MercadoPago" },
                new { ID = "NA", Name = "NA" },
                new { ID = "PP", Name = "PayPal" },
                new { ID = "DH", Name = "Descuento por Haberes" }
            },
            "ID", "Name", "");
        }

        public int Crear(CreacionEdicionItemCuentaPorReservaViewModel vm)
        {
            int idItemCuenta = 0;

            ItemsCuentaPorReserva itemCuenta = new ItemsCuentaPorReserva();
            itemCuenta.idReserva = vm.idReserva;
            itemCuenta.fecha = DateHelper.stringToDateTime(vm.fecha);
            itemCuenta.fechaModificacion = DateHelper.obtenerFechaActual();
            itemCuenta.formaPago = vm.formaDePago;
            itemCuenta.concepto = vm.concepto;
            itemCuenta.comentarios = vm.comentarios;            
            itemCuenta.monto = Convert.ToDecimal(vm.monto.Replace("$ ", ""), new CultureInfo("en-US"));

            crear(itemCuenta);
            idItemCuenta = itemCuenta.idItemCuenta;

            return idItemCuenta;
        }

        public void Eliminar(int idItemCuenta)
        {
            int idAlojamiento = getIdAlojamiento();
            ItemsCuentaPorReserva itemCuenta = DB.ItemsCuentaPorReservas.Where(t => t.idItemCuenta == idItemCuenta && t.Reserva.idAlojamiento == idAlojamiento).FirstOrDefault();
            DB.ItemsCuentaPorReservas.Remove(itemCuenta);
            DB.SaveChanges();
        }

        public int Editar(CreacionEdicionItemCuentaPorReservaViewModel vm)
        {
            int idItemCuenta = 0;
            ItemsCuentaPorReserva itemCuenta = get(vm.idItemCuenta);
            itemCuenta.idReserva = vm.idReserva;
            itemCuenta.fecha = DateHelper.stringToDateTime(vm.fecha);
            itemCuenta.fechaModificacion = DateHelper.obtenerFechaActual();
            itemCuenta.formaPago = vm.formaDePago;
            itemCuenta.concepto = vm.concepto;
            itemCuenta.comentarios = vm.comentarios;
            itemCuenta.monto = Convert.ToDecimal(vm.monto.Replace("$ ", ""), new CultureInfo("en-US"));
            return idItemCuenta;
        }
        public void crear(ItemsCuentaPorReserva itemCuenta)
        {
            itemCuenta.fechaCreacion = DateHelper.obtenerFechaActual();
            DB.ItemsCuentaPorReservas.Add(itemCuenta);
            DB.SaveChanges();
        }

        public void editar(ItemsCuentaPorReserva itemCuenta)
        {
            itemCuenta.fechaModificacion = DateHelper.obtenerFechaActual();
            DB.Entry(itemCuenta).State = EntityState.Modified;
            DB.SaveChanges();
        }

    }
}