﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.ViewModels;
using ReservasWeb.Models;
using ReservasWeb.Utils;
using System.Web.Mvc;
using System.Data.Entity;
using Newtonsoft.Json;
using System.Collections;

namespace ReservasWeb.Services
{
    public class HabitacionesService : BaseService
    {
        public HabitacionesService(ReservasWebEntities db) : base(db)
        {
        }

        public Habitacione getHabitacion(int id)
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.Habitaciones.Where(h => h.idHabitacion == id && h.TiposHabitacione.idAlojamiento == idAlojamiento).FirstOrDefault();
        }

        public Habitacione getHabitacion(string nombre, int idHabitacion)
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.Habitaciones.Where(h => h.nombre == nombre && (h.idHabitacion != idHabitacion || idHabitacion == 0) && h.TiposHabitacione.idAlojamiento == idAlojamiento).FirstOrDefault();
        }

        public List<Habitacione> getHabitacionesPorReserva(int idReserva)
        {
            int idAlojamiento = getIdAlojamiento();
            List<Habitacione> listado = DB.HabitacionesPorReservas.Where(t => t.Habitacione.TiposHabitacione.idAlojamiento == idAlojamiento && t.idReserva == idReserva).Select(t => t.Habitacione).OrderBy(t => t.TiposHabitacione.nombreEs).OrderBy(t => t.nombre).ToList();
            return listado;
        }

        public List<HabitacionesPorReserva> getHabitacionesPorReservaPorId(int idReserva)
        {
            int idAlojamiento = getIdAlojamiento();
            List<HabitacionesPorReserva> listado = DB.HabitacionesPorReservas.Where(t => t.Habitacione.TiposHabitacione.idAlojamiento == idAlojamiento && t.idReserva == idReserva).OrderBy(t => t.dia).OrderBy(t => t.Habitacione.TiposHabitacione.nombreEs).OrderBy(t => t.Habitacione.nombre).ToList();
            return listado;
        }

        public List<HabitacionesPorReserva> getHabitacionesPorReservaPorHabitacionBusqueda(int idReserva, int idAlojamiento)
        {
            List<HabitacionesPorReserva> listado = DB.HabitacionesPorReservas.Where(t => t.Habitacione.TiposHabitacione.idAlojamiento == idAlojamiento && t.idReserva == idReserva ).OrderBy(t => t.idHabitacion).OrderBy(t => t.dia).ToList();
            return listado;
        }

        public List<HabitacionesPorReserva> getHabitacionesPorReservaPorHabitacion(int idReserva, int? idHabitacion = null)
        {
            int idAlojamiento = getIdAlojamiento();
            List<HabitacionesPorReserva> listado = DB.HabitacionesPorReservas.Where(t => t.Habitacione.TiposHabitacione.idAlojamiento == idAlojamiento && t.idReserva == idReserva && (idHabitacion == null || t.idHabitacion == idHabitacion)).OrderBy(t => t.idHabitacion).OrderBy(t => t.dia).ToList();
            return listado;
        }

        public List<HabitacionesPorReserva> getHabitacionesPorReservaPorHabitacionPorFecha(int idReserva, int idHabitacion, DateTime fecha)
        {
            int idAlojamiento = getIdAlojamiento();
            List<HabitacionesPorReserva> listado = DB.HabitacionesPorReservas.Where(t => t.Habitacione.TiposHabitacione.idAlojamiento == idAlojamiento && t.idReserva == idReserva && t.idHabitacion == idHabitacion && t.dia == fecha).OrderBy(t => t.idHabitacion).OrderBy(t => t.dia).ToList();
            return listado;
        }

        public HabitacionesPorReserva getHabitacionesPorReservaPorHabitacionPorFecha(int idReserva, int idHabitacion, int idCondicionReserva, DateTime fecha)
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.HabitacionesPorReservas.Where(t => t.Habitacione.TiposHabitacione.idAlojamiento == idAlojamiento && t.idReserva == idReserva && t.idHabitacion == idHabitacion && ((idCondicionReserva == -1 && t.idCondReserva == null) || (idCondicionReserva > 0 && t.idCondReserva == idCondicionReserva)) && DbFunctions.TruncateTime(t.dia) == DbFunctions.TruncateTime(fecha)).FirstOrDefault();            
        }

        public List<Habitacione> getHabitacionesListadoAbm()
        {
            int idAlojamiento = getIdAlojamiento();
            List<Habitacione> listado = DB.Habitaciones.Where(t => t.TiposHabitacione.idAlojamiento == idAlojamiento).OrderBy(t => t.TiposHabitacione.nombreEs).OrderBy(t => t.nombre).ToList();
            return listado;
        }

        public List<Habitacione> getHabitacionesOrdenadoPorReservas(int idAlojamiento)
        {
            List<Habitacione> listado = null;
            try
            {
                //estado reserva = 3 = CONFIRMADO
                listado = DB.HabitacionesPorReservas.Where(t => t.Habitacione.TiposHabitacione.idAlojamiento == idAlojamiento).OrderByDescending(t => t.Reserva.idReserva).Select(t => t.Habitacione).ToList();
            }
            catch (Exception)
            {                
            }
            return listado;
        }

        public int getCantidadDisponiblesWebMostrador()
        {
            int cantidad = 0;
            try
            {
                int idAlojamiento = getIdAlojamiento();
                cantidad = DB.Habitaciones.Where(t => t.TiposHabitacione.idAlojamiento == idAlojamiento && t.disponibleAbonoHabitacion == true).ToList().Count();
            }
            catch (Exception)
            {                
            }
            return cantidad;
        }


        public int getCantidadDisponiblesWebMostradorExceptoHabitacion(int idHabitacion)
        {
            int cantidad = 0;
            try
            {
                int idAlojamiento = getIdAlojamiento();
                cantidad = DB.Habitaciones.Where(t => t.TiposHabitacione.idAlojamiento == idAlojamiento && t.disponibleAbonoHabitacion == true && t.idHabitacion != idHabitacion).ToList().Count();
            }
            catch (Exception)
            {
            }
            return cantidad;
        }

        public int getCantidadDisponiblesWebMostrador(int idAlojamiento)
        {
            int cantidad = 0;
            try
            {
                cantidad = DB.Habitaciones.Where(t => t.TiposHabitacione.idAlojamiento == idAlojamiento && t.disponibleAbonoHabitacion == true).ToList().Count();
            }
            catch (Exception)
            {
            }
            return cantidad;
        }

        public List<Habitacione> getHabitacionesPorTipoHabitacion(int idTipoHabitacion)
        {
            int idAlojamiento = getIdAlojamiento();
            List<Habitacione> listado = DB.Habitaciones.Where(t => t.TiposHabitacione.idAlojamiento == idAlojamiento && t.TiposHabitacione.idTipoHabitacion == idTipoHabitacion).ToList();
            return listado;
        }

        public List<Habitacione> getHabitaciones()
        {
            int idAlojamiento = getIdAlojamiento();
            List<Habitacione> listado = DB.Habitaciones.Where(t => t.TiposHabitacione.idAlojamiento == idAlojamiento ).ToList();
            return listado;
        }

        public List<HabitacionGrilla> getHabitacionesPorTipoHabitacion_Grilla(int idTipoHabitacion)
        {
            int idAlojamiento = getIdAlojamiento();
            var consulta = (from x in DB.Habitaciones.Where(t => t.TiposHabitacione.idAlojamiento == idAlojamiento && (t.TiposHabitacione.idTipoHabitacion == idTipoHabitacion || idTipoHabitacion == 0)).OrderBy(t => t.TiposHabitacione.nombreEs).OrderBy(t => t.nombre)
                            select new HabitacionGrilla
                            {
                                name = x.nombre + "<br /><div class=\"tipoHabitacionGrilla\">" + x.TiposHabitacione.nombreEs + "</div>",
                                id = x.idHabitacion,
                                capacity = (x.TiposHabitacione.plazas != null) ? x.TiposHabitacione.plazas.Value : 1,
                                status = x.idEstado.Value.ToString()
                            }).ToList();

            return consulta;
        }

        public SelectList getComboEstados()
        {
            SelectList estados = new SelectList(new[]{
                new { ID = "1", Name = ComboHelper.GetNombreEstadoHabitacion(1) },
                new { ID = "2", Name = ComboHelper.GetNombreEstadoHabitacion(2) },
                new { ID = "3", Name = ComboHelper.GetNombreEstadoHabitacion(3) },
                new { ID = "4", Name = ComboHelper.GetNombreEstadoHabitacion(4) }
            }, "ID", "Name", "");
            return estados;
        }

        public EdicionHabitacionesViewModel getHabitacionEdicion(int idHabitacion)
        {
            TiposHabitacionService svTip = new TiposHabitacionService(this.DB);
            Habitacione unaHab = DB.Habitaciones.Find(idHabitacion);
            EdicionHabitacionesViewModel vm = new EdicionHabitacionesViewModel();
            vm.idHabitacion = unaHab.idHabitacion;
            vm.nombre = unaHab.nombre;
            if (unaHab.idEstado.HasValue)
                vm.idEstado = unaHab.idEstado.Value;
            vm.idTipoHabitacion = unaHab.TiposHabitacione.idTipoHabitacion;
            if (unaHab.disponibleAbonoHabitacion.HasValue)
                vm.disponibleAbonoHabitacion = unaHab.disponibleAbonoHabitacion.Value;
            vm.listaEstados = getComboEstados();
            vm.listaTiposHabitacion = svTip.getComboTiposHabitacion();
            return vm;
        }

        public void Eliminar(int idHabitacion)
        {
            int idAlojamiento = getIdAlojamiento();
            Habitacione unaHab = DB.Habitaciones.Where(t => t.idHabitacion == idHabitacion && t.TiposHabitacione.idAlojamiento == idAlojamiento).FirstOrDefault();
            DB.Habitaciones.Remove(unaHab);
            DB.SaveChanges();
        }

        public void EliminarHabitacionesPorReservaBusqueda(int idReserva, int idAlojamiento)
        {
            List<HabitacionesPorReserva> habitacionesReserva = getHabitacionesPorReservaPorHabitacionBusqueda(idReserva,idAlojamiento);
            foreach (HabitacionesPorReserva habitacionReserva in habitacionesReserva)
            {
                DB.HabitacionesPorReservas.Remove(habitacionReserva);
                DB.SaveChanges();
            }
        }

        public void EliminarHabitacionesPorReserva(int idReserva)
        {
            List<HabitacionesPorReserva> habitacionesReserva = getHabitacionesPorReservaPorHabitacion(idReserva);
            foreach (HabitacionesPorReserva habitacionReserva in habitacionesReserva)
            {
                DB.HabitacionesPorReservas.Remove(habitacionReserva);
                DB.SaveChanges();
            }
        }

        public void EliminarHabitacionesPorReserva(int idReserva, int idHabitacion)
        {
            List<HabitacionesPorReserva> habitacionesReserva = getHabitacionesPorReservaPorHabitacion(idReserva, idHabitacion);
            foreach(HabitacionesPorReserva habitacionReserva in habitacionesReserva)
            {
                DB.HabitacionesPorReservas.Remove(habitacionReserva);
                DB.SaveChanges();
            }
        }

        public void EliminarHabitacionesPorReservaPorFecha(int idReserva, int idHabitacion, DateTime fecha)
        {
            List<HabitacionesPorReserva> habitacionesReserva = getHabitacionesPorReservaPorHabitacionPorFecha(idReserva, idHabitacion, fecha);
            foreach (HabitacionesPorReserva habitacionReserva in habitacionesReserva)
            {
                DB.HabitacionesPorReservas.Remove(habitacionReserva);
                DB.SaveChanges();
            }
        }

        public int Crear(CreacionHabitacionesViewModel vm)
        {
            Habitacione unaHab = new Habitacione();
            unaHab.nombre = vm.nombre;
            unaHab.idEstado = vm.idEstado;
            unaHab.idTipoHabitacion = vm.idTipoHabitacion;
            unaHab.disponibleAbonoHabitacion = vm.disponibleAbonoHabitacion;            
            unaHab.fechaCreacion = DateTime.Now;
            unaHab.fechaModificacion = DateTime.Now;
            DB.Habitaciones.Add(unaHab);
            DB.SaveChanges();
            return unaHab.idHabitacion;
        }

        public void Editar(EdicionHabitacionesViewModel vm)
        {
            Habitacione unaHab = DB.Habitaciones.Find(vm.idHabitacion);
            unaHab.nombre = vm.nombre;
            unaHab.idEstado = vm.idEstado;
            unaHab.idTipoHabitacion = vm.idTipoHabitacion;
            unaHab.fechaModificacion = DateTime.Now;
            DB.Entry(unaHab).State = EntityState.Modified;
            DB.SaveChanges();
        }

        public void Editar(Habitacione habitacion)
        {
            habitacion.fechaModificacion = DateTime.Now;
            DB.Entry(habitacion).State = EntityState.Modified;
            DB.SaveChanges();
        }

        public void EditarDisponibleAbonoHabitacion(int id, bool disponible)
        {
            Habitacione unaHab = DB.Habitaciones.Find(id);
            unaHab.disponibleAbonoHabitacion = disponible;
            unaHab.fechaModificacion = DateTime.Now;
            DB.Entry(unaHab).State = EntityState.Modified;
            DB.SaveChanges();
        }

        public IEnumerable getHabitacionesComboDinamico(int idTipoHabitacion)
        {
            return (from i in DB.Habitaciones
                         where i.idTipoHabitacion == idTipoHabitacion
                         select new
                         {
                             i.idHabitacion,
                             i.nombre
                         }).ToList();            
        }

        public IEnumerable getCondicionesComboDinamico(int idTipoHabitacion)
        {
            return (from i in DB.CondReservas
                    from z in i.TarifasBases
                    where z.idTipoHabitacion == idTipoHabitacion && z.abierta == true
                    select new
                    {
                        i.idCondReserva,
                        i.nombreEs
                    }).Distinct().ToList();
        }

        public SelectList getComboHabitaciones(int idTipoHabitacion)
        {
            return new SelectList(this.getHabitacionesPorTipoHabitacion(idTipoHabitacion), "idHabitacion", "nombre");
        }

        public SelectList getComboHabitacionesTodas()
        {
            return new SelectList(this.getHabitaciones(), "idHabitacion", "nombre");
        }

        public SelectList getComboHabitacionesTodasBusqueda()
        {
            List<Habitacione> lst = this.getHabitaciones();
            Habitacione item = new Habitacione();
            item.idHabitacion = 0;
            item.nombre = "Todos";
            lst.Add(item);
            return new SelectList(lst, "idHabitacion", "nombre");
        }

        public int getCantidadTotalHabitacionesDisponibles()
        {
            int idAlojamiento = getIdAlojamiento();
            return DB.Habitaciones.Where(t => t.TiposHabitacione.idAlojamiento == idAlojamiento && t.idEstado != 4).Count();
        }

    }
}