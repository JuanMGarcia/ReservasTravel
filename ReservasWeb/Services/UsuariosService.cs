﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.Data.Entity;

namespace ReservasWeb.Services
{
    public class UsuariosService : BaseService
    {
        public UsuariosService(ReservasWebEntities db) : base(db)
        {
        }

        public List<Usuario> getByIdRol(int idRol)
        {
            return (from i in DB.Usuarios.Where(i => i.idRol == idRol)
                                         select i).ToList<Usuario>();
        }

        public Usuario get(int id)
        {
            return (from i in DB.Usuarios.Where(i => i.idUsuario == id)
                    select i).FirstOrDefault();            
        }

        public Usuario crear(Role rol, string nombre, string apellido, string usuarioNombre, string clave, string email)
        {
            Usuario usuario = new Usuario();
            usuario.Role = rol;
            usuario.nombre = nombre;
            usuario.apellido = apellido;
            usuario.usuario1 = usuarioNombre;
            usuario.clave = clave;
            usuario.email = email;
            usuario.fechaCreacion = DateTime.Now;
            crear(usuario);
            return usuario;
        }

        public void crear(Usuario usuario)
        {
            usuario.fechaCreacion = DateTime.Now;
            DB.Usuarios.Add(usuario);
            DB.SaveChanges();
        }

        public void editar(Usuario usuario)
        {
            usuario.fechaModificacion = DateTime.Now;
            DB.Entry(usuario).State = EntityState.Modified;
            DB.SaveChanges();
        }
    }
}