﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.Data.Entity;
using System.Collections;
using System.Web.Mvc;

namespace ReservasWeb.Services
{
    public class CiudadesService : BaseService
    {

        public CiudadesService(ReservasWebEntities db) : base(db)
        {
        }

        public Ciudade get(int id)
        {
            Ciudade ciudad = null;
            List<Ciudade> ciudadList = (from i in DB.Ciudades.Where(i => i.idCiudad == id)
                                                 select i).ToList<Ciudade>();
            if (ciudadList.Count() > 0)
            {
                ciudad = ciudadList.ElementAt(0);
            }

            return ciudad;
        }

        public Ciudade getByNombreEs(string nombreEs)
        {
            Ciudade ciudad = null;
            List<Ciudade> ciudadList = (from i in DB.Ciudades.Where(i => i.nombreEs == nombreEs)
                                        select i).ToList<Ciudade>();
            if (ciudadList.Count() > 0)
            {
                ciudad = ciudadList.ElementAt(0);
            }

            return ciudad;
        }

        public void crear(Ciudade ciudad)
        {            
            DB.Ciudades.Add(ciudad);
            DB.SaveChanges();
        }

        public void editar(Ciudade ciudad)
        {
            DB.Entry(ciudad).State = EntityState.Modified;
            DB.SaveChanges();
        }

        public SelectList getPaisesCombo()
        {
            return new SelectList(this.getPaises(), "idPais", "nombreEs");            
        }

        public IEnumerable getProvinciasCombo(int idPais)
        {
            var provs = (from i in DB.Provincias
                         where i.idPais == idPais
                         select new
                         {
                             i.idProvincia,
                             i.nombreEs
                         }).ToList();
            return provs;
        }

        public IEnumerable getCiudadesCombo(int idProvincia)
        {
            var ciuds = (from i in DB.Ciudades
                         where i.idProvincia == idProvincia
                         select new
                         {
                             i.idCiudad,
                             i.nombreEs
                         }).ToList();
            return ciuds;
        }

        public List<Pais> getPaises()
        {
            return DB.Paises.OrderBy(p => p.nombreEs).ToList();
        }

        public List<Provincia> getProvincias(int idPais)
        {
            return DB.Provincias.Where(p => p.idPais == idPais).ToList();
        }

        public List<Ciudade> getCiudades(int idProvincia)
        {
            return DB.Ciudades.Where(c => c.idProvincia == idProvincia).ToList();
        }

        public Ciudade getCiudad(int idCiudad)
        {
            return DB.Ciudades.Find(idCiudad);
        }
    }
}