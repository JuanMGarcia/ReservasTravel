﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using ReservasWeb.ViewModels;
using System.Web.Mvc;

namespace ReservasWeb.Services
{
    public class TiposAlojamientoService : BaseService
    {
        public TiposAlojamientoService(ReservasWebEntities db) : base(db)
        {
        }

        public SelectList getComboTiposAlojamientos()
        {
            List<TiposAlojamiento> lista = DB.TiposAlojamientoes.ToList();
            return new SelectList(lista, "idTipoAlojamiento", "nombreEs");
        }

    }
}