﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReservasWeb.Models;
using ReservasWeb.ViewModels;

namespace ReservasWeb.Services
{
    public class ZonasService : BaseService
    {
        public ZonasService(ReservasWebEntities db) : base(db)
        {
        }

        public List<Zona> getZonas(int idCiudad)
        {
            return DB.Zonas.Where(t => t.idCiudad == idCiudad).ToList();
        }
    }
}