﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.Data.Entity;

namespace ReservasWeb.Services
{
    public class ModulosService : BaseService
    {
        public ModulosService(ReservasWebEntities db) : base(db)
        {
        }

        public Modulo get(int id)
        {
            Modulo modulo = null;
            List<Modulo> moduloList = (from i in DB.Modulos.Where(i => i.idModulo == id)
                                                               select i).ToList<Modulo>();

            if (moduloList.Count() > 0)
            {
                modulo = moduloList.ElementAt(0);
            }

            return modulo;
        }

        public void crear(Modulo modulo)
        {
            DB.Modulos.Add(modulo);
            DB.SaveChanges();
        }

        public void editar(Modulo modulo)
        {
            DB.Entry(modulo).State = EntityState.Modified;
            DB.SaveChanges();
        }
    }
}