﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using ReservasWeb.ViewModels;
using ReservasWeb.Utils;
using System.Data.Entity;
using System.Web.Mvc;

namespace ReservasWeb.Services
{
    public class EmailsService : BaseService
    {
        public EmailsService(ReservasWebEntities db) : base(db)
        {
        }

        public List<Email> getEmails()
        {
            int idAlojamiento = getIdAlojamiento();
            List<Email> lista = DB.Emails.Where(t => t.idAlojamiento == idAlojamiento).ToList();
            return lista;
        }

        public Email getEmail(int id)
        {
            int idAlojamiento = getIdAlojamiento();
            Email email = DB.Emails.Where(t => t.idAlojamiento == idAlojamiento && t.idEmail == id).FirstOrDefault();
            return email;
        }

        public void Eliminar(int idEmail)
        {
            Email email = DB.Emails.Find(idEmail);
            DB.Emails.Remove(email);
            DB.SaveChanges();
        }

        public void crear(Email email)
        {
            email.idAlojamiento = getIdAlojamiento();
            email.fechaCreacion = DateTime.Now;
            DB.Emails.Add(email);
            DB.SaveChanges();
        }

        public void editar(Email email)
        {
            email.fechaModificacion = DateTime.Now;
            DB.Entry(email).State = EntityState.Modified;
            DB.SaveChanges();
        }
    }
}