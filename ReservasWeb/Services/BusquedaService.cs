﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using ReservasWeb.Models;
using System.Data.Entity;
using ReservasWeb.ViewModels;
using ReservasWeb.Utils;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;

namespace ReservasWeb.Services
{
    public class BusquedaService : BaseService
    {
        public BusquedaService(ReservasWebEntities db) : base(db)
        {
        }

        public List<CondReserva> obtenerCondRsv(int idAlojamiento)
        {
            List<CondReserva> listado = DB.CondReservas.Where(f => f.idAlojamiento == idAlojamiento)
                .ToList();

            return listado;
        }

        public List<CondReserva> obtenerCondRsvxTipoHab(int idAlojamiento, List<TiposHabitacione> tiposHab)
        {
            List<CondReserva> listado = DB.CondReservas.Where(c => c.idAlojamiento == idAlojamiento).ToList();
            return listado;
        }




        public List<TiposHabitacione> obtenerTiposHab(int IdAlojamiento, int capacidad)
        {
            List<TiposHabitacione> listado = DB.TiposHabitaciones.Where(r => r.idAlojamiento == IdAlojamiento)
                .Where(r => r.plazas <= capacidad)
                .ToList();
            return listado;
        }


        public TiposHabitacione obtenerTipoHab(int idTipoHab)
        {
            TiposHabitacione unTipo = new TiposHabitacione();
            unTipo = DB.TiposHabitaciones.Find(idTipoHab);
            return unTipo;
        }

        public int obtenerCantTipoHab(int idTipoHab)
        {
            // return DB.Habitaciones.Where(s => s.idTipoHabitacion == idTipoHab).Count();
            return 2;
        }

        public decimal obtenerTarifaTipoHab(int idTipoHab, int idd)
        {
            TarifasBase tf = DB.TarifasBases.Where(s => s.idTipoHabitacion == idTipoHab).FirstOrDefault();
            decimal resultado = 1;

            switch (idd)
            {

                case 1:
                    resultado = tf.tarifaLunes;
                    break;
                case 2:
                    resultado = tf.tarifaMartes.Value; 
                    break;
                case 3:
                    resultado = tf.tarifaMiercoles.Value;
                    break;
                case 4:
                    resultado = tf.tarifaJueves.Value;
                    break;
                case 5:
                    resultado = tf.tarifaViernes.Value;
                    break;
            }
            return resultado;

        }


        public int GuardarGarantia(PrereservaViewModel vm)
        {
            string idHotel = vm.idAlojamiento.ToString();
            var sesion = System.Web.HttpContext.Current.Session["SesionInvitado"+idHotel] as SesionInvitado;
            if (sesion == null)
            {
                return 0;//error
            }
            else
            {
                if (IsValidNumber(vm.nroTarjeta))
                {
                    //se cifra el mensaje                     
                    Alojamiento unAloja = DB.Alojamientos.Find(sesion.idAlojamiento);
                    int idcliente = unAloja.idCliente;
                    Reserva unRsv = DB.Reservas.Find(sesion.idReserva);
                    unRsv.cvcTarjeta = Encrypto.Encriptar(vm.cvcTarjeta, sesion.idAlojamiento, idcliente);
                    unRsv.titularTarjeta = Encrypto.Encriptar(vm.titularTarjeta, sesion.idAlojamiento, idcliente);
                    unRsv.nroTarjeta = Encrypto.Encriptar(vm.nroTarjeta, sesion.idAlojamiento, idcliente);
                    unRsv.fechaTarjeta = Encrypto.Encriptar(vm.mesTarjeta + "/" + vm.anioTarjeta, sesion.idAlojamiento, idcliente);
                    string tmp2 = Encrypto.Desencriptar(unRsv.titularTarjeta, sesion.idAlojamiento, idcliente);
                    DB.SaveChanges();
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
        }


        public static bool IsValidNumber(string number)
        {
            number = number.Replace(" ","");
            int[] DELTAS = new int[] { 0, 1, 2, 3, 4, -4, -3, -2, -1, 0 };
            int checksum = 0;
            char[] chars = number.ToCharArray();
            for (int i = chars.Length - 1; i > -1; i--)
            {
                int j = ((int)chars[i]) - 48;
                checksum += j;
                if (((i - chars.Length) % 2) == 0)
                    checksum += DELTAS[j];
            }

            return ((checksum % 10) == 0);
        }

        public decimal getTarifaPorFecha(DateTime fechaIni,DateTime fechaFin,int idTipoHabitacion,int? idCondicion,int idAlojamiento, List<DiaRsv> listDias)
        {
            decimal dinero = 0;
            bool tieneEstadia = false;
            int precioCuna = 0;
            int precioMenor = 0;
            int precioAdulto = 0;

            TarifasBase tarifaBase;
            TarifasBase tarifaBaseSC;
            TarifasPorDia tarifasPorDia;
            TarifasBaseService tarifasBaseService = new TarifasBaseService(this.DB);
            TarifasPorDiaService tarifasPorDiaService = new TarifasPorDiaService(this.DB);
            decimal precioHabitacion = 0;
            tarifaBase = tarifasBaseService.getInvitado(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]), idCondicion, idAlojamiento);
            tarifaBaseSC = tarifasBaseService.getInvitado(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]), null, idAlojamiento);
            DateTime dia;
            int estadia = 1;
            if (tarifaBase.estadiaMinima.HasValue)
            { 
                tieneEstadia = tarifaBase.estadiaMinima != 1;
                estadia = tarifaBase.estadiaMinima.Value;
             }
            int cont = 0;

            for (int i = 0; i < (fechaFin - fechaIni).TotalDays; i++)
            {                        
                dia = fechaIni.AddDays(i);
                tarifasPorDia = tarifasPorDiaService.getInvitado(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]), idCondicion, dia,idAlojamiento);
                if (tarifasPorDia != null && tarifasPorDia.estadiaMinima.HasValue)
                {
                    tieneEstadia = tarifasPorDia.estadiaMinima != 1;
                    estadia = tarifasPorDia.estadiaMinima.Value;
                }

                if (tieneEstadia)
                {
                    cont++;
                }

                if (tarifasPorDia != null)
                { 
                    precioHabitacion = tarifasPorDia.tarifaBase;
                }
                else
                {
                        switch ((int)dia.DayOfWeek)
                        {
                            case 0:
                                    if (tarifaBase.tarifaDomingo.HasValue && tarifaBase.tarifaDomingo!=0)
                                    {
                                        precioHabitacion = tarifaBase.tarifaDomingo.Value;
                                    }
                                    else
                                    {
                                        if (tarifaBaseSC.tarifaDomingo.HasValue && tarifaBaseSC.tarifaDomingo != 0)
                                        {
                                            precioHabitacion = tarifaBaseSC.tarifaDomingo.Value;
                                        }
                                        else
                                        {
                                            precioHabitacion = tarifaBaseSC.tarifaLunes;
                                        }
                                    }
                            
                                break;
                            case 1:
                                    if (tarifaBase.tarifaLunes != 0)
                                    {
                                        precioHabitacion = tarifaBase.tarifaLunes;
                                    }
                                    else
                                    {
                                        precioHabitacion = tarifaBaseSC.tarifaLunes;
                                    }
                                    
                                break;
                            case 2:
                                    if (tarifaBase.tarifaMartes.HasValue && tarifaBase.tarifaMartes!=0)
                                    {
                                        precioHabitacion = tarifaBase.tarifaMartes.Value;
                                    }
                                    else
                                    {
                                        if (tarifaBaseSC.tarifaMartes.HasValue && tarifaBaseSC.tarifaMartes !=0)
                                        {
                                            precioHabitacion = tarifaBaseSC.tarifaMartes.Value;
                                        }
                                        else
                                        {
                                            precioHabitacion = tarifaBaseSC.tarifaLunes;
                                        }
                                        
                                    }
                                
                                break;
                            case 3:
                                    if (tarifaBase.tarifaMiercoles.HasValue && tarifaBase.tarifaMiercoles!=0)
                                    {
                                        precioHabitacion = tarifaBase.tarifaMiercoles.Value;
                                    }
                                    else
                                    {
                                        if (tarifaBaseSC.tarifaMiercoles.HasValue && tarifaBaseSC.tarifaMiercoles != 0)
                                        {
                                            precioHabitacion = tarifaBaseSC.tarifaMiercoles.Value;
                                        }
                                        else
                                        {
                                            precioHabitacion = tarifaBaseSC.tarifaLunes;
                                        }
                                    }
                            
                                break;
                            case 4:
                                    if (tarifaBase.tarifaJueves.HasValue && tarifaBase.tarifaJueves!=0)
                                    {
                                        precioHabitacion = tarifaBase.tarifaJueves.Value;
                                    }
                                    else
                                    {
                                        if (tarifaBaseSC.tarifaJueves.HasValue && tarifaBaseSC.tarifaJueves != 0)
                                        {
                                            precioHabitacion = tarifaBaseSC.tarifaJueves.Value;
                                        }
                                        else
                                        {
                                            precioHabitacion = tarifaBaseSC.tarifaLunes;
                                        }
                                    }
 
                                break;
                            case 5:
                                    if (tarifaBase.tarifaViernes.HasValue && tarifaBase.tarifaViernes!=0)
                                    {
                                        precioHabitacion = tarifaBase.tarifaViernes.Value;
                                    }
                                    else
                                    {
                                        if (tarifaBaseSC.tarifaViernes.HasValue && tarifaBaseSC.tarifaViernes != 0)
                                        {
                                            precioHabitacion = tarifaBaseSC.tarifaViernes.Value;
                                        }
                                        else
                                        {
                                            precioHabitacion = tarifaBaseSC.tarifaLunes;
                                        }
                                    }
                           
                                break;
                            case 6:
                                    if (tarifaBase.tarifaSabado.HasValue && tarifaBase.tarifaSabado!=0)
                                    {
                                        precioHabitacion = tarifaBase.tarifaSabado.Value;
                                    }
                                    else
                                    {
                                        if (tarifaBaseSC.tarifaSabado.HasValue && tarifaBaseSC.tarifaSabado.Value!=0)
                                        {
                                            precioHabitacion = tarifaBaseSC.tarifaSabado.Value;
                                        }
                                        else
                                        {
                                            precioHabitacion = tarifaBaseSC.tarifaLunes;
                                        }
                                        
                                    }
                            
                                break;
                        }

                    }

                //MAURO
                if(precioHabitacion == 0)
                {
                    dinero = -1;
                    break;
                }
                    
                dinero = dinero + precioHabitacion;

                DiaRsv iDia = new DiaRsv();
                iDia.precioHabitacion       = precioHabitacion;
                iDia.precioAdicionalAdulto  = precioAdulto;
                iDia.precioAdicionalCuna    = precioCuna;
                iDia.precioAdicionalMenor   = precioMenor;
                iDia.dia = dia;

                listDias.Add(iDia);
            }                 
            if (tieneEstadia && cont<estadia)
            {
                dinero = -1;//error
            }
            return dinero;
        }

        public decimal getTarifaAdultoPorFecha(DateTime fechaIni, DateTime fechaFin, int idTipoHabitacion, int? idCondicion,int idAlojamiento)
        {
            decimal dinero = 0;

            TarifasBase tarifaBase;
            TarifasBase tarifaBaseSC;
            TarifasPorDia tarifasPorDia;
            TarifasBaseService tarifasBaseService = new TarifasBaseService(this.DB);
            TarifasPorDiaService tarifasPorDiaService = new TarifasPorDiaService(this.DB);
            decimal precioAdulto = 0;
            tarifaBase = tarifasBaseService.getInvitado(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]), idCondicion,idAlojamiento);
            tarifaBaseSC = tarifasBaseService.getInvitado(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]), null, idAlojamiento);
            DateTime dia;
            for (int i = 0; i < (fechaFin - fechaIni).TotalDays; i++)
            {
                dia = fechaIni.AddDays(i);
                tarifasPorDia = tarifasPorDiaService.getInvitado(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]), idCondicion, dia,idAlojamiento);
                if (tarifasPorDia != null && tarifasPorDia.adicionalAdulto.HasValue)
                    precioAdulto = tarifasPorDia.adicionalAdulto.Value;
                else
                {
                    precioAdulto = 0;
                    if (tarifaBase.adicionalAdulto.HasValue)
                    {
                        precioAdulto = tarifaBase.adicionalAdulto.Value;
                    }
                    else
                    {
                        if (tarifaBaseSC.adicionalAdulto.HasValue)
                            precioAdulto = tarifaBaseSC.adicionalAdulto.Value;
                    }
                }
                dinero = dinero + precioAdulto;

            }

            return dinero;
        }

        public decimal getTarifaMenorPorFecha(DateTime fechaIni, DateTime fechaFin, int idTipoHabitacion, int? idCondicion,int idAlojamiento)
        {
            decimal dinero = 0;

            TarifasBase tarifaBase;
            TarifasBase tarifaBaseSC;
            TarifasPorDia tarifasPorDia;
            TarifasBaseService tarifasBaseService = new TarifasBaseService(this.DB);
            TarifasPorDiaService tarifasPorDiaService = new TarifasPorDiaService(this.DB);
            decimal precioMenor = 0;
            tarifaBase = tarifasBaseService.getInvitado(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]), idCondicion,idAlojamiento);
            tarifaBaseSC = tarifasBaseService.getInvitado(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]), null, idAlojamiento);
            DateTime dia;
            for (int i = 0; i < (fechaFin - fechaIni).TotalDays; i++)
            {
                dia = fechaIni.AddDays(i);
                tarifasPorDia = tarifasPorDiaService.getInvitado(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]), idCondicion, dia,idAlojamiento);
                if (tarifasPorDia != null && tarifasPorDia.adicionalMenor.HasValue)
                    precioMenor = tarifasPorDia.adicionalMenor.Value;
                else
                {
                    precioMenor = 0;
                    if (tarifaBase.adicionalMenor.HasValue)
                    {
                        precioMenor = tarifaBase.adicionalMenor.Value;
                    }
                    else
                    {
                        if (tarifaBaseSC.adicionalMenor.HasValue)
                            precioMenor = tarifaBaseSC.adicionalMenor.Value;
                    }
                }
                dinero = dinero + precioMenor;
            }
            return dinero;
        }

        public decimal getTarifaCunaPorFecha(DateTime fechaIni, DateTime fechaFin, int idTipoHabitacion, int? idCondicion,int idAlojamiento)
        {
            decimal dinero = 0;

            TarifasBase tarifaBase;
            TarifasBase tarifaBaseSC;
            TarifasPorDia tarifasPorDia;
            TarifasBaseService tarifasBaseService = new TarifasBaseService(this.DB);
            TarifasPorDiaService tarifasPorDiaService = new TarifasPorDiaService(this.DB);
            decimal precioCuna = 0;
            tarifaBase = tarifasBaseService.getInvitado(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]), idCondicion,idAlojamiento);
            tarifaBaseSC = tarifasBaseService.getInvitado(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]), null, idAlojamiento);
            DateTime dia;
            for (int i = 0; i < (fechaFin - fechaIni).TotalDays; i++)
            {
                dia = fechaIni.AddDays(i);
                tarifasPorDia = tarifasPorDiaService.getInvitado(idTipoHabitacion, Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]), idCondicion, dia,idAlojamiento);
                if (tarifasPorDia != null && tarifasPorDia.adicionalCuna.HasValue)
                    precioCuna = tarifasPorDia.adicionalCuna.Value;
                else
                {
                    precioCuna = 0;
                    if (tarifaBase.adicionalCuna.HasValue)
                    {
                        precioCuna = tarifaBase.adicionalCuna.Value;
                    }
                    else
                    {
                        if (tarifaBaseSC.adicionalMenor.HasValue)
                            precioCuna = tarifaBaseSC.adicionalCuna.Value;
                    }
                }
                dinero = dinero + precioCuna;
            }
            return dinero;
        }

        public void BorrarPreCargasViejas()
        {
            int idAlojamiento = getIdAlojamiento();
            int webCanal = Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]);
            DateTime hoy = DateTime.Now;
            int minutos = Convert.ToInt32(ConfigurationManager.AppSettings["MinutosSesionBusqueda"]);
            hoy = hoy.AddMinutes(minutos);

            List<Reserva> Rsv = DB.Reservas.Where(r => r.idAlojamiento == idAlojamiento && r.idCanal == webCanal && r.idEstado == 1 && r.fechaCreacion<hoy).ToList<Reserva>();

            foreach (Reserva item in Rsv)
            {
                if (item.fechaCreacion<hoy)
                {
                    List<HistorialReserva> hResv = DB.HistorialReservas.Where(r => r.idAlojamiento == idAlojamiento && r.idReserva == item.idReserva).ToList<HistorialReserva>();

                    foreach (HistorialReserva hItem in hResv)
                    {
                        DB.HistorialReservas.Remove(hItem);
                        DB.SaveChanges();
                    }

                    eliminarReserva(item.idReserva, idAlojamiento);
                }
            }
        }

        public void eliminarReserva(int idReserva,int idAlojamiento)
        {
            HabitacionesService habitacionesService = new HabitacionesService(this.DB);
            Reserva unRsv = getReserva(idReserva);
            
            habitacionesService.EliminarHabitacionesPorReservaBusqueda(idReserva,idAlojamiento);
            unRsv.HabitacionesPorReservas.Clear();
            DB.Reservas.Remove(unRsv);
            DB.SaveChanges();            
        }

        public Reserva getReserva(int idReserva)
        {
            return DB.Reservas.Find(idReserva);
        }

        public void CrearComprobante(CargarComprobanteViewModel vm)
        {
            int idReserva = vm.idReserva;
            Reserva rsv = DB.Reservas.Find(idReserva);

            //si ya existe el comprobante no lo creo.


            Comprobante unCb = DB.Comprobantes.Where(s => s.idReserva == vm.idReserva).FirstOrDefault(); 
            if (unCb== null)
            {   
                unCb = new Comprobante();

            unCb.idAlojamiento = vm.idAlojamiento;
            unCb.idHuesped = rsv.idHuesped;
            unCb.monto = vm.monto;
            unCb.nombreBanco = vm.tbNombreBanco;
            unCb.numeroCuenta = vm.numeroCuenta;
            unCb.numeroOperacion = vm.numeroOperacion;
            unCb.observaciones = vm.observaciones;
            unCb.sucursal = vm.sucursal;
            unCb.idReserva = vm.idReserva;
            unCb.verificado = false;
            unCb.fecha = vm.fecha;

            unCb.fechaCreacion = DateTime.Now;

            DB.Comprobantes.Add(unCb);
            DB.SaveChanges();
            }
        }

        public void CargarDetalleReserva(CargarComprobanteViewModel vm,int idReserva)
        {
            Reserva Rsv = DB.Reservas.Find(idReserva);
            List<HabitacionesPorReserva> HabResev0 = Rsv.HabitacionesPorReservas.Where(r=>r.idReserva == idReserva).OrderBy(r=>r.idHabitacion).ToList();
            var ListHab = HabResev0.GroupBy(v => v.idHabitacion).Select(v => v.FirstOrDefault());

            foreach (HabitacionesPorReserva itemHab in ListHab)
            {
                decimal tarifa = 0;
                decimal tarifaAd = 0;
                decimal tarifaNn = 0;
                decimal auxTarifa = 0;

                int idTipoHabitacion = 0;
                var itemDias = DB.HabitacionesPorReservas.Where(t => t.idHabitacion == itemHab.idHabitacion && t.idReserva == idReserva).ToList();
                foreach (HabitacionesPorReserva itemDia in itemDias)
                {
                    tarifa = tarifa + itemDia.precioHabitacion;
                    idTipoHabitacion = itemDia.Habitacione.idTipoHabitacion;
                    if (itemDia.precioAdicionalAdulto.HasValue)
                    { 
                        tarifaAd = tarifaAd + itemDia.precioAdicionalAdulto.Value;
                    }
                    if (itemDia.precioAdicionalMenor.HasValue)
                    {
                        tarifaNn = tarifaNn + itemDia.precioAdicionalMenor.Value;
                    }
                }
                TiposHabitacione tipoHab = DB.TiposHabitaciones.Find(idTipoHabitacion);
                int idPosicion = vm.listado.IndexOf(tipoHab);
                if (idPosicion < 0)
                {
                    vm.listado.Add(tipoHab);
                    vm.listadoTarifa.Add(tarifa);

                    AdicionalItem adItem = new AdicionalItem();
                    adItem.precio = tarifaAd;

                    AdicionalItem nnItem = new AdicionalItem();
                    nnItem.precio = tarifaNn;
                    vm.listadoElige.Add(1);

                    vm.listadoTarifaAdulto.Add(adItem);
                    vm.listadoTarifaNino.Add(nnItem);
                }
                else
                {
                    auxTarifa = vm.listadoTarifa[idPosicion];
                    auxTarifa = auxTarifa + tarifa;
                    vm.listadoTarifa[idPosicion] = auxTarifa;

                    AdicionalItem auxAd = new AdicionalItem();
                    auxAd = vm.listadoTarifaAdulto[idPosicion];
                    auxAd.precio = auxAd.precio + tarifaAd;
                    vm.listadoTarifaAdulto[idPosicion] = auxAd;

                    vm.listadoElige[idPosicion]++;

                    auxAd = new AdicionalItem();
                    auxAd = vm.listadoTarifaNino[idPosicion];
                    auxAd.precio = auxAd.precio + tarifaNn;
                    vm.listadoTarifaNino[idPosicion] = auxAd;   
                }

                if (itemHab.impuesto.HasValue)
                {
                    vm.impuesto = itemHab.impuesto.Value;
                }
            }

        }

        public void Ejecutar(ResultadoBusquedaViewModel vm, bool control)
        {
            
            string idHotel = vm.idAlojamiento.ToString();
            vm.listadoCant = new List<int>();
            vm.listadoCantAdulto = new List<int>();
            vm.listadoCantNino = new List<int>();

            vm.listadoTarifa = new List<decimal>();
            vm.listadoTarifaAdulto = new List<AdicionalItem>();
            vm.listadoTarifaNino = new List<AdicionalItem>();
            vm.listadoElige = new List<int>();
            vm.listadoEligeAdulto = new List<int>();
            vm.listadoEligeNino = new List<int>();

            vm.combinaElige = new List<int>();
            vm.listadoCondicionesPorHab = new List<int>();

            List<LHabDisponibles> LLDisponible = new List<LHabDisponibles>();
            List<Habitacione> LDispo;
            LHabDisponibles LHabDispo;

            List<LDiaRsv> LLDia = new List<LDiaRsv>();
            LDiaRsv LXDia;
            List<DiaRsv> ListDia;
            List<TiposHabitacione> ListTH = new List<TiposHabitacione>();
            
            if (control==false)
            {                
                ListTH = DB.TiposHabitaciones.Where(s => s.idAlojamiento == vm.idAlojamiento).OrderByDescending(c => c.plazas).ToList();
            }
            vm.listarsv = new List<string>();
            vm.listarsvdes = new List<string>();
            vm.listapoli = new List<string>();
            vm.listapolides = new List<string>();

            AdicionalItem auxAdi;
            CondicionesReservaService condicionesReservaSV = new CondicionesReservaService(DB);
            List<CondReserva> condicionesReservas;

            CondReserva condicion;
            int? condicionId = 0;
            


            foreach (TiposHabitacione itemTH in ListTH)
            {
                int countHab = 0;
                int webCanal = Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]);

                var itemC0 = from c in DB.CondReservas.Where(d => d.idAlojamiento == vm.idAlojamiento)
                             from f in c.TarifasBases.Where(r => r.idTipoHabitacion == itemTH.idTipoHabitacion && r.abierta == true && r.idCanal == webCanal).ToList()
                             from g in c.TarifasPorDias.Where(h => h.idTipoHabitacion == itemTH.idTipoHabitacion && h.abierta == true && h.idCanal == webCanal &&
                            (h.permiteIngreso == true || h.fecha != vm.fechaIni) && (h.permiteEgreso == true || h.fecha != vm.fechaFin) && h.tarifaBase > 0 &&
                             (vm.fechaIni <= h.fecha && h.fecha <= vm.fechaFin))
                             select c;
                var itemC = itemC0.GroupBy(b => b.idCondReserva).Select(b => b.FirstOrDefault());

                condicionesReservas = itemC.ToList<CondReserva>();

                LDispo = new List<Habitacione>();

                var itemHab0 = from c in DB.Habitaciones.Where(h => h.idTipoHabitacion == itemTH.idTipoHabitacion && h.disponibleAbonoHabitacion == true && h.estado != "4")
                               from f in c.TiposHabitacione.TarifasBases.Where(r => r.idTipoHabitacion == itemTH.idTipoHabitacion && r.abierta == true && r.idCanal == webCanal).ToList()
                               from g in c.TiposHabitacione.TarifasPorDias.Where(h => h.idTipoHabitacion == itemTH.idTipoHabitacion && h.tarifaBase > 0 &&
                               h.abierta == true && h.idCanal == webCanal && (vm.fechaIni <= h.fecha && h.fecha <= vm.fechaFin)
                               && (h.permiteIngreso == true || h.fecha != vm.fechaIni) && (h.permiteEgreso == true || h.fecha != vm.fechaFin)
                               )
                               select c;
                var itemHab = itemHab0.GroupBy(v => v.idHabitacion).Select(v => v.FirstOrDefault());                

                LHabDispo = new LHabDisponibles();
                LHabDispo.listado = LDispo;
                LLDisponible.Add(LHabDispo);
      
                //condiciones
                for (int x = -1; x < condicionesReservas.Count(); x++)
                {
                    if (condicionesReservas.Count()!=0 || x == -1)
                    {

                        if (x == -1)
                        {
                            condicion = null;
                            condicionId = null;
                        }
                        else
                        {
                            condicion = condicionesReservas.ElementAt(x);
                            condicionId = condicion.idCondReserva;
                        }

                        bool fechaVetada = false;
                        List<TarifasPorDia> disponibi = DB.TarifasPorDias.Where(r => r.idTipoHabitacion == itemTH.idTipoHabitacion 
                                                                                  && r.idCanal == webCanal 
                                                                                  && vm.fechaIni <= r.fecha 
                                                                                  && r.fecha <= vm.fechaFin 
                                                                                  && r.idCondReserva == condicionId
                                                                                ).ToList<TarifasPorDia>();
                        int minimoDispo = 999;
                        bool tarifaBaseMayorCero = false;
                        foreach (TarifasPorDia tf in disponibi)
                        {
                            if (tf.disponibilidadCanal.HasValue && tf.disponibilidadCanal < minimoDispo)
                            {
                                minimoDispo = tf.disponibilidadCanal.Value;
                            }
                            if (tf.permiteIngreso == false && tf.fecha == vm.fechaIni)
                            {
                                fechaVetada = true;
                            }

                            if (tf.permiteEgreso == false && tf.fecha == vm.fechaFin)
                            {
                                fechaVetada = true;
                            }

                            if (tf.abierta == false && vm.fechaIni <= tf.fecha && tf.fecha <= vm.fechaFin)
                            {
                                fechaVetada = true;
                            }
                            if (tf.tarifaBase > 0)
                                tarifaBaseMayorCero = true;
                        }

                        if (fechaVetada)
                        {
                            minimoDispo = 0;
                        }

                        countHab = 0;
                        foreach (Habitacione itemH in itemHab)
                        {
                            var itemR = from y in DB.Reservas.Where(r => r.idAlojamiento == vm.idAlojamiento &&
                                        ((r.fechaEntrada >= vm.fechaIni && r.fechaEntrada < vm.fechaFin) || (r.fechaSalida > vm.fechaIni && r.fechaSalida <= vm.fechaFin))
                                        && r.idEstado != 4)
                                        from z in y.HabitacionesPorReservas.Where(h => h.idHabitacion == itemH.idHabitacion).ToList()
                                        select y;
                            if (itemR.Count() == 0)
                            {
                                if (countHab < minimoDispo)
                                {
                                    countHab++;
                                    LDispo.Add(itemH);
                                }
                            }

                        }

                        if (countHab>0)
                        {
                            if (condicionId == null && tarifaBaseMayorCero)
                            //if ( !condicionId.HasValue || (condicionId.HasValue && condicionId.Value == 0))                            
                            {
                                //vm.listadoCondicionesPorHab.Add(0);



                                switch (vm.idioma)
                                {
                                    case "es":
                                        vm.listarsv.Add(" ");
                                        vm.listarsvdes.Add(" ");
                                        vm.listapoli.Add(" ");
                                        vm.listapolides.Add(" ");
                                        break;

                                    case "pt":
                                        vm.listarsv.Add(" ");
                                        vm.listarsvdes.Add(" ");
                                        vm.listapoli.Add(" ");
                                        vm.listapolides.Add(" ");
                                        break;

                                    case "en":
                                        vm.listarsv.Add(" ");
                                        vm.listarsvdes.Add(" ");
                                        vm.listapoli.Add(" ");
                                        vm.listapolides.Add(" ");
                                        break;

                                    default:
                                        vm.listarsv.Add(" ");
                                        vm.listarsvdes.Add(" ");
                                        vm.listapoli.Add(" ");
                                        vm.listapolides.Add(" ");
                                        break;
                                }


                            }
                            else if (condicionId.HasValue && condicionId.Value > 0)
                            {
                                //vm.listadoCondicionesPorHab.Add(condicion.idCondReserva);
                                switch (vm.idioma)
                                {
                                    case "es":
                                        vm.listarsv.Add(condicion.nombreEs);
                                        vm.listarsvdes.Add(condicion.descripcionEs);
                                        vm.listapoli.Add(condicion.PoliticasCancelacions.First().nombreEs);
                                        vm.listapolides.Add(condicion.PoliticasCancelacions.First().descripcionEs);
                                        break;

                                    case "pt":
                                        vm.listarsv.Add(condicion.nombrePt);
                                        vm.listarsvdes.Add(condicion.descripcionPt);
                                        vm.listapoli.Add(condicion.PoliticasCancelacions.First().nombrePt);
                                        vm.listapolides.Add(condicion.PoliticasCancelacions.First().descripcionPt);
                                        break;

                                    case "en":
                                        vm.listarsv.Add(condicion.nombreEn);
                                        vm.listarsvdes.Add(condicion.descripcionEn);
                                        vm.listapoli.Add(condicion.PoliticasCancelacions.First().nombreEn);
                                        vm.listapolides.Add(condicion.PoliticasCancelacions.First().descripcionEn);
                                        break;

                                    default:
                                        vm.listarsv.Add(condicion.nombreEs);
                                        vm.listarsvdes.Add(condicion.descripcionEs);
                                        vm.listapoli.Add(condicion.PoliticasCancelacions.First().nombreEs);
                                        vm.listapolides.Add(condicion.PoliticasCancelacions.First().descripcionEs);
                                        break;
                                }
                            }
                        }                    
                    }

                    if (countHab > 0)
                    {
                        if (itemTH.camasAdicionales.HasValue)
                        {
                            if (itemTH.permiteAdicionalAdultos.Value == false)
                            {
                                vm.listadoCantAdulto.Add(0);
                                auxAdi = new AdicionalItem();
                                auxAdi.cantidad = 0;
                                auxAdi.permite = false;
                                auxAdi.precio = 0;
                                vm.listadoTarifaAdulto.Add(auxAdi);
                            }
                            else
                            {
                                auxAdi = new AdicionalItem();
                                auxAdi.precio = getTarifaAdultoPorFecha(vm.fechaIni, vm.fechaFin, itemTH.idTipoHabitacion, condicionId,vm.idAlojamiento);
                                auxAdi.cantidad = itemTH.camasAdicionales.Value;
                                auxAdi.permite = true;                               
                                vm.listadoTarifaAdulto.Add(auxAdi);
                                vm.listadoCantAdulto.Add(itemTH.camasAdicionales.Value);
                            }

                            if (itemTH.permiteAdicionalMenores.Value == false)
                            {
                                vm.listadoCantNino.Add(0);
                                auxAdi = new AdicionalItem();
                                auxAdi.cantidad = 0;
                                auxAdi.permite = false;
                                auxAdi.precio = 0;
                                vm.listadoTarifaNino.Add(auxAdi);
                            }
                            else
                            {
                                auxAdi = new AdicionalItem();
                                auxAdi.precio = getTarifaMenorPorFecha(vm.fechaIni, vm.fechaFin, itemTH.idTipoHabitacion, condicionId,vm.idAlojamiento);
                                auxAdi.cantidad = itemTH.camasAdicionales.Value;
                                auxAdi.permite = true;
                                vm.listadoTarifaNino.Add(auxAdi);
                                vm.listadoCantNino.Add(itemTH.camasAdicionales.Value);                                
                            }
                        }
                        else
                        {
                            vm.listadoCantAdulto.Add(0);
                            vm.listadoCantNino.Add(0);
                            auxAdi = new AdicionalItem();
                            auxAdi.cantidad = 0;
                            auxAdi.permite = false;
                            auxAdi.precio = 0;
                            vm.listadoTarifaNino.Add(auxAdi);
                            vm.listadoTarifaAdulto.Add(auxAdi);
                        }

                        ListDia = new List<DiaRsv>();
                        decimal dinero = getTarifaPorFecha(vm.fechaIni, vm.fechaFin, itemTH.idTipoHabitacion, condicionId, vm.idAlojamiento, ListDia);
                        if (dinero > 0)
                        {
                            vm.listado.Add(itemTH);
                            vm.listadoCant.Add(countHab);
                            vm.listadoElige.Add(0);
                            vm.listadoEligeAdulto.Add(0);
                            vm.listadoEligeNino.Add(0);                            
                            vm.listadoTarifa.Add(dinero);
                            if (condicionId == null)
                            {
                                vm.listadoCondicionesPorHab.Add(0);
                            }
                            else
                            {
                                vm.listadoCondicionesPorHab.Add(condicionId.Value);
                            }
                            LXDia = new LDiaRsv();
                            LXDia.listado = ListDia;
                            LLDia.Add(LXDia);
                        }
                    }
                }
            }

            Alojamiento aloj = DB.Alojamientos.Find(vm.idAlojamiento);
            vm.ubicacion = new GoogleMap();
            vm.ubicacion.direccion = aloj.direccion;
            vm.ubicacion.zoomMapa = aloj.zoomMapa;
            vm.ubicacion.latitud = aloj.latitud;
            vm.ubicacion.longitud = aloj.longitud;
            vm.ubicacion.tipoMapa = aloj.tipoMapa;
            vm.ubicacion.pais = aloj.Ciudade.Provincia.Pais.nombreEs;
            vm.ubicacion.provincia = aloj.Ciudade.Provincia.nombreEs;
            vm.ubicacion.ciudad = aloj.Ciudade.nombreEs;

            int auxPasajero = vm.cantPasajeros;
            vm.oferta = new List<string>();
            int i = -1;

            int cant = 0;
            decimal tarifa = 0;
            decimal total = 0;

            vm.ofertaBooking = new List<OfertaItem>();            

            foreach (TiposHabitacione item in vm.listado)
            {
                i++;
                if (auxPasajero > 0)
                {
                    if (auxPasajero > item.plazas)
                    {
                        cant = 0;
                        tarifa = 0;
                        while(auxPasajero > item.plazas && cant < vm.listadoCant[i])
                        {
                            cant++;
                            tarifa =tarifa+ vm.listadoTarifa[i];
                            auxPasajero = auxPasajero - item.plazas.Value;

                        }
                        total = total + tarifa;
                        vm.combinaElige.Add(cant);
                        vm.combina.Add(item);
                        vm.oferta.Add(cant.ToString() + " - " + item.nombreEs + " " + vm.listarsv[i] + " $" + tarifa.ToString());

                        OfertaItem auxOfer = new OfertaItem();                        
                        auxOfer.cantidad = cant;
                        auxOfer.nombre = item.nombreEs + " " + vm.listarsv[i];
                        auxOfer.precio = tarifa;
                        vm.ofertaBooking.Add(auxOfer);
                    }
                    else
                    {
                        if (auxPasajero == item.plazas)
                        {
                            vm.oferta.Add("1 - "+item.nombreEs + " " + vm.listarsv[i] +" $"+vm.listadoTarifa[i].ToString());

                            OfertaItem auxOfer = new OfertaItem();
                            auxOfer.cantidad = 1;
                            auxOfer.nombre = item.nombreEs + " " + vm.listarsv[i];
                            auxOfer.precio = vm.listadoTarifa[i];
                            vm.ofertaBooking.Add(auxOfer);

                            auxPasajero = auxPasajero - item.plazas.Value;
                            total = total+vm.listadoTarifa[i];
                            vm.combinaElige.Add(1);
                            vm.combina.Add(item);
                            break;
                        }
                        else
                        {
                            vm.combinaElige.Add(0);
                            vm.combina.Add(item);
                        }
                    }
                }
                else
                {
                    vm.combinaElige.Add(0);
                    vm.combina.Add(item);
                    break;
                }

            }
            decimal impuesto = 0;
            if (aloj.impuesto.HasValue)
            { 
                impuesto = total * aloj.impuesto.Value / 100;
            }
            vm.oferta.Add("Total : $" + total.ToString("N", new CultureInfo("en-US")));
            DB.SaveChanges();

            var sesion = System.Web.HttpContext.Current.Session["SesionInvitado" + idHotel] as SesionInvitado;
            if (null == sesion)
            {

                vm.listado.Reverse();
                //MAURO
                //vm.listadoCant.Reverse();
                if (vm.listadoCantAdulto != null)
                {
                    vm.listadoCantAdulto.Reverse();
                }
                
                if (vm.listadoCantNino != null)
                {
                    vm.listadoCantNino.Reverse();
                }
                
                if (vm.listadoDisponible != null)
                {
                    vm.listadoDisponible.Reverse();
                }
                
                vm.listadoElige.Reverse();

                if (vm.listadoEligeAdulto != null)
                {
                    vm.listadoEligeAdulto.Reverse();
                }
                
                if (vm.listadoEligeNino != null)
                {
                    vm.listadoEligeNino.Reverse();
                }
                
                if (vm.listadoTarifa != null)
                { 
                    //MAURO
                    //vm.listadoTarifa.Reverse();
                }

                if (vm.listadoTarifaAdulto != null)
                {
                    vm.listadoTarifaAdulto.Reverse();
                }
                
                if (vm.listadoTarifaNino != null)
                {
                    vm.listadoTarifaNino.Reverse();
                }
                
                //MAURO
                //LLDia.Reverse();

                if (vm.combina != null)
                {
                    vm.combina.Reverse();
                }
                
                if (vm.combinaElige != null)
                {
                    vm.combinaElige.Reverse();
                }
                
                if (vm.combinaEligeAdulto != null)
                {
                    vm.combinaEligeAdulto.Reverse();
                }
                
                if (vm.combinaEligeNino != null)
                {
                    vm.combinaEligeNino.Reverse();
                }
          
                sesion = new SesionInvitado();
                sesion.fechaEntrada = vm.fechaIni;
                sesion.fechaSalida = vm.fechaFin;
                sesion.idAlojamiento = vm.idAlojamiento;
                sesion.pasajeros = vm.pasajeros;
                sesion.colorBarra = vm.colorBarra;
                sesion.colorBoton = vm.colorBoton;
                sesion.backurl = vm.backurlHotel;
                sesion.urlLogo = vm.urlLogoHotel;
                sesion.listadoCant = vm.listadoCant;
                sesion.colorLogo = vm.colorBarra;
                sesion.direccionHotel = vm.direccionHotel;
                sesion.estrellas = vm.estrellas;
                sesion.urlImagenMini = vm.urlLogoHotel;
                sesion.telHotel = vm.telefonoHotel;
                sesion.listadoTarifa = vm.listadoTarifa;
                sesion.listadoTarifaAdulto = vm.listadoTarifaAdulto;
                sesion.listadoTarifaNino = vm.listadoTarifaNino;
                sesion.listadoDias = LLDia;
                sesion.listadoDisponible = LLDisponible;

                sesion.combinaElige = vm.combinaElige;
                sesion.combina = vm.combina;
                sesion.ofertaBooking = vm.ofertaBooking;

                sesion.listadoCondicionPorHab = vm.listadoCondicionesPorHab;
                sesion.listadoNombCondicionPorHab = vm.listarsv;

                vm.totalOferta = total.ToString("N", new CultureInfo("en-US"));
                vm.impuestoOferta = impuesto.ToString("N", new CultureInfo("en-US"));

                System.Web.HttpContext.Current.Session["SesionInvitado" + idHotel] = sesion;
            }
            
        }        
    }
}