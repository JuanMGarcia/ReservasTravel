﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using System.Data.Entity;

namespace ReservasWeb.Services
{
    public class PermisosService : BaseService
    {
        public PermisosService(ReservasWebEntities db) : base(db)
        {
        }

        //public List<Usuario> getByIdRol(int idRol)
        //{
        //    return (from i in DB.Usuarios.Where(i => i.idRol == idRol)
        //                                 select i).ToList<Usuario>();
        //}
        public void crear(Modulo modulo, Role Rol, bool listar, bool alta, bool baja, bool modificacion)
        {
            Permiso permiso = new Permiso();
            permiso.Modulo = modulo;
            permiso.Role = Rol;
            permiso.listar = listar;
            permiso.alta = alta;
            permiso.baja = baja;
            permiso.modificacion = modificacion;
            permiso.fechaCreacion = DateTime.Now;
            crear(permiso);            
        }
        public void crear(Permiso permiso)
        {
            permiso.fechaCreacion = DateTime.Now;
            DB.Permisos.Add(permiso);
            DB.SaveChanges();
        }

        public void editar(Permiso permiso)
        {
            permiso.fechaModificacion = DateTime.Now;
            DB.Entry(permiso).State = EntityState.Modified;
            DB.SaveChanges();
        }
        
        public bool listarxRol(int idRol,int idModulo)
        {
            var perm = DB.Permisos.Where(x => x.idRol == idRol && x.idModulo == idModulo && x.listar==true).ToList();

            return perm.Count == 1;
        }
        public bool altaxRol(int idRol, int idModulo)
        {
            var perm = DB.Permisos.Where(x => x.idRol == idRol && x.idModulo == idModulo && x.alta==true).ToList();

            return perm.Count == 1;
        }
        public bool bajaxRol(int idRol, int idModulo)
        {
            var perm = DB.Permisos.Where(x => x.idRol == idRol && x.idModulo == idModulo && x.baja==true).ToList();

            return perm.Count == 1;
        }
        public bool modificarxRol(int idRol, int idModulo)
        {
            var perm = DB.Permisos.Where(x => x.idRol == idRol && x.idModulo == idModulo && x.modificacion==true).ToList();

            return perm.Count == 1;
        }
    }
}