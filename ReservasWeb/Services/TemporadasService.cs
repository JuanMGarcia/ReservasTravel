﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using ReservasWeb.ViewModels;
using ReservasWeb.Utils;
using System.Data.Entity;
using System.Web.Mvc;
using System.Configuration;
using System.Globalization;
using System.Threading;

namespace ReservasWeb.Services
{
    public class TemporadasService : BaseService
    {
        public TemporadasService(ReservasWebEntities db) : base(db)
        {
        }

        public List<Temporada> getTemporadasPorAlojamiento(string tipo)
        {
            int idAlojamiento = getIdAlojamiento();
            List<Temporada> lista = DB.Temporadas.Where(t => t.idAlojamiento == idAlojamiento && t.tipo == tipo).ToList();
            return lista;
        }

        public Temporada getTemporada(int idTemporada)
        {
            int idAlojamiento = getIdAlojamiento();
            List<Temporada> lista = DB.Temporadas.Where(t => t.idAlojamiento == idAlojamiento && t.idTemporada == idTemporada).ToList();
            if (lista != null && lista.Count > 0)
                return lista.ElementAt(0);
            else
                return null;
        }

        public SelectList getComboTipo()
        {
            return new SelectList(new[]
            {
                new { ID = "", Name = "-- Seleccionar --" },
                new { ID = "T", Name = "Temporada" },
                new { ID = "FE", Name = "Fecha Especial" }
            },
            "ID", "Name", "");
        }

        public List<SelectListItem> getComboCssColor(string colorSeleccionado)
        {
            bool selected = false;
            string color = "";
            List<SelectListItem> coloresList = new List<SelectListItem>();
            if (colorSeleccionado == null)
                selected = true;
            selected = false;
            for (int i = 0; i < 24; i++)
            {
                color = "#" + ConfigurationManager.AppSettings["color" + (i + 1).ToString()];
                if ("#" + colorSeleccionado == color)
                    selected = true;
                
                coloresList.Add(new SelectListItem { Value = color, Text = color, Selected = selected });
                selected = false;
            }
            return coloresList;
        }

        public CreacionEdicionTemporadaViewModel getTemporadaEdicion(int idTemporada)
        {
            Temporada temporada = DB.Temporadas.Find(idTemporada);
            CreacionEdicionTemporadaViewModel vm = new CreacionEdicionTemporadaViewModel();
            vm.idTemporada = temporada.idTemporada;
            vm.nombre = temporada.nombre;
            vm.tipo = temporada.tipo;
            vm.cssColor = temporada.cssColor;
            vm.descripcion = temporada.descripcion;
            vm.fechaDesde = temporada.fechaDesde.ToShortDateString();
            vm.fechaHasta = temporada.fechaHasta.ToShortDateString();
            return vm;
        }

        public int Crear(CreacionEdicionTemporadaViewModel vm)
        {
            int idTemporada = 0;
            if (EsTemporadaValida(vm))
            {
                Temporada temporada = new Temporada();
                temporada.nombre = vm.nombre;
                temporada.tipo = vm.tipo;
                temporada.descripcion = vm.descripcion;
                temporada.fechaDesde = DateHelper.stringToDateTime(vm.fechaDesde);
                temporada.fechaHasta = DateHelper.stringToDateTime(vm.fechaHasta);
                temporada.idAlojamiento = getIdAlojamiento();
                temporada.cssColor = vm.cssColor.Replace("#", "");
                crear(temporada);
                idTemporada = temporada.idTemporada;
            }

            return idTemporada;
        }

        public void Eliminar(int idTemporada)
        {
            int idAlojamiento = getIdAlojamiento();
            Temporada temporada = DB.Temporadas.Where(t => t.idTemporada == idTemporada && t.idAlojamiento == idAlojamiento).FirstOrDefault();
            DB.Temporadas.Remove(temporada);
            DB.SaveChanges();
        }

        public int Editar(CreacionEdicionTemporadaViewModel vm)
        {
            int idTemporada = 0;
            if (EsTemporadaValida(vm))
            {
                Temporada temporada = DB.Temporadas.Find(vm.idTemporada);
                temporada.nombre = vm.nombre;
                temporada.tipo = vm.tipo;
                temporada.descripcion = vm.descripcion;
                temporada.fechaDesde = DateHelper.stringToDateTime(vm.fechaDesde);
                temporada.fechaHasta = DateHelper.stringToDateTime(vm.fechaHasta);
                temporada.idAlojamiento = getIdAlojamiento();
                temporada.cssColor = vm.cssColor.Replace("#","");
                editar(temporada);
                idTemporada = temporada.idTemporada;
            }
            return idTemporada;
        }
        public void crear(Temporada temporada)
        {
            temporada.fechaCreacion = DateTime.Now;
            DB.Temporadas.Add(temporada);
            DB.SaveChanges();
        }

        public void editar(Temporada temporada)
        {
            temporada.fechaModificacion = DateTime.Now;
            DB.Entry(temporada).State = EntityState.Modified;
            DB.SaveChanges();
        }

        private bool EsTemporadaValida(CreacionEdicionTemporadaViewModel vm)
        {
            bool esValida = true;
            int idAlojamiento = getIdAlojamiento();
            DateTime fechaDesde = DateHelper.stringToDateTime(vm.fechaDesde);
            DateTime fechaHasta = DateHelper.stringToDateTime(vm.fechaHasta);
            var haySolapamiento = DB.Temporadas.Any(s => s.idAlojamiento == idAlojamiento && s.tipo == vm.tipo && (vm.idTemporada == 0 || s.idTemporada != vm.idTemporada) && ((s.fechaDesde <= fechaDesde && fechaDesde <= s.fechaHasta) || (s.fechaDesde <= fechaHasta && fechaHasta <= s.fechaHasta)));

            //si el Tipo es "Fecha Especial" == FE, entonces controlo que sea maximo de 20 dias 
            if (haySolapamiento 
                || ((fechaHasta - fechaDesde).TotalDays > 20 && vm.tipo == ConfigurationManager.AppSettings["tipoFechaEspecial"])
                || ((fechaHasta - fechaDesde).TotalDays > 370 && vm.tipo == ConfigurationManager.AppSettings["tipoTemporada"]))
                esValida = false;

            return esValida;
        }

    }
}