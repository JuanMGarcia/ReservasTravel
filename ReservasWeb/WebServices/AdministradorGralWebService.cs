﻿using ReservasWeb.Models;
using ReservasWeb.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Services;
using System.Web.Services;
using System.Xml.Linq;

//namespace ReservasWeb.WebServices
//{
    [WebService]
    public class AdministradorGralWebService
    {

        const string SUPER_ADMIN = "SUPER_ADMIN";

    //    [HttpPost]
    //    [WebMethod]
    //    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //    public String CrearAlojamiento(int idCliente, string nombreCliente, string emailCliente, int idProducto, string nombre, string ciudadNombre, bool standBy, int cantidadHabitaciones)
    //    {
    //        ClientesService clientesService = new ClientesService(this.DB);
    //        CiudadesService ciudadesService = new CiudadesService(this.DB);
    //        CanalesService canalesService = new CanalesService(this.DB);
    //        AlojamientosService alojamientosService = new AlojamientosService(this.DB);
    //        //int idAlojamiento = 0;
    //        try
    //        {
    //            Cliente cliente = clientesService.get(idCliente);
    //            if (cliente == null)
    //                cliente = clientesService.crear(idCliente, nombreCliente, emailCliente, false);                
            
    //                Alojamiento alojamiento = alojamientosService.getByIdProductoAdmin(idProducto); 

    //                if (alojamiento == null)
    //                {
    //                    alojamiento = new Alojamiento();                    
    //                    Ciudade ciudad = ciudadesService.getByNombreEs(ciudadNombre);
                
    //                    if (ciudad != null)
    //                            alojamiento.idCiudad = ciudad.idCiudad;

    //                    if (ciudad != null && ciudad.Provincia.idPais == Convert.ToInt32(ConfigurationManager.AppSettings["idPaisArgentina"]))
    //                        alojamiento.idMoneda = Convert.ToInt32(ConfigurationManager.AppSettings["idMonedaPesosArg"]);
    //                    else
    //                        alojamiento.idMoneda = Convert.ToInt32(ConfigurationManager.AppSettings["idMonedaDolaresUSA"]);
                        
    //                    alojamiento.idProductoAdmin = idProducto;
    //                    alojamiento.idCliente = cliente.idCliente;
    //                    alojamiento.nombreEs = nombre;
    //                    alojamiento.standBy = false;
    //                    alojamiento.cantidadHabitaciones = cantidadHabitaciones;
    //                    //alojamiento.horaCheckin = "12:00";
    //                    //alojamiento.horaCheckout = "12:00";

    //                    alojamientosService.crear(alojamiento);

    //                    CanalesPorAlojamiento canalesPorAlojamiento = new CanalesPorAlojamiento();
    //                    canalesPorAlojamiento.idAlojamiento = alojamiento.idAlojamiento;
    //                    canalesPorAlojamiento.idCanal = Convert.ToInt32(ConfigurationManager.AppSettings["idCanalWeb"]);
    //                    canalesService.crear(canalesPorAlojamiento);

    //                    canalesPorAlojamiento = new CanalesPorAlojamiento();
    //                    canalesPorAlojamiento.idAlojamiento = alojamiento.idAlojamiento;
    //                    canalesPorAlojamiento.idCanal = Convert.ToInt32(ConfigurationManager.AppSettings["idCanalMostrador"]);
    //                    canalesService.crear(canalesPorAlojamiento);

    //                        //String resultado = EstilosDefault(alojamiento.idAlojamiento);
    //                        //if (resultado == "NO")
    //                        //    return "NO_ESTILOS";

    //                        //resultado = SliderDefault(alojamiento.idAlojamiento);
    //                        //if (resultado == "NO")
    //                        //    return "NO_SLIDERS";

    //                        //resultado = ImagenQuienesSomosDefault(alojamiento.idAlojamiento);
    //                        //if (resultado == "NO")
    //                        //    return "NO_QUIENES_SOMOS";

    //                        //idAlojamiento = alojamiento.idAlojamiento;
    //            }
    //            else
    //            {
    //                //alojamiento.website = website.Replace("http://", "").Replace("https://", "").Replace("/", ""); ;
    //                alojamiento.standBy = standBy;
    //                alojamientosService.editar(alojamiento);
    //                //idAlojamiento = alojamiento.idAlojamiento;
    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            //return Json(new { Resultado = "NO" }, JsonRequestBehavior.AllowGet);
    //            Console.WriteLine(e.Message);
    //            return "ERROR";
    //        //return e.Message;
    //        }

    //        //return Json(new { Resultado = "OK" }, JsonRequestBehavior.AllowGet);
    //        return "ok";
    //        //return idAlojamiento.ToString();
    //    }

    //    [WebMethod]
    //    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //    public String UsuarioClaveAlojamiento(int idCliente, int idProducto, string nombre, string apellido, string nombreUsuario, string clave, string emailCliente)
    //    {
    //        ClientesService clientesService = new ClientesService(this.db);
    //        RolesService rolesService = new RolesService(this.DB);
    //        AlojamientosService alojamientosService = new AlojamientosService(this.DB);
    //        AlojamientosPorRolService alojamientosPorRolService = new AlojamientosPorRolService(this.DB);
    //        UsuariosService usuariosService = new UsuariosService(this.DB);

    //        try
    //        {                
    //            Alojamiento alojamiento = alojamientosService.getByIdProductoAdmin(idProducto);
    //            Cliente cliente = clientesService.get(idCliente);
    //            string claveNuevaMD5 = MD5Hash(clave);
    //        if (cliente.Usuario != null)
    //        {
    //            cliente.Usuario.usuario1 = nombreUsuario;
    //            cliente.Usuario.clave = claveNuevaMD5;
    //            cliente.Usuario.email = emailCliente;
    //            usuariosService.editar(cliente.Usuario);
    //        }
    //        else {
    //            if (alojamiento != null){
    //            Usuario usuario = crearSuperAdmin(alojamiento, nombre, apellido, nombreUsuario, claveNuevaMD5, emailCliente);
    //            cliente.idUsuario = usuario.idUsuario;
    //            clientesService.editar(cliente);
    //        }            

    //        }

    //        if (alojamiento != null)
    //        {
    //            if (alojamientosPorRolService.getByIdAlojamientoAndNombreRol(alojamiento.idAlojamiento, Convert.ToInt32(ConfigurationManager.AppSettings["idRolSuperAdmin"])) == null)
    //            {
    //                alojamientosPorRolService.crear(alojamiento, cliente.Usuario.Role);
    //            }
    //        }

    //    }
    //        catch (Exception e)
    //        {
    //            Console.WriteLine(e.Message);
    //            //return "ERROR";
    //            return e.Message;
    //        }

    //        return "ok";
    //    }

    //    private Usuario crearSuperAdmin(Alojamiento alojamiento, string nombre, string apellido, string usuarioNombre, string clave, string emailCliente)
    //    {
    //        PermisosService permisosService = new PermisosService();
    //        RolesService rolesService = new RolesService(this.db);
    //        ModulosService modulosService = new ModulosService(this.db);
    //        UsuariosService usuariosService = new UsuariosService(this.db);
    //        AlojamientosPorRolService alojamientosPorRolService = new AlojamientosPorRolService(this.db);

    //        Role rol = rolesService.crear(ConfigurationManager.AppSettings["USUARIO_SUPER_ADMIN"]);
    //        permisosService.crear(modulosService.get(Convert.ToInt32(ConfigurationManager.AppSettings["MODULO_HABITACIONES"])), rol, true, true, true, true);
    //        permisosService.crear(modulosService.get(Convert.ToInt32(ConfigurationManager.AppSettings["MODULO_PROPIEDADES"])), rol, true, true, true, true);
    //        permisosService.crear(modulosService.get(Convert.ToInt32(ConfigurationManager.AppSettings["MODULO_CARACTERISTICAS"])), rol, true, true, true, true);
    //        permisosService.crear(modulosService.get(Convert.ToInt32(ConfigurationManager.AppSettings["MODULO_TIPOS_HABITACION"])), rol, true, true, true, true);
    //        permisosService.crear(modulosService.get(Convert.ToInt32(ConfigurationManager.AppSettings["MODULO_GALERIA_IMAGENES"])), rol, true, true, true, true);
    //        permisosService.crear(modulosService.get(Convert.ToInt32(ConfigurationManager.AppSettings["MODULO_MERCADO_PAGO"])), rol, true, true, true, true);
    //        permisosService.crear(modulosService.get(Convert.ToInt32(ConfigurationManager.AppSettings["MODULO_TEMPORADAS"])), rol, true, true, true, true);
    //        permisosService.crear(modulosService.get(Convert.ToInt32(ConfigurationManager.AppSettings["MODULO_TARIFAS"])), rol, true, true, true, true);
    //        permisosService.crear(modulosService.get(Convert.ToInt32(ConfigurationManager.AppSettings["MODULO_RESERVAS"])), rol, true, true, true, true);
    //        permisosService.crear(modulosService.get(Convert.ToInt32(ConfigurationManager.AppSettings["MODULO_USUARIOS"])), rol, true, true, true, true);

    //        //Role rol = rolesService.get(Convert.ToInt32(ConfigurationManager.AppSettings["idRolSuperAdmin"]));

    //        alojamientosPorRolService.crear(alojamiento, rol);

    //        Usuario usuario = usuariosService.crear(rol, nombre, apellido, usuarioNombre, clave, emailCliente);            
    //        return usuario;
    //    }

    //[WebMethod]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public String AlojamientoStandBy(int idProducto, bool standBy)
    //{
    //    AlojamientosService alojamientosService = new AlojamientosService(this.db);
    //    try
    //    {
    //        Alojamiento alojamiento = alojamientosService.getByIdProductoAdmin(idProducto);

    //        if (alojamiento != null)
    //        {
    //            alojamiento.standBy = standBy;
    //            alojamientosService.editar(alojamiento);
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        Console.WriteLine(e.Message);
    //        return "ERROR";
    //    }

    //    return "ok";        
    //}

    //[WebMethod]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public String ClienteStandBy(int idCliente, bool standBy)
    //{
    //    ClientesService clientesService = new ClientesService(this.db);
    //    try
    //    {
    //        Cliente cliente = clientesService.get(idCliente);

    //        if (cliente != null)
    //        {
    //            cliente.standBy = standBy;
    //            clientesService.editar(cliente);
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        Console.WriteLine(e.Message);
    //        return "ERROR";
    //    }

    //    return "ok";
    //}

    //public string MD5Hash(string input)
    //    {
    //        byte[] data = new MD5CryptoServiceProvider().ComputeHash(Encoding.Unicode.GetBytes(input));
    //        var sb = new StringBuilder();
    //        for (var i = 0; i < data.Length; i++)
    //            sb.Append(data[i].ToString("x2"));
    //        return sb.ToString();
    //    }

    //    private void CopyDirectory(DirectoryInfo source, DirectoryInfo destination)
    //    {
    //        if (!destination.Exists)
    //        {
    //            destination.Create();
    //        }

    //        // Copy all files.
    //        FileInfo[] files = source.GetFiles();
    //        foreach (FileInfo file in files)
    //        {
    //            file.CopyTo(Path.Combine(destination.FullName,
    //                file.Name));
    //        }

    //        // Process subdirectories.
    //        DirectoryInfo[] dirs = source.GetDirectories();
    //        foreach (DirectoryInfo dir in dirs)
    //        {
    //            // Get destination directory.
    //            string destinationDir = Path.Combine(destination.FullName, dir.Name);

    //            // Call CopyDirectory() recursively.
    //            CopyDirectory(dir, new DirectoryInfo(destinationDir));
    //        }
    //    }

    }
//}