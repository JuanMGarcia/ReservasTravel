﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using System.Xml;
using ReservasWeb.Utils.Channel;
using System.Configuration;
using ReservasWeb.ViewModels;
using ReservasWeb.Models;
using ReservasWeb.Services;
using ReservasWeb.Utils;

namespace ReservasWeb.WebServices
{
    public class ChannelWebService : BaseService
    {
        public ChannelWebService(ReservasWebEntities db) : base(db)
        {
        }

        public static string postXMLData(string destinationUrl, string requestXml)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                return responseStr;
            }
            return null;
        }

        public void getReservations()
        {
            string xml = "";
            xml = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["RoomCloud_reservations"]));
            xml = xml.Replace("##TOCKEN##", ConfigurationManager.AppSettings["RoomCloud_API_KEY"]);
            xml = xml.Replace("##USUARIO##", ConfigurationManager.AppSettings["RoomCloud_USER"]);
            xml = xml.Replace("##CLAVE##", ConfigurationManager.AppSettings["RoomCloud_PASS"]);
            string Resultado = ChannelWebService.postXMLData(ConfigurationManager.AppSettings["RoomCloud_URL_PROD"], xml);



            CreacionEdicionReservaGrilla vm = new CreacionEdicionReservaGrilla();

            List<reservation> listReservas = new List<reservation>();
            
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(Resultado);
            XmlNodeList listNodos = doc.DocumentElement.ChildNodes;                        
            foreach (XmlNode itemNode in listNodos)
            {
                if (itemNode.Name == "reservation")
                {
                    reservation rsv = new reservation();
                    rsv.id = itemNode.Attributes["id"].Value;
                    rsv.portalId = itemNode.Attributes["portalId"].Value;
                    rsv.checkin = itemNode.Attributes["checkin"].Value;
                    rsv.checkout = itemNode.Attributes["checkout"].Value;
                    rsv.firstname = itemNode.Attributes["firstName"].Value;
                    rsv.lastname = itemNode.Attributes["lastName"].Value;
                    rsv.countRooms = Int32.Parse(itemNode.Attributes["rooms"].Value);
                    rsv.adults = Int32.Parse(itemNode.Attributes["adults"].Value);
                    rsv.children = Int32.Parse(itemNode.Attributes["children"].Value);
                    rsv.persons = Int32.Parse(itemNode.Attributes["persons"].Value);
                    rsv.city = itemNode.Attributes["city"].Value;
                    rsv.country = itemNode.Attributes["country"].Value;
                    rsv.email = itemNode.Attributes["email"].Value;
                    rsv.telephone = itemNode.Attributes["telephone"].Value;
                    rsv.address = itemNode.Attributes["address"].Value;
                    rsv.zipcode = itemNode.Attributes["zipCode"].Value;
                    rsv.price = itemNode.Attributes["price"].Value;
                    rsv.prepaid = itemNode.Attributes["prepaid"].Value;
                    rsv.status = Int32.Parse(itemNode.Attributes["status"].Value);
                    rsv.cancelbydetail = itemNode.Attributes["cancelbydetail"].Value;
                    rsv.cancancel = Int32.Parse(itemNode.Attributes["cancancel"].Value);
                    rsv.currencycode = itemNode.Attributes["currencycode"].Value;
                    rsv.paymenttype = itemNode.Attributes["paymentType"].Value;
                    rsv.offer = itemNode.Attributes["offer"].Value;
                    rsv.notes = itemNode.Attributes["notes"].Value;
                    rsv.creation_date = itemNode.Attributes["creation_date"].Value;
                    rsv.dlm = itemNode.Attributes["dlm"].Value;
                    rsv.rooms = new List<room>();

                    
                    string nodeMessage = itemNode.InnerText;
                    if (itemNode.HasChildNodes)
                    {
                        foreach (XmlNode itemNod2 in itemNode)
                        {
                            if (itemNod2.Name == "room")
                            {
                                room habitacion = new room();
                                habitacion.dayprices = new List<dayprice>();
                                string n2Node = itemNod2.Name;
                                habitacion.id = itemNod2.Attributes["id"].Value;
                                habitacion.description = itemNod2.Attributes["description"].Value;
                                habitacion.checkin = itemNod2.Attributes["checkin"].Value;
                                habitacion.checkout = itemNod2.Attributes["checkout"].Value;
                                habitacion.rateId = itemNod2.Attributes["rateId"].Value;
                                habitacion.quantity = Int32.Parse(itemNod2.Attributes["quantity"].Value);
                                habitacion.currency = itemNod2.Attributes["currency"].Value;
                                habitacion.price = itemNod2.Attributes["price"].Value;
                                habitacion.adults = Int32.Parse(itemNod2.Attributes["adults"].Value);
                                habitacion.children = Int32.Parse(itemNod2.Attributes["children"].Value);
                                habitacion.commission = itemNod2.Attributes["commission"].Value;
                                habitacion.status = Int32.Parse(itemNod2.Attributes["status"].Value);
                                rsv.rooms.Add(habitacion);
                            }

                            if (itemNod2.Name == "dayPrice")
                            {
                                dayprice dia = new dayprice();

                                string dayprice_day = itemNod2.Attributes["day"].Value;
                                string dayprice_roomId = itemNod2.Attributes["roomId"].Value;
                                string dayprice_roomDescription = itemNod2.Attributes["roomDescription"].Value;
                                string dayprice_portalRoomDescription = itemNod2.Attributes["portalRoomDescription"].Value;
                                string dayprice_rateId = itemNod2.Attributes["rateId"].Value;
                                string dayprice_rateDescription = itemNod2.Attributes["rateDescription"].Value;
                                string dayprice_portalRateDescription = itemNod2.Attributes["portalRateDescription"].Value;
                                string dayprice_price = itemNod2.Attributes["price"].Value;
                                string dayprice_extraAdultPrice = itemNod2.Attributes["extraAdultPrice"].Value;
                                string dayprice_childrenPrice = itemNod2.Attributes["childrenPrice"].Value;

                                dia.day = dayprice_day;
                                dayprice_price = dayprice_price.Replace(".", ",");
                                dia.price = dayprice_price;
                                dia.roomId = Int32.Parse(dayprice_roomId);
                                dia.rateId = Int32.Parse(dayprice_rateId);

                                room rM = rsv.rooms.Find(x => x.id == dia.roomId.ToString());
                                if (rM != null)
                                {
                                    rM.dayprices.Add(dia);
                                }                                
                                
                            }

                            if (itemNod2.Name == "guest")
                            {
                                string guest_roomId = itemNod2.Attributes["roomId"].Value;
                                string guest_firstName = itemNod2.Attributes["firstName"].Value;
                                string guest_secondName = itemNod2.Attributes["secondName"].Value;
                                string guest_email = itemNod2.Attributes["email"].Value;
                                string guest_address = itemNod2.Attributes["address"].Value;
                                string guest_zipCode = itemNod2.Attributes["zipCode"].Value;
                                string guest_city = itemNod2.Attributes["city"].Value;
                                string guest_country = itemNod2.Attributes["country"].Value;
                            }

                        }
                    }
                    
                    listReservas.Add(rsv);            
                }
                                
            }

            

            foreach (reservation itemR in listReservas)
            { 
                Reserva existeRsv = DB.Reservas.Where(s => s.idReservaLink == itemR.id).FirstOrDefault();
                if (existeRsv == null)
                {
                    int idPortal = Int32.Parse(itemR.portalId);
                    Canale canal = DB.Canales.Where(s => s.idPortal == idPortal).FirstOrDefault();
                    Reserva unRsv = new Reserva();

                    //reserva nueva
                    unRsv.fechaEntrada = DateTime.Parse(itemR.checkin);
                    unRsv.fechaSalida = DateTime.Parse(itemR.checkout);
                    
                    // nuevo huesped
                    Huespede unHue = new Huespede();
                    unHue.apellido = itemR.lastname;
                    unHue.nombre = itemR.firstname;
                    unHue.tipoDocumento = "DNI";
                    unHue.nroDocumento = "0";
                    unHue.email = itemR.email;
                    unHue.fechaCrecion = DateTime.Now;
                    unHue.tipo = "Persona";
                    unHue.idPais = 1;
                    DB.Huespedes.Add(unHue);

                    DB.SaveChanges();

                    unRsv.idHuesped = unHue.idHuesped;
                    unRsv.idCanal = canal.idCanal;
                    unRsv.idAlojamiento = getIdAlojamiento();
                    unRsv.idEstado = ChannelHelper.conversionEstado(itemR.status);
                    unRsv.idTipoPago = 1;
                    unRsv.idTipoPagoEnHotel = 1;
                    unRsv.fechaEstado = DateTime.Now;
                    unRsv.fechaCreacion = DateTime.Now;
                    unRsv.idReservaLink = itemR.id;

                unRsv.comentarios = itemR.lastname;
                DB.Reservas.Add(unRsv);
                DB.SaveChanges();
                foreach (room item in itemR.rooms)
                {
                    foreach (dayprice itemD in item.dayprices)
                    {
                        HabitacionesPorReserva unHRSV = new HabitacionesPorReserva();
                        unHRSV.idReserva = unRsv.idReserva;
                        unHRSV.dia = DateTime.Parse(itemD.day);
                        unHRSV.fechaCreacion = DateTime.Now;
                        
                        int idAlojamiento = getIdAlojamiento();
                        TiposHabitacione tipohab = DB.TiposHabitaciones.Where(s => s.idAlojamiento == idAlojamiento && s.idLink == itemD.roomId).FirstOrDefault();
                        int cant = -1;
                        int idHabitacion = 0;
                        if (tipohab != null)
                            {
                                List<Habitacione> hab = DB.Habitaciones.Where(s => s.idTipoHabitacion == tipohab.idTipoHabitacion).ToList();
                                foreach (Habitacione Jtem in hab)
                                {
                                    cant = getCantidadReservasPorHabitacion(Jtem.idHabitacion, DateTime.Parse(item.checkin), DateTime.Parse(item.checkout));
                                    if (cant == 0)
                                    {
                                        idHabitacion = Jtem.idHabitacion;
                                        break;
                                    }
                                }
                                if (hab != null && cant == 0)
                                {
                                    unHRSV.idHabitacion = idHabitacion;
                                    unHRSV.precioHabitacion = decimal.Parse(item.price);
                                    unHRSV.impuesto = 0; // en teoria vendria en precio final
                                    unHRSV.fechaCreacion = DateTime.Now;
                                    DB.HabitacionesPorReservas.Add(unHRSV);
                                    DB.SaveChanges();
                                }
                            }
                   }                
                    
                }
            }

            }
        }

        public int getCantidadReservasPorHabitacion(int idHabitacion, DateTime inicio, DateTime fin)
        {
            int idAlojamiento = getIdAlojamiento();
            //no considero las reservas con estado = 4 => CANCELADA
            int cantidad = (from x in DB.Reservas.Where(t => t.idAlojamiento == idAlojamiento  && t.idEstado != 4 && ((t.fechaEntrada >= inicio && t.fechaEntrada < fin) || (t.fechaSalida > inicio && t.fechaSalida <= fin)))
                            from z in x.HabitacionesPorReservas.Where(h => h.idHabitacion == idHabitacion)
                            select new ReservaGrilla
                            {
                                id = x.idReserva,
                                text = x.Huespede.apellido + ", " + x.Huespede.nombre,
                                start = x.fechaEntrada,
                                end = x.fechaSalida,
                                resource = z.idHabitacion,
                                bubbleHtml = "",
                                status = x.idEstado.Value.ToString()
                            }).ToList().Count();
            return cantidad;
        }
        public SelectList getComboHabFuera(string xmlString , bool offline)
        {
            if (offline==false)
            { 
                string xmlPath = "";
                
                xmlPath = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["RoomCloud_getRooms"]));
                xmlPath = xmlPath.Replace("##TOCKEN##", ConfigurationManager.AppSettings["RoomCloud_API_KEY"]);
                xmlPath = xmlPath.Replace("##USUARIO##", ConfigurationManager.AppSettings["RoomCloud_USER"]);
                xmlPath = xmlPath.Replace("##CLAVE##", ConfigurationManager.AppSettings["RoomCloud_PASS"]);

                xmlString = postXMLData(ConfigurationManager.AppSettings["RoomCloud_URL_PROD"], xmlPath);
            }
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlString);

            XmlNodeList listNodos = doc.DocumentElement.ChildNodes;
            
            List<atom> listAtom = new List<atom>();

            atom atomo = new atom { ID = "0", Name = "Ninguno" };
            listAtom.Add(atomo);

            foreach (XmlNode itemNode in listNodos)
            {
                if (itemNode.Name=="room")
                { 
                    string nombreNode = itemNode.Name;
                    string nodeId = itemNode.Attributes["id"].Value;
                    string nodeAttr = itemNode.Attributes["description"].Value;
                    string nodeBeds = itemNode.Attributes["beds"].Value;
                    
                    atomo = new atom { ID = nodeId, Name = nodeAttr };
                    listAtom.Add(atomo);


                    string nodeMessage = itemNode.InnerText;
                    if (itemNode.HasChildNodes)
                    {                        
                        foreach (XmlNode itemNod2 in itemNode)
                        {
                            if (itemNod2.Name == "rate")
                            { 
                                string n2Node = itemNod2.Name;
                            }
                        }
                    }
                }
            }

            return new SelectList(listAtom, "ID", "Name");


          

            //return new SelectList(new[]
            //{
            //    new { ID = "0", Name = "Seleccione" },
            //    new { ID = "1", Name = "Parcela Tipo 1" },
            //    new { ID = "2", Name = "Habitacion Doble " }
            //},
            //"ID", "Name", "");
        }
    }
}