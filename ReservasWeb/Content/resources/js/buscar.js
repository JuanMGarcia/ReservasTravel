$(function () {
    //$( "#datepicker2_locale_checkout" ).datepicker({
    //	showOn: "button",
    //	buttonImage: "/Content/resources/img/calendar.gif",
    //	buttonImageOnly: true,
    //	buttonText: "Elegir Fecha CheckOut"
    //});
    //$( "#datepicker1_locale_checkin" ).datepicker({
    //	showOn: "button",
    //	buttonImage: "/Content/resources/img/calendar.gif",
    //	buttonImageOnly: true,
    //	buttonText: "Elegir Fecha CheckIn"
    //});
    var dateFormat = "dd/mm/yy",
        from = $("#datepicker1_locale_checkin")
            .datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3,
                minDate: 'today',
                showOn: "button",
                buttonImage: "/Content/resources/img/calendar.gif",
                buttonImageOnly: true,
                buttonText: "Elegir Fecha CheckIn"
            })
            .on("change", function () {                    
                to.datepicker('option', 'minDate', getDate(this));
            }),
        to = $("#datepicker2_locale_checkout")
            .datepicker({                
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3,
                minDate: 'today',
                showOn: "button",
                buttonImage: "/Content/resources/img/calendar.gif",
                buttonImageOnly: true,
                buttonText: "Elegir Fecha CheckIn"
            })
            .on("change", function () {
                from.datepicker('option', 'maxDate', getDate(this));
        });

    function getDate(element) {
        var date;
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }

        return date;
    }

    $('#toggleBusqueda').click(function (event) {
        console.log('400');
        $('#resumenBusqueda').slideToggle(150);
        $('#cambiarBusqueda').slideToggle(150);
    });

    $('.scrollTo').click(function (event) {
        event.preventDefault();
        var whereTo = $(this).attr('href');
        $('html, body').animate({ scrollTop: $(whereTo).offset().top }, 1000);
        $(this).blur();
    });

    /*var arrCant = [];
    var arrTip = [];*/

    $('.btnMas').click(function (event) {
        var tb = $('#cantP');
        var num = Number(tb.val());
        tb.val(num + 1);
        tb.html(tb.val());

        var id = $(this).attr('id');
        id = id.substring(3);
        if ($(this).hasClass('deshabilitado')) {            
        } else {        
            $("#reservaCargando").fadeIn("fast");                                
            var tb = $('#inputCuantas' + id);
            var num = Number(tb.val());
            if ((num + 1) > tb.attr("max")) {
                $("#reservaCargando").fadeOut("fast");
                return false;
            } else {
                tb.val(num + 1);
                var tipo = tb.attr('data-tipo');
                $('#compradas_tipo_' + tipo + ' .cant').html(tb.val());
                $('#detalle_' + tipo + ' .cant').html(tb.val());
                $('#compradas_tipo_' + tipo).removeClass('hidden');
                $('#detalle_' + tipo).removeClass('hidden');
                var hiddenTarifas = $('#tarifasHidden').val();
                var ArrayTarifas = hiddenTarifas.split('|');
                var unTarifa = ArrayTarifas[id];
                unTarifa = unTarifa.replace(',', '.');
                var subtotal = parseFloat(unTarifa) * parseFloat(tb.val());
                $('#compradas_tipo_' + tipo + ' .precDato').html(subtotal.toFixed(2));
                calcularTotal();
                //alert(tb.val());
                //alert(ArrayTarifas[id]);

                /*if (arrTip.length <= 0) {
                    arrTip.push(tb.attr('data-tipo'));
                    arrCant.push(tb.val());
                } else {
                    var indice = arrTip.indexOf(tb.attr('data-tipo'));
                    if (indice < 0) {
                        arrTip.push(tb.attr('data-tipo'));
                        arrCant.push(tb.val());
                    } else {
                        arrCant[indice] = tb.val();
                    }
                }*/
                $("#men"+id).removeClass('deshabilitado');
                /*$("#tipoArray").val(arrTip);
                $("#cantidadArray").val(arrCant);*/
            }
            if (tb.val() == tb.attr("max")) {
                $(this).addClass('deshabilitado');
            } else {
                $(this).removeClass('deshabilitado');
            }
            $("#reservaCargando").fadeOut("fast");
        }
    });

    $('.btnMasAdulto').click(function (event) {

        var tb = $('#cantP');
        var num = Number(tb.val());
        tb.val(num + 1);
        tb.html(tb.val());

        var id = $(this).attr('id');
        id = id.substring(9);
        if ($(this).hasClass('deshabilitado')) {
        } else {
            $("#reservaCargando").fadeIn("fast");
            var tb = $('#inputCuantasAdulto' + id);
            var num = Number(tb.val());
            if ((num + 1) > tb.attr("max")) {
                $("#reservaCargando").fadeOut("fast");
                return false;
            } else {
                tb.val(num + 1);
                var tipo = tb.attr('data-tipoA');
                $('#compradas_tipo_adulto_' + tipo + ' .cantA').html(tb.val());
              /*  $('#detalle_' + tipo + ' .cant').html(tb.val());*/
                $('#compradas_tipo_adulto_' + tipo).removeClass('hidden');
         /*       $('#detalle_' + tipo).removeClass('hidden');*/
                var hiddenTarifasAdulto = $('#tarifasAdultoHidden').val();
                var ArrayTarifasAdulto = hiddenTarifasAdulto.split('|');
                var unTarifaAd = ArrayTarifasAdulto[id];
                unTarifaAd = unTarifaAd.replace(',', '.');
                var subtotalAdulto = parseFloat(unTarifaAd) * parseFloat(tb.val());
                $('#compradas_tipo_adulto_' + tipo + ' .precADato').html(subtotalAdulto.toFixed(2));
                calcularTotal();
                //alert(tb.val());
                //alert(ArrayTarifas[id]);

                /*if (arrTip.length <= 0) {
                    arrTip.push(tb.attr('data-tipo'));
                    arrCant.push(tb.val());
                } else {
                    var indice = arrTip.indexOf(tb.attr('data-tipo'));
                    if (indice < 0) {
                        arrTip.push(tb.attr('data-tipo'));
                        arrCant.push(tb.val());
                    } else {
                        arrCant[indice] = tb.val();
                    }
                }*/
                $("#menAdulto" + id).removeClass('deshabilitado');
                /*$("#tipoArray").val(arrTip);
                $("#cantidadArray").val(arrCant);*/
            }
            if (tb.val() == tb.attr("max")) {
                $(this).addClass('deshabilitado');
            } else {
                $(this).removeClass('deshabilitado');
            }
            $("#reservaCargando").fadeOut("fast");
        }
    });

    $('.btnMasNino').click(function (event) {
        var tb = $('#cantP');
        var num = Number(tb.val());
        tb.val(num + 1);
        tb.html(tb.val());


        var id = $(this).attr('id');
        id = id.substring(7);
        if ($(this).hasClass('deshabilitado')) {
        } else {
            $("#reservaCargando").fadeIn("fast");
            var tb = $('#inputCuantasNino' + id);
            var num = Number(tb.val());
            if ((num + 1) > tb.attr("max")) {
                $("#reservaCargando").fadeOut("fast");
                return false;
            } else {
                tb.val(num + 1);
                var tipo = tb.attr('data-tipoN');
                $('#compradas_tipo_nino_' + tipo + ' .cantN').html(tb.val());
                /*  $('#detalle_' + tipo + ' .cant').html(tb.val());*/
                $('#compradas_tipo_nino_' + tipo).removeClass('hidden');
                /*       $('#detalle_' + tipo).removeClass('hidden');*/
                var hiddenTarifasNino = $('#tarifasNinoHidden').val();
                var ArrayTarifasNino = hiddenTarifasNino.split('|');
                var unTarifaNn = ArrayTarifasNino[id];
                unTarifaNn = unTarifaNn.replace(',', '.');
                var subtotalNino = parseFloat(unTarifaNn) * parseFloat(tb.val());
                $('#compradas_tipo_nino_' + tipo + ' .precN').html(subtotalNino.toFixed(2));
                calcularTotal();
                //alert(tb.val());
                //alert(ArrayTarifas[id]);

                /*if (arrTip.length <= 0) {
                    arrTip.push(tb.attr('data-tipo'));
                    arrCant.push(tb.val());
                } else {
                    var indice = arrTip.indexOf(tb.attr('data-tipo'));
                    if (indice < 0) {
                        arrTip.push(tb.attr('data-tipo'));
                        arrCant.push(tb.val());
                    } else {
                        arrCant[indice] = tb.val();
                    }
                }*/
                $("#menNino" + id).removeClass('deshabilitado');
                /*$("#tipoArray").val(arrTip);
                $("#cantidadArray").val(arrCant);*/
            }
            if (tb.val() == tb.attr("max")) {
                $(this).addClass('deshabilitado');
            } else {
                $(this).removeClass('deshabilitado');
            }
            $("#reservaCargando").fadeOut("fast");
        }
    });

    $('.btnMenos').click(function (event) {
        var tb = $('#cantP');
        var num = Number(tb.val());
        tb.val(num - 1);
        tb.html(tb.val());

        var id = $(this).attr('id');
        id = id.substring(3);
        if ($(this).hasClass('deshabilitado')) {
        } else {
            $("#reservaCargando").fadeIn("fast");
            var tb = $('#inputCuantas' + id);
            var num = Number(tb.val());
            if (num == 0 || isNaN(num)) {

            } else {                
                if (tb.val() > 0) {
                    tb.val(num - 1);
                }
                var tipo = tb.attr('data-tipo');
                $('#compradas_tipo_' + tipo + ' .cant').html(tb.val());
                $('#detalle_' + tipo + ' .cant').html(tb.val());
                var hiddenTarifas = $('#tarifasHidden').val();
                var ArrayTarifas = hiddenTarifas.split('|');
                var unTarifa = ArrayTarifas[id];
                unTarifa = unTarifa.replace(',', '.');
                var subtotal = parseFloat(unTarifa) * parseFloat(tb.val());
                $('#compradas_tipo_' + tipo + ' .prec').html(subtotal.toFixed(2));
                calcularTotal();
                if (tb.val() == 0 || isNaN(num)) {
                    $('#compradas_tipo_' + tipo).addClass('hidden');
                    $('#detalle_' + tipo).addClass('hidden');
                }
            }

            if (tb.val() > 0) {
                $(this).removeClass('deshabilitado');
            } else {
                $(this).addClass('deshabilitado');
            }

            if (tb.val() < tb.attr("max")) {
                $('#mas' + id).removeClass('deshabilitado')
            }
            $("#reservaCargando").fadeOut("fast");
        }
    });

    $('.btnMenosAdulto').click(function (event) {
        var tb = $('#cantP');
        var num = Number(tb.val());
        tb.val(num - 1);
        tb.html(tb.val());

        var id = $(this).attr('id');
        id = id.substring(9);
        if ($(this).hasClass('deshabilitado')) {
        } else {
            $("#reservaCargando").fadeIn("fast");
            var tb = $('#inputCuantasAdulto' + id);
            var num = Number(tb.val());
            if (num == 0 || isNaN(num)) {

            } else {
                if (tb.val() > 0) {
                    tb.val(num - 1);
                }
                var tipo = tb.attr('data-tipoA');
                $('#compradas_tipo_adulto_' + tipo + ' .cantA').html(tb.val());
             /*   $('#detalle_' + tipo + ' .cant').html(tb.val());*/
                var hiddenTarifasAdulto = $('#tarifasAdultoHidden').val();
                var ArrayTarifasAdulto = hiddenTarifasAdulto.split('|');
                var unTarifaAd = ArrayTarifasAdulto[id];
                unTarifaAd = unTarifaAd.replace(',', '.');
                var subtotalAdulto = parseFloat(unTarifaAd) * parseFloat(tb.val());
                $('#compradas_tipo_adulto_' + tipo + ' .precADato').html(subtotalAdulto.toFixed(2));
                calcularTotal();
                if (tb.val() == 0 || isNaN(num)) {
                    $('#compradas_tipo_adulto_' + tipo).addClass('hidden');
                  /*  $('#detalle_' + tipo).addClass('hidden'); */
                }
            }

            if (tb.val() > 0) {
                $(this).removeClass('deshabilitado');
            } else {
                $(this).addClass('deshabilitado');
            }

            if (tb.val() < tb.attr("max")) {
                $('#masAdulto' + id).removeClass('deshabilitado')
            }
            $("#reservaCargando").fadeOut("fast");
        }
    });

    $('.btnMenosNino').click(function (event) {
        var tb = $('#cantP');
        var num = Number(tb.val());
        tb.val(num - 1);
        tb.html(tb.val());

        var id = $(this).attr('id');
        id = id.substring(7);
        if ($(this).hasClass('deshabilitado')) {
        } else {
            $("#reservaCargando").fadeIn("fast");
            var tb = $('#inputCuantasNino' + id);
            var num = Number(tb.val());
            if (num == 0 || isNaN(num)) {

            } else {
                if (tb.val() > 0) {
                    tb.val(num - 1);
                }
                var tipo = tb.attr('data-tipoN');
                $('#compradas_tipo_nino_' + tipo + ' .cantN').html(tb.val());
                /*   $('#detalle_' + tipo + ' .cant').html(tb.val());*/
                var hiddenTarifasNino = $('#tarifasNinoHidden').val();
                var ArrayTarifasNino = hiddenTarifasNino.split('|');
                var unTarifaNn = ArrayTarifasNino[id];
                unTarifaNn = unTarifaNn.replace(',', '.');
                var subtotalNino = parseFloat(unTarifaNn) * parseFloat(tb.val());
                $('#compradas_tipo_nino_' + tipo + ' .precN').html(subtotalNino.toFixed(2));
                calcularTotal();
                if (tb.val() == 0 || isNaN(num)) {
                    $('#compradas_tipo_nino_' + tipo).addClass('hidden');
                    /*  $('#detalle_' + tipo).addClass('hidden'); */
                }
            }

            if (tb.val() > 0) {
                $(this).removeClass('deshabilitado');
            } else {
                $(this).addClass('deshabilitado');
            }

            if (tb.val() < tb.attr("max")) {
                $('#masNino' + id).removeClass('deshabilitado')
            }
            $("#reservaCargando").fadeOut("fast");
        }
    });

    $(".tbCant").keypress(function (evt) {
        evt.preventDefault();
    });
    $(".tbCantAdulto").keypress(function (evt) {
        evt.preventDefault();
    });
    $(".tbCantNino").keypress(function (evt) {
        evt.preventDefault();
    });

    function calcularTotal() {
        var hiddenTarifas = $('#tarifasHidden').val();
        var hiddenTarifasAdulto = $('#tarifasAdultoHidden').val();
        var hiddenTarifasNino = $('#tarifasNinoHidden').val();
        var hiddenImpuesto = $('#impuestoHidden').val();
        var ArrayTarifas = hiddenTarifas.split('|');
        var total = 0;
        for (var i = 0; i < ArrayTarifas.length; i++) {
            var tb = $('#inputCuantas' + i);
            var unTarifa = ArrayTarifas[i];
            unTarifa = unTarifa.replace(',', '.');
            total += parseFloat(unTarifa) * parseFloat(tb.val());
        }

        var ArrayTarifasAdulto = hiddenTarifasAdulto.split('|');
        //var total = 0;
        for (var i = 0; i < ArrayTarifasAdulto.length; i++) {
            var tbA = $('#inputCuantasAdulto' + i);
            if (ArrayTarifasAdulto[i] != 0) {
                var unTarifaAd = ArrayTarifasAdulto[i];
                unTarifaAd = unTarifaAd.replace(',', '.');
                total += parseFloat(unTarifaAd) * parseFloat(tbA.val());
            }
         }

        var ArrayTarifasNino = hiddenTarifasNino.split('|');
        //var total = 0;
        for (var i = 0; i < ArrayTarifasNino.length; i++) {
            var tbN = $('#inputCuantasNino' + i);
            if (ArrayTarifasNino[i] != 0) {
                var unTarifaNn = ArrayTarifasNino[i];
                unTarifaNn = unTarifaNn.replace(',', '.');
                total += parseFloat(unTarifaNn) * parseFloat(tbN.val());
            }
        }


        var impuesto = parseFloat(hiddenImpuesto);
        var importeImpuesto = 0;
        if (total > 0) {            
            if (!isNaN(impuesto) && impuesto > 0) {                
                importeImpuesto = total * (impuesto / 100);
                //importeImpuesto = importeImpuesto.replace(',', '.');
                total = total + importeImpuesto;
                $('#impuestoListado').removeClass('hidden');                
                $('#impuestoListado .precDato').html(importeImpuesto.toFixed(2));
            }
            $('.lineaTotal').removeClass('hidden');
            $('.tituDetallePago').removeClass('hidden');
        } else {
            $('#impuestoListado').addClass('hidden');
            $('.lineaTotal').addClass('hidden');
            $('.tituDetallePago').addClass('hidden');
        }

        $('#totalReservaDato').html(total.toFixed(2));
    }





    
});