﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;

namespace ReservasWeb.Utils
{
    public class SesionInvitado
    {
        public int idAlojamiento { get; set; }
        public DateTime fechaEntrada { get; set; }
        public DateTime fechaSalida { get; set; }
        public int pasajeros { get; set; }
        public string backurl { get; set; }

        public string urlLogo { get; set; }
        public string colorLogo { get; set; }
        public string colorBarra { get; set; }
        public string colorBoton { get; set; }
        public string colorTextoBoton { get; set; }
        public List<TiposHabitacione> listado { get; set; }
        public List<int> listadoCant { get; set; }
        public List<int> listadoElige { get; set; }
        public List<int> listadoEligeAdulto { get; set; }
        public List<int> listadoEligeNino { get; set; }
        public List<decimal> listadoTarifa { get; set; }
        public List<AdicionalItem> listadoTarifaAdulto { get; set; }
        public List<AdicionalItem> listadoTarifaNino { get; set; }
        public int idReserva { get; set; }
        public int idHuesped { get; set; }

        public string urlImagenMini { get; set; }
        public string direccionHotel { get; set; }

        public string telHotel { get; set; }
        public string nombreHotel { get; set; }
        public string emailHotel { get; set; }

        public string estrellas { get; set; }
        public string idioma { get; set; }
        public decimal impuesto { get; set; }
        public List<LDiaRsv> listadoDias { get; set; }
        public bool tieneLogo { get; set; }
        public List<LHabDisponibles> listadoDisponible { get; set; }
        public string noches { get; set; }
        public bool reservaConfirmada { get; set; }
        public List<int> combinaElige { get; set; }
        public List<TiposHabitacione> combina { get; set; }
        public List<OfertaItem> ofertaBooking { get; set; }
        public string contadorSesion { get; set; }
        public int contadorSegundos { get; set; }
        public int habitaciones { get; set; }

        //Transferencia Bancaria
        public string tbNombreBanco { get; set; }
        public string tbTipoCuenta { get; set; }
        public string tbTitular { get; set; }
        public string tbNumeroCuenta { get; set; }
        public string tbCBU { get; set; }
        public string tbCUIT { get; set; }
        public string tbSucursal { get; set; }

        ///Mercado Pago
        public decimal total { get; set; }
        public int idMP { get; set; }
        public string linkMP { get; set; }
        public List<int> listadoCondicionPorHab { get; set; }
        public List<string> listadoNombCondicionPorHab { get; set; }
    }
    public class LDiaRsv
    {
        public List<DiaRsv> listado { get; set; } 
    }
    public class LHabDisponibles
    {
        public List<Habitacione> listado { get; set; }
    }
}