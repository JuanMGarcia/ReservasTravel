﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace ReservasWeb.Utils
{
    public static class ComboHelper
    {
        public static string GetNombreEstadoHabitacion(int idEstado)
        {
            string resultado = "";
            switch (idEstado)
            {
                case 1:
                    resultado = "Habilitada";
                    break;
                case 2:
                    resultado = "Mantenimiento";
                    break;
                case 3:
                    resultado = "Limpieza";
                    break;
                case 4:
                    resultado = "Deshabilitada";
                    break;
            }

            return resultado;
        }

        public static string GetNombreEstadoReserva(int idEstado)
        {
            string resultado = "";
            switch (idEstado)
            {
                case 1:
                    resultado = "Precarga";
                    break;
                case 2:
                    resultado = "Pendiente";
                    break;
                case 3:
                    resultado = "Confirmado";
                    break;
                case 4:
                    resultado = "Cancelado";
                    break;
                case 5:
                    resultado = "Tarjeta Invalida";
                    break;
            }

            return resultado;
        }


        public static string getLinkHotel(int idTipo)
        {
            string resultado = "";
            switch (idTipo)
            {
                case -1:
                    resultado = "No Existe Alojamiento";
                    break;
                case 0:
                    resultado = "Desconectado";
                    break;
                case 1:
                    resultado = "Conectado - Desactivado";
                    break;
                case 2:
                    resultado = "Conectado - Activado";
                    break;
            }

            return resultado;
        }

        public static string getNombreTipoPagoCuenta(string pago)
        {
            string resultado = "";
            switch (pago)
            {
                case "1":
                    resultado = "Efectivo";
                    break;
                case "2":
                    resultado = "Tarjeta de Debito";
                    break;
                case "3":
                    resultado = "Tarjeta de Credito";
                    break;
                case "4":
                    resultado = "Deposito";
                    break;

            }
            return resultado;
        }

        public static string getNombreConcepto(string concepto)
        {
            string resultado = "";
            switch (concepto)
            {
                case "CH":
                    resultado = "Cargo";
                    break;
                case "P":
                    resultado = "Pago";
                    break;
                case "D":
                    resultado = "Descuento";
                    break;
            }
            return resultado;
        }
        public static string GetNombreTipoMediodePago(int idTipo)
        {
            string resultado = "";
            switch (idTipo)
            {
                case 0:
                    resultado = "Seleccione una forma de Pago";
                    break;
                case 1:
                    resultado = "Paga en Hotel - Efectivo";
                    break;
                case 2:
                    resultado = "MercadoPago";
                    break;
                case 3:
                    resultado = "Paypal";
                    break;
                case 4:
                    resultado = "TodoPago";
                    break;
                case 5:
                    resultado = "Transferencia Bancaria";
                    break;
                case 6:
                    resultado = "Paga en Hotel - Debito";
                    break;
                case 7:
                    resultado = "Paga en Hotel - Credito";
                    break;

            }

            return resultado;
        }

        public static string GetNombreTipoPagoEnHotel(int idTipoPagoEnHotel)
        {
            string resultado = "";
            switch (idTipoPagoEnHotel)
            {
                case 1:
                    resultado = "Contado";
                    break;
                case 2:
                    resultado = "Tarjeta Débito";
                    break;
                case 3:
                    resultado = "Tarjeta Crédito";
                    break;
            }

            return resultado;
        }

        public static string GetSimboloMoneda(int idMoneda)
        {
            return (Convert.ToInt32(ConfigurationManager.AppSettings["idMonedaPesosArg"]) == idMoneda ? ConfigurationManager.AppSettings["simboloMonedaPesosArg"] : ConfigurationManager.AppSettings["simboloMonedaDolaresUSA"]);
        }

        public static SelectList getComboEstados()
        {
            return new SelectList(new[]
            {
                new { ID = "0", Name = "Todos" },
                new { ID = "1", Name = "Precarga" },
                new { ID = "2", Name = "Pendiente"},
                new { ID = "3", Name = "Confirmado"},
                new { ID = "4", Name = "Cancelado"},
                new { ID = "5", Name = "Tarjeta Invalida"}
            },
            "ID", "Name", "");
        }

        public static SelectList getComboTipoPagos()
        {
            return new SelectList(new[]
            {
                new { ID = "0", Name = "Seleccione"},
                new { ID = "1", Name = "Efectivo" },
                new { ID = "2", Name = "Tarjeta de Debito" },
                new { ID = "3", Name = "Tarjeta de Credito"},
                new { ID = "4", Name = "Deposito"}
            },
            "ID", "Name", "");

        }

    }
}