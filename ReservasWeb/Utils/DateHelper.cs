﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

namespace ReservasWeb.Utils
{
    public static class DateHelper
    {
        public static DateTime stringToDateTime(string fecha)
        {
            return DateTime.Parse(fecha, Thread.CurrentThread.CurrentCulture, DateTimeStyles.AssumeLocal);
        }

        public static DateTime obtenerFechaActual()
        {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Argentina Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
        }

    }
}