﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReservasWeb.Models;
using MailKit.Net.Imap;
using MailKit;
using System.Configuration;
using ReservasWeb.ViewModels;

namespace ReservasWeb.Utils
{
    public class MailKit
    {

        public static List<EmailViewModel> getEmails()
        {

            List<EmailViewModel> emails = new List<EmailViewModel>();
            EmailViewModel email = null;
            using (var client = new ImapClient())
            {
                // For demo-purposes, accept all SSL certificates
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect("imap.gmail.com", 993, true);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                client.Authenticate(ConfigurationManager.AppSettings["usuarioGmail"], ConfigurationManager.AppSettings["claveGmail"]);

                // The Inbox folder is always available on all IMAP servers...
                var inbox = client.Inbox;
                inbox.Open(FolderAccess.ReadOnly);

                //Console.WriteLine("Total messages: {0}", inbox.Count);
                //Console.WriteLine("Recent messages: {0}", inbox.Recent);



                //for (int i = 0; i < inbox.Count; i++)
                
                var message = inbox.Where(m => m.MessageId == "AANLkTi=jdcqio+_VUtyYiU-Gp=j5WmKO1Sc8Gc4saYqk@mail.gmail.com").FirstOrDefault();
                email = new EmailViewModel();
                email.idMensaje = message.MessageId;
                email.de = message.From.ToString();
                email.cc = message.Cc.ToString();
                email.cco = message.Bcc.ToString();
                email.asunto = message.Subject.ToString();
                email.cuerpo = message.TextBody;
                emails.Add(email);
                //for (int i = 0; i < 10; i++)
                //{
                //    var message = inbox.GetMessage(i);                    
                //    email = new EmailViewModel();
                //    email.idMensaje = message.MessageId;
                //    email.de = message.From.ToString();
                //    email.cc = message.Cc.ToString();
                //    email.cco = message.Bcc.ToString();
                //    email.asunto = message.Subject.ToString();
                //    email.cuerpo = message.TextBody;
                //    emails.Add(email);                                      
                //    //Console.WriteLine("Subject: {0}", message.Subject);
                //}

                

                client.Disconnect(true);
            }

            return emails;
        }

    }
}