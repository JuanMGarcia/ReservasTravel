﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReservasWeb.Utils
{
    public class GoogleMap
    {
        [Display(Name = "Latitud")]
        public string latitud { get; set; }
        [Display(Name = "Longitud")]
        public string longitud { get; set; }
        [Display(Name = "Zoom")]
        public int? zoomMapa { get; set; }
        [Display(Name = "Tipo")]
        public string tipoMapa { get; set; }
        [Display(Name = "Dirección")]
        public string direccion { get; set; }
        [Display(Name = "Ciudad")]
        public string ciudad { get; set; }
        [Display(Name = "Provincia")]
        public string provincia { get; set; }
        [Display(Name = "País")]
        public string pais { get; set; }
    }
}