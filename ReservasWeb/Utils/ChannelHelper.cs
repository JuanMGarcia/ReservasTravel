﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.Utils
{
    public class ChannelHelper
    {
        public static int conversionEstado(int status)
        {
            int resultado = 0;
            switch (status)
            {
                case 2:
                    resultado = 2;
                    break;
                case 3:
                    resultado = 4;
                    break;
                case 4:
                    resultado = 3;
                    break;
                case 5:
                    resultado = 1;
                    break;
                case 6:
                    resultado = 1;
                    break;
                case 7:
                    resultado = 4;
                    break;
                case 8:
                    resultado = -1;
                    break;
            }
            return resultado;
        }
    }
}