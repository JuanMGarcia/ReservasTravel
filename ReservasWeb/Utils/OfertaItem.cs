﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.Utils
{
    public class OfertaItem
    {
        public decimal precio { get; set; }
        public int cantidad { get; set; }
        public string nombre { get; set; }
    }
}