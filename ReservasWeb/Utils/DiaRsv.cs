﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.Utils
{
    public class DiaRsv
    {
        public DateTime dia { get; set; }
        public int camasMatrimoniales { get; set; }
        public int camasSimples { get; set; }
        public int adicionalesAdultos { get; set; }
        public int adicionalesMenores { get; set; }
        public int adicionalesCunas { get; set; }
        public decimal precioHabitacion { get; set; }
        public decimal precioAdicionalAdulto { get; set; }
        public decimal precioAdicionalMenor { get; set; }
        public decimal precioAdicionalCuna { get; set; }

    }
}