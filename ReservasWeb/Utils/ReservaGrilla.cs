﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.Utils
{
    public class ReservaGrilla
    {
        public int id { get; set; }
        public string text { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public int resource { get; set; }
        public string bubbleHtml { get; set; }
        public string status { get; set; }
        public int idCanal { get; set; }
    }
}