﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.Utils.Channel
{
    public class reservation
    {
        public string id { get; set; }
        public string portalId { get; set; }
        public string checkin { get; set; }
        public string checkout { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public int countRooms { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public int persons { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string email { get; set; }
        public string telephone { get; set; }
        public string address { get; set; }
        public string zipcode { get; set; }
        public string price { get; set; }
        public string prepaid { get; set; }
        public int status { get; set; }
        public string cancelbydetail { get; set; }
        public int cancancel { get; set; }
        public string currencycode { get; set; }
        public string paymenttype { get; set; }
        public string offer { get; set; }
        public string notes { get; set; }
        public string creation_date { get; set; }
        public string dlm { get; set; }
        public List<room> rooms { get; set; }
    }
}

 

