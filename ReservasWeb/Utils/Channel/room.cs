﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.Utils.Channel
{
    public class room
    {
        public string id { get; set; }
        public string description { get; set; }
        public string checkin { get; set; }
        public string checkout { get; set; }
        public string rateId { get; set; }
        public int quantity { get; set; }
        public string currency { get; set; }
        public string price { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public string commission { get; set; }
        public int status { get; set; }
        public List<dayprice> dayprices { get; set; }
    }
}



