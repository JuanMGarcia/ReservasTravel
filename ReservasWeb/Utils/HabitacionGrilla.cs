﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.Utils
{
    public class HabitacionGrilla
    {
        public string name { get; set; }
        public int id { get; set; }
        public int capacity { get; set; }
        public string status { get; set; }
    }
}