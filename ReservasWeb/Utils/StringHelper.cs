﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;

namespace ReservasWeb.Utils
{
    public static class StringHelper
    {
        public static string removeWhitespaces(string str)
        {
            return Regex.Replace(str, @"\s", "");
        }
    }
}