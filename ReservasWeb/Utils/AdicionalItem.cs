﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.Utils
{
    public class AdicionalItem
    {
        public decimal precio { get; set; }
        public int cantidad { get; set; }
        public bool permite { get; set; }
    }

}