﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.Utils
{
    public class Sesion
    {
        public string authId { get; set; }
        public int idUsuario { get; set; }
        public int idCliente { get; set; }
        public int idRol { get; set; }
        public string usuario { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string email { get; set; }
        public int idAlojamientoActual { get; set; }
        public string nombreAlojamiento { get; set; }
        //public List<AlojamientoSesion> listadoHoteles { get; set; }
    }

    public class AlojamientoSesion
    {
        public int idAlojamiento { get; set; }
        public string nombreAlojamiento { get; set; }
    }
}