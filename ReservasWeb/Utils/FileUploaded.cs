﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservasWeb.Utils
{
    public class FileUploaded
    {
        public int id { get; set; }
        public String url { get; set; }
        public String thumbnailUrl { get; set; }
        public String name { get; set; }
        public String type { get; set; }
        public int size { get; set; }
        public String deleteUrl { get; set; }
        public String deleteType { get; set; }
    }
}