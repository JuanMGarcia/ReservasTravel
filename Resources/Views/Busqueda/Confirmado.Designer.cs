﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources.Views.Busqueda {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Confirmado {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Confirmado() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Views.Busqueda.Confirmado", typeof(Confirmado).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fecha Entrada.
        /// </summary>
        public static string FechaEntrada {
            get {
                return ResourceManager.GetString("FechaEntrada", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fecha Salida.
        /// </summary>
        public static string FechaSalida {
            get {
                return ResourceManager.GetString("FechaSalida", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Habitaciones.
        /// </summary>
        public static string Habitaciones {
            get {
                return ResourceManager.GetString("Habitaciones", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pasajeros.
        /// </summary>
        public static string Pasajeros {
            get {
                return ResourceManager.GetString("Pasajeros", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Su Reserva fue confirmada.
        /// </summary>
        public static string ReservaConfirmado {
            get {
                return ResourceManager.GetString("ReservaConfirmado", resourceCulture);
            }
        }
    }
}
